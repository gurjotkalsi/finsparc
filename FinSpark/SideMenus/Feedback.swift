//
//  Feedback.swift
//  FinSpark
//
//  Created by Minkle Garg on 17/02/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit
import iOSDropDown

class Feedback: UIViewController, UITextViewDelegate, UITextFieldDelegate{
    @IBOutlet weak var dropdownBtn: DropDown!
    @IBOutlet weak var userEmail: UITextField!
    @IBOutlet weak var descriptionField: UITextView!
    var placeholderLabel : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        descriptionField.addBorder()
        descriptionField.addRoundCorners(6)
        self.userEmail.text = LoginModel().localData().email
        descriptionField.delegate = self
        placeholderLabel = UILabel()
        placeholderLabel.text = "Enter your text..."
        placeholderLabel.font = UIFont.italicSystemFont(ofSize: (descriptionField.font?.pointSize)!)
        placeholderLabel.sizeToFit()
        descriptionField.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: (descriptionField.font?.pointSize)! / 2)
        placeholderLabel.textColor = UIColor.lightGray
        placeholderLabel.isHidden = !descriptionField.text.isEmpty
        self.CustomizeNavBar_with(title: "", button_text: "Feedback & Support", bartint: UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1), backButton: true) {
                   self.navigationController?.popViewController(animated: true)
               }
        
        // The view to which the drop down will appear on
        dropdownBtn.optionArray = ["Report a problem", "Give a suggestion", "Ask a question"]
        dropdownBtn.isSearchEnable = false
        // The list of items to display. Can be changed dynamically
        // Do any additional setup after loading the view.
        
        dropdownBtn.didSelect{(selectedText , index ,id) in
            self.dropdownBtn.text = selectedText
          //  self.dropdownBtn.setTitle(selectedText, for: .normal)

        }
//        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
//          print("Selected item: \(item) at index: \(index)")
//                                       self.dropdownBtn.setTitle(item, for: .normal)
//        }

        // Will set a custom width instead of the anchor view width
      //  dropDown.frame.size.width = dropdownBtn.frame.size.width
        // Do any additional setup after loading the view.
    }
    func textViewDidChange(_ textView: UITextView) {
        dropdownBtn.showList()
           placeholderLabel.isHidden = !textView.text.isEmpty
       }
    
   
    
    @IBAction func dropdownAction(_ sender: UIButton) {
        
        dropdownBtn.optionArray = ["Report a problem", "Give a suggestion", "Ask a question"]
        dropdownBtn.showList()
    }
    
    @IBAction func onClickedSave(_ sender: UIButton)
    {
        if dropdownBtn.text == "Select" {
            self.ShowToast(message: "Please select your question")
        }else if descriptionField.text == "" {
            self.ShowToast(message: "Please enter description")
        }else{
            if !isInternetConnected() {
                self.ShowToast(message: "Check internet connection!")
                return
            }

            self.AddLoader()
            FeedbackModel().sendFeedback(parameter: ["email_id":userEmail.text as Any , "description":descriptionField.text as Any,"question_type":dropdownBtn.text as Any,"id":"0","user_id":LoginModel().localData().finsparc_user_id]) { (pmodel) in
                
                self.RemoveLoader()
                self.ShowToast(message: pmodel.msg) {
                    self.dropdownBtn.text = "Select"
                    self.descriptionField.text = ""
                    self.placeholderLabel.isHidden = !self.descriptionField.text.isEmpty
                }
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
