//
//  ChangePassword.swift
//  FinSpark
//
//  Created by Minkle Garg on 17/02/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextFields

class ChangePassword: UIViewController {

    @IBOutlet var currentpassword_tf: MDCTextField!
    @IBOutlet var newpassword_tf: MDCTextField!
    @IBOutlet var confirmpassword_tf: MDCTextField!
    
    @IBOutlet weak var btnHideUnhidecurrentPassword: UIButton!
    @IBOutlet weak var btnHideUnhidenewPassword: UIButton!
    @IBOutlet weak var btnHideUnhideconfirmPassword: UIButton!

    var currentpasswordControl:MDCTextInputControllerUnderline!
    var newpasswordControl:MDCTextInputControllerUnderline!
    var confirmpasswordControl:MDCTextInputControllerUnderline!
    
    @IBOutlet var update_btn: UIButton!
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.navigationController?.isNavigationBarHidden = false
//               self.navigationController?.navigationBar.isTranslucent = false
//               self.navigationController?.navigationBar.barTintColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)

       self.CustomizeNavBar_with(title: "", button_text: "Change Password", bartint: UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1), backButton: true) {
                  self.navigationController?.popViewController(animated: true)
              }
      //  self.update_btn.roundUp()
        
        currentpasswordControl = currentpassword_tf.floatingTextField()
        newpasswordControl = newpassword_tf.floatingTextField()
        confirmpasswordControl = confirmpassword_tf.floatingTextField()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    
    //MARK:-
    
    @IBAction func UpdateAction(_ sender: UIButton) {
        
    }
    
    @IBAction func secureAction(_ sender: UIButton) {
        
        if sender.tag == 10 {
            if currentpassword_tf.isSecureTextEntry
            {
                self.currentpassword_tf.isSecureTextEntry = false
            }else
            {
                self.currentpassword_tf.isSecureTextEntry = true
            }
        } else if sender.tag == 10 {
            if newpassword_tf.isSecureTextEntry
            {
                self.newpassword_tf.isSecureTextEntry = false
            }else
            {
                self.newpassword_tf.isSecureTextEntry = true
            }
        }else
        {
           if confirmpassword_tf.isSecureTextEntry
            {
                self.confirmpassword_tf.isSecureTextEntry = false
            }else
            {
                self.confirmpassword_tf.isSecureTextEntry = true
            }
        }
    }
    
}
