//
//  SideMenu.swift
//  Sales Tracker
//
//  Created by Gurjot Kalsi on 14/01/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class SideMenu: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var slider: UIView!
    @IBOutlet var tableview: UITableView!
    @IBOutlet var frontConstant: NSLayoutConstraint!
    @IBOutlet var cancel_button: UIButton!
    
    @IBOutlet var user_name: UILabel!
    @IBOutlet var user_mail: UILabel!
    @IBOutlet var user_contact: UILabel!
    
    var titlesArray = ["About us", "Contact us", "Edit profile", "Change password", "Finsparc presentation", "T & C", "Feedback & Support", "Logout"]
    
    var imagesArray = ["aboutus", "contactus", "editprofile", "changepwd", "presentation", "tandc", "feedback", "logout" ]
    
    var expandSalesRep = false
    var expandLeads = false
    
    //MARK:-
    override func viewDidLoad() {

        super.viewDidLoad()
        
        cancel_button.fitImage()
//        ColorStatusBar(color: .red)
        self.frontConstant.constant = CGFloat(0) - self.view.frame.size.width
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.SlideIn), name: NSNotification.Name("showMenu"), object: nil)
        //        NotificationCenter.default.addObserver(self, selector: #selector(self.SlideOut), name: NSNotification.Name("hideMenu"), object: nil)
        
        let edgePanRight = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(self.screenEdgeSwipedRight(_:)))
        edgePanRight.edges = .left
        self.view.addGestureRecognizer(edgePanRight)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    @objc func screenEdgeSwipedRight(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            self.SlideOut()
        }
    }
    
    @objc func SlideIn() {
        self.user_name.text = LoginModel().localData().name
        self.user_mail.text = LoginModel().localData().email
        self.user_contact.text = LoginModel().localData().mobile
        UIView.animate(withDuration: 0.5) {
            self.frontConstant.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func TapToHide(_ sender: Any) {
        self.SlideOut()
    }
    
    @objc func SlideOut() {
        print("called")
        UIView.animate(withDuration: 0.5, animations: {
            self.frontConstant.constant = CGFloat(0) - self.view.frame.size.width
            self.view.layoutIfNeeded()
        }) { (finished) in
            if finished {
                MenuDelegate.view.removeFromSuperview()
                MenuDelegate.removeFromParent()
            }
        }
    }
    
    //MARK:-
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titlesArray.count
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = self.tableview.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as? MenuCell
        if cell == nil {
            cell = MenuCell(style: .default, reuseIdentifier: "MenuCell")
        }
        cell?.selectionStyle = .none

        cell?.menuText.text = self.titlesArray[indexPath.row]
        cell?.menuImage.image = UIImage(named: self.imagesArray[indexPath.row])
                
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.SlideOut()
        
        if indexPath.row == 0 {
            NotificationCenter.default.post(name: NSNotification.Name("NavigateTo"), object: "AboutUs")
        }else if indexPath.row == 1 {
            NotificationCenter.default.post(name: NSNotification.Name("NavigateTo"), object: "ContactUs")
        }else if indexPath.row == 2 {
            NotificationCenter.default.post(name: NSNotification.Name("NavigateTo"), object: "EditProfile")
        }else if indexPath.row == 3 {
            NotificationCenter.default.post(name: NSNotification.Name("NavigateTo"), object: "ChangePassword")
        }else if indexPath.row == 4 {
            NotificationCenter.default.post(name: NSNotification.Name("NavigateTo"), object: "Presentation")
        }else if indexPath.row == 5 {
            NotificationCenter.default.post(name: NSNotification.Name("NavigateTo"), object: "TandC")
        }
           else if indexPath.row == 6 {
                NotificationCenter.default.post(name: NSNotification.Name("NavigateTo"), object: "Feedback")
            }
        else if indexPath.row == 7 {
            UserDefaults.standard.set(false, forKey: Constants.OneTimeLogin)
            UserDefaults.standard.synchronize()
            NotificationCenter.default.post(name: NSNotification.Name(NotificationsText.setRoot), object: "Login")
        }
        
    }
    
    
}
