//
//  EditProfile.swift
//  FinSpark
//
//  Created by Minkle Garg on 17/02/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextFields

class EditProfile: UIViewController {

    @IBOutlet var username_tf: MDCTextField!
    @IBOutlet var email_tf: MDCTextField!
    @IBOutlet var number_tf: MDCTextField!
    
    var usernameControl:MDCTextInputControllerUnderline!
    var emailControl:MDCTextInputControllerUnderline!
    var numberControl:MDCTextInputControllerUnderline!
    
    @IBOutlet var update_btn: UIButton!
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
//       self.navigationController?.isNavigationBarHidden = false
//        self.navigationController?.navigationBar.isTranslucent = false
//        self.navigationController?.navigationBar.barTintColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)

//        self.LeftBarButton(image_name: "backicon", button_text: " aaa Edit Profile", text_color: .white){ () -> () in
//            self.ShowMenu()
//        }
        self.CustomizeNavBar_with(title: "", button_text: "Edit Profile", bartint: UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1), backButton: true) {
            self.navigationController?.popViewController(animated: true)
        }
       // self.update_btn.roundUp()
        
        usernameControl = username_tf.floatingTextField()
        emailControl = email_tf.floatingTextField()
        numberControl = number_tf.floatingTextField()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    //MARK:-
    
    @IBAction func UpdateAction(_ sender: UIButton) {
        
    }
    
    @IBAction func BackAction(_ sender: UIButton) {
           
       }
    
}
