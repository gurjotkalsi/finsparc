//
//  ContactUs.swift
//  FinSpark
//
//  Created by Minkle Garg on 17/02/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class ContactUs: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.CustomizeNavBar_with(title: "", button_text: "Contact Us", bartint: UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1), backButton: true) {
                   self.navigationController?.popViewController(animated: true)
               }
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
