//
//  FNOPulseModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 24/04/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class FNOPulseModel: NSObject {
    static var shared = FNOPulseModel()
    
    var status = ""
    var message = ""
    
    var cdate = ""
    var expiry = ""
    
  //  var market_today = MarketTodayModel()
    var fear_greed_index = FearGreedIndexModel()
    var fear_greed_chart = FearGreedChartModel()
    var list = [MarketTodayModel]() //current_expiry_score_card

    func process(completion: @escaping ( _ model : FNOPulseModel) -> () ) {
        
        Services().getService(url: ServiceURL.fnoPulse, parameters: [:]) { (result) in
            
            print(result)
            
            self.status = onlyString(result.value(forKey: "status"))
            self.message = onlyString(result.value(forKey: "message"))
            
            if let resultArray = result.value(forKey: "result") as? NSArray {
                if let resultArrayValue = resultArray[0] as? NSDictionary {
                    
                    self.cdate = onlyString(resultArrayValue.value(forKey: "cdate"))
                    self.expiry = onlyString(resultArrayValue.value(forKey: "expiry"))
                    
                    
                    if let mitemArray = resultArrayValue.value(forKey: "market_today") as? NSArray {
                        self.list.removeAll()
                        for item in mitemArray {
                            self.list.append(MarketTodayModel(item as! NSDictionary))
                        }
                    }
//                    if let mItem = resultArrayValue.value(forKey: "market_today") as? NSArray {
//                      //  self.market_today.append(MarketTodayModel(mItem))
//                        self.market_today = MarketTodayModel(mItem)
//                    }
                    
                    if let fgiItem = resultArrayValue.value(forKey: "fear_greed_index") as? NSArray {
                        self.fear_greed_index = FearGreedIndexModel(fgiItem)
                    }
                    
                    if let fchartItem = resultArrayValue.value(forKey: "fear_greed_chart") as? NSDictionary {
                        self.fear_greed_chart = FearGreedChartModel(fchartItem)
                    }
                    
                }
            }
            
            completion(self)
        }
    }
    
}

class MarketTodayModel: NSObject {
    //var list = [MarketTodayModel]()

    var nifty_close = ""
    var nifty_change_percent = ""
    var current_sum_nifty_oi = ""
    var change_oi_nifty = ""
    var volume_nifty = ""
    var volume_niftypercent = ""
    var score_nf = ""
    var call_ratio_nf = ""
    var put_ratio_nf = ""
    var nf_color = ""
    var curr_nifty_wap_call = ""
    var curr_nifty_wap_put = ""
    var bn_close = ""
    var banknifty_change_percent = ""
    var current_sum_bn_oi = ""
    var change_oi_banknifty = ""
    var volume_banknifty = ""
    var volume_bankniftypercent = ""
    var score_bn = ""
    var call_ratio_bn = ""
    var put_ratio_bn = ""
    var bn_color = ""
    var curr_banknifty_wap_call = ""
    var curr_banknifty_wap_put = ""
    var change = ""

    override init() {}
    
    init(_ dict : NSDictionary)
    {
        self.nifty_close = onlyString(dict.value(forKey: "close")).getTwoDecimalString()
        self.nifty_change_percent = onlyString(dict.value(forKey: "change_percent")).getTwoDecimalString()
        self.change = onlyString(dict.value(forKey: "change")).getTwoDecimalString()

        self.current_sum_nifty_oi = onlyString(dict.value(forKey: "current_sum_oi")).getIntegerString()
        
        self.change_oi_nifty = onlyString(dict.value(forKey: "change_oi"))
        
        self.volume_nifty = onlyString(dict.value(forKey: "volume")).getIntegerString()
        
        self.volume_niftypercent = onlyString(dict.value(forKey: "volume_percent"))
        self.score_nf = onlyString(dict.value(forKey: "score")).getTwoDecimalString()
        self.call_ratio_nf = onlyString(dict.value(forKey: "call_ratio")).getTwoDecimalString()
        self.put_ratio_nf = onlyString(dict.value(forKey: "put_ratio")).getTwoDecimalString()
      //  self.nf_color = onlyString(dict.value(forKey: "nf_color"))
        self.curr_nifty_wap_call = onlyString(dict.value(forKey: "curr_wap_call"))
        self.curr_nifty_wap_put = onlyString(dict.value(forKey: "curr_wap_put"))
      //  self.bn_close = onlyString(dict.value(forKey: "bn_close")).getTwoDecimalString()
        self.banknifty_change_percent = onlyString(dict.value(forKey: "banknifty_change_percent")).getTwoDecimalString()
      
        self.call_ratio_bn = onlyString(dict.value(forKey: "call_ratio")).getTwoDecimalString()
        self.put_ratio_bn = onlyString(dict.value(forKey: "put_ratio")).getTwoDecimalString()
  
    }
}

class FearGreedIndexModel: NSObject {
    
    var list = [FearGreedIndexItem]()
    
    var change_in_nifty_eod = FearGreedIndexItem()
    var change_in_nifty_five_days = FearGreedIndexItem()
    var per_lot_profit_eod_days = FearGreedIndexItem()
    var per_lot_profit_5_days = FearGreedIndexItem()
    var per_gainers = FearGreedIndexItem()
    var avg_stock_score = FearGreedIndexItem()
    var per_stock_above_5_dma = FearGreedIndexItem()
    var ffmi_score_outof_100 = FearGreedIndexItem()
    var ffmi_score_msg = FearGreedIndexItem()
    
    override init() { }
    init(_ arra : NSArray) {
                
            for item in arra {
                if let dumb = item as? NSDictionary {
                    let symbol = onlyString(dumb.value(forKey: "Particulars"))
                    
                    if symbol == "Change in Nifty EOD" {
                        self.change_in_nifty_eod = FearGreedIndexItem(dumb)
                    }
                    
                    if symbol == "Change in Nifty Five Days" {
                        self.change_in_nifty_five_days = FearGreedIndexItem(dumb)
                    }
                    
                    if symbol == "Per Lot Profit EOD Days" {
                        self.per_lot_profit_eod_days = FearGreedIndexItem(dumb)
                    }
                    
                    if symbol == "Per Lot Profit 5 Days" {
                        self.per_lot_profit_5_days = FearGreedIndexItem(dumb)
                    }
                    
                    if symbol == "Per Gainers" {
                        self.per_gainers = FearGreedIndexItem(dumb)
                    }
                    
                    if symbol == "AVG Stock Score" {
                        self.avg_stock_score = FearGreedIndexItem(dumb)
                    }
                    
                    if symbol == "Per Stock Above 5DMA" {
                        self.per_stock_above_5_dma = FearGreedIndexItem(dumb)
                    }
                    
                    if symbol == "Score out of 100" {
                        self.ffmi_score_outof_100 = FearGreedIndexItem(dumb)
                    }
                    
                    if symbol == "Score Message" {
                        self.ffmi_score_msg = FearGreedIndexItem(dumb)
                    }
                }
            }
        
        self.list.removeAll()
        self.list.append(self.change_in_nifty_eod)
        self.list.append(self.change_in_nifty_five_days)
        self.list.append(self.per_lot_profit_eod_days)
        self.list.append(self.per_lot_profit_5_days)
        self.list.append(self.per_gainers)
        self.list.append(self.avg_stock_score)
        self.list.append(self.per_stock_above_5_dma)
        self.list.append(self.ffmi_score_outof_100)
        self.list.append(self.ffmi_score_msg)
    }
}

class FearGreedIndexItem: NSObject {
    var particulars = ""
    var ltd = ""
    var prev_day = ""
    
    override init() { }
    init(_ dict : NSDictionary) {
        let symbol = onlyString(dict.value(forKey: "Particulars"))
        
        if symbol == "Change in Nifty EOD" {
            self.particulars = "Change in Nifty (EOD)"
        }
        
        if symbol == "Change in Nifty Five Days" {
            self.particulars = "Change in Nifty (Five Days)"
        }
        
        if symbol == "Per Lot Profit EOD Days" {
            self.particulars = "Per Lot Profit (EOD)"
        }
        
        if symbol == "Per Lot Profit 5 Days" {
            self.particulars = "Per Lot Profit (Five Days)"
        }
        
        if symbol == "Per Gainers" {
            self.particulars = "% Gainers"
        }
        
        if symbol == "AVG Stock Score" {
            self.particulars = "Average Stocks Score"
        }
        
        if symbol == "Per Stock Above 5DMA" {
            self.particulars = "% Stocks above (Five Days)"
        }
        
        if symbol == "Score out of 100" {
            self.particulars = "Score out of 100"
        }
        
        self.prev_day = onlyString(dict.value(forKey: "prev_day")).getTwoDecimalString()
        self.ltd = onlyString(dict.value(forKey: "ltd")).getTwoDecimalString()
        if symbol == "Score Message" {
            self.particulars = "Score Message"
            self.prev_day = onlyString(dict.value(forKey: "prev_day"))
            self.ltd = onlyString(dict.value(forKey: "ltd"))
        }
    }
}

class FearGreedChartModel: NSObject {
    
    var mood_of_market = ""
    
    var day = [FearGreedChartItem]()
    var week = [FearGreedChartItem]()
    var last_3_month = [FearGreedChartItem]()
    var last_6_month = [FearGreedChartItem]()
    
    override init() { }
    init(_ dict : NSDictionary) {
        self.mood_of_market = onlyString(dict.value(forKey: "mood_of_market"))
        
        self.day.removeAll()
        self.week.removeAll()
        self.last_3_month.removeAll()
        self.last_6_month.removeAll()
        
        if let dayArray = dict.value(forKey: "day") as? NSArray {
            for dayItem in dayArray {
             self.day.append(FearGreedChartItem(dayItem as! NSDictionary))
            }
        }
        
        if let weekArray = dict.value(forKey: "week") as? NSArray {
            for weekItem in weekArray {
                self.week.append(FearGreedChartItem(weekItem as! NSDictionary))
            }
        }
        
        if let threeArray = dict.value(forKey: "last_3_month") as? NSArray {
            for threeItem in threeArray {
                self.last_3_month.append(FearGreedChartItem(threeItem as! NSDictionary))
            }
        }
        
        if let sixArray = dict.value(forKey: "last_6_month") as? NSArray {
            for sixItem in sixArray {
                self.last_6_month.append(FearGreedChartItem(sixItem as! NSDictionary))
            }
        }
        
        
    }
}

class FearGreedChartItem: NSObject {
    var cdate = ""
    var mmi_score = ""
    
    override init() { }
    init(_ dict : NSDictionary) {
//        print("cdate",dict.value(forKey: "cdate") as Any)
//        let inputFormatter = DateFormatter()
//        inputFormatter.dateFormat = "yyyy-MM-dd"
//        let showDate = inputFormatter.date(from: onlyString(dict.value(forKey: "cdate")))
//        inputFormatter.dateFormat = "dd-MM-yyyy"
//        let resultString = inputFormatter.string(from: showDate!)
//        print(resultString)
//        self.cdate = resultString
        self.cdate = onlyString(dict.value(forKey: "date"))
        self.mmi_score = onlyString(dict.value(forKey: "mmi_score"))
    }
}

