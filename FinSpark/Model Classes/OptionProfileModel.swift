//
//  OptionProfileModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 09/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class OptionProfileModel: NSObject {
    static let shared = OptionProfileModel()
    
    var option_close = ""
    var option_close_percent = ""
    var precent_close_5 = ""
    var option_trigger_price = ""
    var oi = ""
    var percent_oi_5 = ""
    var percent_close_expiry = ""
    var percent_oi_expiry = ""
    var percent_chg_vol = ""
    var future_rank = ""
    var stock_count = ""
    var future_score = ""
    var future_score_difference = ""
    var future_trigger_price = ""
    var future_closing = ""
    var future_price_diff = ""
    var percent_change_since_last_expiry = ""
    var wap = ""
    var future_vol = ""
    var future_vol_diff = ""
    var call_strength = ""
    var put_strength = ""
    var percent_poi = ""
    var con_color = ""
    var cdate = ""
    
    var list = [OptionProfileObject]()
    
    func process(completion: @escaping ( _ model : OptionProfileModel) -> () ) {
        
        Services().getService(url: ServiceURL.optionprofile, parameters: [:]) { (result) in
            
            if let response = result.value(forKey: "result") as? NSArray {
                print(response[0])
                if let dict = response[0] as? NSDictionary {
                    self.option_close = onlyString(dict.value(forKey: "option_close"))
                    self.option_close_percent = onlyString(dict.value(forKey: "option_close_percent"))
                    self.precent_close_5 = onlyString(dict.value(forKey: "precent_close_5"))
                    self.option_trigger_price = onlyString(dict.value(forKey: "option_trigger_price")).getTwoDecimalString()
                    self.oi = onlyString(dict.value(forKey: "oi"))
                    self.percent_oi_5 = onlyString(dict.value(forKey: "percent_oi_5"))
                    self.percent_close_expiry = onlyString(dict.value(forKey: "percent_close_expiry"))
                    self.percent_oi_expiry = onlyString(dict.value(forKey: "percent_oi_expiry"))
                    self.percent_chg_vol = onlyString(dict.value(forKey: "percent_chg_vol"))
                    self.future_rank = onlyString(dict.value(forKey: "future_rank"))
                    self.stock_count = onlyString(dict.value(forKey: "stock_count"))
                    self.future_score = onlyString(dict.value(forKey: "future_score"))
                    self.future_score_difference = onlyString(dict.value(forKey: "future_score_difference"))
                    self.future_trigger_price = onlyString(dict.value(forKey: "future_trigger_price"))
                    self.future_closing = onlyString(dict.value(forKey: "future_closing"))
                    self.future_price_diff = onlyString(dict.value(forKey: "future_price_diff"))
                    self.percent_change_since_last_expiry = onlyString(dict.value(forKey: "percent_change_since_last_expiry"))
                    self.wap = onlyString(dict.value(forKey: "wap")).getTwoDecimalString()
                    self.future_vol = onlyString(dict.value(forKey: "future_vol"))
                    self.future_vol_diff = onlyString(dict.value(forKey: "future_vol_diff"))
                    self.call_strength = onlyString(dict.value(forKey: "call_strength")).getTwoDecimalString()
                    self.put_strength = onlyString(dict.value(forKey: "put_strength")).getTwoDecimalString()
                    self.percent_poi = onlyString(dict.value(forKey: "percent_poi"))
                    self.con_color = onlyString(dict.value(forKey: "con_color"))
                    self.cdate = onlyString(dict.value(forKey: "cdate"))
                    
                    self.list.removeAll()
                    if let arra = dict.value(forKey: "days_5_data") as? NSArray {
                        for item in arra {
                            self.list.append(OptionProfileObject(item as! NSDictionary))
                        }
                    }
                }
                
            }
        }
        
    }
    
}

class OptionProfileObject: NSObject {
    var close = ""
    var open_int = ""
    var chg_in_oi = ""
    var volume = ""
    var timestamp_finsparc_bhavcopy = ""
    var future_price = ""
    
    override init() { }
    
    init(_ dict: NSDictionary) {
        self.close = onlyString(dict.value(forKey: "close"))
        self.open_int = onlyString(dict.value(forKey: "open_int"))
        self.chg_in_oi = onlyString(dict.value(forKey: "chg_in_oi"))
        self.volume = onlyString(dict.value(forKey: "volume"))
        self.timestamp_finsparc_bhavcopy = onlyString(dict.value(forKey: "timestamp_finsparc_bhavcopy"))
        self.future_price = onlyString(dict.value(forKey: "future_price"))
    }
}
