//
//  TradeModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 01/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class TradeModel: NSObject {
    
    static let shared = TradeModel()
    
    var data = NSDictionary()
    var tradeTypes = [String]()
    
    func getTradeInfo(trade: String, completion: @escaping ( _ model : TradeModel) -> () ) {
        
        Services().getService(url: ServiceURL.tradeInfo + trade, parameters: [:]) { (result) in
            
            if let response = result.value(forKey: "result") as? NSArray {
                if let dict = response[0] as? NSDictionary {
                    self.data = dict
                    self.tradeTypes = dict.allKeys as! [String]
                }
            }
            
            completion(self)
        }
    }
    
    func tradeKeys(ofTrade:String) -> [String] {
        if let object = self.data.value(forKey: ofTrade) as? NSDictionary {
            return object.allKeys as! [String]
        }
        
        return []
    }
    
    func tradeValues(ofKey:String, ofTrade:String) -> [TradeItem] {
        if let object = self.data.value(forKey: ofTrade) as? NSDictionary {
            if let item = object.value(forKey: ofKey) as? NSArray {
                var list = [TradeItem]()
                for part in item {
                    list.append(TradeItem(part as! NSDictionary))
                }
                return list
            }
        }
        
        return [TradeItem]()
    }
    
    func tradeDates(ofKey:String, ofTrade:String) -> [String] {
        if let object = self.data.value(forKey: ofTrade) as? NSDictionary {
            if let item = object.value(forKey: ofKey) as? NSArray {
                var list = [String]()
                for part in item {
                    let TItem = TradeItem(part as! NSDictionary)
                    list.append(TItem.cdate)
                }
                return list
            }
        }
        
        return []
    }

}
