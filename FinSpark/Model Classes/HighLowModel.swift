//
//  HighLowModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 08/05/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class HighLowModel: NSObject {

    static var shared = HighLowModel()
    
    var status = ""
    var message = ""
    
    var cdate = ""
    var current_expiry = ""
    var next_expiry = ""
    
    var current_expiry_high = [HighLowItem]()
    var current_expiry_low = [HighLowItem]()
    var current_seven_days_high = [HighLowItem]()
    var current_seven_days_low = [HighLowItem]()
    var current_eleven_days_high = [HighLowItem]()
    var current_eleven_days_low = [HighLowItem]()
    
    var next_expiry_high = [HighLowItem]()
    var next_expiry_low = [HighLowItem]()
    var next_seven_days_high = [HighLowItem]()
    var next_seven_days_low = [HighLowItem]()
    var next_eleven_days_high = [HighLowItem]()
    var next_eleven_days_low = [HighLowItem]()
    
    func process(completion: @escaping ( _ model : HighLowModel) -> () ) {
        
        Services().getService(url: ServiceURL.highLowReport, parameters: [:]) { (result) in
            
            print(result)
            
            self.status = onlyString(result.value(forKey: "status"))
            self.message = onlyString(result.value(forKey: "message"))
            
            self.current_expiry_high = [HighLowItem]()
            self.current_expiry_low = [HighLowItem]()
            self.current_seven_days_high = [HighLowItem]()
            self.current_seven_days_low = [HighLowItem]()
            self.current_eleven_days_high = [HighLowItem]()
            self.current_eleven_days_low = [HighLowItem]()
            
            self.next_expiry_high = [HighLowItem]()
            self.next_expiry_low = [HighLowItem]()
            self.next_seven_days_high = [HighLowItem]()
            self.next_seven_days_low = [HighLowItem]()
            self.next_eleven_days_high = [HighLowItem]()
            self.next_eleven_days_low = [HighLowItem]()
            
            if let resultArray = result.value(forKey: "result") as? NSArray {
                if let response = resultArray[0] as? NSDictionary {
                    
                    self.cdate = onlyString(response.value(forKey: "cdate"))
                    self.current_expiry = onlyString(response.value(forKey: "current_expiry"))
                    self.next_expiry = onlyString(response.value(forKey: "next_expiry"))
                    
                    if let valueArray = response.value(forKey: "current_expiry_high") as? NSArray {
                        for item in valueArray {
                            self.current_expiry_high.append( HighLowItem(item as! NSDictionary) )
                        }
                    }
                    
                    if let valueArray = response.value(forKey: "current_expiry_low") as? NSArray {
                        for item in valueArray {
                            self.current_expiry_low.append( HighLowItem(item as! NSDictionary) )
                        }
                    }
                    
                    if let valueArray = response.value(forKey: "current_seven_days_high") as? NSArray {
                        for item in valueArray {
                            self.current_seven_days_high.append( HighLowItem(item as! NSDictionary) )
                        }
                    }
                    
                    if let valueArray = response.value(forKey: "current_seven_days_low") as? NSArray {
                        for item in valueArray {
                            self.current_seven_days_low.append( HighLowItem(item as! NSDictionary) )
                        }
                    }
                    
                    if let valueArray = response.value(forKey: "current_eleven_days_high") as? NSArray {
                        for item in valueArray {
                            self.current_eleven_days_high.append( HighLowItem(item as! NSDictionary) )
                        }
                    }
                    
                    if let valueArray = response.value(forKey: "current_eleven_days_low") as? NSArray {
                        for item in valueArray {
                            self.current_eleven_days_low.append( HighLowItem(item as! NSDictionary) )
                        }
                    }
                    
                    if let valueArray = response.value(forKey: "next_expiry_high") as? NSArray {
                        for item in valueArray {
                            self.next_expiry_high.append( HighLowItem(item as! NSDictionary) )
                        }
                    }
                    
                    if let valueArray = response.value(forKey: "next_expiry_low") as? NSArray {
                        for item in valueArray {
                            self.next_expiry_low.append( HighLowItem(item as! NSDictionary) )
                        }
                    }
                    
                    if let valueArray = response.value(forKey: "nextseven_days_high") as? NSArray {
                        for item in valueArray {
                            self.next_seven_days_high.append( HighLowItem(item as! NSDictionary) )
                        }
                    }
                    
                    if let valueArray = response.value(forKey: "next_seven_days_low") as? NSArray {
                        for item in valueArray {
                            self.next_seven_days_low.append( HighLowItem(item as! NSDictionary) )
                        }
                    }
                    
                    if let valueArray = response.value(forKey: "next_eleven_days_high") as? NSArray {
                        for item in valueArray {
                            self.next_eleven_days_high.append( HighLowItem(item as! NSDictionary) )
                        }
                    }
                    
                    if let valueArray = response.value(forKey: "next_eleven_days_low") as? NSArray {
                        for item in valueArray {
                            self.next_eleven_days_low.append( HighLowItem(item as! NSDictionary) )
                        }
                    }
                                        
                }
            }
            
            completion(self)
            
        }
    }
    
    
}

class HighLowItem: NSObject {
    
    var symbol = ""
    var score = ""
    var trigger_price  = ""  //": 341.032570202104,
    var price_close = ""
    var change_since_last_day = ""
    
    var expiry_high_max = ""
    var high_5 = ""
    var low_5 = ""
    var high_11 = ""
    var low_11 = ""
    var rank = ""

    override init() {}
    init(_ dict: NSDictionary) {
        self.symbol = onlyString(dict.value(forKey: "symbol"))
        self.score = onlyString(dict.value(forKey: "score"))
        self.trigger_price = onlyString(dict.value(forKey: "trigger_price")).getTwoDecimalString()
        self.price_close = onlyString(dict.value(forKey: "price_close")).getTwoDecimalString()
        self.change_since_last_day = onlyString(dict.value(forKey: "change_since_last_day"))
        
        self.expiry_high_max = onlyString(dict.value(forKey: "expiry_high_max"))
        self.rank = onlyString(dict.value(forKey: "rank"))

        self.high_5 = onlyString(dict.value(forKey: "high_5"))
        self.low_5 = onlyString(dict.value(forKey: "low_5"))
        self.high_11 = onlyString(dict.value(forKey: "high_11")).getTwoDecimalString()
        self.low_11 = onlyString(dict.value(forKey: "low_11"))
    }
    
}




