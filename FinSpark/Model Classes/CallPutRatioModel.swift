//
//  CallPutRatioModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 04/05/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class CallPutRatioModel: NSObject {
    
    static var shared = CallPutRatioModel()
    
    var status = ""
    var message = ""
    
    var current_callput = [RatioItem]()
    var next_callput = [RatioItem]()
    
    func process(completion: @escaping ( _ model : CallPutRatioModel) -> () ) {
        
        Services().getService(url: ServiceURL.callPutRatio, parameters: [:]) { (result) in
            
            print(result)
            
            self.status = onlyString(result.value(forKey: "status"))
            self.message = onlyString(result.value(forKey: "message"))
            
            self.current_callput.removeAll()
            self.next_callput.removeAll()
            
            if let resultArray = result.value(forKey: "result") as? NSArray {
                if let response = resultArray[0] as? NSDictionary {
                    
                    if let valueArray = response.value(forKey: "current_callput") as? NSArray {
                        for item in valueArray {
                            self.current_callput.append(RatioItem(item as! NSDictionary))
                        }
                    }
                    
                    if let valueArray1 = response.value(forKey: "next_callput") as? NSArray {
                        for item1 in valueArray1 {
                            self.next_callput.append(RatioItem(item1 as! NSDictionary))
                        }
                    }
                    
                }
            }
            
            completion(self)
            
        }
    }
    
}


class RatioItem: NSObject {
    
    var symbol = ""
    var total = ""
    var call_value = ""
    var put_value = ""
    var call_ratio = ""
    var put_ratio = ""
    var last_date = ""
    var current_expiry = ""
    
    override init() {}
    init(_ dict : NSDictionary) {
        self.symbol = onlyString(dict.value(forKey: "symbol"))
        self.total = onlyString(dict.value(forKey: "total")).getTwoDecimalString()
        self.call_value = onlyString(dict.value(forKey: "call_value")).getTwoDecimalString()
        self.put_value = onlyString(dict.value(forKey: "put_value")).getTwoDecimalString()
        self.call_ratio = onlyString(dict.value(forKey: "call_ratio")).getTwoDecimalString()
        self.put_ratio = onlyString(dict.value(forKey: "put_ratio")).getTwoDecimalString()
        self.last_date = onlyString(dict.value(forKey: "last_date"))
        self.current_expiry = onlyString(dict.value(forKey: "current_expiry"))
    }
    
}
