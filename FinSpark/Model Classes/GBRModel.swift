//
//  GBRModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 22/04/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class GBRModel: NSObject {

    static var shared = GBRModel()
    
    var status = ""
    var message = ""
    var cdate = ""
    var expiry = ""

    var current_green_future = [GBRItem]() //result
    var current_red_future = [GBRItem]() //result
    var current_blue_future = [GBRItem]() //result
    var next_green_future = [GBRItem]() //result
    var next_red_future = [GBRItem]() //result
    var next_blue_future = [GBRItem]() //result
    
    func process(completion: @escaping ( _ model : GBRModel) -> () ) {
        
        Services().getService(url: ServiceURL.gbrReport, parameters: [:]) { (result) in

            self.status = onlyString(result.value(forKey: "status"))
            self.message = onlyString(result.value(forKey: "message"))
            
            self.current_green_future.removeAll()
            self.current_red_future.removeAll()
            self.current_blue_future.removeAll()
            self.next_green_future.removeAll()
            self.next_red_future.removeAll()
            self.next_blue_future.removeAll()
            
            if let resultArray = result.value(forKey: "result") as? NSArray {
                if let resultArrayValue = resultArray[0] as? NSDictionary {
                 self.cdate = onlyString(resultArrayValue.value(forKey: "cdate"))
                 self.expiry = onlyString(resultArrayValue.value(forKey: "current_expiry"))

                    if let valueArray1 = resultArrayValue.value(forKey: "current_green_future") as? NSArray {
                        for item1 in valueArray1 {
                            self.current_green_future.append(GBRItem(item1 as! NSDictionary))
                        }
                    }
                    
                    if let valueArray2 = resultArrayValue.value(forKey: "current_red_future") as? NSArray {
                        for item2 in valueArray2 {
                            self.current_red_future.append(GBRItem(item2 as! NSDictionary))
                        }
                    }
                    
                    if let valueArray3 = resultArrayValue.value(forKey: "current_blue_future") as? NSArray {
                        for item3 in valueArray3 {
                            self.current_blue_future.append(GBRItem(item3 as! NSDictionary))
                        }
                    }
                    
                    
                    if let valueArray4 = resultArrayValue.value(forKey: "next_green_future") as? NSArray {
                        for item4 in valueArray4 {
                            self.next_green_future.append(GBRItem(item4 as! NSDictionary))
                        }
                    }
                    
                    if let valueArray4 = resultArrayValue.value(forKey: "next_red_future") as? NSArray {
                        for item4 in valueArray4 {
                            self.next_red_future.append(GBRItem(item4 as! NSDictionary))
                        }
                    }
                    
                    if let valueArray5 = resultArrayValue.value(forKey: "next_blue_future") as? NSArray {
                        for item5 in valueArray5 {
                            self.next_blue_future.append(GBRItem(item5 as! NSDictionary))
                        }
                    }
                    
                    
                }
            }
            
            
            completion(self)
        }
        
    }
    
}

class GBRItem : NSObject {
    var symbol = ""
    var total = ""
    var rank = ""
    var trigger_price = ""
    var future_closing = ""
    var percent_change_last_days_price = ""
    var color_count = ""
    
    override init() {}
    
    init(_ dict : NSDictionary) {
        self.symbol = onlyString(dict.value(forKey: "symbol"))
        self.total = onlyString(dict.value(forKey: "total"))
        self.rank = onlyString(dict.value(forKey: "rank"))
        self.trigger_price = onlyString(dict.value(forKey: "trigger_price")).getTwoDecimalString()
        self.future_closing = onlyString(dict.value(forKey: "future_closing"))
        self.percent_change_last_days_price = onlyString(dict.value(forKey: "percent_change_last_days_price"))
        self.color_count = onlyString(dict.value(forKey: "color_count"))
    }
}
