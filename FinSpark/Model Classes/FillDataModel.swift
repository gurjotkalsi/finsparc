//
//  FillDataModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 10/05/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class FillDataModel: NSObject {

    static var shared = FillDataModel()
    
    var status = ""
    var message = ""
    
    var cdate = ""
    var expiry = ""
    
    var list = [FillDataItem]() //fii_data
    
    func process(completion: @escaping ( _ model : FillDataModel) -> () ) {
        
        Services().getService(url: ServiceURL.fillData, parameters: [:]) { (result) in
            
            print(result)
            
            self.status = onlyString(result.value(forKey: "status"))
            self.message = onlyString(result.value(forKey: "message"))
            
            if let resultArray = result.value(forKey: "result") as? NSArray {
                if let response = resultArray[0] as? NSDictionary {
                    
                    self.cdate = onlyString(response.value(forKey: "cdate"))
                    self.expiry = onlyString(response.value(forKey: "expiry"))
                    print("edate",self.expiry)
                    self.list.removeAll()
                    if let valueArray = response.value(forKey: "fii_data") as? NSArray {
                        for item in valueArray {
                            self.list.append(FillDataItem(item as! NSDictionary))
                        }
                    }
                    
                }
            }
            
            completion(self)
            
        }
    }
    
}

class FillDataItem: NSObject {
    
    var ltd = ""
    var prev_day = ""
    var Particulars = ""
    
    override init() {}
    init(_ dict: NSDictionary) {
        self.ltd = onlyString(dict.value(forKey: "ltd")).getTwoDecimalString()
        self.prev_day = onlyString(dict.value(forKey: "prev_day")).getTwoDecimalString()
        self.Particulars = onlyString(dict.value(forKey: "Particulars"))
    }
}
