//
//  SearchModel.swift
//  FinSpark
//
//  Created by Minkle Garg on 16/07/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class SearchModel: NSObject {

    static var shared = SearchModel()
    
    var status = ""
    var message = ""
    
    var cdate = ""
    var expiry = ""
    
    var list = [FillDataItem]() //fii_data
    
    func process(completion: @escaping ( _ model : SearchModel) -> () ) {
        
        Services().getService(url: ServiceURL.fillData, parameters: [:]) { (result) in
            
            print(result)
            
            self.status = onlyString(result.value(forKey: "status"))
            self.message = onlyString(result.value(forKey: "message"))
            
            if let resultArray = result.value(forKey: "result") as? NSArray {
                if let response = resultArray[0] as? NSDictionary {
                    
                    self.cdate = onlyString(response.value(forKey: "cdate"))
                    self.expiry = onlyString(response.value(forKey: "expiry"))
                    print("edate",self.expiry)
                    self.list.removeAll()
                    if let valueArray = response.value(forKey: "fii_data") as? NSArray {
                        for item in valueArray {
                            self.list.append(FillDataItem(item as! NSDictionary))
                        }
                    }
                    
                }
            }
            
            completion(self)
            
        }
    }
    
}
