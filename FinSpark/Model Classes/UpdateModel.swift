//
//  UpdateModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 27/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class UpdateModel: NSObject {
    
    static let shared = UpdateModel()
    
    var status = "" //Status | if 200 it is a success
    var msg = "" //Msg
    
    func updatePassword(parameter : NSDictionary, completion: @escaping ( _ model : UpdateModel) -> () ) {
        
        Services().postService(url: ServiceURL.update_password, parameters: parameter) { (result) in
            
            self.status = onlyString(result.value(forKey: "Status"))
            self.msg = onlyString(result.value(forKey: "Msg"))
            
            completion(self)
        }
    }
    
    
}

