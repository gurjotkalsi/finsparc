//
//  StockListModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 01/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class StockListModel: NSObject {
    
    static let shared = StockListModel()
    
    var status = ""
    var message = ""
    var list = [String]()
    
    func process(completion: @escaping ( _ model : StockListModel) -> () ) {
        
        Services().getService(url: ServiceURL.stockList, parameters: [:]) { (result) in
            
            self.status = onlyString(result.value(forKey: "status"))
            self.message = onlyString(result.value(forKey: "message"))
            
            if let arra = result.value(forKey: "result") as? NSArray {
                self.list.removeAll()
                for item in arra  {
                    if let dict = item as? NSDictionary {
                        self.list.append(onlyString(dict.value(forKey: "stock")))
                    }
                }
            }
            
            completion(self)
        }
    }
    
}

struct TradeItem {
    var symbol: String
    var expiry_dt: String
    var strike_pr: String
    var option_type: String
    var close: String
    var settle_pr: String
    var open_int: String
    var cdate: String
    var stock: String
    
    init() {
        symbol = ""
        expiry_dt = ""
        strike_pr = ""
        option_type = ""
        close = ""
        settle_pr = ""
        open_int = ""
        cdate = ""
        stock = ""
    }
    
    init(_ dict:NSDictionary) {
        symbol = onlyString(dict.value(forKey: "symbol"))
                let inputFormatter = DateFormatter()
                inputFormatter.dateFormat = "yyyy-MM-dd"
                let eexpiry_dt = inputFormatter.date(from: onlyString(dict.value(forKey: "expiry_dt")))
        let ccdate_dt = inputFormatter.date(from: onlyString(dict.value(forKey: "cdate")))

              //  inputFormatter.dateFormat = "dd-MM-yyyy"
                let resultString = inputFormatter.string(from: eexpiry_dt!)
                print(resultString)
        expiry_dt = inputFormatter.string(from: eexpiry_dt!)
        strike_pr = onlyString(dict.value(forKey: "strike_pr"))
        option_type = onlyString(dict.value(forKey: "option_type"))
        close = onlyString(dict.value(forKey: "close"))
        settle_pr = onlyString(dict.value(forKey: "settle_pr"))
        open_int = onlyString(dict.value(forKey: "open_int"))
        cdate = inputFormatter.string(from: ccdate_dt!)
        stock = onlyString(dict.value(forKey: "stock"))
    }
}
