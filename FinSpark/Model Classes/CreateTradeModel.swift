//
//  CreateTradeModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 01/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class CreateTradeModel: NSObject {

    static let shared = CreateTradeModel()
    
    var status = ""
    var message = ""
    
    func process(params: NSDictionary, completion: @escaping ( _ model : CreateTradeModel) -> () ) {

            Services().postService(url: ServiceURL.createPortfolioTrade, parameters: params) { (result) in

       // Services().postService(url: ServiceURL.createPortfolioTrade, Headers: headers, parameters: params) {
            print("result",result)
            
            self.status = onlyString(result.value(forKey: "status"))
            self.message = onlyString(result.value(forKey: "message"))
            
            completion(self)
        }
    }
    
    func updateprocess(params: NSDictionary, completion: @escaping ( _ model : CreateTradeModel) -> () ) {
           Services().postService(url: ServiceURL.updateTrade, parameters: params) { (result) in
           print("result",result)
           
           self.status = onlyString(result.value(forKey: "status"))
           self.message = onlyString(result.value(forKey: "message"))
           
           completion(self)
        }
    }
}
