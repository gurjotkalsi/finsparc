//
//  WatchlistModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 22/05/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class WatchlistModel: NSObject {

    static let shared = WatchlistModel()
    
    var status = ""
    var message = ""
    
    func create(params: NSDictionary, completion: @escaping ( _ model : WatchlistModel) -> () ) {
        
        Services().postService(url: ServiceURL.insertWatchlist, parameters: params) { (result) in
            
            self.status = onlyString(result.value(forKey: "status"))
            self.message = onlyString(result.value(forKey: "message"))
            completion(self)
        }
        
    }
    
}
