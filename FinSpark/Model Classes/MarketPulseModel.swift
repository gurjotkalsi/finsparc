//
//  MarketPulseModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 08/04/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class MarketPulseModel: NSObject {
 
    var id = ""
    var nifty_change = ""
    var nifty_change_percent = ""
    var niftyit_change = ""
    var niftyit_change_percent = ""
    var banknifty_change = ""
    var banknifty_change_percent = ""
    var marketnow = ""
    var mmi_score = ""
    var change_val_inlakh = ""
    var change_volume = ""
    var change_percent_val_inlakh = ""
    var change_percent_volume = ""
    var stock_profit = ""
    var stock_loss = ""
    var stock_above_wap = ""
    var stock_below_wap = ""
    var nifty_close = ""
    var banknifty_close = ""
    var niftyit_close = ""
    var cdate = ""
    var expiry_dt = ""
    var score_nf = ""
    var score_bn = ""
    var score_ni = ""
    var call_ratio_nf = ""
    var call_ratio_bn = ""
    var call_ratio_ni = ""
    var put_ratio_nf = ""
    var put_ratio_bn = ""
    var put_ratio_ni = ""
    var previous_mmi_score = ""
    var call_ratio = ""
    var previous_call_ratio = ""
    var put_ratio = ""
    var previous_put_ratio = ""
    var curr_nifty_wap_call = ""
    var curr_nifty_wap_put = ""
    var curr_banknifty_wap_call = ""
    var curr_banknifty_wap_put = ""
    var prev_nifty_wap_call = ""
    var prev_nifty_wap_put = ""
    var prev_banknifty_wap_call = ""
    var prev_banknifty_wap_put = ""
    var nf_color = ""
    var bn_color = ""
    var ni_color = ""
    var index_future = ""
    var index_option = ""
    var stock_future = ""
    var stock_option = ""
    var previous_index_future = ""
    var previous_index_option = ""
    var previous_stock_future = ""
    var previous_stock_option = ""
    var close_vix = ""
    var change = ""
    var nifty_current_sum_oi = ""
    var banknifty_current_sum_oi = ""
    var nifty_chg_sum_oi = ""
    var banknifty_chg_sum_oi = ""
    var volume_nifty = ""
    var volume_banknifty = ""
    var volume_niftypercent = ""
    var volume_bankniftypercent = ""
    
    func process(completion: @escaping ( _ model : MarketPulseModel) -> () ) {
        
        Services().getService(url: ServiceURL.market_pulse, parameters: [:]) { (result) in
            
            if let response = result.value(forKey: "result") as? NSArray {
                if let dict = response[0] as? NSDictionary {
                    
                    self.id = onlyString(dict.value(forKey: "id"))
                    self.nifty_change = onlyString(dict.value(forKey: "nifty_change"))
                    self.nifty_change_percent = onlyString(dict.value(forKey: "nifty_change_percent"))
                    self.niftyit_change = onlyString(dict.value(forKey: "niftyit_change"))
                    self.niftyit_change_percent = onlyString(dict.value(forKey: "niftyit_change_percent"))
                    self.banknifty_change = onlyString(dict.value(forKey: "banknifty_change"))
                    self.banknifty_change_percent = onlyString(dict.value(forKey: "banknifty_change_percent"))
                    
                    self.marketnow = onlyString(dict.value(forKey: "marketnow"))
                    self.mmi_score = onlyString(dict.value(forKey: "mmi_score"))
                    self.change_val_inlakh = onlyString(dict.value(forKey: "change_val_inlakh"))
                    self.change_volume = onlyString(dict.value(forKey: "change_volume"))
                    self.change_percent_val_inlakh = onlyString(dict.value(forKey: "change_percent_val_inlakh"))
                    self.change_percent_volume = onlyString(dict.value(forKey: "change_percent_volume"))
                    self.stock_profit = onlyString(dict.value(forKey: "stock_profit"))
                    self.stock_loss = onlyString(dict.value(forKey: "stock_loss"))
                    
                    self.stock_above_wap = onlyString(dict.value(forKey: "stock_above_wap"))
                    self.stock_below_wap = onlyString(dict.value(forKey: "stock_below_wap"))
                    self.nifty_close = onlyString(dict.value(forKey: "nifty_close"))
                    self.banknifty_close = onlyString(dict.value(forKey: "banknifty_close"))
                    self.niftyit_close = onlyString(dict.value(forKey: "niftyit_close"))
                    self.cdate = onlyString(dict.value(forKey: "cdate"))
                    self.expiry_dt = onlyString(dict.value(forKey: "expiry_dt"))
                    self.score_nf = onlyString(dict.value(forKey: "score_nf"))
                    self.score_bn = onlyString(dict.value(forKey: "score_bn"))
                    self.score_ni = onlyString(dict.value(forKey: "score_ni"))
                    
                    self.call_ratio_nf = onlyString(dict.value(forKey: "call_ratio_nf"))
                    self.call_ratio_bn = onlyString(dict.value(forKey: "call_ratio_bn"))
                    self.call_ratio_ni = onlyString(dict.value(forKey: "call_ratio_ni"))
                    self.put_ratio_nf = onlyString(dict.value(forKey: "put_ratio_nf"))
                    self.put_ratio_bn = onlyString(dict.value(forKey: "put_ratio_bn"))
                    self.put_ratio_ni = onlyString(dict.value(forKey: "put_ratio_ni"))
                    self.previous_mmi_score = onlyString(dict.value(forKey: "previous_mmi_score"))
                    self.call_ratio = onlyString(dict.value(forKey: "call_ratio"))
                    self.previous_call_ratio = onlyString(dict.value(forKey: "previous_call_ratio"))
                    self.put_ratio = onlyString(dict.value(forKey: "put_ratio"))
                    
                    self.previous_put_ratio = onlyString(dict.value(forKey: "previous_put_ratio"))
                    self.curr_nifty_wap_call = onlyString(dict.value(forKey: "curr_nifty_wap_call"))
                    self.curr_nifty_wap_put = onlyString(dict.value(forKey: "curr_nifty_wap_put"))
                    self.curr_banknifty_wap_call = onlyString(dict.value(forKey: "curr_banknifty_wap_call"))
                    self.curr_banknifty_wap_put = onlyString(dict.value(forKey: "curr_banknifty_wap_put"))
                    self.prev_nifty_wap_call = onlyString(dict.value(forKey: "prev_nifty_wap_call"))
                    self.prev_nifty_wap_put = onlyString(dict.value(forKey: "prev_nifty_wap_put"))
                    self.prev_banknifty_wap_call = onlyString(dict.value(forKey: "prev_banknifty_wap_call"))
                    self.prev_banknifty_wap_put = onlyString(dict.value(forKey: "prev_banknifty_wap_put"))
                    self.nf_color = onlyString(dict.value(forKey: "nf_color"))
                    self.bn_color = onlyString(dict.value(forKey: "bn_color"))
                    self.ni_color = onlyString(dict.value(forKey: "ni_color"))
                    
                    self.index_future = onlyString(dict.value(forKey: "index_future"))
                    self.index_option = onlyString(dict.value(forKey: "index_option"))
                    self.stock_future = onlyString(dict.value(forKey: "stock_future"))
                    self.stock_option = onlyString(dict.value(forKey: "stock_option"))
                    
                    self.previous_index_future = onlyString(dict.value(forKey: "previous_index_future"))
                    self.previous_index_option = onlyString(dict.value(forKey: "previous_index_option"))
                    self.previous_stock_future = onlyString(dict.value(forKey: "previous_stock_future"))
                    self.previous_stock_option = onlyString(dict.value(forKey: "previous_stock_option"))
                    self.close_vix = onlyString(dict.value(forKey: "close_vix"))
                    self.change = onlyString(dict.value(forKey: "change"))
                    self.nifty_current_sum_oi = onlyString(dict.value(forKey: "nifty_current_sum_oi"))
                    self.banknifty_current_sum_oi = onlyString(dict.value(forKey: "banknifty_current_sum_oi"))
                    self.nifty_chg_sum_oi = onlyString(dict.value(forKey: "nifty_chg_sum_oi"))
                    self.banknifty_chg_sum_oi = onlyString(dict.value(forKey: "banknifty_chg_sum_oi"))
                    
                    self.volume_nifty = onlyString(dict.value(forKey: "volume_nifty"))
                    self.volume_banknifty = onlyString(dict.value(forKey: "volume_banknifty"))
                    self.volume_niftypercent = onlyString(dict.value(forKey: "volume_niftypercent"))
                    self.volume_bankniftypercent = onlyString(dict.value(forKey: "volume_bankniftypercent"))
                    
                }
            }
            
            completion(self)
        }
    }
    
}
