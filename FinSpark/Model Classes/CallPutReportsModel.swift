//
//  CallPutReportsModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 03/05/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

enum CallPutType {
    case callCurrent
    case callNext
    case putCurrent
    case putNext
}

class CallPutReportsModel: NSObject {
    
    static var shared = CallPutReportsModel()
    
    var status = ""
    var message = ""
    
    var most_active_indices = [ReportItem]()
    var most_active_stock = [ReportItem]()
    var strong_near_money = [ReportSecondItem]()
    var weak_near_money = [ReportSecondItem]()
    
    var add_most_oi = ReportThirdItem()
    var add_most_oi_5 = ReportThirdItem()
    var shed_most_oi = ReportThirdItem()
    var shed_most_oi_5 = ReportThirdItem()
    
    func process(type:CallPutType, completion: @escaping ( _ model : CallPutReportsModel) -> () ) {
        
        var url = ServiceURL.callReportCurrent
        if type == .callCurrent {
            url = ServiceURL.callReportCurrent
        }else if type == .callNext {
            url = ServiceURL.callReportNext
        }else if type == .putCurrent {
            url = ServiceURL.putReportCurrent
        }else {
            url = ServiceURL.putReportNext
        }
        
        Services().getService(url: url, parameters: [:]) { (result) in
            
            print(result)
            
            self.status = onlyString(result.value(forKey: "status"))
            self.message = onlyString(result.value(forKey: "message"))
            
            self.most_active_indices.removeAll()
            self.most_active_stock.removeAll()
            self.strong_near_money.removeAll()
            self.weak_near_money.removeAll()
            self.add_most_oi = ReportThirdItem()
            self.add_most_oi_5 = ReportThirdItem()
            self.shed_most_oi = ReportThirdItem()
            self.shed_most_oi_5 = ReportThirdItem()
            
            if let resultArray = result.value(forKey: "result") as? NSArray {
                if let dictArray = resultArray[0] as? NSDictionary {
                    
                    if let array1 = dictArray.value(forKey: "most_active_indices") as? NSArray {
                        for item1 in array1 {
                            self.most_active_indices.append(ReportItem(item1 as! NSDictionary))
                        }
                    }

                    if let array2 = dictArray.value(forKey: "most_active_stock") as? NSArray {
                        for item2 in array2 {
                            self.most_active_stock.append(ReportItem(item2 as! NSDictionary))
                        }
                    }
                    
                    if let array3 = dictArray.value(forKey: "strong_near_money") as? NSArray {
                        for item3 in array3 {
                            self.strong_near_money.append(ReportSecondItem(item3 as! NSDictionary, type: type))
                        }
                    }
                    
                    if let array4 = dictArray.value(forKey: "weak_near_money") as? NSArray {
                        for item4 in array4 {
                            self.weak_near_money.append(ReportSecondItem(item4 as! NSDictionary, type: type))
                        }
                    }

                    if let addOIArray = dictArray.value(forKey: "add_most_oi") as? NSArray {
                        if addOIArray.count != 0 {
                            self.add_most_oi = ReportThirdItem(addOIArray[0] as! NSDictionary, type: type)
                        }
                    }
                    
                    if let addOIArray5 = dictArray.value(forKey: "add_most_oi_5") as? NSArray {
                        if addOIArray5.count != 0 {
                            self.add_most_oi_5 = ReportThirdItem(addOIArray5[0] as! NSDictionary, type: type)
                        }
                    }
                    
                    if let shedOIArray = dictArray.value(forKey: "shed_most_oi") as? NSArray {
                        if shedOIArray.count != 0 {
                            self.shed_most_oi = ReportThirdItem(shedOIArray[0] as! NSDictionary, type: type)
                        }
                    }
                    
                    if let shedOIArray5 = dictArray.value(forKey: "shed_most_oi_5") as? NSArray {
                        if shedOIArray5.count != 0 {
                            self.shed_most_oi_5 = ReportThirdItem(shedOIArray5[0] as! NSDictionary, type: type)
                        }
                    }
                    
                }
            }
            
            completion(self)
            
        }
    }

}

class ReportItem: NSObject {
    
    var symbol = ""
    var expiry_dt = ""
    var cdate = ""
    var stike_pr = ""
    var close = ""
    var contratcs = ""
    var trigger_price = ""
    var previous_close = ""
    var price_diff = ""
    var sw = ""
    var expiry_high = ""
    var expiry_low = ""
    
    override init() { }
    init(_ dict: NSDictionary) {
        self.symbol = onlyString(dict.value(forKey: "symbol"))
        self.expiry_dt = onlyString(dict.value(forKey: "expiry_dt"))
        self.cdate = onlyString(dict.value(forKey: "cdate"))
        self.stike_pr = onlyString(dict.value(forKey: "stike_pr")).getTwoDecimalString()
        self.close = onlyString(dict.value(forKey: "close")).getTwoDecimalString()
        self.contratcs = onlyString(dict.value(forKey: "contratcs")).getTwoDecimalString()
        self.trigger_price = onlyString(dict.value(forKey: "trigger_price")).getTwoDecimalString()
        self.previous_close = onlyString(dict.value(forKey: "previous_close")).getTwoDecimalString()
        self.price_diff = onlyString(dict.value(forKey: "price_diff")).getTwoDecimalString()
        self.sw = onlyString(dict.value(forKey: "sw"))
        self.expiry_high = onlyString(dict.value(forKey: "expiry_high")).getTwoDecimalString()
        self.expiry_low = onlyString(dict.value(forKey: "expiry_low")).getTwoDecimalString()
    }

}

class ReportSecondItem: NSObject {
    
    var symbol = ""
    var future_closing = ""
    var itm_call_strike = ""
    
    var call_trigger = ""
    var call_ltp = ""
    
    var last_date = ""
    var current_expiry = ""
    
    override init() { }
    init(_ dict: NSDictionary, type:CallPutType) {
        self.symbol = onlyString(dict.value(forKey: "symbol"))
        self.future_closing = onlyString(dict.value(forKey: "future_closing")).getTwoDecimalString()
        self.itm_call_strike = onlyString(dict.value(forKey: "itm_call_strike")).getTwoDecimalString()
        
        if type == .putNext || type == .putCurrent {
            self.call_trigger = onlyString(dict.value(forKey: "put_trigger")).getTwoDecimalString()
            self.call_ltp = onlyString(dict.value(forKey: "put_ltp")).getTwoDecimalString()
        }else{
            self.call_trigger = onlyString(dict.value(forKey: "call_trigger")).getTwoDecimalString()
            self.call_ltp = onlyString(dict.value(forKey: "call_ltp")).getTwoDecimalString()
        }
        
        self.last_date = onlyString(dict.value(forKey: "last_date"))
        self.current_expiry = onlyString(dict.value(forKey: "current_expiry"))
    }
    
}

class ReportThirdItem: NSObject {
    
    var last_date = ""
    var data = [ReportThirdItemObject]()
    
    override init() { }
    init(_ dict: NSDictionary, type:CallPutType) {
        self.last_date = onlyString(dict.value(forKey: "last_date"))
        
        self.data.removeAll()
        if let data = dict.value(forKey: "data") as? NSArray {
            for item in data {
                self.data.append(ReportThirdItemObject(item as! NSDictionary, type: type))
            }
        }
    }
    
}

class ReportThirdItemObject: NSObject {
    var symbol = ""
    var stike_pr = ""
    var close = ""
    var pclose = ""
    var close_diff = ""
    var expiry_dt = ""
    var open_int = ""
    
    var call_oi = ""
    
    override init() { }
    init(_ data: NSDictionary, type:CallPutType) {
        
        self.symbol = onlyString(data.value(forKey: "symbol"))
        self.stike_pr = onlyString(data.value(forKey: "stike_pr")).getTwoDecimalString()
        self.close = onlyString(data.value(forKey: "close")).getTwoDecimalString()
        self.pclose = onlyString(data.value(forKey: "pclose")).getTwoDecimalString()
        self.close_diff = onlyString(data.value(forKey: "close_diff")).getTwoDecimalString()
        self.expiry_dt = onlyString(data.value(forKey: "expiry_dt"))
        self.open_int = onlyString(data.value(forKey: "open_int")).getTwoDecimalString()
        
        if type == .putNext || type == .putCurrent {
            self.call_oi = onlyString(data.value(forKey: "put_oi")).getTwoDecimalString()
        }else{
            self.call_oi = onlyString(data.value(forKey: "call_oi")).getTwoDecimalString()
        }
        
    }
    
}
