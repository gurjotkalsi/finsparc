//
//  DailyReportModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 14/05/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class DailyReportModel: NSObject {
    
    static var shared = DailyReportModel()
    
    var status = ""
    var message = ""
    
    var current_date_value = DailyReportObject()
    var last_month_value = DailyReportObject()
    
    func process(type: String, completion: @escaping ( _ model : DailyReportModel) -> () ) {
        Services().getService(url: ServiceURL.dailyReport+type+"/weekly/2020-04-30", parameters: [:]) { (result) in
          //  print("url",ServiceURL.dailyReport+type+"/weekly/2020-04-30)
            print(result)
            
            self.status = onlyString(result.value(forKey: "status"))
            self.message = onlyString(result.value(forKey: "message"))
            
            if let resultArray = result.value(forKey: "result") as? NSArray {
                if let response = resultArray[0] as? NSDictionary {
                    
                    if let valueArray = response.value(forKey: "current_date_value") as? NSArray {
                        self.current_date_value = DailyReportObject(valueArray[0] as! NSDictionary)
                    }
                    if let valueArray = response.value(forKey: "last_month_value") as? NSArray {
                        self.last_month_value = DailyReportObject(valueArray[0] as! NSDictionary)
                    }
                }
            }
            completion(self)
        }
    }
}

class DailyReportObject: NSObject {
    
    var cdate = ""
    var expiry_dt = ""
    var future_price = ""
    var total_sum = ""
    var change_oi_last_day = ""
    var change_oi_5_day = ""
    var change_close = ""
    
    var current_details = [DailyReportObjectItem]()
    var last_month_details = [DailyReportObjectItem]()
    
    override init() { }
    init(_ dict: NSDictionary) {
        self.cdate = onlyString(dict.value(forKey: "cdate"))
        self.expiry_dt = onlyString(dict.value(forKey: "expiry_dt"))
        self.future_price = onlyString(dict.value(forKey: "future_price"))
        self.total_sum = onlyString(dict.value(forKey: "total_sum"))
        self.change_oi_last_day = onlyString(dict.value(forKey: "change_oi_last_day"))
        self.change_oi_5_day = onlyString(dict.value(forKey: "change_oi_5_day"))
        self.change_close = onlyString(dict.value(forKey: "change_close"))
        
        self.current_details.removeAll()
        self.last_month_details.removeAll()
        
        if let valueArray = dict.value(forKey: "current_details") as? NSArray {
            for item in valueArray {
                self.current_details.append(DailyReportObjectItem(item as! NSDictionary))
            }
        }
        
        if let valueArray = dict.value(forKey: "last_month_details") as? NSArray {
            for item in valueArray {
                self.last_month_details.append(DailyReportObjectItem(item as! NSDictionary))
            }
        }
        
    }
}

class DailyReportObjectItem: NSObject {
    
    var stike_pr = ""
    var close = ""
    var trigger = ""
    var open_int = ""
    var avg_oi = ""
    var change_oi = ""
    var sw = ""
    var diff_avg_oi = ""
    
    override init() { }
    init(_ dict: NSDictionary) {
        self.stike_pr = onlyString(dict.value(forKey: "stike_pr"))
        self.close = onlyString(dict.value(forKey: "close"))
        self.trigger = onlyString(dict.value(forKey: "trigger"))
        self.open_int = onlyString(dict.value(forKey: "open_int"))
        self.avg_oi = onlyString(dict.value(forKey: "avg_oi"))
        self.change_oi = onlyString(dict.value(forKey: "change_oi"))
        self.sw = onlyString(dict.value(forKey: "sw"))
        self.diff_avg_oi = onlyString(dict.value(forKey: "diff_avg_oi"))
    }
}
