//
//  FeedbackModel.swift
//  FinSpark
//
//  Created by Minkle Garg on 06/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class FeedbackModel: NSObject {
    
    static let shared = FeedbackModel()
    
    var status = "" //Status | if 200 it is a success
    var msg = "" //Msg
    
    func sendFeedback(parameter : NSDictionary, completion: @escaping ( _ model : FeedbackModel) -> () ) {
        
        Services().postService(url: ServiceURL.feedback, parameters: parameter) { (result) in
            
            self.status = onlyString(result.value(forKey: "status"))
            self.msg = onlyString(result.value(forKey: "message"))
            
            completion(self)
        }
    }
    
    
}
