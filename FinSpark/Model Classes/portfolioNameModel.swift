//
//  portfolioNameModel.swift
//  FinSpark
//
//  Created by Minkle Garg on 11/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class portfolioNameModel: NSObject {
    
    static let shared = portfolioNameModel()
    
    var status = "" //Status | if 200 it is a success
    var msg = "" //Msg
    
    func sendName(parameter : NSDictionary, completion: @escaping ( _ model : portfolioNameModel) -> () ) {
        
        Services().postService(url: ServiceURL.portfolioname, parameters: parameter) { (result) in
            
            self.status = onlyString(result.value(forKey: "status"))
            self.msg = onlyString(result.value(forKey: "message"))
            
            completion(self)
        }
    }
    
    
}
