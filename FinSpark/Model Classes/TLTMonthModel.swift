//
//  TLTMonthModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 05/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class TLTMonthModel: NSObject {

    static let shared = TLTMonthModel()
    
    var status = ""
    var message = ""
    
    var dates = [String]()
    
    func process(completion: @escaping ( _ model : TLTMonthModel) -> () ) {
        
        Services().getService(url: ServiceURL.tltMonthData, parameters: [:]) { (result) in
            
            self.status = onlyString(result.value(forKey: "status"))
            self.message = onlyString(result.value(forKey: "message"))
            
            self.dates.removeAll()
            if let response = result.value(forKey: "date") as? NSArray {
                for item in response {
                    let value = onlyString((item as! NSDictionary).value(forKey: "date"))
//                    let inputFormatter = DateFormatter()
//                    inputFormatter.dateFormat = "yyyy-MM-dd"
//                    let showDate = inputFormatter.date(from: value)
//                    inputFormatter.dateFormat = "dd-MM-yyyy"
//                    let resultString = inputFormatter.string(from: showDate!)
//                    print(resultString)
                   // self.date = resultString
                    self.dates.append(value)
                }
            }
            
            completion(self)
        }
    }
    
}
