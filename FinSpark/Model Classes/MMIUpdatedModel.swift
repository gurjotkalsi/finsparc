//
//  MMIUpdatedModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 08/04/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class MMIUpdatedModel: NSObject {

    var id = ""
    var cdate = ""
    var prev_date = ""
    var weighted_score = ""
    var wap_stock_score = ""
    var days_5_score = ""
    var top_gainer_score = ""
    var chnage_in_nifty_score = ""
    var gain_future_score = ""
    var gain_option_score = ""
    var nifty_change_3_days_score = ""
    var weighted_avg_score = ""
    var gain_future_avg_score = ""
    var prev_weighted_avg_score = ""
    var prev_gain_future_avg_score = ""
    var prev_weighted_score = ""
    var prev_wap_stock_score = ""
    var prev_days_5_score = ""
    var prev_top_gainer_score = ""
    var prev_chnage_in_nifty_score = ""
    var prev_gain_future_score = ""
    var prev_gain_option_score = ""
    var prev_nifty_change_3_days_score = ""
    var curr_tot = ""
    var prev_tot = ""
    var curr_score = ""
    var prev_score = ""
    var curr_mmi = ""
    var prev_mmi = ""
    
    func process(completion: @escaping ( _ model : MMIUpdatedModel) -> () ) {
        
        Services().getService(url: ServiceURL.mmi_score_updated, parameters: [:]) { (result) in
            
            if let response = result.value(forKey: "result") as? NSArray {
                if let dict = response[0] as? NSDictionary {
                    self.id = onlyString(dict.value(forKey: "id"))
                    self.cdate = onlyString(dict.value(forKey: "cdate"))
                    self.prev_date = onlyString(dict.value(forKey: "prev_date"))
                    self.weighted_score = onlyString(dict.value(forKey: "weighted_score"))
                    self.wap_stock_score = onlyString(dict.value(forKey: "wap_stock_score"))
                    self.days_5_score = onlyString(dict.value(forKey: "days_5_score"))
                    self.top_gainer_score = onlyString(dict.value(forKey: "top_gainer_score"))
                    self.chnage_in_nifty_score = onlyString(dict.value(forKey: "chnage_in_nifty_score"))
                    self.gain_future_score = onlyString(dict.value(forKey: "gain_future_score"))
                    self.gain_option_score = onlyString(dict.value(forKey: "gain_option_score"))
                    self.nifty_change_3_days_score = onlyString(dict.value(forKey: "nifty_change_3_days_score"))
                    self.weighted_avg_score = onlyString(dict.value(forKey: "weighted_avg_score"))
                    self.gain_future_avg_score = onlyString(dict.value(forKey: "gain_future_avg_score"))
                    self.prev_weighted_avg_score = onlyString(dict.value(forKey: "prev_weighted_avg_score"))
                    self.prev_gain_future_avg_score = onlyString(dict.value(forKey: "prev_gain_future_avg_score"))
                    self.prev_weighted_score = onlyString(dict.value(forKey: "prev_weighted_score"))
                    self.prev_wap_stock_score = onlyString(dict.value(forKey: "prev_wap_stock_score"))
                    self.prev_days_5_score = onlyString(dict.value(forKey: "prev_days_5_score"))
                    self.prev_top_gainer_score = onlyString(dict.value(forKey: "prev_top_gainer_score"))
                    self.prev_chnage_in_nifty_score = onlyString(dict.value(forKey: "prev_chnage_in_nifty_score"))
                    self.prev_gain_future_score = onlyString(dict.value(forKey: "prev_gain_future_score"))
                    self.prev_gain_option_score = onlyString(dict.value(forKey: "prev_gain_option_score"))
                    self.prev_nifty_change_3_days_score = onlyString(dict.value(forKey: "prev_nifty_change_3_days_score"))
                    self.curr_tot = onlyString(dict.value(forKey: "curr_tot"))
                    self.prev_tot = onlyString(dict.value(forKey: "prev_tot"))
                    self.curr_score = onlyString(dict.value(forKey: "curr_score"))
                    self.prev_score = onlyString(dict.value(forKey: "prev_score"))
                    self.curr_mmi = onlyString(dict.value(forKey: "curr_mmi"))
                    self.prev_mmi = onlyString(dict.value(forKey: "prev_mmi"))
                }
            }
            
            completion(self)
            
        }
    }

    
}
