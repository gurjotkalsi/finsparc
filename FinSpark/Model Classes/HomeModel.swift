//
//  HomeModel.swift
//  
//
//  Created by Gurjot Kalsi on 20/04/20.
//

import UIKit

class HomeModel: NSObject {
    
    static var shared = HomeModel()
    
    var status = ""
    var statusmessage = ""
    
    var nifty = FuturesData()
    var bankNifty = FuturesData()
    var feargreed = FearGreedModel()
    var perlotprofit = PerLotProfit()
    var stockinfo = StockInfo()
    
    func process(completion: @escaping ( _ model : HomeModel) -> () ) {
        
        Services().getService(url: ServiceURL.viewhomepage, parameters: [:]) { (result) in
            
            if let dataArray = result.value(forKey: "result") as? NSArray {
                if let data = dataArray[0] as? NSDictionary {
                    
                    self.status = onlyString(data.value(forKey: "status"))
                    self.statusmessage = onlyString(data.value(forKey: "statusmessage"))
                    
                    self.nifty = FuturesData(data.value(forKey: "niftyfutures") as! NSDictionary)
                    self.bankNifty = FuturesData(data.value(forKey: "bankniftyfutures") as! NSDictionary)
                    
                    self.feargreed = FearGreedModel(data.value(forKey: "feargreedindex") as! NSDictionary)
                    self.perlotprofit = PerLotProfit(data.value(forKey: "perlotprofit") as! NSDictionary)
                    self.stockinfo = StockInfo(data.value(forKey: "stock_info") as! NSDictionary)
                    
                }
            }
            completion(self)
        }
    }
}

class FuturesData : NSObject {
    var cdate = ""
    var bn_close = ""
    var nifty_close = ""
    var nifty_change = ""
    var banknifty_change = ""
    var nifty_change_percent = ""
    var banknifty_change_percent = ""
    var graph = [GraphObject]()
    
    override init() {}
    
    init(_ dict:NSDictionary) {
        self.cdate = onlyString(dict.value(forKey: "cdate"))
        self.bn_close = onlyString(dict.value(forKey: "bn_close"))
        self.nifty_close = onlyString(dict.value(forKey: "nifty_close"))
        self.nifty_change = onlyString(dict.value(forKey: "nifty_change"))
        self.banknifty_change = onlyString(dict.value(forKey: "banknifty_change"))
        self.nifty_change_percent = onlyString(dict.value(forKey: "nifty_change_percent"))
        self.banknifty_change_percent = onlyString(dict.value(forKey: "banknifty_change_percent"))
        
        if let gdata = dict.value(forKey: "graph") as? NSArray {
            for item in gdata {
                self.graph.append(GraphObject(item as! NSDictionary))
            }
        }
    }
}

class GraphObject: NSObject {
    var date = ""
    var close = ""
    
    override init() {}
    
    init(_ dict:NSDictionary) {
        let inputFormatter = DateFormatter()
               inputFormatter.dateFormat = "yyyy-MM-dd"
               let showDate = inputFormatter.date(from: onlyString(dict.value(forKey: "date")))
               inputFormatter.dateFormat = "dd-MM-yyyy"
               let resultString = inputFormatter.string(from: showDate!)
               print(resultString)
        self.date = resultString
        self.close = onlyString(dict.value(forKey: "close"))
    }
}

class FearGreedModel : NSObject {
    
    var cdate = ""
    var curr_mmi = ""
    var curr_score = ""
    
    override init() {}
    
    init(_ dict:NSDictionary) {
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd"
        let showDate = inputFormatter.date(from: onlyString(dict.value(forKey: "cdate")))
        inputFormatter.dateFormat = "dd-MM-yyyy"
        let resultString = inputFormatter.string(from: showDate!)
        print(resultString)
        self.cdate = resultString
        self.curr_mmi = onlyString(dict.value(forKey: "curr_mmi"))
        self.curr_score = onlyString(dict.value(forKey: "curr_score"))
    }
}


class PerLotProfit : NSObject {
    var date = ""
    var plp_5 = ""
    var plp_per = ""
    var expiry_dt = ""
    var market_now = ""
    var nifty_change = ""
    var current_nifty = ""
    var banknifty_change = ""
    var current_banknifty = ""
    var nifty_change_percent = ""
    var banknifty_change_percent = ""
    
    override init() {}
    
    init(_ dict:NSDictionary) {
//        let inputFormatter = DateFormatter()
//        inputFormatter.dateFormat = "yyyy-MM-dd"
//        let showDate = inputFormatter.date(from: onlyString(dict.value(forKey: "date")))
//        inputFormatter.dateFormat = "dd/MM-yyyy"
//        let resultString = inputFormatter.string(from: showDate!)
//        print(resultString)
        self.date = onlyString(dict.value(forKey: "date"))
        self.plp_5 = onlyString(dict.value(forKey: "plp_5"))
        self.plp_per = onlyString(dict.value(forKey: "plp_per"))
        self.expiry_dt = onlyString(dict.value(forKey: "expiry_dt"))
        self.market_now = onlyString(dict.value(forKey: "market_now"))
        self.nifty_change = onlyString(dict.value(forKey: "nifty_change"))
        self.current_nifty = onlyString(dict.value(forKey: "current_nifty"))
        self.banknifty_change = onlyString(dict.value(forKey: "banknifty_change"))
        self.current_banknifty = onlyString(dict.value(forKey: "current_banknifty"))
        self.nifty_change_percent = onlyString(dict.value(forKey: "nifty_change_percent"))
        self.banknifty_change_percent = onlyString(dict.value(forKey: "banknifty_change_percent"))
    }
}

class StockInfo : NSObject {
    var declined = ""
    var adavanced = ""
    var unchanged = ""
    
    override init() {}
    
    init(_ dict:NSDictionary) {
        self.declined = onlyString(dict.value(forKey: "declined"))
        self.adavanced = onlyString(dict.value(forKey: "adavanced"))
        self.unchanged = onlyString(dict.value(forKey: "unchanged"))
    }
}

struct ResultResponse : Codable {
    var result : [DictResult]
    
    struct DictResult : Codable {
        var status : Int
        var statusmessage : String
    }
}
