//
//  ScoreCardModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 29/04/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

enum ScoreCardTypes {
    case all
    case industry
    case nifty
}

class ScoreCardModel: NSObject {

    static var shared = ScoreCardModel()
    
    var status = ""
    var message = ""
    
    var cdate = ""
    var current_expiry = ""
    var next_expiry = ""
    
    var list = [ScoreCardItem]() //current_expiry_score_card
    var next_list = [ScoreCardItem]() //next_expiry_score_card
    
    
    
    func process(type: ScoreCardTypes, completion: @escaping ( _ model : ScoreCardModel) -> () ) {
        
        var url = ServiceURL.scoreCardAll
        if type == .all {
            url = ServiceURL.scoreCardAll
        } else if type == .industry {
            url = ServiceURL.scoreCardIndustry
        } else {
            //nifty
            url = ServiceURL.scoreCardNifty
        }
        
        Services().getService(url: url, parameters: [:]) { (result) in
        
            print(result)
            
            self.status = onlyString(result.value(forKey: "status"))
            self.message = onlyString(result.value(forKey: "message"))
            
            if let resultArray = result.value(forKey: "result") as? NSArray {
                if let resultArrayValue = resultArray[0] as? NSDictionary {
                    
                    self.cdate = onlyString(resultArrayValue.value(forKey: "cdate"))
                    self.current_expiry = onlyString(resultArrayValue.value(forKey: "current_expiry"))
                    self.next_expiry = onlyString(resultArrayValue.value(forKey: "next_expiry"))
                    
                    if let valueArray = resultArrayValue.value(forKey: "current_expiry_score_card") as? NSArray {
                        self.list.removeAll()
                        for item in valueArray {
                            self.list.append(ScoreCardItem(item as! NSDictionary))
                        }
                    }
                    
                    if let nextValueArray = resultArrayValue.value(forKey: "next_expiry_score_card") as? NSArray {
                        self.next_list.removeAll()
                        for nitem in nextValueArray {
                            self.next_list.append(ScoreCardItem(nitem as! NSDictionary))
                        }
                    }
                    
                }
            }
            
            completion(self)
            
        }
    }
    
    
    func industryprocess(industryid: String, completion: @escaping ( _ model : ScoreCardModel) -> () ) {
        
       
        
        Services().getService(url: ServiceURL.scoreCardSelectedIndustry + industryid, parameters: [:]) { (result) in
        
            print(result)
            
            self.status = onlyString(result.value(forKey: "status"))
            self.message = onlyString(result.value(forKey: "message"))
            
            if let resultArray = result.value(forKey: "result") as? NSArray {
                if let resultArrayValue = resultArray[0] as? NSDictionary {
                    
                    self.cdate = onlyString(resultArrayValue.value(forKey: "cdate"))
                    self.current_expiry = onlyString(resultArrayValue.value(forKey: "current_expiry"))
                    self.next_expiry = onlyString(resultArrayValue.value(forKey: "next_expiry"))
                    
                    if let valueArray = resultArrayValue.value(forKey: "current_expiry_score_card") as? NSArray {
                        self.list.removeAll()
                        for item in valueArray {
                            self.list.append(ScoreCardItem(item as! NSDictionary))
                        }
                    }
                    
                    if let nextValueArray = resultArrayValue.value(forKey: "next_expiry_score_card") as? NSArray {
                        self.next_list.removeAll()
                        for nitem in nextValueArray {
                            self.next_list.append(ScoreCardItem(nitem as! NSDictionary))
                        }
                    }
                    
                }
            }
            
            completion(self)
            
        }
    }
    
}

class ScoreCardItem: NSObject {
    var symbol = ""
    var score = ""
    var rank = ""
    var price_close = ""
    var percent_price_change = ""
    var change_expiry_percent = ""
    var seven_days_trends = ""
    
    override init() { }
    init(_ dict: NSDictionary) {
        self.symbol = onlyString(dict.value(forKey: "symbol"))
        self.score = onlyString(dict.value(forKey: "score"))
        self.rank = onlyString(dict.value(forKey: "rank"))
        self.price_close = onlyString(dict.value(forKey: "price_close"))
        self.percent_price_change = onlyString(dict.value(forKey: "percent_price_change"))
        self.change_expiry_percent = onlyString(dict.value(forKey: "change_expiry_percent"))
        self.seven_days_trends = onlyString(dict.value(forKey: "seven_days_trends"))
    }
    
}
