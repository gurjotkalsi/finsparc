//
//  ForgotModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 27/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class ForgotModel: NSObject {

    static let shared = ForgotModel()
    
    var status = "" //Status | if 200 it is a success
    var msg = "" //Msg
    
    func getOTP(parameter : NSDictionary, completion: @escaping ( _ model : ForgotModel) -> () ) {
        
        Services().postService(url: ServiceURL.forgot_password, parameters: parameter) { (result) in
            
            print(result)
            self.status = onlyString(result.value(forKey: "Status"))
            self.msg = onlyString(result.value(forKey: "Msg"))
            
            completion(self)
        }
    }
    
}

class ForgotVerifyModel: NSObject {
    
    static let shared = ForgotVerifyModel()
    
    var status = "" //Status | if 200 it is a success
    var msg = "" //Msg
    
    func verifyOTP(parameter : NSDictionary, completion: @escaping ( _ model : ForgotVerifyModel) -> () ) {
        
        Services().postService(url: ServiceURL.verify_forget_password, parameters: parameter) { (result) in
            
            self.status = onlyString(result.value(forKey: "Status"))
            self.msg = onlyString(result.value(forKey: "Msg"))
            
            completion(self)
        }
    }
    
    
}
