//
//  PortfolioDelete.swift
//  FinSpark
//
//  Created by Minkle Garg on 15/07/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class PortfolioDelete: NSObject {
    
    static let shared = PortfolioDelete()
    
    var status = "" //Status | if 200 it is a success
    var msg = "" //Msg
    
    func delete(parameter : NSDictionary, completion: @escaping ( _ model : PortfolioDelete) -> () ) {
        
        Services().postService(url: ServiceURL.deleteportfolio, parameters: parameter) { (result) in
            
            self.status = onlyString(result.value(forKey: "status"))
            self.msg = onlyString(result.value(forKey: "message"))
            
            completion(self)
        }
    }
    
    
}
