//
//  TransactionModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 05/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class TransactionModel: NSObject {
    
    static let shared = TransactionModel()
    
    var status = ""
    var message = ""
    
    var user_id = ""
    var portfolio_id = ""
    var stock_id = ""
    var mtm_price = ""
    var portfolio_name = ""
    var last_close = ""
    var booked = ""
    
    var transactions = [TransactionObject]()
    
    func process(portfolioid:String, stockid: String, stock:String, completion: @escaping ( _ model : TransactionModel) -> () ) {
        Services().getService(url: "\(ServiceURL.transaction)\(portfolioid)\("/")\(LoginModel().localData().finsparc_user_id)\("/")\(stockid)\("/")\(stock)\("/all")", parameters: [:]) { (result) in
            
            self.status = onlyString(result.value(forKey: "status"))
            self.message = onlyString(result.value(forKey: "message"))
            
            if let response = result.value(forKey: "result") as? NSArray {
                if response.count > 0
                {
                if let dict = response[0] as? NSDictionary {
                    
                    self.user_id = onlyString(result.value(forKey: "user_id"))
                    self.portfolio_id = onlyString(result.value(forKey: "portfolio_id"))
                    self.stock_id = onlyString(result.value(forKey: "stock_id"))
                    self.mtm_price = onlyString(result.value(forKey: "mtm_price"))
                    self.portfolio_name = onlyString(result.value(forKey: "portfolio_name"))
                    self.last_close = onlyString(result.value(forKey: "last_close"))
                    self.booked = onlyString(result.value(forKey: "booked"))
                    
                    self.transactions.removeAll()
                    if let arra = dict.value(forKey: "transaction") as? NSArray {
                        for item in arra {
                            self.transactions.append(TransactionObject(item as! NSDictionary))
                        }
                    }
                  }
                }
            }
            
            completion(self)
        }
    }
    
    
}

class TransactionObject: NSObject {
    
    var finsparc_transaction_id = ""
    var bhavcopy_id = ""
    var portfolio_id = ""
    var date = ""
    var stock_name = ""
    var type = ""
    var active_status = ""
    var created_on = ""
    var selleing_date = ""
    var selling_price = ""
    var price = ""
    var stock_id = ""
    var quantity = ""
    var last_close = ""
    var selling_quantity = ""
    var user_id = ""
    var complete_status = ""
    var remaining_qty = ""
    var remaining_sell_qty = ""
    var booked = ""
    var mtm = ""
    var pruchase_date = ""
    var filter_price = ""
    var open_buy_price = ""
    var option_type = ""
    var expiry_dt = ""
    var strike_pr = ""
    var stock = ""
    var portfolio_name = ""
    var instrument_type = ""
    
    override init() {}
    init(_ dict: NSDictionary) {
        self.finsparc_transaction_id = onlyString(dict.value(forKey: "finsparc_transaction_id"))
        self.bhavcopy_id = onlyString(dict.value(forKey: "bhavcopy_id"))
        self.portfolio_id = onlyString(dict.value(forKey: "portfolio_id"))
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd"
        let showDate = inputFormatter.date(from: onlyString(dict.value(forKey: "date")))
        inputFormatter.dateFormat = "dd-MM-yyyy"
        let resultString = inputFormatter.string(from: showDate!)
        print(resultString)
        self.date = resultString
      //  self.date = onlyString(dict.value(forKey: "date"))
        
        self.stock_name = onlyString(dict.value(forKey: "stock_name"))
        self.type = onlyString(dict.value(forKey: "type"))
        self.active_status = onlyString(dict.value(forKey: "active_status"))
        self.created_on = onlyString(dict.value(forKey: "created_on"))
        self.selleing_date = onlyString(dict.value(forKey: "selleing_date"))
        self.selling_price = onlyString(dict.value(forKey: "selling_price"))
        self.price = onlyString(dict.value(forKey: "price"))
        self.stock_id = onlyString(dict.value(forKey: "stock_id"))
        self.quantity = onlyString(dict.value(forKey: "quantity"))
        self.last_close = onlyString(dict.value(forKey: "last_close"))
        self.selling_quantity = onlyString(dict.value(forKey: "selling_quantity"))
        self.user_id = onlyString(dict.value(forKey: "user_id"))
        self.complete_status = onlyString(dict.value(forKey: "complete_status"))
        self.remaining_qty = onlyString(dict.value(forKey: "remaining_qty"))
        self.remaining_sell_qty = onlyString(dict.value(forKey: "remaining_sell_qty"))
        self.booked = onlyString(dict.value(forKey: "booked"))
        self.mtm = onlyString(dict.value(forKey: "mtm"))
        self.pruchase_date = onlyString(dict.value(forKey: "pruchase_date"))
        self.filter_price = onlyString(dict.value(forKey: "filter_price"))
        self.open_buy_price = onlyString(dict.value(forKey: "open_buy_price"))
        self.option_type = onlyString(dict.value(forKey: "option_type"))
        self.expiry_dt = onlyString(dict.value(forKey: "expiry_dt"))
        self.strike_pr = onlyString(dict.value(forKey: "strike_pr"))
        self.stock = onlyString(dict.value(forKey: "stock"))
        self.portfolio_name = onlyString(dict.value(forKey: "portfolio_name"))
        self.instrument_type = onlyString(dict.value(forKey: "instrument_type"))
    }
}
