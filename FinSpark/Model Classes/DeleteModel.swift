//
//  DeleteModel.swift
//  FinSpark
//
//  Created by Minkle Garg on 08/07/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class DeleteModel: NSObject {
    
    static let shared = DeleteModel()
    
    var status = "" //Status | if 200 it is a success
    var msg = "" //Msg
    
    func deleteTrade(id : String,stockid : String,optiontype : String, completion: @escaping ( _ model : DeleteModel) -> () ) {
        Services().getService(url: "http://3.6.6.59:7005/deleteFinsparcStock/\(id)\("/")\(stockid)\("/")\(optiontype)", parameters: [:]) { (result) in

            self.status = onlyString(result.value(forKey: "Status"))
            self.msg = onlyString(result.value(forKey: "message"))
            
            completion(self)
        }
    }
    
    
}
