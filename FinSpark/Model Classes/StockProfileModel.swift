//
//  StockProfileModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 09/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class StockProfileModel: NSObject {

    static let shared = StockProfileModel()
    
    var status = ""
    var message = ""
    
    var symbol_par = ""
    var rank = ""
    var stock_count = ""
    var total = ""
    var score_difference = ""
    var trigger_price = ""
    var con_color = ""
    var future_closing = ""
    var price_dufference = ""
    var percent_change_since_last_expiry = ""
    var wap = ""
    var volume = ""
    var volume_difference = ""
    var oi_high_11 = ""
    var oi_low_11 = ""
    var current_oi_sum = ""
    var change_oi = ""
    var change_oi_percent = ""
    var change_oi_5_days = ""
    var percent_change_oi_5_days = ""
    var itm_above_strike_price = ""
    var itm_below_strike_price = ""
    var call_change_oi = ""
    var call_open_int = ""
    var call_contracts = ""
    var put_change_oi = ""
    var put_open_int = ""
    var put_contracts = ""
    var profile_call_trigger = ""
    var profile_put_trigger = ""
    var itm_call_strike = ""
    var itm_put_strike = ""
    var call_itm_open_int = ""
    var put_itm_open_int = ""
    var itm_call_contracts = ""
    var itm_put_contracts = ""
    var itm_call_change_oi = ""
    var itm_put_change_oi = ""
    
    var lot = ""
    var itm_call = ""
    var itm_put = ""
    var itm_close_call = ""
    var itm_close_put = ""
    var put_below_close = ""
    var call_above_close = ""
    var review_itm_put = ""
    var review_itm_call = ""
    var itm_above_call = ""
    var itm_below_call = ""
    var itm_call_daysavg = ""
    var itm_put_daysavg = ""
    var call_above_daysavg = ""
    var put_below_daysavg = ""
    var call_strength = ""
    var put_strength = ""
    var cdate = ""
    var expiry_dt = ""
    var annualise_volatility = ""
    var data_5 = ""
    var data_30 = ""
    var high = ""
    var low = ""
    var dma_40 = ""
    
    var reviews = [ReviewObject]()
    var call_json = [JSONItem]()
    var put_json = [JSONItem]()
    var days_5_data = [DayFiveItem]()
    
    func process(company:String, completion: @escaping ( _ model : StockProfileModel) -> () ) {
        
//        let parameter = "WIPRO" //else use company
//        (url: "\(ServiceURL.portfolio)\(LoginModel().localData().finsparc_user_id)\("/")\(type.rawValue)", parameters: [:])
        Services().getService(url: "\(ServiceURL.stockprofile)\(company)", parameters: [:]) { (result) in
            self.status = onlyString(result.value(forKey: "status"))
            self.message = onlyString(result.value(forKey: "message"))
            
            let resultarray = result.value(forKey: "result") as? NSArray
            if resultarray!.count > 0
            {
            if let response = result.value(forKey: "result") as? NSArray {
                print(response[0])
                
                if let dict = response[0] as? NSDictionary {
                    self.symbol_par = onlyString(dict.value(forKey: "symbol_par"))
                    self.rank = onlyString(dict.value(forKey: "rank"))
                    self.stock_count = onlyString(dict.value(forKey: "stock_count"))
                    self.total = onlyString(dict.value(forKey: "total"))
                    self.score_difference = onlyString(dict.value(forKey: "score_difference"))
                    self.trigger_price = onlyString(dict.value(forKey: "trigger_price")).getTwoDecimalString()
                    self.con_color = onlyString(dict.value(forKey: "con_color"))
                    self.future_closing = onlyString(dict.value(forKey: "future_closing"))
                    self.price_dufference = onlyString(dict.value(forKey: "price_dufference"))
                    self.percent_change_since_last_expiry = onlyString(dict.value(forKey: "percent_change_since_last_expiry")).getTwoDecimalString()
                    self.wap = onlyString(dict.value(forKey: "wap")).getTwoDecimalString()
                    self.volume = onlyString(dict.value(forKey: "volume"))
                    self.volume_difference = onlyString(dict.value(forKey: "volume_difference")).getTwoDecimalString()
                    self.oi_high_11 = onlyString(dict.value(forKey: "oi_high_11")).getTwoDecimalString()
                    self.oi_low_11 = onlyString(dict.value(forKey: "oi_low_11")).getTwoDecimalString()
                    self.current_oi_sum = onlyString(dict.value(forKey: "current_oi_sum")).getTwoDecimalString()
                    self.change_oi = onlyString(dict.value(forKey: "change_oi")).getTwoDecimalString()
                    self.change_oi_percent = onlyString(dict.value(forKey: "change_oi_percent")).getTwoDecimalString()
                    self.change_oi_5_days = onlyString(dict.value(forKey: "change_oi_5_days")).getTwoDecimalString()
                    self.percent_change_oi_5_days = onlyString(dict.value(forKey: "percent_change_oi_5_days")).getTwoDecimalString()
                    self.itm_above_strike_price = onlyString(dict.value(forKey: "itm_above_strike_price"))
                    self.itm_below_strike_price = onlyString(dict.value(forKey: "itm_below_strike_price"))
                    self.call_change_oi = onlyString(dict.value(forKey: "call_change_oi"))
                    self.call_open_int = onlyString(dict.value(forKey: "call_open_int"))
                    self.call_contracts = onlyString(dict.value(forKey: "call_contracts"))
                    self.put_change_oi = onlyString(dict.value(forKey: "put_change_oi"))
                    self.put_open_int = onlyString(dict.value(forKey: "put_open_int"))
                    self.put_contracts = onlyString(dict.value(forKey: "put_contracts"))
                    self.profile_call_trigger = onlyString(dict.value(forKey: "profile_call_trigger"))
                    self.profile_put_trigger = onlyString(dict.value(forKey: "profile_put_trigger"))
                    self.itm_call_strike = onlyString(dict.value(forKey: "itm_call_strike"))
                    self.itm_put_strike = onlyString(dict.value(forKey: "itm_put_strike"))
                    self.call_itm_open_int = onlyString(dict.value(forKey: "call_itm_open_int"))
                    self.put_itm_open_int = onlyString(dict.value(forKey: "put_itm_open_int"))
                    self.itm_call_contracts = onlyString(dict.value(forKey: "itm_call_contracts"))
                    self.itm_put_contracts = onlyString(dict.value(forKey: "itm_put_contracts"))
                    self.itm_call_change_oi = onlyString(dict.value(forKey: "itm_call_change_oi"))
                    self.itm_put_change_oi = onlyString(dict.value(forKey: "itm_put_change_oi"))
                    
                    self.lot = onlyString(dict.value(forKey: "lot"))
                    self.itm_call = onlyString(dict.value(forKey: "itm_call"))
                    self.itm_put = onlyString(dict.value(forKey: "itm_put"))
                    self.itm_close_call = onlyString(dict.value(forKey: "itm_close_call"))
                    self.itm_close_put = onlyString(dict.value(forKey: "itm_close_put"))
                    self.put_below_close = onlyString(dict.value(forKey: "put_below_close"))
                    self.call_above_close = onlyString(dict.value(forKey: "call_above_close"))
                    self.review_itm_put = onlyString(dict.value(forKey: "review_itm_put"))
                    self.review_itm_call = onlyString(dict.value(forKey: "review_itm_call"))
                    self.itm_above_call = onlyString(dict.value(forKey: "itm_above_call"))
                    self.itm_below_call = onlyString(dict.value(forKey: "itm_below_call"))
                    self.itm_call_daysavg = onlyString(dict.value(forKey: "itm_call_daysavg"))
                    self.itm_put_daysavg = onlyString(dict.value(forKey: "itm_put_daysavg"))
                    self.call_above_daysavg = onlyString(dict.value(forKey: "call_above_daysavg"))
                    self.put_below_daysavg = onlyString(dict.value(forKey: "put_below_daysavg"))
                    self.call_strength = onlyString(dict.value(forKey: "call_strength")).getTwoDecimalString()
                    self.put_strength = onlyString(dict.value(forKey: "put_strength")).getTwoDecimalString()
                    self.cdate = onlyString(dict.value(forKey: "cdate"))
                    self.expiry_dt = onlyString(dict.value(forKey: "expiry_dt"))
                    self.annualise_volatility = onlyString(dict.value(forKey: "annualise_volatility")).getTwoDecimalString()
                    self.data_5 = onlyString(dict.value(forKey: "data_5")).getTwoDecimalString()
                    self.data_30 = onlyString(dict.value(forKey: "data_30")).getTwoDecimalString()
                    self.high = onlyString(dict.value(forKey: "high")).getTwoDecimalString()
                    self.low = onlyString(dict.value(forKey: "low")).getTwoDecimalString()
                    self.dma_40 = onlyString(dict.value(forKey: "dma_40")).getTwoDecimalString()
                    
                    self.reviews.removeAll()
                    if let arra = dict.value(forKey: "review") as? NSArray {
                        for item in arra {
                            self.reviews.append(ReviewObject(item as! NSDictionary))
                        }
                    }
                    
                    self.call_json.removeAll()
                    if let arra = dict.value(forKey: "call_json") as? NSArray {
                        for item in arra {
                            self.call_json.append(JSONItem(item as! NSDictionary))
                        }
                    }
                    
                    self.put_json.removeAll()
                    if let arra = dict.value(forKey: "put_json") as? NSArray {
                        for item in arra {
                            self.put_json.append(JSONItem(item as! NSDictionary))
                        }
                    }
                    
                    self.days_5_data.removeAll()
                    if let arra = dict.value(forKey: "days_5_data") as? NSArray {
                        for item in arra {
                            self.days_5_data.append(DayFiveItem(item as! NSDictionary))
                        }
                    }
                    
                }
            }
        }
            completion(self)
        }
    }
    
}

class ReviewObject: NSObject {
    
    var consecutive_days = 0
    var con_color = ""
    var future_closing = 0.0
    var review_sell_trigger = 0.0
    var review_buy_trigger = 0.0
    var total_score = ""
    var review_itm_call = 0
    var review_itm_put = 0
    var oi_percent = ""
    var expiry = 0
    var percent_10 = 0.0
    var itm_call_strike = ""
    var itm_put_strike = ""
    var contract_ratio_put = ""
    var contract_ratio_call = ""
    var wap_call = 0
    var wap_put = 0
    var rank = ""
    var stock_count = ""
    var max_contract_ce = ""
    var min_contract_pe = ""
    var ce_contract = 0
    var pe_contract = 0

    var prices = [ReviewItem]() //review
    
    override init() { }
    init(_ dict: NSDictionary) {
        self.consecutive_days = dict.value(forKey: "consecutive_days") as! Int
        self.con_color = onlyString(dict.value(forKey: "con_color"))
        self.future_closing = dict.value(forKey: "future_closing") as! Double
        self.review_sell_trigger = dict.value(forKey: "review_sell_trigger") as! Double
        self.review_buy_trigger = dict.value(forKey: "review_buy_trigger") as! Double
        self.total_score = onlyString(dict.value(forKey: "total_score"))
        self.review_itm_call = dict.value(forKey: "review_itm_call") as! Int
        self.review_itm_put = dict.value(forKey: "review_itm_put") as! Int
        self.oi_percent = onlyString(dict.value(forKey: "oi_percent"))
        self.expiry = dict.value(forKey: "expiry")  as! Int
        self.percent_10 = dict.value(forKey: "percent_10") as! Double
        self.itm_call_strike = onlyString(dict.value(forKey: "itm_call_strike"))
        self.itm_put_strike = onlyString(dict.value(forKey: "itm_put_strike"))
        self.contract_ratio_put = onlyString(dict.value(forKey: "contract_ratio_put"))
        self.contract_ratio_call = onlyString(dict.value(forKey: "contract_ratio_call"))
        self.wap_call = dict.value(forKey: "wap_call") as! Int
        self.wap_put = dict.value(forKey: "wap_put") as! Int
        self.rank = onlyString(dict.value(forKey: "rank"))
        self.stock_count = onlyString(dict.value(forKey: "stock_count"))
        self.max_contract_ce = onlyString(dict.value(forKey: "max_contract_ce"))
        self.min_contract_pe = onlyString(dict.value(forKey: "min_contract_pe"))
        self.ce_contract = dict.value(forKey: "ce_contract") as! Int
        self.pe_contract = dict.value(forKey: "pe_contract") as! Int
        
        self.prices.removeAll()
        if let arra = dict.value(forKey: "review") as? NSArray {
            for item in arra {
                self.prices.append(ReviewItem(item as! NSDictionary))
            }
        }
        
    }
    
}

class ReviewItem: NSObject {
    var price_high = ""
    var price_low = ""
    
    override init() { }
    init(_ dict: NSDictionary) {
        self.price_high = onlyString(dict.value(forKey: "price_high"))
        self.price_low = onlyString(dict.value(forKey: "price_low"))
    }
}

class JSONItem: NSObject {
    
    var close = ""
    var open_int = ""
    var stike_pr = ""
    var round = ""
    var change_poi = ""
    
    var cum_premium_cumlots = ""
    
    override init() { }
    init(_ dict: NSDictionary) {
        self.close = onlyString(dict.value(forKey: "close"))
        self.open_int = onlyString(dict.value(forKey: "open_int"))
        self.stike_pr = onlyString(dict.value(forKey: "stike_pr")).getTwoDecimalString()
        self.round = onlyString(dict.value(forKey: "round"))
        self.change_poi = onlyString(dict.value(forKey: "change_poi"))
        self.cum_premium_cumlots = onlyString(dict.value(forKey: "cum_premium_cumlots")).getTwoDecimalString()
    }
}

class DayFiveItem: NSObject {
    var close = ""
    var open_int = ""
    var chg_in_oi = ""
    var volume = ""
    var timestamp_finsparc_bhavcopy = ""
    
    override init() { }
    init(_ dict: NSDictionary) {
        self.close = onlyString(dict.value(forKey: "close"))
        self.open_int = onlyString(dict.value(forKey: "open_int"))
        self.chg_in_oi = onlyString(dict.value(forKey: "chg_in_oi"))
        self.volume = onlyString(dict.value(forKey: "volume"))
        self.timestamp_finsparc_bhavcopy = onlyString(dict.value(forKey: "timestamp_finsparc_bhavcopy"))
    }
}
