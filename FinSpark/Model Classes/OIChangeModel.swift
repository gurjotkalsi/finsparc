//
//  OIChangeModel.swift
//  
//
//  Created by Gurjot Kalsi on 25/04/20.
//

import UIKit

class OIChangeModel: NSObject {
        
    static var shared = OIChangeModel()
    
    var status = ""
    var message = ""
    
    var current_oi_change = [OIItem]()
    var current_significant_chg_oi  = [OISignificantChangeItem]()
    
    var current_add_oi_last_day = [OIItem]()
    var current_add_oi_last_7_day = [OIItem]()
    var current_add_oi_last_15_day = [OIItem]()
    var current_add_oi_last_month = [OIItem]()
    
    var current_shed_oi_last_day = [OIItem]()
    var current_shed_oi_last_7_day = [OIItem]()
    var current_shed_oi_last_15_day = [OIItem]()
    var current_shed_oi_last_month = [OIItem]()

    var next_oi_change = [OIItem]()
    var next_significant_chg_oi  = [OISignificantChangeItem]()
    
    var next_add_oi_last_day = [OIItem]()
    var next_add_oi_last_7_day = [OIItem]()
    var next_add_oi_last_15_day = [OIItem]()
    var next_add_oi_last_month = [OIItem]()
    
    var next_shed_oi_last_day = [OIItem]()
    var next_shed_oi_last_7_day = [OIItem]()
    var next_shed_oi_last_15_day = [OIItem]()
    var next_shed_oi_last_month = [OIItem]()
    
    var cdate = ""
    var expiry = ""

    func process(completion: @escaping ( _ model : OIChangeModel) -> () ) {
        
        Services().getService(url: ServiceURL.oichangereport, parameters: [:]) { (result) in
            
            self.status = onlyString(result.value(forKey: "status"))
            self.message = onlyString(result.value(forKey: "message"))
            
            self.current_oi_change.removeAll()
            self.current_significant_chg_oi.removeAll()
            self.current_add_oi_last_day.removeAll()
            self.current_add_oi_last_7_day.removeAll()
            self.current_add_oi_last_15_day.removeAll()
            self.current_add_oi_last_month.removeAll()
            self.current_shed_oi_last_day.removeAll()
            self.current_shed_oi_last_7_day.removeAll()
            self.current_shed_oi_last_15_day.removeAll()
            self.current_shed_oi_last_month.removeAll()
            
            self.next_oi_change.removeAll()
            self.next_significant_chg_oi.removeAll()
            self.next_add_oi_last_day.removeAll()
            self.next_add_oi_last_7_day.removeAll()
            self.next_add_oi_last_15_day.removeAll()
            self.next_add_oi_last_month.removeAll()
            self.next_shed_oi_last_day.removeAll()
            self.next_shed_oi_last_7_day.removeAll()
            self.next_shed_oi_last_15_day.removeAll()
            self.next_shed_oi_last_month.removeAll()
            
            if let resultArray = result.value(forKey: "result") as? NSArray {
                if let resultDictionary = resultArray[0] as? NSDictionary {
                    self.cdate = onlyString(resultDictionary.value(forKey: "cdate"))
                    self.expiry = onlyString(resultDictionary.value(forKey: "current_expiry"))

                    //Map data here to class OIItem
                    if let valueArray = resultDictionary.value(forKey: "current_oi_change") as? NSArray {
                        for item in valueArray {
                            self.current_oi_change.append(OIItem(item as! NSDictionary))
                        }
                    }
                    
                    if let valueArray = resultDictionary.value(forKey: "current_significant_chg_oi") as? NSArray {
                        for item in valueArray {
                            self.current_significant_chg_oi.append(OISignificantChangeItem(item as! NSDictionary))
                        }
                    }
                    
                    if let valueArray = resultDictionary.value(forKey: "current_add_oi_last_day") as? NSArray {
                        for item in valueArray {
                            self.current_add_oi_last_day.append(OIItem(item as! NSDictionary))
                        }
                    }
                    
                    if let valueArray = resultDictionary.value(forKey: "current_add_oi_last_7_day") as? NSArray {
                        for item in valueArray {
                            self.current_add_oi_last_7_day.append(OIItem(item as! NSDictionary))
                        }
                    }
                    
                    if let valueArray = resultDictionary.value(forKey: "current_add_oi_last_15_day") as? NSArray {
                        for item in valueArray {
                            self.current_add_oi_last_15_day.append(OIItem(item as! NSDictionary))
                        }
                    }
                    
                    if let valueArray = resultDictionary.value(forKey: "current_add_oi_last_month") as? NSArray {
                        for item in valueArray {
                            self.current_add_oi_last_month.append(OIItem(item as! NSDictionary))
                        }
                    }
                    
                    if let valueArray = resultDictionary.value(forKey: "current_shed_oi_last_day") as? NSArray {
                        for item in valueArray {
                            self.current_shed_oi_last_day.append(OIItem(item as! NSDictionary))
                        }
                    }
                    
                    if let valueArray = resultDictionary.value(forKey: "current_shed_oi_last_7_day") as? NSArray {
                        for item in valueArray {
                            self.current_shed_oi_last_7_day.append(OIItem(item as! NSDictionary))
                        }
                    }
                    
                    if let valueArray = resultDictionary.value(forKey: "current_shed_oi_last_15_day") as? NSArray {
                        for item in valueArray {
                            self.current_shed_oi_last_15_day.append(OIItem(item as! NSDictionary))
                        }
                    }
                    
                    if let valueArray = resultDictionary.value(forKey: "current_shed_oi_last_month") as? NSArray {
                        for item in valueArray {
                            self.current_shed_oi_last_month.append(OIItem(item as! NSDictionary))
                        }
                    }
                    
                    
                    //NEXT
                    if let valueArray = resultDictionary.value(forKey: "next_oi_change") as? NSArray {
                        for item in valueArray {
                            self.next_oi_change.append(OIItem(item as! NSDictionary))
                        }
                    }
                    
                    if let valueArray = resultDictionary.value(forKey: "next_significant_chg_oi") as? NSArray {
                        for item in valueArray {
                            self.next_significant_chg_oi.append(OISignificantChangeItem(item as! NSDictionary))
                        }
                    }
                    
                    if let valueArray = resultDictionary.value(forKey: "next_add_oi_last_day") as? NSArray {
                        for item in valueArray {
                            self.next_add_oi_last_day.append(OIItem(item as! NSDictionary))
                        }
                    }
                    
                    if let valueArray = resultDictionary.value(forKey: "next_add_oi_last_7_day") as? NSArray {
                        for item in valueArray {
                            self.next_add_oi_last_7_day.append(OIItem(item as! NSDictionary))
                        }
                    }
                    
                    if let valueArray = resultDictionary.value(forKey: "next_add_oi_last_15_day") as? NSArray {
                        for item in valueArray {
                            self.next_add_oi_last_15_day.append(OIItem(item as! NSDictionary))
                        }
                    }
                    
                    if let valueArray = resultDictionary.value(forKey: "next_add_oi_last_month") as? NSArray {
                        for item in valueArray {
                            self.next_add_oi_last_month.append(OIItem(item as! NSDictionary))
                        }
                    }
                    
                    if let valueArray = resultDictionary.value(forKey: "next_shed_oi_last_day") as? NSArray {
                        for item in valueArray {
                            self.next_shed_oi_last_day.append(OIItem(item as! NSDictionary))
                        }
                    }
                    
                    if let valueArray = resultDictionary.value(forKey: "next_shed_oi_last_7_day") as? NSArray {
                        for item in valueArray {
                            self.next_shed_oi_last_7_day.append(OIItem(item as! NSDictionary))
                        }
                    }
                    
                    if let valueArray = resultDictionary.value(forKey: "next_shed_oi_last_15_day") as? NSArray {
                        for item in valueArray {
                            self.next_shed_oi_last_15_day.append(OIItem(item as! NSDictionary))
                        }
                    }
                    
                    if let valueArray = resultDictionary.value(forKey: "next_shed_oi_last_month") as? NSArray {
                        for item in valueArray {
                            self.next_shed_oi_last_month.append(OIItem(item as! NSDictionary))
                        }
                    }
                    
                    
                }
            }
            
            completion(self)
            
        }
    }

}

class OISignificantChangeItem: NSObject {
    
    var symbol = ""
    var future_closing = ""
    var percent_change_last_days_price = ""
    var open_int = ""
    var percent_expiry_oi = ""
    var change_oi = ""
    var volume = ""
    var percent_volume = ""
    
    override init() { }
    init(_ dict: NSDictionary) {
        self.symbol = onlyString(dict.value(forKey: "symbol"))
        self.future_closing = onlyString(dict.value(forKey: "future_closing")).getTwoDecimalString()
        self.percent_change_last_days_price = onlyString(dict.value(forKey: "percent_change_last_days_price"))
        self.open_int = onlyString(dict.value(forKey: "open_int")).getTwoDecimalString()
        self.percent_expiry_oi = onlyString(dict.value(forKey: "percent_expiry_oi"))
        self.change_oi = onlyString(dict.value(forKey: "change_oi")).getTwoDecimalString()
        self.volume = onlyString(dict.value(forKey: "volume")).getTwoDecimalString()
        self.percent_volume = onlyString(dict.value(forKey: "percent_volume"))
    }
}

//current_add_oi_last_day
class OIItem: NSObject {
    
    var symbol = ""
    var change_expiry_percent = ""
    var percent_change_last_days_price = ""
    var current_oi_sum = ""
    var percent_expiry_oi = ""
    var percent_change_last_7_days_price = ""
    var percent_change_last_7_days_oi = ""
    var percent_change_last_days_oi = ""
    var percent_change_last_15_days_price = ""
    var percent_change_last_15_days_oi = ""
    var percent_change_last_month_price = ""
    var percent_change_last_month_oi = ""
    
    override init() { }
    init(_ dict: NSDictionary) {
        self.symbol = onlyString(dict.value(forKey: "symbol"))
        self.change_expiry_percent = onlyString(dict.value(forKey: "change_expiry_percent")).getTwoDecimalString()
        self.percent_change_last_days_price = onlyString(dict.value(forKey: "percent_change_last_days_price")).getTwoDecimalString()
        self.current_oi_sum = onlyString(dict.value(forKey: "current_oi_sum")).getTwoDecimalString()
        
        self.percent_expiry_oi = onlyString(dict.value(forKey: "percent_expiry_oi")).getTwoDecimalString()
        
        self.percent_change_last_days_oi = onlyString(dict.value(forKey: "percent_change_last_days_oi")).getTwoDecimalString()
        
        self.percent_change_last_7_days_price = onlyString(dict.value(forKey: "percent_change_last_7_days_price")).getTwoDecimalString()
        self.percent_change_last_7_days_oi = onlyString(dict.value(forKey: "percent_change_last_7_days_oi")).getTwoDecimalString()
        
        self.percent_change_last_15_days_price = onlyString(dict.value(forKey: "percent_change_last_15_days_price")).getTwoDecimalString()
        self.percent_change_last_15_days_oi = onlyString(dict.value(forKey: "percent_change_last_15_days_oi")).getTwoDecimalString()
//
        self.percent_change_last_month_price = onlyString(dict.value(forKey: "percent_change_last_month_price")).getTwoDecimalString()
        self.percent_change_last_month_oi = onlyString(dict.value(forKey: "percent_change_last_month_oi")).getTwoDecimalString()
    }
}
