//
//  MMIGraphModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 08/04/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class MMIGraphModel: NSObject {

    var cdate_list = NSMutableArray()
    var mmi_score_list = NSMutableArray()
    
    func process(completion: @escaping ( _ model : MMIGraphModel) -> () ) {
        
        Services().getService(url: ServiceURL.mmi_scrore_graph, parameters: [:]) { (result) in
            
            if let response = result.value(forKey: "result") as? NSArray {
                if let dict = response[0] as? NSDictionary {
                    
                    if let xarray = dict.value(forKey: "cdate") as? NSArray {
                        self.cdate_list.removeAllObjects()
                        for item in xarray {
                            if let obj = item as? NSDictionary {
                                self.cdate_list.add( onlyString(obj.value(forKey: "cdate")) )
                            }
                        }
                    }
                    
                    if let yarray = dict.value(forKey: "mmi_score") as? NSArray {
                        self.mmi_score_list.removeAllObjects()
                        for item in yarray {
                            if let obj = item as? NSDictionary {
                                self.mmi_score_list.add( onlyString(obj.value(forKey: "mmi_score")) )
                            }
                        }
                    }
                    
                    
                }
            }
            
            completion(self)
        }
    }
    
}
