//
//  FSNotesModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 07/05/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class FSNotesModel: NSObject {

    static var shared = FSNotesModel()
    
    var status = ""
    var message = ""
    
    var current_week_date = ""
    var prev_week_date = ""
    var expiry = ""
    
    var daily = [DailyNoteItem]()
    var dailyView = ""
    var weeklyView = ""
    
    var fsnotesweek = [WeeklyNote]()
    
    func process(completion: @escaping ( _ model : FSNotesModel) -> () ) {
        
        Services().getService(url: ServiceURL.fsNotes, parameters: [:]) { (result) in
            
            print(result)
            
            self.status = onlyString(result.value(forKey: "status"))
            self.message = onlyString(result.value(forKey: "message"))
            
            
            if let resultArray = result.value(forKey: "result") as? NSArray {
                if let response = resultArray[0] as? NSDictionary {
                    
                    self.dailyView = onlyString(response.value(forKey: "daily_our_view"))
                    self.weeklyView = onlyString(response.value(forKey: "weekly_our_view"))
                    
                    self.daily.removeAll()
                    self.fsnotesweek.removeAll()
                    
                    if let dailyData = response.value(forKey: "daily") as? NSArray {
                        for item in dailyData {
                            self.daily.append(DailyNoteItem(item as! NSDictionary))
                        }
                    }
                    
                    if let weeklyData = response.value(forKey: "fsnotesweek") as? NSArray {
                        for item in weeklyData {
                            self.fsnotesweek.append(WeeklyNote(item as! NSDictionary))
                        }
                    }
                    
                }
            }
            
            completion(self)
            
        }
    }
    
}

class DailyNoteItem: NSObject {
    var ltd = ""
    var prev_day = ""
    var Particulars = ""
    
    override init() {}
    init(_ dict: NSDictionary) {
        
        
       
        self.Particulars = onlyString(dict.value(forKey: "Particulars"))
        
        if self.Particulars !=  "Score Message"
        {
            self.ltd = onlyString(dict.value(forKey: "ltd")).getTwoDecimalString()
            self.prev_day = onlyString(dict.value(forKey: "prev_day")).getTwoDecimalString()
        }else
        {
            self.ltd = onlyString(dict.value(forKey: "ltd"))
            self.prev_day = onlyString(dict.value(forKey: "prev_day"))
        }
    }
}

class WeeklyNote: NSObject {

    var particulars = "" //Particulars
    var curr_nifty_oi = ""
    var prev_nifty_oi = ""
    var prev_nifty_low = ""
    var prev_nifty_high = ""
    var curr_nifty_close = ""
    var prev_nifty_close = ""
    var current_nifty_low = ""
    var current_nifty_high = ""
    var prev_nifty_weekly_change = ""
    var current_nifty_weekly_change = ""
    
    override init() { }
    init(_ itemOne: NSDictionary) {
        
        self.particulars = onlyString(itemOne.value(forKey: "Particulars"))
        self.curr_nifty_oi = onlyString(itemOne.value(forKey: "curr_oi"))
        self.prev_nifty_oi = onlyString(itemOne.value(forKey: "prev_oi"))
        self.prev_nifty_low = onlyString(itemOne.value(forKey: "prev_low"))
        self.prev_nifty_high = onlyString(itemOne.value(forKey: "prev_high"))
        self.curr_nifty_close = onlyString(itemOne.value(forKey: "curr_close"))
        self.prev_nifty_close = onlyString(itemOne.value(forKey: "prev_close"))
        self.current_nifty_low = onlyString(itemOne.value(forKey: "current_low"))
        self.current_nifty_high = onlyString(itemOne.value(forKey: "current_high"))
        self.prev_nifty_weekly_change = onlyString(itemOne.value(forKey: "prev_weekly_change")).getTwoDecimalString()
        self.current_nifty_weekly_change = onlyString(itemOne.value(forKey: "current_weekly_change"))
        
    }
}
