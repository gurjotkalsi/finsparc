//
//  StockRankingModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 10/05/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

//NOTE: nifty id is watchlist_id and industry_id is id


import UIKit

enum StockTypes {
    case all
    case nifty
    case industrial
}

class StockRankingModel: NSObject {

    static var shared = StockRankingModel()
    
    var list = [StockRankingItem]() //rankData
    
    var status = ""
    var message = ""
    var result = ""
    var industryList = [IndustryItem]()
    
    func getIndustries(completion: @escaping ( _ model : StockRankingModel) -> () ) {
        Services().getService(url: ServiceURL.stockInductryList, parameters: [:]) { (result) in
            self.status = onlyString(result.value(forKey: "status"))
            self.message = onlyString(result.value(forKey: "message"))
            
            if let resultArray = result.value(forKey: "result") as? NSArray {
                self.industryList.removeAll()
                for item in resultArray {
                    self.industryList.append(IndustryItem(item as! NSDictionary))
                }
            }
            
            completion(self)
        }
    }
    
    func process(type:StockTypes, completion: @escaping ( _ model : StockRankingModel) -> () ) {
        
        var strUrl = ServiceURL.stockRatingAll
        if type == .all {
            strUrl = ServiceURL.stockRatingAll
        } else if type == .nifty {
            strUrl = ServiceURL.stockRatingNifty
        } else {
            //industry
            strUrl = ServiceURL.stockRatingIndustrial
        }
        
        Services().getService(url: strUrl, parameters: [:]) { (result) in
            
            print(result)
                    
            self.list.removeAll()
            if let valueArray = result.value(forKey: "rankData") as? NSArray {
                for item in valueArray {
                    self.list.append(StockRankingItem(item as! NSDictionary))
                }
            }
                    
            completion(self)
            
        }
    }
    
    func process(industry:String, completion: @escaping ( _ model : StockRankingModel) -> () ) {
        
        let strUrl = "http://3.6.6.59:7005/viewfinsparcstockranking/"+industry+"/all"
        
        Services().getService(url: strUrl, parameters: [:]) { (result) in
            
            print(result)
            
            self.list.removeAll()
            if let valueArray = result.value(forKey: "rankData") as? NSArray {
                for item in valueArray {
                    self.list.append(StockRankingItem(item as! NSDictionary))
                }
            }
            
            completion(self)
            
        }
    }
}

class StockRankingItem: NSObject {
    
    var symbol = ""
    var rank = ""
    var last_week_rank = ""
    var last_month_rank = ""
    var last_month_percent = ""
    var last_3_month_percent = ""
    var cdate = ""
    var expiry_dt = ""
    
    override init() {}
    init(_ dict: NSDictionary) {
        self.symbol = onlyString(dict.value(forKey: "symbol"))
        self.rank = onlyString(dict.value(forKey: "rank"))
        self.last_week_rank = onlyString(dict.value(forKey: "last_week_rank"))
        self.last_month_rank = onlyString(dict.value(forKey: "last_month_rank"))
        self.last_month_percent = onlyString(dict.value(forKey: "last_month_percent"))
        self.last_3_month_percent = onlyString(dict.value(forKey: "last_3_month_percent"))
        self.cdate = onlyString(dict.value(forKey: "cdate"))
        self.expiry_dt = onlyString(dict.value(forKey: "expiry_dt"))
    }
}

class IndustryItem: NSObject {
    var _id = "" //id
    var name = ""
    var parent_id = ""
    var watchlist_id = ""
    var option_type = ""
    var contract = ""
    var type = ""
    var bhavcopy_id = ""
    var expiry_dt = ""
    var strike_pr = ""
    
    override init() {}
    init(_ dict: NSDictionary) {
        self._id = onlyString(dict.value(forKey: "id"))
        self.name = onlyString(dict.value(forKey: "name"))
        self.parent_id = onlyString(dict.value(forKey: "parent_id"))
        self.watchlist_id = onlyString(dict.value(forKey: "watchlist_id"))
        self.option_type = onlyString(dict.value(forKey: "option_type"))
        self.contract = onlyString(dict.value(forKey: "contract"))
        self.type = onlyString(dict.value(forKey: "type"))
        self.bhavcopy_id = onlyString(dict.value(forKey: "bhavcopy_id"))
        self.expiry_dt = onlyString(dict.value(forKey: "expiry_dt"))
        self.strike_pr = onlyString(dict.value(forKey: "strike_pr"))
    }
    
}
