//
//  MarketModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 27/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class MarketModel: NSObject {

    var list = [MarketItemModel]()
    
    func getList(symbolname: String, completion: @escaping ( _ model : MarketModel) -> () ) {
        //symbol/asc
        Services().getService(url: "http://3.6.6.59:7005/viewfinsparcmarketnowdetails/" + symbolname + "/asc", parameters: [:]) { (result) in
            
            print(result)
            
            self.list.removeAll()
            
            if let data = result.value(forKey: "result") as? NSArray {
                for item in data {
                    self.list.append(MarketItemModel(item as! NSDictionary))
                }
            }
            
            completion(self)
        }
    }
}

class MarketItemModel: NSObject {
    
    var symbol = ""
    var lot = ""
    var diff_in_close = ""
    var close = ""
    var lotcon = ""
    var diff_in_close_5 = ""
    
    init(_ dict : NSDictionary) {
        self.symbol = onlyString(dict.value(forKey: "symbol"))
        self.lot = onlyString(dict.value(forKey: "lot"))
        self.diff_in_close = onlyString(dict.value(forKey: "diff_in_close"))
        self.close = onlyString(dict.value(forKey: "close"))
        self.lotcon = onlyString(dict.value(forKey: "lotcon"))
        self.diff_in_close_5 = onlyString(dict.value(forKey: "diff_in_close_5"))
    }
    
}
