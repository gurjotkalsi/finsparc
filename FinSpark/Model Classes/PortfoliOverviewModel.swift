//
//  PortfoliOverviewModel.swift
//  FinSpark
//
//  Created by Minkle Garg on 12/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit
enum PortfolioverviewFilters:String {
    case all = "all"
    case day = "day"
    case week = "week"
    case month = "month"
}

class PortfoliOverviewModel: NSObject {

    static var shared = PortfoliOverviewModel()
    
    var status = ""
    var message = ""
    var list = [PorfolioverviewItem]()
    
    var objectsList = [PortfolioverviewObject]()
    
    func process(type:PortfolioverviewFilters, completion: @escaping ( _ model : PortfoliOverviewModel) -> () ) {
      //  print("url","\(ServiceURL.portfolio)\(LoginModel().localData().finsparc_user_id)\("/")\(type.rawValue)")
        Services().getService(url: "\(ServiceURL.portfolio)\("1")\("/")\(type.rawValue)", parameters: [:]) { (result) in
            
            print(result)
            
            self.status = onlyString(result.value(forKey: "status"))
            self.message = onlyString(result.value(forKey: "message"))
            
            if let resultArray = result.value(forKey: "result") as? NSArray {
                self.list.removeAll()
                for item in resultArray {
                    self.list.append(PorfolioverviewItem(item as! NSDictionary))
                }
            }
            
            completion(self)
            
        }
    }
    
    func details(parameter : NSDictionary, type:PortfolioverviewFilters, completion: @escaping ( _ model : PortfoliOverviewModel) -> () ) {
        //"\(ServiceURL.portfolio)\(type.rawValue)"
        
        Services().postService(url: "http://3.6.6.59:7005/viewfinsparc_stockholding", parameters: parameter) { (result) in
            
            if let resultArray = result.value(forKey: "result") as? NSArray {
                self.objectsList.removeAll()
                for item in resultArray {
                    self.objectsList.append(PortfolioverviewObject(item as! NSDictionary))
                }
            }
            completion(self)
        }
    }
}


class PorfolioverviewItem: NSObject {

    var finsparc_portfolio_id = ""
    var portfolio_name = ""
    var user_id = ""
    var created_on = ""
    var active_status = ""
    var portfolio_booked_amount = ""
    var mtmjson = ""
    
    override init() {}
    init(_ dict: NSDictionary) {
        self.finsparc_portfolio_id = onlyString(dict.value(forKey: "finsparc_portfolio_id"))
        self.portfolio_name = onlyString(dict.value(forKey: "portfolio_name"))
        self.user_id = onlyString(dict.value(forKey: "user_id"))
        self.created_on = onlyString(dict.value(forKey: "created_on"))
        self.active_status = onlyString(dict.value(forKey: "active_status"))
        self.portfolio_booked_amount = onlyString(dict.value(forKey: "portfolio_booked_amount"))
        self.mtmjson = onlyString(dict.value(forKey: "mtmjson"))
    }
    
}

class PortfolioverviewObject: NSObject {
    var finsparc_stock_id = ""
    var price = ""
    var quantity = ""
    var bhavcopy_id = ""
    var user_id = ""
    var transaction_date = ""
    var transaction_type = ""
    var portfolio_id = ""
    var instrument_name = ""
    var instrument_type = ""
    var contract = ""
    var expiry_dt = ""
    var strike_pr = ""
    var option_type = ""
    var close = ""
    var selling_price = ""
    var selling_quantity = ""
    var selling_date = ""
    var booked = ""
    var created_on = ""
    var portfolio_name = ""
    var status = ""
    var last_close = ""
    var stock = ""
    var open_buy_price = ""
    var mtm = ""
    var remaining_sell_qty = ""
    var remaining_qty = ""
    var qty = ""
    var bprice = ""
    var sqty = ""
    var sprice = ""
    
    override init() {}
    init(_ dict: NSDictionary) {
        self.finsparc_stock_id = onlyString(dict.value(forKey: "finsparc_stock_id"))
        self.price = onlyString(dict.value(forKey: "price"))
        self.quantity = onlyString(dict.value(forKey: "quantity"))
        self.bhavcopy_id = onlyString(dict.value(forKey: "bhavcopy_id"))
        self.user_id = onlyString(dict.value(forKey: "user_id"))
        self.transaction_date = onlyString(dict.value(forKey: "transaction_date"))
        self.transaction_type = onlyString(dict.value(forKey: "transaction_type"))
        self.portfolio_id = onlyString(dict.value(forKey: "portfolio_id"))
        self.instrument_name = onlyString(dict.value(forKey: "instrument_name"))
        self.instrument_type = onlyString(dict.value(forKey: "instrument_type"))
        self.contract = onlyString(dict.value(forKey: "contract"))
        self.expiry_dt = onlyString(dict.value(forKey: "expiry_dt"))
        self.strike_pr = onlyString(dict.value(forKey: "strike_pr"))
        self.option_type = onlyString(dict.value(forKey: "option_type"))
        self.close = onlyString(dict.value(forKey: "close"))
        self.selling_price = onlyString(dict.value(forKey: "selling_price"))
        self.selling_quantity = onlyString(dict.value(forKey: "selling_quantity"))
        self.selling_date = onlyString(dict.value(forKey: "selling_date"))
        self.booked = onlyString(dict.value(forKey: "booked"))
        self.created_on = onlyString(dict.value(forKey: "created_on"))
        self.portfolio_name = onlyString(dict.value(forKey: "portfolio_name"))
        self.status = onlyString(dict.value(forKey: "status"))
        self.last_close = onlyString(dict.value(forKey: "last_close"))
        self.stock = onlyString(dict.value(forKey: "stock"))
        self.open_buy_price = onlyString(dict.value(forKey: "open_buy_price"))
        self.mtm = onlyString(dict.value(forKey: "mtm"))
        self.remaining_sell_qty = onlyString(dict.value(forKey: "remaining_sell_qty"))
        self.remaining_qty = onlyString(dict.value(forKey: "remaining_qty"))
        self.qty = onlyString(dict.value(forKey: "qty"))
        self.bprice = onlyString(dict.value(forKey: "bprice"))
        self.sqty = onlyString(dict.value(forKey: "sqty"))
        self.sprice = onlyString(dict.value(forKey: "sprice"))
    }
    
}
