//
//  DailyDataModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 06/05/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class DailyDataModel: NSObject {
    
    static var shared = DailyDataModel()
    
    var status = ""
    var message = ""
    
    var current_date_value = DailyDataItem()
    var last_month_value = DailyDataItem()
    
    func process(completion: @escaping ( _ model : DailyDataModel) -> () ) {
        
        Services().getService(url: ServiceURL.dailyData, parameters: [:]) { (result) in
            
            print(result)
            
            self.status = onlyString(result.value(forKey: "status"))
            self.message = onlyString(result.value(forKey: "message"))
            
            self.current_date_value = DailyDataItem()
            self.last_month_value = DailyDataItem()
            
            if let resultArray = result.value(forKey: "result") as? NSArray {
                if let response = resultArray[0] as? NSDictionary {
                    
                    if let valueArray = response.value(forKey: "current_date_value") as? NSArray {
                        if let vdict = valueArray[0] as? NSDictionary {
                            self.current_date_value = DailyDataItem(vdict)
                        }
                    }
                    
                    if let valueArray1 = response.value(forKey: "last_month_value") as? NSArray {
                        if let vdict1 = valueArray1[0] as? NSDictionary {
                            self.last_month_value = DailyDataItem(vdict1)
                        }
                    }
                    
                }
            }
            
            completion(self)
            
        }
    }
    
}

class DailyDataItem: NSObject {
    
    var cdate = ""
    var future_price = ""
    var total_sum = ""
    var change_oi_last_day = ""
    var change_oi_5_day = ""
    var change_close = ""
    var array_to_json = [DailyDataItemObject]()
    
    override init() {}
    init(_ dict : NSDictionary) {
        self.cdate = onlyString(dict.value(forKey: "cdate"))
        self.future_price = onlyString(dict.value(forKey: "future_price"))
        self.total_sum = onlyString(dict.value(forKey: "total_sum"))
        self.change_oi_last_day = onlyString(dict.value(forKey: "change_oi_last_day"))
        self.change_oi_5_day = onlyString(dict.value(forKey: "change_oi_5_day"))
        self.change_close = onlyString(dict.value(forKey: "change_close"))
        
        self.array_to_json.removeAll()
        if let arraying = dict.value(forKey: "array_to_json") as? NSArray {
            for item in arraying {
                self.array_to_json.append(DailyDataItemObject(item as! NSDictionary))
            }
        }
        
    }
    
}

class DailyDataItemObject: NSObject {
    
    var strike_pr = ""
    var close = ""
    var trigger = "" //two digits
    var open_int = ""
    var avg_oi = ""
    var change_oi = ""
    var sw = ""
    var diff_avg_oi = ""
    
    override init() {}
    init(_ dict : NSDictionary) {
        self.strike_pr = onlyString(dict.value(forKey: "stike_pr"))
        self.close = onlyString(dict.value(forKey: "close"))
        self.trigger = onlyString(dict.value(forKey: "trigger")).getTwoDecimalString()
        self.open_int = onlyString(dict.value(forKey: "open_int"))
        self.avg_oi = onlyString(dict.value(forKey: "avg_oi"))
        self.change_oi = onlyString(dict.value(forKey: "change_oi"))
        self.sw = onlyString(dict.value(forKey: "sw"))
        self.diff_avg_oi = onlyString(dict.value(forKey: "diff_avg_oi"))
    }
    
}
