//
//  ViewTransactionModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 10/07/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class ReviewModel: NSObject {
    
    static let shard = ReviewModel()
    var list = [ViewTransObject]()
    
    func process(completion: @escaping ( _ model : ReviewModel) -> () ) {
        
        Services().getService(url: "\(ServiceURL.reviewsurl)\(LoginModel().localData().finsparc_user_id)" , parameters: [:]) { (result) in
            
            if let response = result.value(forKey: "result") as? [NSDictionary] {
                self.list.removeAll()
                for item in response {
                    self.list.append(ViewTransObject(item))
                }
            }
            
            completion(self)
        }
    }

}

class ViewTransObject: NSObject {
    
    var user_id = ""
    var symbol = ""
    var transaction = [ViewTransItem]()
    var review_details = [ReviewObject]()

    override init() { }
    
    init(_ dict:NSDictionary) {
        self.user_id = onlyString(dict.value(forKey: "user_id"))
        self.symbol = onlyString(dict.value(forKey: "symbol"))
        
        if let tarra = dict.value(forKey: "transaction") as? [NSDictionary] {
            for item in tarra {
                self.transaction.append(ViewTransItem(item))
            }
        }
        
        if let rarra = dict.value(forKey: "review_details") as? [NSDictionary] {
            for item in rarra {
                self.review_details.append(ReviewObject(item))
            }
        }

    }
    
    //ReviewObject
}

class ViewTransItem: NSObject {
    
    var price = ""
    var quantity = ""
    var user_id = ""
    var type = ""
    var stock_name = ""
    var option_type = ""
    var expiry_dt = ""
    var strike_pr = ""
    var selling_price = ""
    var selling_quantity = ""
    var booked = ""
    var last_close = ""
    var open_buy_price = ""
    var remaining_qty = ""
    var mtm_price = ""
    
    override init() { }
    
    init(_ dict:NSDictionary) {
        self.price = onlyString(dict.value(forKey: "price"))
        self.quantity = onlyString(dict.value(forKey: "quantity"))
        self.user_id = onlyString(dict.value(forKey: "user_id"))
        self.type = onlyString(dict.value(forKey: "type"))
        self.stock_name = onlyString(dict.value(forKey: "stock_name"))
        self.option_type = onlyString(dict.value(forKey: "option_type"))
        self.expiry_dt = onlyString(dict.value(forKey: "expiry_dt"))
        self.strike_pr = onlyString(dict.value(forKey: "strike_pr"))
        self.selling_price = onlyString(dict.value(forKey: "selling_price"))
        self.selling_quantity = onlyString(dict.value(forKey: "selling_quantity"))
        self.booked = onlyString(dict.value(forKey: "booked"))
        self.last_close = onlyString(dict.value(forKey: "last_close"))
        self.open_buy_price = onlyString(dict.value(forKey: "open_buy_price"))
        self.remaining_qty = onlyString(dict.value(forKey: "remaining_qty"))
        self.mtm_price = onlyString(dict.value(forKey: "mtm_price"))
    }
    
    //ReviewObject
}
