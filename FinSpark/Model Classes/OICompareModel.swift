//
//  OICompareModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 09/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class OICompareModel: NSObject {

    static let shared = OICompareModel()
    
    var status = ""
    var message = ""
    
    var list = [OICompareObject]()
    
    func process(completion: @escaping ( _ model : OICompareModel) -> () ) {
        
        Services().postService(url: "http://3.6.6.59:7005/oiComparision", parameters: ["symbol":"ACC"]) { (result) in

            self.status = onlyString(result.value(forKey: "status"))
            self.message = onlyString(result.value(forKey: "message"))
            
            self.list.removeAll()
            if let response = result.value(forKey: "result") as? NSArray {
                for item in response {
                    self.list.append(OICompareObject(item as! NSDictionary))
                }
            }
            
            completion(self)
        }
    }
    
}

class OICompareObject: NSObject {
    var cdate = ""
    var future_price = ""
    var total_sum = ""
    var agg_oi_ce = ""
    var agg_oi_pe = ""
    var last_month_date = ""
    var last_month_future_price = ""
    var last_month_total_sum = ""
    var agg_oi_ce_last_month = ""
    var agg_oi_pe_last_month = ""
    var expiry_dt = ""
    
    override init() { }
    init(_ dict:NSDictionary) {
        self.cdate = onlyString(dict.value(forKey: "cdate"))
        self.future_price = onlyString(dict.value(forKey: "future_price"))
        self.total_sum = onlyString(dict.value(forKey: "total_sum"))
        self.agg_oi_ce = onlyString(dict.value(forKey: "agg_oi_ce"))
        self.agg_oi_pe = onlyString(dict.value(forKey: "agg_oi_pe"))
        self.last_month_date = onlyString(dict.value(forKey: "last_month_date"))
        self.last_month_future_price = onlyString(dict.value(forKey: "last_month_future_price"))
        self.last_month_total_sum = onlyString(dict.value(forKey: "last_month_total_sum"))
        self.agg_oi_ce_last_month = onlyString(dict.value(forKey: "agg_oi_ce_last_month"))
        self.agg_oi_pe_last_month = onlyString(dict.value(forKey: "agg_oi_pe_last_month"))
        self.expiry_dt = onlyString(dict.value(forKey: "expiry_dt"))
    }
    
}
