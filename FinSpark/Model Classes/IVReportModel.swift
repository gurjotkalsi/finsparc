//
//  IVReportModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 29/04/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class IVReportModel: NSObject {
    
    static var shared = IVReportModel()
    
    var status = ""
    var message = ""
    var list = [IVReportItem]()
    
    func process(completion: @escaping ( _ model : IVReportModel) -> () ) {
        
        Services().getService(url: ServiceURL.ivReport, parameters: [:]) { (result) in
            
            print(result)
            
            self.status = onlyString(result.value(forKey: "status"))
            self.message = onlyString(result.value(forKey: "message"))
            
            if let resultArray = result.value(forKey: "result") as? NSArray {
                self.list.removeAll()
                for item in resultArray {
                    self.list.append(IVReportItem(item as! NSDictionary))
                }
            }
            
            completion(self)
            
        }
    }
    
}

class IVReportItem: NSObject {
    var cdate = ""
    var symbol = ""
    var iv_last_day = ""
    var iv_last_week = ""
    var iv_last_month = ""
    var change_over_last_week = ""
    var change_over_last_month = ""
    var expiry_dt = ""

    override init() { }
    init(_ dict: NSDictionary) {
        self.cdate = onlyString(dict.value(forKey: "cdate"))
        self.expiry_dt = onlyString(dict.value(forKey: "expiry_dt"))
        self.symbol = onlyString(dict.value(forKey: "symbol"))
        self.iv_last_day = onlyString(dict.value(forKey: "iv_last_day"))
        self.iv_last_week = onlyString(dict.value(forKey: "iv_last_week"))
        self.iv_last_month = onlyString(dict.value(forKey: "iv_last_month"))
        self.change_over_last_week = onlyString(dict.value(forKey: "change_over_last_week"))
        self.change_over_last_month = onlyString(dict.value(forKey: "change_over_last_month"))
    }
    
}
