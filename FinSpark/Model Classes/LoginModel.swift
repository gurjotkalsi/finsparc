//
//  LoginModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 26/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class LoginModel: NSObject {

    var status = "" //Status | if 200 it is a success
    var msg = "" //Msg
    
    var finsparc_user_id = "" //Data->finsparc_user_id
    var name = "" //Data->
    var mobile = "" //Data->
    var email = "" //Data->
    var password = "" //Data->
    var created_on = "" //Data->
    var last_updated_on = "" //Data->
    var usertype = "" //Data->
    
    
    func process(parameter : NSDictionary, completion: @escaping ( _ model : LoginModel) -> () ) {
        
        Services().postService(url: ServiceURL.login, parameters: parameter) { (result) in
            
            print(result)
            self.status = onlyString(result.value(forKey: "Status"))
            self.msg = onlyString(result.value(forKey: "Msg"))
            
            if let dataArray = result.value(forKey: "Data") as? NSArray {
                if let data = dataArray[0] as? NSDictionary {
                    self.writeUserData(data)
                    _ = self.assignData(data)
                }
            }
            
            completion(self)
        }
    }
    
    private func writeUserData(_ dataDict : NSDictionary) {
        let data = NSKeyedArchiver.archivedData(withRootObject: dataDict)
        UserDefaults.standard.set(data, forKey: "DefaultKeys.userData")
        UserDefaults.standard.synchronize()
    }
    
    private func assignData(_ data : NSDictionary) -> LoginModel {
        self.finsparc_user_id = onlyString(data.value(forKey: "finsparc_user_id"))
        self.name = onlyString(data.value(forKey: "name"))
        self.mobile = onlyString(data.value(forKey: "mobile"))
        self.email = onlyString(data.value(forKey: "email"))
        self.password = onlyString(data.value(forKey: "password"))
        self.created_on = onlyString(data.value(forKey: "created_on"))
        self.last_updated_on = onlyString(data.value(forKey: "last_updated_on"))
        self.usertype = onlyString(data.value(forKey: "usertype"))
        
        return self
    }
    
    func localData() -> LoginModel {
        if let data = UserDefaults.standard.value(forKey: "DefaultKeys.userData") as? Data {
            let dataDict = NSKeyedUnarchiver.unarchiveObject(with: data) as! NSDictionary
            return self.assignData(dataDict)
        }
        
        return self
    }

    
}

