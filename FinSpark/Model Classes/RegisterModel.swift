//
//  RegisterModel.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 26/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

//ERROR in API: User is getting registered multiple times when attempted to register again

import UIKit

class RegisterModel: NSObject {

    static let shared = RegisterModel()
    
    var insert_otp = "" // result->insert_otp
    var register_user = "" // result->register_user
    
    func getOTP(parameter : NSDictionary, completion: @escaping ( _ model : RegisterModel) -> () ) {
        
        Services().postService(url: ServiceURL.register_otp_verify, parameters: parameter) { (result) in
            
            print(result)
            
            if let data = result.value(forKey: "result") as? NSArray {
                if let dict = data[0] as? NSDictionary {
                    self.insert_otp = onlyString(dict.value(forKey: "insert_otp"))
                }else{
                    self.insert_otp = ""
                }
            }else{
                self.insert_otp = ""
            }
            
            completion(self)
        }
    }

    
    func userRegister(parameter : NSDictionary, completion: @escaping ( _ model : RegisterModel) -> () ) {
        
        Services().postService(url: ServiceURL.register, parameters: parameter) { (result) in
            
            print(result)
            
            if let data = result.value(forKey: "result") as? NSArray {
                if let dict = data[0] as? NSDictionary {
                    self.register_user = onlyString(dict.value(forKey: "register_user"))
                }else{
                    self.register_user = ""
                }
            }else{
                self.register_user = ""
            }
            
            completion(self)
        }
    }
    
    
}
