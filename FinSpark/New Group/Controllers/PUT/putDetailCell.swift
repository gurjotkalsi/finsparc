//
//  putDetailCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 27/02/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class putDetailCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    var typeofPuttext: String = ""
    
    @IBOutlet weak var collectionView: UICollectionView!
    var object : Any!
    var typeclass : NSObject.Type?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.register(UINib(nibName: "topTabCell", bundle: nil), forCellWithReuseIdentifier: "topTabCell")
        
        //typeofPuttext = UserDefaults.standard.string(forKey: "type")!
        let userdefaults = UserDefaults.standard
        if userdefaults.string(forKey: "type") != nil{
           typeofPuttext = UserDefaults.standard.string(forKey: "type")!
        } else {
        }

        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if typeofPuttext == "Indices" {
            return 5
        } else if typeofPuttext == "NTM" {
            return 5
        } else if typeofPuttext == "OI" {
            return 4

           // titlesArray = ["Symbol", "Strike Price", "Trigger Price", "Price Close", "OI (Ch. OI)", "% Ch. In\nOI"]
        }else
        {
            return 0
        }
    }
    
    func returnValue(index:Int) -> String {
        if typeclass == ReportItem.self {
            if let valueObject = object as? ReportItem {

                if index == 0 {
                    print("symbol",valueObject.symbol)
                    return valueObject.symbol + "\n" + "(" + valueObject.expiry_dt + ")"
                }else if index == 1 {
                    return valueObject.stike_pr
                } else if index == 2 {
                    return valueObject.trigger_price
                } else if index == 3 {
                    return valueObject.close + "\n" + "(" + valueObject.price_diff + ")"
                } else {
                    return valueObject.expiry_high + "\n" + valueObject.expiry_low
                }
//                else {
//                    return valueObject.expiry_low
//                }
                    
            }
        }else if typeclass == ReportSecondItem.self {
            if let valueObject = object as? ReportSecondItem {
                if index == 0 {
                    return valueObject.symbol
                }else if index == 1 {
                    return valueObject.itm_call_strike
                } else if index == 2 {
                    return valueObject.call_trigger
                } else if index == 3 {
                    return valueObject.call_ltp
                } else if index == 4 {
                    return valueObject.future_closing
                } else {
                    return ""
                }
            }
        }else{
            if let valueObject = object as? ReportThirdItemObject {
                if index == 0 {
                    return valueObject.symbol
                }else if index == 1 {
                    return valueObject.stike_pr
                } else if index == 2 {
                    return valueObject.pclose
                } else if index == 3 {
                    return valueObject.call_oi
                } else if index == 4 {
                    return valueObject.open_int
                } else {
                    return valueObject.call_oi
                }
            }
        }
        
        return ""
    }
    
    func returnColor(index:Int) -> UIColor {
        if typeclass == ReportItem.self {
            if let valueObject = object as? ReportItem {
                if index == 0 {
                    return UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
                }else if index == 1 {
                    return UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
                } else if index == 2 {
                    return UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
                } else if index == 3 {
                    if let value = Float(valueObject.price_diff) {
                               if value < 0
                               {
                    return   .red
                        }else
                               {
                                return CustomColours.green
                        }
                }
                }else if index == 4 {
                    return UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
                } else {
                    return UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
                }
                    
           // }
            }
        }else if typeclass == ReportSecondItem.self {
            if let valueObject = object as? ReportSecondItem {
                if index == 0 {
                    return UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
                }else if index == 1 {
                    return UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
                } else if index == 2 {
                    return UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
                } else if index == 3 {
                    return UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
                } else if index == 4 {
                    if let value = Float(valueObject.future_closing) {
                       if value < 0
                       {
                          return   .red
                        }else
                        {
                          return CustomColours.green
                        }
                }
            }
                else {
                    return UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
                }
            }
        }else{
            if let valueObject = object as? ReportThirdItemObject {
                if index == 0 {
                    return UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
                }else if index == 1 {
                    return UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
                } else if index == 2 {
//                    if let value = Float(valueObject.close) {
//                               if value < 0
//                               {
//                    return   .red
//                        }else
//                               {
//                                return CustomColours.green
//                        }
                    return UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)

               // }
                }else if index == 3 {
                        if let value = NSInteger(valueObject.pclose) {
                                   if value < 0
                                   {
                        return   .red
                            }else
                                   {
                                    return CustomColours.green
                            }
                    }else
                        {
                            return CustomColours.green

                    }
                } else if index == 4 {
                    return UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
                }
                else {
                    print("valueObject.call_oi",valueObject.call_oi)
                    let value = NSInteger(valueObject.call_oi)
                    print("value",value as Any)
                            if let value = Float(valueObject.call_oi) {
                                       if value < 0
                                       {
                            return   .red
                                }else
                                       {
                                        return CustomColours.green
                                }
                }
            }
        }
        }
        return UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "topTabCell", for: indexPath as IndexPath) as! topTabCell
        
        cell.lbl_Headings.text = returnValue(index: indexPath.item)
        if self.typeofPuttext == "Indices"  && indexPath.item == 2
        {
            
        }
       // cell.lbl_Headings.roundUp()
        if self.typeofPuttext != "OI" {
            cell.lbl_Headings.font = UIFont(name: "Assistant-Regular", size: 12.6)

        }else
        {
            cell.lbl_Headings.font = UIFont(name: "Assistant-Regular", size: 13.2)

        }
        cell.lbl_Headings.textColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
        cell.lbl_Headings.numberOfLines = 0
        cell.lbl_Headings.textColor = returnColor(index: indexPath.item)
        if self.typeofPuttext != "NTM" {
          //  cell.lbl_Headings.textColor = UIColor(red: 0.274, green: 0.726, blue: 0, alpha: 1)
        }
        
        if indexPath.row == 0
        {
            cell.lbl_Headings.lineBreakMode = .byWordWrapping
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineHeightMultiple = 0.84
            cell.lbl_Headings.textAlignment = .left
            cell.lbl_Headings.attributedText = NSMutableAttributedString(string: self.returnValue(index: indexPath.item), attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
        } else {
            cell.lbl_Headings.textAlignment = .right
        }
        
//        if indexPath.row == 3 {
//            cell.lbl_Headings.textColor = UIColor(red: 0.925, green: 0.046, blue: 0.046, alpha: 1)
//        }
        
        if self.typeofPuttext == "IV" {
            cell.lbl_Headings.font = UIFont(name: "Assistant-Regular", size: 13.5)
            cell.lbl_Headings.textColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
            cell.lbl_Headings.textAlignment = .center
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      //  var width = (collectionView.frame.size.width - 40) / 6
        
        var width = (collectionView.frame.size.width - 40) / 5

        if typeofPuttext == "NTM" {
            width = (collectionView.frame.size.width - 40) / 5
        }
        else if typeofPuttext == "OI"
        {
            width = (collectionView.frame.size.width - 40) / 4.1
        }
        return CGSize(width: width , height: width)
    }
    
   
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let vc = self.storyboard.instantiateViewController(withIdentifier: "OptionVC") as! OptionVC
//        self.navigationController?.pushViewController(vc, animated: true)

//        let vc = (((self.storyboard? as AnyObject)! ?? <#default value#>)).instantiateViewController(withIdentifier: "OptionVC") as! OptionVC
//                  self.navigationController?.pushViewController(vc, animated: true)
    }

}
