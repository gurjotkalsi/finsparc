//
//  putCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 24/02/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class putCell: UITableViewCell {
    @IBOutlet weak var title_Label: UILabel!
    @IBOutlet weak var sub_Label: UILabel!
    @IBOutlet var sublabelConstraint: NSLayoutConstraint!

    @IBOutlet weak var border_View: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        border_View.addBorder()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
