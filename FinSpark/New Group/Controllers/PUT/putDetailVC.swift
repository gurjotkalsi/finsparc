//
//  putDetailVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 27/02/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class putDetailVC: UIViewController {
    @IBOutlet weak var collctionview: UICollectionView!
    @IBOutlet var viewConstant: NSLayoutConstraint!

    @IBOutlet weak var collctionviewTopTabs: UICollectionView!
    @IBOutlet weak var putTableView: UITableView!
    
    @IBOutlet var expiry_date: UILabel!
    @IBOutlet var eod_date: UILabel!
    
    var typeofPuttext: String = ""
    var titlesArray = ["Symbol", "Strike price", "Trigger Price", "Price Close", "Expiry High\nExpiry Low"]
    var titleheading: String = ""
    var titleArray = ["Last Day", "Last Week"]

    var objectsArray = NSArray()
    var typeclass : NSObject.Type?
    
    //MARK:-
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.CustomizeNavBar_with(title: "", button_text: titleheading, bartint: UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1), backButton: true) {
            self.navigationController?.popViewController(animated: true)
        }
        
        if typeclass == ReportItem.self {
            print("ReportItem")
        }else if typeclass == ReportSecondItem.self {
            print("ReportSecondItem")
        }else{
            print("ReportThirdItemObject")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.viewConstant.constant = 0
        if typeofPuttext == "Indices" {
            titlesArray = ["Symbol", "Strike Price", "Trigger Price", "Price Close", "Expiry High\nExpiry Low"]
        } else if typeofPuttext == "NTM" {
            titlesArray = ["Symbol", "Strike Price", "Put Trigger Price", "Put Price Close", "Futures Price Close"]
        } else if typeofPuttext == "OI" {
            self.viewConstant.constant = 35

            titlesArray = ["Symbol", "Strike Price", "Price", "OI \n▵ in OI"]

           // titlesArray = ["Symbol", "Strike Price", "Trigger Price", "Price Close", "OI (Ch. OI)", "% Ch. In\nOI"]
        } else {
            titlesArray = ["Symbol", "Strike Price", "Price", "OI \n▵ in OI"]
        }
        collctionview.register(UINib(nibName: "topTabCell", bundle: nil), forCellWithReuseIdentifier: "topTabCell")
        collctionview.reloadData()

        collctionviewTopTabs.register(UINib(nibName: "topTabCell", bundle: nil), forCellWithReuseIdentifier: "topTabCell")
        collctionviewTopTabs.reloadData()
    }
    //MARK:-
}

extension putDetailVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objectsArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfSectionsInTableView section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "putDetailCell", for: indexPath) as! putDetailCell
        cell.selectionStyle = .none

        cell.typeofPuttext = self.typeofPuttext
        cell.object = self.objectsArray.object(at: indexPath.row)
        cell.typeclass = self.typeclass
        return cell
    }
}

extension putDetailVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collctionviewTopTabs
        {

        return titlesArray.count
        }else
        {
            return 2
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collctionviewTopTabs.dequeueReusableCell(withReuseIdentifier: "topTabCell", for: indexPath) as! topTabCell
        if collectionView == collctionviewTopTabs
        {
            cell.lbl_Headings.text = self.titlesArray[indexPath.row]
             print("headings",cell.lbl_Headings.text)
            // cell.lbl_Headings.roundUp()
             if indexPath.row == 0 {
                 cell.lbl_Headings.lineBreakMode = .byWordWrapping
                 let paragraphStyle = NSMutableParagraphStyle()
                 paragraphStyle.lineHeightMultiple = 0.84
                 cell.lbl_Headings.attributedText = NSMutableAttributedString(string: self.titlesArray[indexPath.row], attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
             } else {
                 cell.lbl_Headings.textAlignment = .right
             }
        }
        else
        {
            cell.lbl_Headings.text = self.titleArray[indexPath.row]
             cell.lbl_Headings.roundUp()

        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var width = (collctionviewTopTabs.frame.size.width - 40) / 5.1
        
        if typeofPuttext == "NTM" {
            width = (collctionviewTopTabs.frame.size.width - 40) / 5.1
        }else if typeofPuttext == "OI"
        {
            width = (collctionviewTopTabs.frame.size.width - 40) / 4.1
        }
        return CGSize(width: width , height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
}
