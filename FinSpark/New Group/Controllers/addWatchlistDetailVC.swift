//
//  addWatchlistDetailVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 14/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit
import iOSDropDown

class addWatchlistDetailVC: UIViewController {
    
    @IBOutlet weak var instrumentBtn: UIButton!
    @IBOutlet weak var typeBtn: UIButton!
    @IBOutlet weak var contractBtn: UIButton!
    @IBOutlet weak var dateField: UITextField!
    
    //Uidate picker
    let datePicker = UIDatePicker()
    let dropDown = DropDown()
    var selectedTag = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        

        // The view to which the drop down will appear on
        dropDown.frame = instrumentBtn.frame // UIView or UIBarButtonItem

        // The list of items to display. Can be changed dynamically
        // Do any additional setup after loading the view.
        dropDown.didSelect{(item , index ,id) in
          print("Selected item: \(item) at index: \(index)")
            if self.selectedTag == 1 {
                            self.instrumentBtn.setTitle(item, for: .normal)
            } else if self.selectedTag == 2 {
                self.typeBtn.setTitle(item, for: .normal)

            }else
            {
                self.contractBtn.setTitle(item, for: .normal)

            }
        }
        showDatePicker()

        // Will set a custom width instead of the anchor view width
      //  dropDown.width = instrumentBtn.frame.size.width
    }
    func showDatePicker(){
       //Formate Date
        datePicker.datePickerMode = .date

        //ToolBar
       let toolbar = UIToolbar();
       toolbar.sizeToFit()

       //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: Selector(("donedatePicker")))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: Selector(("cancelDatePicker")))
       toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)

    // add toolbar to textField
    dateField.inputAccessoryView = toolbar
     // add datepicker to textField
    dateField.inputView = datePicker
    }
    
    func donedatePicker(){
    //For date formate
     let formatter = DateFormatter()
     formatter.dateFormat = "dd-MM-yyyy"
     dateField.text = formatter.string(from: datePicker.date)
     //dismiss date picker dialog
     self.view.endEditing(true)
      }

        func cancelDatePicker(){
         //cancel button dismiss datepicker dialog
          self.view.endEditing(true)
        }
     
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           self.tabBarController?.tabBar.isHidden = true
    }
    
    @IBAction func instrumentAction(_ sender: UIButton) {
        
        selectedTag = 1
       // sender.isSelected = !sender.isSelected
        dropDown.optionArray = ["ACC", "Call", "Put", "FutStk"]

        dropDown.showList()
    }
    
    @IBAction func typeAction(_ sender: UIButton) {
        selectedTag = 2

         //  sender.isSelected = !sender.isSelected
        dropDown.optionArray = ["Futures", "Call", "Put"]

           dropDown.showList()
       }
    
    @IBAction func contractAction(_ sender: UIButton) {
        selectedTag = 3

            //  sender.isSelected = !sender.isSelected
        dropDown.optionArray = ["1300", "1500", "1800", "2100", "2400"]

              dropDown.showList()
          }
    
    @IBAction func dateAction(_ sender: UIButton) {
           selectedTag = 4

               //  sender.isSelected = !sender.isSelected
           dropDown.optionArray = ["1300", "1500", "1800", "2100", "2400"]

                 dropDown.showList()
             }
    @IBAction func popAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
