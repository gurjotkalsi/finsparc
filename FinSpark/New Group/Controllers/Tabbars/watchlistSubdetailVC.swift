//
//  watchlistSubdetailVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 22/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//
//oisubdetailVC

import UIKit

class watchlistSubdetailVC: UIViewController {
    @IBOutlet var viewConstant: NSLayoutConstraint!

    @IBOutlet var tableview: UITableView!
    @IBOutlet weak var collctionviewTopTabs: UICollectionView!

    var collapsedSize = CGFloat(55)
    var expandedSize = CGFloat(375)
    
    var expand = true
    
    var prevIndex : IndexPath?
    var currentIndex : IndexPath?
    
    var titlestopArray = ["Data", "Graph"]
    var selection = NSMutableArray();
    var company = ""

    var titleArray = ["Stock Profile", "5 Days Data", "OI in lacs (for all Expires)", "Options", "Implied Volatility", "Finsparc Summary"]
    var collapsedarray = [true, true, true,true, true, true]
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        sliderHolder.fadeOut { }
        self.viewConstant.constant = 0
        self.tabBarController?.tabBar.isHidden = true

        
        collctionviewTopTabs.register(UINib(nibName: "topTabCell", bundle: nil), forCellWithReuseIdentifier: "topTabCell")
       let indexpath = 0
       selection.add(indexpath)
       collctionviewTopTabs.reloadData()
        // You know them 😉
        self.tableview.dataSource = self
        self.tableview.delegate = self
        
        tableview.register(UINib(nibName: "searchCell", bundle: nil), forCellReuseIdentifier: "searchCell")
        tableview.register(UINib(nibName: "oifirstCell", bundle: nil), forCellReuseIdentifier: "oifirstCell")
        tableview.register(UINib(nibName: "oiCell", bundle: nil), forCellReuseIdentifier: "oiCell")
        tableview.register(UINib(nibName: "volatilityCell", bundle: nil), forCellReuseIdentifier: "volatilityCell")
        tableview.register(UINib(nibName: "summaryCell", bundle: nil), forCellReuseIdentifier: "summaryCell")
        tableview.register(UINib(nibName: "OptionCustomCell", bundle: nil), forCellReuseIdentifier: "OptionCustomCell")
        tableview.register(UINib(nibName: "collapsedcell", bundle: nil), forCellReuseIdentifier: "collapsedcell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sliderHolder.fadeOut {}

        self.CustomizeNavBar_with(title: "", button_text: company, bartint: UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1), backButton: true) {
            self.navigationController?.popViewController(animated: true)
            sliderHolder.fadeIn {}
        }

        StockProfileModel.shared.process(company: company) { (model) in
            
            self.RemoveLoader()
                        self.tableview.reloadData()

        }    }
    //MARK:-


}

//MARK: Collectionview Methods
extension watchlistSubdetailVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           return CGSize(width: collectionView.frame.width / 3.3, height: collectionView.frame.height)
       }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.collctionviewTopTabs.dequeueReusableCell(withReuseIdentifier: "topTabCell", for: indexPath) as! topTabCell
        cell.lbl_Headings.text = self.titlestopArray[indexPath.row]
        cell.lbl_Headings.roundUp()
        cell.lbl_Headings.textAlignment = .center
        if selection.contains(indexPath.row) {
        cell.lbl_Headings.backgroundColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
        cell.lbl_Headings.textColor = UIColor.white

           }else{
        cell.lbl_Headings.backgroundColor = UIColor.clear
        cell.lbl_Headings.textColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)

           }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if selection.contains(indexPath.row) {
            selection.removeAllObjects()
        }else{
            selection.removeAllObjects()
            selection.add(indexPath.row)
        }
        collectionView.reloadData()
    }
   
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 10))
        return footerView
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
}

extension watchlistSubdetailVC : UITableViewDataSource {

     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let collapse = collapsedarray[indexPath.section]

        if indexPath.section == 0 {
                    if collapse {
                        return 55
                     }else{
                        return 270

                     }
            

         }
            else if indexPath.section == 1 {

                   if collapse {
                                                   return 55
                                                }else{
                                                   return 55 * 1
                                                }
                
            }
        else if indexPath.section == 2 {

               if collapse {
                                               return 55
                                            }else{
                                               return 168
                                            }
            
        }
            else if indexPath.section == 3 {

                   if collapse {
                                                   return 55
                                                }else{
                                                   return 672
                                                }
                
            }
         else if indexPath.section == 4 {
                             if collapse {
                                 return 55
                              }else{
                                 return 140
                              }
        } else
         {
            if collapse {
                                                       return 55
                                                    }else{
                                                       return 300
                                                    }        }
       }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1
        {
            let collapse = collapsedarray[section]

            if collapse {
                return 1
            }else{
                return StockProfileModel.shared.days_5_data.count + 2
            }
        }else
        {
        return 1
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.titleArray.count
    }
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let collapse = collapsedarray[indexPath.section]

        if indexPath.section == 0 {

            let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath) as! searchCell
            cell.date.text = ""

            if collapse {
                cell.viewConstant.constant = 55
             }else{
                cell.viewConstant.constant = 270.0
             }
            cell.consistencyranking.text = StockProfileModel.shared.rank + " / " + StockProfileModel.shared.stock_count
            cell.score.text = StockProfileModel.shared.total
            cell.tprice.attributedText = StockProfileModel.shared.trigger_price.italic(cell.tprice.font.pointSize)
            cell.priceclose.text = StockProfileModel.shared.future_closing + "" + " (" +  StockProfileModel.shared.price_dufference + ")"
            cell.lastexpiry.text = StockProfileModel.shared.percent_change_since_last_expiry + "%"
            cell.wap.text = StockProfileModel.shared.wap
            cell.highlow.text = StockProfileModel.shared.high + " / " + StockProfileModel.shared.low
            cell.volume.text = StockProfileModel.shared.volume
            cell.dma.text = StockProfileModel.shared.dma_40 + "" + " (" +  StockProfileModel.shared.volume_difference + ")"
            cell.selectionStyle = .none
            
            return cell
        }else if indexPath.section == 1 {

          
             if indexPath.row == 0
             {
                let cell = tableView.dequeueReusableCell(withIdentifier: "collapsedcell", for: indexPath) as! collapsedcell
                                          if collapse {
                                            cell.label.isHidden = true
                                                  }else{
                                                     cell.label.isHidden = false
                                                  }
                return cell

             }
            else if indexPath.row == 1
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "oifirstCell", for: indexPath) as! oifirstCell
                          

                cell.symbol.text = "Date"
               cell.expiry.text = "Volume \n ('000)"
               cell.lastexpiry.text = "OI (in Lacs)"
               cell.oiexpiry.text = "▵OI (in Lacs)"
               cell.lastweek.text = "Price Close (in Rs)"
                return cell

            }else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "oifirstCell", for: indexPath) as! oifirstCell
//                if collapse {
//                                                    cell.viewConstant.constant = 55
//                                                 }else{
//                                                    cell.viewConstant.constant = 66 * 5
//                                                 }
            let object : DayFiveItem = StockProfileModel.shared.days_5_data[indexPath.row - 2]
            cell.symbol.text = object.timestamp_finsparc_bhavcopy
            cell.expiry.text = object.volume
            cell.lastexpiry.text = object.open_int
            cell.oiexpiry.text = object.chg_in_oi
            cell.lastweek.text = object.close
                
                cell.selectionStyle = .none
                return cell

            }
        }
        else if indexPath.section == 2 {

            let cell = tableView.dequeueReusableCell(withIdentifier: "oiCell", for: indexPath) as! oiCell
            cell.date.text = ""

            if collapse {
                       cell.viewConstant.constant = 55
                    }else{
                       cell.viewConstant.constant = 168
                    }
            cell.current.text = StockProfileModel.shared.current_oi_sum
                       cell.ltd.text = StockProfileModel.shared.change_oi
                       cell.day.text = StockProfileModel.shared.change_oi_5_days
                       cell.highlow.text = StockProfileModel.shared.oi_high_11 + " / " + StockProfileModel.shared.oi_low_11
            
            cell.selectionStyle = .none
            return cell
        }else if indexPath.section == 3 {

            let cell = tableView.dequeueReusableCell(withIdentifier: "OptionCustomCell", for: indexPath) as! OptionCustomCell
            cell.company = company
            if collapse {
                                                       cell.viewConstant.constant = 55
                                                    }else{
                                                       cell.viewConstant.constant = 672
                                                    }
            cell.selectionStyle = .none
            return cell
        }else if indexPath.section == 4 {

            let cell = tableView.dequeueReusableCell(withIdentifier: "volatilityCell", for: indexPath) as! volatilityCell
            cell.date.text = ""

            cell.day.text = StockProfileModel.shared.annualise_volatility + "%"
              cell.week.text = StockProfileModel.shared.data_5 + "%"
              cell.month.text = StockProfileModel.shared.data_30 + "%"
            cell.selectionStyle = .none
            if collapse {
                                            cell.viewConstant.constant = 55
                                         }else{
                                            cell.viewConstant.constant = 140
                                         }
            return cell
        }else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "summaryCell", for: indexPath) as! summaryCell
                       cell.selectionStyle = .none
            if collapse {
                                                       cell.viewConstant.constant = 55
                                                    }else{
                                                       cell.viewConstant.constant = 300
                                                    }
            if StockProfileModel.shared.reviews.count > 0
            {
            let reviewobject = StockProfileModel.shared.reviews[0]
            let reviewattributedString: NSMutableAttributedString = NSMutableAttributedString(string: "")

                if reviewobject.future_closing > reviewobject.review_buy_trigger
            {
                let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "Stock is above trigger price \n")
                               attributedString.setColorForText(textForAttribute: "above", withColor: UIColor.green)

                reviewattributedString.append(attributedString)
                
            }else if reviewobject.future_closing < reviewobject.review_buy_trigger
            {
                let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "Stock is below trigger price \n")
                attributedString.setColorForText(textForAttribute: "below", withColor: UIColor.red)

                    reviewattributedString.append(attributedString)

            }else if reviewobject.future_closing == reviewobject.review_buy_trigger
            {
                let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "Stock is below trigger price \n")
                               attributedString.setColorForText(textForAttribute: "below", withColor: UIColor.red)
                reviewattributedString.append(attributedString)

            }

                let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "Finsparc score for the stock is " + reviewobject.total_score +  "\n")
                attributedString.setColorForText(textForAttribute: "Finsparc score for the stock is " + reviewobject.total_score, withColor: CustomColours.textGrey)
                reviewattributedString.append(attributedString)

 
                let attributedsString: NSMutableAttributedString = NSMutableAttributedString(string: "Stock ranks at " + reviewobject.rank + " out of " + reviewobject.stock_count + "\n")
                               attributedsString.setColorForText(textForAttribute: "Finsparc score for the stock is " + reviewobject.total_score, withColor: CustomColours.textGrey)
                reviewattributedString.append(attributedsString)


                if reviewobject.review_itm_call == 1
                {
                    let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "NTM Call (" + reviewobject.itm_call_strike + ") of the stock is +ve \n")
                    attributedString.setColorForText(textForAttribute: "+ve", withColor: UIColor.green)
                        reviewattributedString.append(attributedString)
                                    }else
                {
                    let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "NTM Call (" + reviewobject.itm_call_strike + ") of the stock is -ve \n")
                                       attributedString.setColorForText(textForAttribute: "-ve", withColor: UIColor.red)

                    reviewattributedString.append(attributedString)

                }

                    if reviewobject.review_itm_put == 1
                    {
                   // cell.review.text = "NTM put (" + reviewobject.itm_put_strike + ") of the stock is -ve"
                        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "NTM put (" + reviewobject.itm_put_strike + ") of the stock is -ve \n")
                        attributedString.setColorForText(textForAttribute: "-ve", withColor: UIColor.green)
                            reviewattributedString.append(attributedString)
                    }else
                    {
                       // cell.review.text = "NTM put (" + reviewobject.itm_put_strike + ") of the stock is +ve"
                        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "NTM put (" + reviewobject.itm_put_strike + ") of the stock is +ve \n")
                        attributedString.setColorForText(textForAttribute: "+ve", withColor: UIColor.red)
                            reviewattributedString.append(attributedString)

                    }
           

            if reviewobject.expiry == 1
            {
                let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "Stock is above last expiry price \n")
                                              attributedString.setColorForText(textForAttribute: "above", withColor: UIColor.green)

                                                  reviewattributedString.append(attributedString)

                
            }else
            {

                    let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "Stock is below last expiry price \n")
                                                                 attributedString.setColorForText(textForAttribute: "above", withColor: UIColor.red)

                                                                reviewattributedString.append(attributedString)

            }
          
            if reviewobject.percent_10 > 0
            {

                 let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "Stock has gained " + String(reviewobject.percent_10) + " in the last 10 day \n")
                                                                                    attributedString.setColorForText(textForAttribute: "gained " + String(reviewobject.percent_10) + "%" , withColor: UIColor.green)

                                                                                        reviewattributedString.append(attributedString)

            }else
            {
               // cell.review.text = "Stock has lost " + String(reviewobject.percent_10) + "% in the last 10 day"

                   let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "Stock has lost " + String(reviewobject.percent_10) + " in the last 10 day \n")
                    attributedString.setColorForText(textForAttribute: "lost " + String(reviewobject.percent_10) + "%" , withColor: UIColor.red)

                        reviewattributedString.append(attributedString)
            }

                    if reviewobject.ce_contract == 1
                    {
                        let attrs1 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 14), NSAttributedString.Key.foregroundColor : CustomColours.textGrey]

                                           let attrs2 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 13), NSAttributedString.Key.foregroundColor : UIColor.green]

                        let attributedString1 = NSMutableAttributedString(string:"Highest volume Call (" + reviewobject.max_contract_ce + ") of the stock is ", attributes:attrs1 as [NSAttributedString.Key : Any])

                                           let attributedString2 = NSMutableAttributedString(string:"+ve \n", attributes:attrs2 as [NSAttributedString.Key : Any])

                                           attributedString1.append(attributedString2)
                                           reviewattributedString.append(attributedString1)
                    }else
                    {
                        let attrs1 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 14), NSAttributedString.Key.foregroundColor : CustomColours.textGrey]

                                                              let attrs2 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 14), NSAttributedString.Key.foregroundColor : UIColor.red]

                        let attributedString1 = NSMutableAttributedString(string:"Highest volume Call (" + reviewobject.max_contract_ce + ") of the stock is ", attributes:attrs1 as [NSAttributedString.Key : Any])

                                                              let attributedString2 = NSMutableAttributedString(string:"-ve \n", attributes:attrs2 as [NSAttributedString.Key : Any])

                                                              attributedString1.append(attributedString2)
                                                              reviewattributedString.append(attributedString1)

                    }
            
                    if reviewobject.pe_contract == 1
                    {
                        let attrs1 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 14), NSAttributedString.Key.foregroundColor : CustomColours.textGrey]

                                           let attrs2 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 13), NSAttributedString.Key.foregroundColor : UIColor.green]

                        let attributedString1 = NSMutableAttributedString(string:"Highest volume Put (" + reviewobject.min_contract_pe + ") of the stock is ", attributes:attrs1 as [NSAttributedString.Key : Any])

                                           let attributedString2 = NSMutableAttributedString(string:"+ve \n", attributes:attrs2 as [NSAttributedString.Key : Any])

                                           attributedString1.append(attributedString2)
                                           reviewattributedString.append(attributedString1)
                    }else
                    {
                        let attrs1 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 14), NSAttributedString.Key.foregroundColor : CustomColours.textGrey]

                                                              let attrs2 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 14), NSAttributedString.Key.foregroundColor : UIColor.red]

                        let attributedString1 = NSMutableAttributedString(string:"Highest volume Put (" + reviewobject.min_contract_pe + ") of the stock is ", attributes:attrs1 as [NSAttributedString.Key : Any])

                                                              let attributedString2 = NSMutableAttributedString(string:"-ve\n", attributes:attrs2 as [NSAttributedString.Key : Any])

                                                              attributedString1.append(attributedString2)
                                                              reviewattributedString.append(attributedString1)

                    }
          
                if reviewobject.wap_call == 1
                {
                    let attrs1 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 14), NSAttributedString.Key.foregroundColor : CustomColours.textGrey]

                                       let attrs2 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 14), NSAttributedString.Key.foregroundColor : UIColor.green]

                    let attributedString1 = NSMutableAttributedString(string:"Overall Call position in the stock is", attributes:attrs1 as [NSAttributedString.Key : Any])

                                       let attributedString2 = NSMutableAttributedString(string:"+ve \n", attributes:attrs2 as [NSAttributedString.Key : Any])

                                       attributedString1.append(attributedString2)
                                       reviewattributedString.append(attributedString1)
                }else
                {
                    let attrs1 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 14), NSAttributedString.Key.foregroundColor : CustomColours.textGrey]

                                                          let attrs2 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 14), NSAttributedString.Key.foregroundColor : UIColor.red]

                    let attributedString1 = NSMutableAttributedString(string:"Overall Call position in the stock is", attributes:attrs1 as [NSAttributedString.Key : Any])

                                                          let attributedString2 = NSMutableAttributedString(string:"-ve \n", attributes:attrs2 as [NSAttributedString.Key : Any])

                                                          attributedString1.append(attributedString2)
                                                          reviewattributedString.append(attributedString1)

                }
                if reviewobject.wap_put == 1
                {
                    let attrs1 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 14), NSAttributedString.Key.foregroundColor : CustomColours.textGrey]

                                       let attrs2 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 14), NSAttributedString.Key.foregroundColor : UIColor.green]

                    let attributedString1 = NSMutableAttributedString(string:"Overall Put position in the stock is ", attributes:attrs1 as [NSAttributedString.Key : Any])

                                       let attributedString2 = NSMutableAttributedString(string:"-ve \n", attributes:attrs2 as [NSAttributedString.Key : Any])

                                       attributedString1.append(attributedString2)
                                       reviewattributedString.append(attributedString1)
                }else
                {
                    let attrs1 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 14), NSAttributedString.Key.foregroundColor : CustomColours.textGrey]

                                                          let attrs2 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 14), NSAttributedString.Key.foregroundColor : UIColor.red]

                    let attributedString1 = NSMutableAttributedString(string:"Overall Put position in the stock is ", attributes:attrs1 as [NSAttributedString.Key : Any])

                                                          let attributedString2 = NSMutableAttributedString(string:"+ve \n", attributes:attrs2 as [NSAttributedString.Key : Any])

                                                          attributedString1.append(attributedString2)
                                                          reviewattributedString.append(attributedString1)

                }
                let innereview = reviewobject.prices
                let attributedssString: NSMutableAttributedString = NSMutableAttributedString(string: "The stock has made 11 days High/Low of Rs " + innereview[0].price_high + " and Rs " + innereview[0].price_low)
                                                             attributedssString.setColorForText(textForAttribute: "above \n", withColor: UIColor.green)

                                                                 reviewattributedString.append(attributedssString)
            
            cell.review.attributedText = reviewattributedString
            }
                       return cell
        }
    }
    
}

extension watchlistSubdetailVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let collapse = !collapsedarray[indexPath.section]
        if collapse {
             expand = false
         }else{
             expand = true
         }
         print("old",collapsedarray)
         collapsedarray[indexPath.section] = collapse
         print("new",collapsedarray)
        self.tableview.reloadData()
        
    }
}
// MARK: - Section Header Delegate
//
extension watchlistSubdetailVC: CollapsibleViewHeaderDelegate {
    
    func togglesSection(_ header: customsearchHeader, section: Int) {
        
       let collapse = !collapsedarray[section]
       if collapse {
            expand = false
        }else{
            expand = true
        }
        print("old",collapsedarray)
        collapsedarray[section] = collapse
        print("new",collapsedarray)

       // header.setCollapsed(expand)
        
        tableview.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
    }
    
}
