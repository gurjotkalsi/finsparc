//
//  PortNav.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 30/05/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class PortNav: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.leftBarButtonItem = nil
        self.navigationController?.navigationItem.leftBarButtonItem = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.leftBarButtonItem = nil
        self.navigationController?.navigationItem.leftBarButtonItem = nil
    }
}
