//
//  tltVCViewController.swift
//  FinSpark
//
//  Created by Minkle Garg on 05/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class tltVCViewController: UIViewController {
    
    @IBOutlet var tableview: UITableView!
    
    var collapsedSize = CGFloat(55)
    var expandedSize = CGFloat(375)
    
    var expand = true
    
    var prevIndex : IndexPath?
    var currentIndex : IndexPath?
    
    var subtitleArray = ["Futures", "Options"]
    var titleArray = ["Current Portfolio", "Past Performance"]
    var collapsedarray = [false, false]
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.tableview.dataSource = self
        self.tableview.delegate = self
        
        let nib = UINib(nibName: "customHeader", bundle: nil)
        tableview.register(nib, forHeaderFooterViewReuseIdentifier: "customHeader")
        tableview.register(UINib(nibName: "TLTCell", bundle: nil), forCellReuseIdentifier: "TLTCell")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
        
        self.LeftBarButton(image_name: "menu", button_text: "  Ten Lot Trades", text_color: .white) {
            self.ShowMenu()
        }
        self.getFNOData()
    }
    
    //MARK:-
    
    func getFNOData() {
        
        FNOPulseModel.shared.process { (fmodl) in
            self.tableview.reloadData()
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           if segue.identifier == "futureDetail" {
               if let vc = segue.destination as? TLTCurrentDetailPageVC {
                   vc.heading = sender as! String
               }
           }
       }
    
}

extension tltVCViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if collapsedarray[section] {
            return 2
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = self.tableview.dequeueReusableHeaderFooterView(withIdentifier: "customHeader") as! customHeader
        header.topTitle.text =  titleArray[section]
        header.topTitle.isUserInteractionEnabled = true
        header.setCollapsed(expand)
        if collapsedarray[section] {
            header.singleLabel.isHidden = true
            header.arrowIcon.image = UIImage(named: "up")
        }else{
            header.singleLabel.isHidden = false
            header.arrowIcon.image = UIImage(named: "down")
        }
        
        header.section = section
        header.delegate = self
        let tapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(headerTapped(_:))
        )
        header.tag = section
        header.addGestureRecognizer(tapGestureRecognizer)
        return header
    }
    
    @objc func headerTapped(_ sender: UITapGestureRecognizer?) {
        guard let section = sender?.view?.tag else {
            return
        }
        if collapsedarray[section] {
            self.collapsedarray[section] = false
        }else{
            self.collapsedarray[section] = true
        }
        self.tableview.reloadData()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TLTCell", for: indexPath) as! TLTCell
        cell.selectionStyle = .none
        cell.symbol_value.text = subtitleArray[indexPath.row]
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            if indexPath.row == 0
            {
            performSegue(withIdentifier: "futureDetail", sender: "Current Future")
            }
            else
            {
                performSegue(withIdentifier: "futureDetail", sender: "Current Option")
            }
        }else{
            performSegue(withIdentifier: "options", sender: nil)
        }
    }
    
}
// MARK: - Section Header Delegate
extension tltVCViewController: CollapsibleTableViewHeaderDelegate {
    
    func toggleSection(_ header: customHeader, section: Int) {
        let collapse = !collapsedarray[section]
        
        if collapse {
            expand = false
        }else{
            expand = true
        }
        
        collapsedarray[section] = collapse
        tableview.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
    }
    
}
