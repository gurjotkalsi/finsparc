//
//  FSNotesVC.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 08/02/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class FSNotesVC: UIViewController {
    
    @IBOutlet weak var collctionviewTopTabs: UICollectionView!
    @IBOutlet weak var notesTableView: UITableView!
    
    @IBOutlet var ourview_lbl: UILabel!
    @IBOutlet var tableHeight: NSLayoutConstraint!
    
    let rowHeight = CGFloat(50)
    var titlesArray = ["Daily", "Weekly"]
    var particularsArray = ["Price Close", "(Weekly)", "High", "Low", "OI"]

    var selection = NSMutableArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collctionviewTopTabs.register(UINib(nibName: "topTabCell", bundle: nil), forCellWithReuseIdentifier: "topTabCell")
        let indexpath = 0
        selection.add(indexpath)
        
        collctionviewTopTabs.reloadData()
        
        self.navigationController?.isNavigationBarHidden = false
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
        self.LeftBarButton(image_name: "menu", button_text: "  FS Notes", text_color: .white) {
            self.ShowMenu()
        }
        
//        notesTableView.register(UINib(nibName: "weeklyNotesCell", bundle: nil), forCellReuseIdentifier: "weeklyNotesCell")
//
//        notesTableView.register(UINib(nibName: "fsnotesweekCell", bundle: nil), forCellReuseIdentifier: "fsnotesweekCell")

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.webservice()
    }
    
    func webservice() {
        self.AddLoader()
        FSNotesModel.shared.process { (modl) in
            
            self.RemoveLoader()
            self.LayoutTable()
            self.notesTableView.reloadData()
        }
    }
    
    func LayoutTable() {
        if self.selection.contains(0) {
            self.ourview_lbl.attributedText = FSNotesModel.shared.dailyView.htmlToAttributedString
        }else{
            self.ourview_lbl.attributedText = FSNotesModel.shared.weeklyView.htmlToAttributedString
        }
        
        var height = CGFloat(0)
        if selection.contains(0) {
            height = CGFloat(FSNotesModel.shared.daily.count) * rowHeight
        }else{
            height = 300.0
        }
        UIView.animate(withDuration: 0.3) {
            self.tableHeight.constant = height
            self.view.layoutIfNeeded()
        }
    }
    
}

extension FSNotesVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selection.contains(0) {
            return FSNotesModel.shared.daily.count
        }else{
            return 1
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if selection.contains(0) {
                return 1
        }else
        {
                return 2
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if selection.contains(0)
        {
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "weeklyNotesCell", for: indexPath) as! weeklyNotesCell

            let item = FSNotesModel.shared.daily[indexPath.row]
           cell1.particular_lbl.text = item.Particulars
           cell1.ltd_lbl.text = item.ltd
           cell1.prev_day_lbl.text = item.prev_day
            return cell1
        }else
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "fsnotesweekCell", for: indexPath) as! fsnotesweekCell

            if indexPath.section == 0
            {
            let item = FSNotesModel.shared.fsnotesweek[0]
                cell.heading.text = "Nifty Future"
            cell.pc1_value.text = item.curr_nifty_close
            cell.pc2_value.text = item.prev_nifty_close
                
                if  let weeklyvalue = Float(item.current_nifty_weekly_change)
                {
                if weeklyvalue < 0
                {
                    cell.weekly1_value.textColor = .red
                    }else
                {
                    cell.weekly1_value.textColor = CustomColours.green
                    }
                }
                
                if  let weeklyvalue = Float(item.prev_nifty_weekly_change)
                {
                if weeklyvalue < 0
                {
                    cell.weekly2_value.textColor = .red
                    }else
                {
                    cell.weekly2_value.textColor = CustomColours.green
                    }
                }
            cell.weekly1_value.text = item.current_nifty_weekly_change
            cell.weekly2_value.text = item.prev_nifty_weekly_change
            cell.high1_value.text = item.current_nifty_high
            cell.high2_value.text = item.prev_nifty_high
            cell.low1_value.text = item.current_nifty_low
            cell.low2_value.text = item.prev_nifty_low
            cell.oi1_value.text = item.curr_nifty_oi
            cell.oi2_value.text = item.prev_nifty_oi
            }else
            {
                let item = FSNotesModel.shared.fsnotesweek[1]
                cell.heading.text = "Bank Nifty Future"

              cell.pc1_value.text = item.curr_nifty_close
              cell.pc2_value.text = item.prev_nifty_close
              cell.weekly1_value.text = item.current_nifty_weekly_change
              cell.weekly2_value.text = item.prev_nifty_weekly_change
              cell.high1_value.text = item.current_nifty_high
              cell.high2_value.text = item.prev_nifty_high
              cell.low1_value.text = item.current_nifty_low
              cell.low2_value.text = item.prev_nifty_low
              cell.oi1_value.text = item.curr_nifty_oi
              cell.oi2_value.text = item.prev_nifty_oi
            }
            return cell

        }
      // return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if selection.contains(0)
        {
            return 50
        }
        else
        {
            return 150
        }
    }
}

extension FSNotesVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titlesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3.3, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.collctionviewTopTabs.dequeueReusableCell(withReuseIdentifier: "topTabCell", for: indexPath) as! topTabCell
        cell.lbl_Headings.text = self.titlesArray[indexPath.row]
        cell.lbl_Headings.roundUp()
        
        if selection.contains(indexPath.row) {
            cell.lbl_Headings.backgroundColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
            cell.lbl_Headings.textColor = UIColor.white
            
        }else{
            cell.lbl_Headings.backgroundColor = UIColor.clear
            cell.lbl_Headings.textColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selection.removeAllObjects()
        selection.add(indexPath.row)
        collectionView.reloadData()
        self.notesTableView.reloadData()
        self.LayoutTable()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
        
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
