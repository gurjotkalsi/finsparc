//
//  fsnotesweekCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 16/07/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class fsnotesweekCell: UITableViewCell {
    @IBOutlet weak var heading: UILabel!

    @IBOutlet weak var pc1_value: UILabel!
    @IBOutlet weak var pc2_value: UILabel!
    @IBOutlet weak var weekly1_value: UILabel!
    @IBOutlet weak var weekly2_value: UILabel!
    @IBOutlet weak var high1_value: UILabel!
    @IBOutlet weak var high2_value: UILabel!
    @IBOutlet weak var low1_value: UILabel!
    @IBOutlet weak var low2_value: UILabel!
    @IBOutlet weak var oi1_value: UILabel!
    @IBOutlet weak var oi2_value: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
