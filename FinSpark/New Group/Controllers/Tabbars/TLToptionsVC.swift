//
//  TLToptionsVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 05/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class TLToptionsVC: UIViewController {
    
    @IBOutlet var tableview: UITableView!
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableview.dataSource = self
        self.tableview.delegate = self
        
        tableview.register(UINib(nibName: "TLTCell", bundle: nil), forCellReuseIdentifier: "TLTCell")
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sliderHolder.fadeOut {}
        
        self.tabBarController?.tabBar.isHidden = true
        self.CustomizeNavBar_with(title: "", button_text: "Past Performance", bartint: UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1), backButton: true) {
            self.navigationController?.popViewController(animated: true)
            sliderHolder.fadeIn { }
        }
        self.getoptionData()
    }
    
    //MARK:-
    func getoptionData() {
        self.AddLoader()
        TLTMonthModel.shared.process { (tltmodl) in
            self.RemoveLoader()
            self.tableview.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pastdetailPage" {
            if let vc = segue.destination as? TLTdetailPage {
                vc.dateHeading = sender as! String
            }
        }
    }
    
}



extension TLToptionsVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TLTMonthModel.shared.dates.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TLTCell", for: indexPath) as! TLTCell
        cell.selectionStyle = .none
        cell.symbol_value.text = TLTMonthModel.shared.dates[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "pastdetailPage", sender: TLTMonthModel.shared.dates[indexPath.row])
    }
    
}
