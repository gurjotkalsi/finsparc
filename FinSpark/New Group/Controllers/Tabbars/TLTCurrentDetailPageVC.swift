//
//  TLTCurrentDetailPageVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 05/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class TLTCurrentDetailPageVC: UIViewController {
    
    @IBOutlet weak var perlotProfitTableView: UITableView!
    var heading = ""
    @IBOutlet var pname: UILabel!
       @IBOutlet var pbooked: UILabel!
       @IBOutlet var pmtm: UILabel!
    var titlesArray = ["All", "Industry", "Nifty50"]
    var selection = NSMutableArray();
    var mrktModel = MarketModel()
    var dateHeading = ""
    //MARK:-
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        sliderHolder.fadeIn { }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.CustomizeNavBar_with(title: heading, button_text: dateHeading, bartint: CustomColours.blue, backButton: true) {
            self.navigationController?.popViewController(animated: true)
        }
        
        self.getMarketList()
        
        sliderHolder.fadeOut {}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true

       
        perlotProfitTableView.register(UINib(nibName: "perlotProfitCell", bundle: nil), forCellReuseIdentifier: "perlotProfitCell")
    }
    //MARK:-

    func getMarketList() {
        self.AddLoader()
        
//        MarketModel().getList { (mkmodel) in
//
//            self.RemoveLoader()
//            self.mrktModel = mkmodel
//            self.perlotProfitTableView.reloadData()
//        }
        StockLTModel.shared.process(urltype: "Current", stockNumber: "", date: "", type: "") { (model) in
            self.RemoveLoader()
            self.perlotProfitTableView.reloadData()
        }

    }
    
}

//MARK:- UITableView Delegates -
extension TLTCurrentDetailPageVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return mrktModel.list.count
        return StockLTModel.shared.list.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "perlotProfitCell", for: indexPath) as! perlotProfitCell
        cell.selectionStyle = .none
        
        let object = StockLTModel.shared.list[indexPath.row]
        cell.symbol_value.text = "\(object.instrument_name) \(object.selling_date)"
               cell.symbol_value.font = UIFont(name: "assistant-regular", size: 12.5)
               cell.price_value.text = object.quantity
               cell.ltp_value.text = object.bprice
               cell.today_plp_value.text  = object.sprice
               cell.five_day_plp_value.text = object.booked.getTwoDecimalString()
               
               cell.symbol_value.textColor = .black
               cell.price_value.textColor = .black
               cell.ltp_value.textColor = .black
               cell.today_plp_value.textColor = .black
               
               if let close_5 = NSInteger(object.booked) {
                   if close_5 < 0 {
                       cell.five_day_plp_value.textColor = .red
                   }else{
                       cell.five_day_plp_value.textColor = .green
                   }
               }
               
//        cell.symbol_value.text = "\(object.symbol)\n\(object.lot)"
//        
//        let attrs1 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 13), NSAttributedString.Key.foregroundColor : CustomColours.textGrey]
//
//        let attrs2 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 12), NSAttributedString.Key.foregroundColor : UIColor.red]
//
//        let attributedString1 = NSMutableAttributedString(string:object.symbol, attributes:attrs1 as [NSAttributedString.Key : Any])
//
//        let attributedString2 = NSMutableAttributedString(string:"\n" + "(" + object.lot + ")", attributes:attrs2 as [NSAttributedString.Key : Any])
//
//        attributedString1.append(attributedString2)
//        cell.symbol_value.attributedText = attributedString1
//        cell.price_value.textColor = CustomColours.green
//
//        if let close = NSInteger(object.close) {
//            if close < 0
//                  {
//                      cell.price_value.textColor = .red
//                  }
//        }
//        
//        cell.price_value.text = object.close
//        cell.ltp_value.textColor = CustomColours.green
//        cell.today_plp_value.textColor = CustomColours.green
//        cell.five_day_plp_value.textColor = CustomColours.green
//
//        if let ltpvalue = NSInteger(object.lotcon) {
//            if ltpvalue < 0
//                  {
//                      cell.ltp_value.textColor = .red
//                  }
//        }
//      
//        cell.ltp_value.text = object.lotcon
//        
//        if let plpvalue = NSInteger(object.diff_in_close) {
//            if plpvalue < 0
//            {
//                cell.today_plp_value.textColor = .red
//            }
//        }
//       // print("plpvalue",plpvalue as Any)
//        
//        
//        if let close_5 = NSInteger(object.diff_in_close_5) {
//            if close_5 < 0
//                   {
//                       cell.five_day_plp_value.textColor = .red
//                   }
//        }
//       
//        cell.today_plp_value.text  = object.diff_in_close
//        cell.five_day_plp_value.text = object.diff_in_close_5
        
        return cell
    }
    
}

