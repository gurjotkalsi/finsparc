//
//  TLTdetailPage.swift
//  FinSpark
//
//  Created by Minkle Garg on 05/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class TLTdetailPage: UIViewController {
    
    @IBOutlet weak var perlotProfitTableView: UITableView!
    @IBOutlet var lbl_date: UILabel!
    
    var dateHeading = ""
    var type = "past"
    
    //MARK:-
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        sliderHolder.fadeIn { }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.CustomizeNavBar_with(title: "", button_text: dateHeading, bartint: CustomColours.blue, backButton: true) {
            self.navigationController?.popViewController(animated: true)
        }
        
        self.getMarketList()
        sliderHolder.fadeOut {}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true

        perlotProfitTableView.register(UINib(nibName: "perlotProfitCell", bundle: nil), forCellReuseIdentifier: "perlotProfitCell")
    }
    //MARK:-

    func getMarketList() {
        self.AddLoader()
        
        StockLTModel.shared.process(urltype: "past", stockNumber: LoginModel().localData().finsparc_user_id, date: dateHeading, type: type) { (model) in
            self.RemoveLoader()
            self.perlotProfitTableView.reloadData()
        }
        
    }
    
}


//MARK:- UITableView Delegates -
extension TLTdetailPage: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return StockLTModel.shared.list.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "perlotProfitCell", for: indexPath) as! perlotProfitCell
        cell.selectionStyle = .none
        
        let object = StockLTModel.shared.list[indexPath.row]
        cell.symbol_value.text = "\(object.instrument_name) \(object.selling_date)"
        cell.symbol_value.font = UIFont(name: "assistant-regular", size: 12.5)
        cell.price_value.text = object.quantity
        cell.ltp_value.text = object.bprice
        cell.today_plp_value.text  = object.sprice
        cell.five_day_plp_value.text = object.booked.getTwoDecimalString()
        
        cell.symbol_value.textColor = .black
        cell.price_value.textColor = .black
        cell.ltp_value.textColor = .black
        cell.today_plp_value.textColor = .black
        
        if let close_5 = NSInteger(object.booked) {
            if close_5 < 0 {
                cell.five_day_plp_value.textColor = .red
            }else{
                cell.five_day_plp_value.textColor = .green
            }
        }
        
        return cell
    }
    
}

