//
//  HomeVC.swift
//  Sales Tracker
//
//  Created by Gurjot Kalsi on 13/01/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit
import Charts


class HomeVC: UIViewController {
    
    @IBOutlet var chartView: BarChartView!
    
    @IBOutlet var scrollview: UIScrollView!
    @IBOutlet var contentview: UIView!
    @IBOutlet var graphview: UIView!
    
    @IBOutlet var perlotprofitview: UIView!
    @IBOutlet var valueview: UIView!
    @IBOutlet var eodview: UIView!
    
    @IBOutlet var collectionview: UICollectionView!
    
    @IBOutlet var nifty_btn: UIButton!
    @IBOutlet var nifty_bank_btn: UIButton!
    
    @IBOutlet var lbl_profit_money: UILabel!
    @IBOutlet var btn_profit_percentage: UIButton!
    @IBOutlet var lbl_date: UILabel!
    
    @IBOutlet var mmi_lbl: UILabel!
    @IBOutlet var mmi_score_lbl: UILabel!
    @IBOutlet var mmi_date_lbl: UILabel!
    
    @IBOutlet var graph_cmoney_lbl: UILabel!
    @IBOutlet var graph_percentage: UIButton!
    @IBOutlet var graph_date_lbl: UILabel!
    
    @IBOutlet var advance_lbl: UILabel!
    @IBOutlet var decline_lbl: UILabel!
    @IBOutlet var unchanged_lbl: UILabel!
    
    @IBOutlet var poster: UIImageView!
    weak var axisFormatDelegate: IAxisValueFormatter?

    var xData: [String] = ["1 Dec", "2 Dec", "3 Dec", "4 Dec", "5 Dec"]
//    let yPoints = [12199.55, 12138, 12072.35, 12191.7, 12139.6]
    
        //var xData: [String] = []
        var yPoints : [Double] = []
    
    var title_array = ["FNO Pulse", "Fll Data", "Stock Rankings", "Score Card", "GBR", "Daily Data", "OI Changes", "IV Changes", "Future \nHigh/Lows", "Call", "Put", "Call/Put \nRatios"]
    
    var image_array = ["Group 404", "Group 410", "Group 403", "Group 423", "Group 418", "Group 383", "oichanges", "ivchanges", "future high-lows", "call", "put", "calputratio"]
    
    var loadOnlyOnce = true
    
    //MARK:-
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if loadOnlyOnce {
            loadOnlyOnce = false
            xData.removeAll()
            yPoints.removeAll()
            self.getHomeData()
        }

        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
        
        self.LeftBarButton(image_name: "menu", button_text: "  FinSparc", text_color: .white) {
            self.ShowMenu()
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
        
        chartView.delegate = self
        chartView.noDataText = "No data..."

       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.updategraph()

    }
    
    func setChart(dataPoints: [String], values: [Double]) {
        
        var dataEntries: [BarChartDataEntry] = []
        
        let firshighlight =  Double(0)
        var secondHighlight =  Double(0)
        
        if values.count != 0
        {
            secondHighlight = values[0]
        }
        for i in 0..<dataPoints.count {
            let entry = BarChartDataEntry(x: Double(i), y: values[i] , data: xData as AnyObject?)
            dataEntries.append(entry)
        }
        
        let chartDataSet = BarChartDataSet(entries: dataEntries, label: "Finsparc")
        let lightGray = UIColor.init(red: 0.9216, green: 0.9216, blue: 0.9216, alpha: 1)
        chartDataSet.colors = [lightGray]
        chartDataSet.drawValuesEnabled = true
        chartDataSet.highlightColor = UIColor.init(red: 0.1961, green: 0.3255, blue: 0.4667, alpha: 1)
        chartDataSet.highlightAlpha = 1
        
        if values.count > 0
        {
        let miny : Double = values[values.count - 1]

        chartView.leftAxis.axisMinimum = miny - 1000.0
        }else
        {
            chartView.leftAxis.axisMinimum = 0
        }
//        chartView.highlightValue(x: 10807.55, dataSetIndex: 4, stackIndex: 4)
//        chartDataSet.highlightEnabled = true
        let data = BarChartData(dataSet: chartDataSet)
        data.barWidth = 0.3
        chartView.data = data
        let hi = Highlight(x:firshighlight, y: secondHighlight, dataSetIndex: 0)
        chartView.highlightValue(hi, callDelegate: true)
      //  chartView.highlightValue(x: values [0], dataSetIndex: 0)

    }
       
    
    override func viewDidLayoutSubviews() {
        self.poster.addRoundCorners(8)
        self.nifty_btn.roundUp()
        self.nifty_bank_btn.roundUp()
        self.graphview.addShadow(radius: 5)
        self.perlotprofitview.addShadow(radius: 5)
        self.valueview.addShadow(radius: 5)
        self.eodview.addShadow(radius: 5)
    }
    
    //MARK:-
    func getHomeData() {
        self.AddLoader()
        HomeModel.shared.process { (hmodel) in
            //- MMIScoreModel Data
            self.mmi_lbl.text = HomeModel.shared.feargreed.curr_mmi
            self.mmi_score_lbl.text = "Score " + HomeModel.shared.feargreed.curr_score
            self.mmi_date_lbl.text = HomeModel.shared.feargreed.cdate
            
            //- GraphModel Model
            let percentage2 = HomeModel.shared.nifty.nifty_change_percent.getTwoDecimalString()
            
            self.graph_percentage.setTitle("  \(percentage2)%", for: .normal)
            self.graph_cmoney_lbl.text = HomeModel.shared.nifty.nifty_close.getTwoDecimalString()
            self.graph_date_lbl.text = HomeModel.shared.nifty.cdate
            //GRAPH IS PENDING
            
            for object in HomeModel.shared.nifty.graph
            {
                if let weekday = self.getDayOfWeek(object.date) {
                    print(weekday)
                    self.xData.append(weekday)

                } else {
                    print("bad input")
                }
                let yvalue  = Double(object.close)
                self.yPoints.append(yvalue!)
            }
            //- PortfolioData Model
            let percentage = HomeModel.shared.perlotprofit.plp_per.getTwoDecimalString()
            
            self.lbl_profit_money.text = "\(HomeModel.shared.perlotprofit.market_now.getTwoDecimalString())"
            if let value = Float(HomeModel.shared.perlotprofit.nifty_change_percent) {
                                      if value < 0
                                      {
                                   self.btn_profit_percentage.setImage(UIImage(named: "diagonal"), for: .normal)
                                   }else
                                          {

                                    self.btn_profit_percentage.setImage(UIImage(named: "diagonalgreen"), for: .normal)
                                   }
            }
            
            self.btn_profit_percentage.setTitle("  \(percentage)%", for: .normal)
            self.lbl_date.text = HomeModel.shared.perlotprofit.date
              if let value = Float(HomeModel.shared.perlotprofit.nifty_change_percent) {
                                                                          if value < 0
                                                                          {
                                                                             self.btn_profit_percentage.tintColor = .red

                                     }else
                                           {
                                                                             
                                                                             self.btn_profit_percentage.tintColor = CustomColours.green
                                     }
                                 }            //- stock data
            self.advance_lbl.text = "Advances: \(HomeModel.shared.stockinfo.adavanced)"
            self.decline_lbl.text = "Declines: \(HomeModel.shared.stockinfo.declined)"
            self.unchanged_lbl.text = "Unchanged: \(HomeModel.shared.stockinfo.unchanged)"
            
            self.resetNiftyFutures()
            self.nifty_btn.backgroundColor = CustomColours.blue
            self.nifty_btn.setTitleColor(.white, for: .normal)
            
            self.RemoveLoader()
            self.updategraph ()

        }
    }
    func getDayOfWeek(_ today:String) -> String? {
               let inputFormatter = DateFormatter()
                inputFormatter.dateFormat = "dd-MM-yyyy"
                let showDate = inputFormatter.date(from: today)
                inputFormatter.dateFormat = "EEE"
                let resultString = inputFormatter.string(from: showDate!)

       return resultString
        
    }
    func updategraph ()
    {
        print("xData", xData)
        print("yPoints", yPoints)

                let xAxis = chartView.xAxis
                xAxis.labelPosition = .bottom
                xAxis.labelFont = .systemFont(ofSize: 12)
        //        xAxis.granularity = 1.6
                xAxis.labelCount = xData.count
        let customformatter = ChartStringFormatter()
        customformatter.nameValues = xData //anything you want
                xAxis.valueFormatter = customformatter
        xAxis.granularity = 1

                xAxis.drawGridLinesEnabled = false
                
                let leftAxis = chartView.leftAxis
                leftAxis.labelFont = .systemFont(ofSize: 12)
                leftAxis.labelCount = yPoints.count
                leftAxis.labelPosition = .outsideChart
                if yPoints.count > 0
                {
                    
                        leftAxis.axisMinimum = 0
                }
                leftAxis.spaceBottom = 5
                leftAxis.drawGridLinesEnabled = true
                
                let rightAxis = chartView.rightAxis
                rightAxis.enabled = false
                
                let l = chartView.legend
                chartView.drawValueAboveBarEnabled = true
                l.enabled = false
                
                setChart(dataPoints: xData, values: yPoints)
    }
    func resetNiftyFutures() {
        
        self.nifty_btn.setTitleColor(.black, for: .normal)
        self.nifty_bank_btn.setTitleColor(.black, for: .normal)
        self.nifty_btn.backgroundColor = .clear
        self.nifty_bank_btn.backgroundColor = .clear
    }
    @IBAction func lotprofitAction() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "perlotProfitVC") as! perlotProfitVC
        vc.expirydate = HomeModel.shared.perlotprofit.expiry_dt
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    //MARK:- IBActions -
    
    @IBAction func ChangeNiftyAction(_ sender: UIButton) {
        
        self.resetNiftyFutures()
        sender.backgroundColor = CustomColours.blue
        sender.setTitleColor(.white, for: .normal)
        
        if sender == self.nifty_btn {
            self.lbl_profit_money.text = "Rs. \(HomeModel.shared.perlotprofit.current_nifty.getTwoDecimalString())"
            self.btn_profit_percentage.setTitle("  \(HomeModel.shared.perlotprofit.nifty_change_percent.getTwoDecimalString())%", for: .normal)
            
            if let value = Float(HomeModel.shared.perlotprofit.nifty_change_percent) {
                                                                if value < 0
                                                                {
                                                                    self.btn_profit_percentage.setImage(UIImage(named: "diagonal"), for: .normal)

                                                                    self.btn_profit_percentage.setTitleColor(.red, for: .normal)

                           }else
                                 {
                                                                   self.btn_profit_percentage.setImage(UIImage(named: "diagonalgreen"), for: .normal)

                                    self.btn_profit_percentage.setTitleColor(CustomColours.green, for: .normal)
                           }
                       }
            self.lbl_date.text = HomeModel.shared.perlotprofit.date
            
            
            let percentage = HomeModel.shared.nifty.nifty_change_percent.getTwoDecimalString()
            self.graph_percentage.setTitle("  \(percentage)%", for: .normal)
            self.graph_cmoney_lbl.text = HomeModel.shared.nifty.nifty_close.getTwoDecimalString()
            self.graph_date_lbl.text = HomeModel.shared.nifty.cdate
            
            self.xData = []
                       self.yPoints = []
                       for object in HomeModel.shared.nifty.graph
                       {
                           if let weekday = self.getDayOfWeek(object.date) {
                               print(weekday)
                               self.xData.append(weekday)

                           } else {
                               print("bad input")
                           }
                           let yvalue  = Double(object.close)
                           self.yPoints.append(yvalue!)
                       }
        }else{
            self.lbl_profit_money.text = "Rs. \(HomeModel.shared.perlotprofit.current_banknifty.getTwoDecimalString())"
            self.btn_profit_percentage.setTitle("  \(HomeModel.shared.perlotprofit.banknifty_change_percent.getTwoDecimalString())%", for: .normal)
            if let value = Float(HomeModel.shared.perlotprofit.banknifty_change_percent) {
                                                     if value < 0
                                                     {
                                                        self.btn_profit_percentage.setImage(UIImage(named: "diagonal"), for: .normal)

                                                        self.btn_profit_percentage.setTitleColor(.red, for: .normal)

                }else
                      {
                                                        self.btn_profit_percentage.setImage(UIImage(named: "diagonalgreen"), for: .normal)

                                                        self.btn_profit_percentage.setTitleColor(CustomColours.green, for: .normal)
                }
            }
            self.lbl_date.text = HomeModel.shared.perlotprofit.date
            
            let percentage = HomeModel.shared.bankNifty.banknifty_change_percent.getTwoDecimalString()
            self.graph_percentage.setTitle("  \(percentage)%", for: .normal)
            self.graph_cmoney_lbl.text = HomeModel.shared.bankNifty.bn_close.getTwoDecimalString()
            self.graph_date_lbl.text = HomeModel.shared.bankNifty.cdate
            self.xData = []
            self.yPoints = []
            for object in HomeModel.shared.bankNifty.graph
            {
                if let weekday = self.getDayOfWeek(object.date) {
                    print(weekday)
                    self.xData.append(weekday)

                } else {
                    print("bad input")
                }
                let yvalue  = Double(object.close)
                self.yPoints.append(yvalue!)
            }
            
        }
        
        self.updategraph()

    }
    
}

extension HomeVC: ChartViewDelegate {
    
}


extension HomeVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/4, height: collectionView.frame.height/4.4)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionview.dequeueReusableCell(withReuseIdentifier: "IconCell", for: indexPath) as! IconCell
        cell.icon.image = UIImage(named: image_array[indexPath.item])
        cell.title.text = title_array[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let identifier = title_array [indexPath.row]
        performSegue(withIdentifier: identifier, sender: nibName)
    }
}

 class ChartStringFormatter: NSObject, IAxisValueFormatter  {

   var nameValues: [String]! =  ["A", "B", "C", "D"]

    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return String(describing: nameValues[Int(value)])
    }
}
