//
//  WatchlistVC.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 08/02/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class WatchlistVC: UIViewController {
    @IBOutlet weak var borderedView: UIView!
    @IBOutlet weak var collctionviewTopTabs: UICollectionView!
    @IBOutlet weak var watchlistTableView: UITableView!
    @IBOutlet weak var addButton: UIButton!

  //

    var titlesArray = ["My Watchlist", "Nifty 50", "Top Gainers", "Top Losers"]
    var selection = NSMutableArray();

    
    //MARK:-
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
//        self.RightBarButton(image_name: "search_white") {
//        let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "searchVC") as! searchVC
//        self.navigationController?.pushViewController(searchVC, animated: true)
//        }
        self.borderedView.setNeedsDisplay()
        self.borderedView.setNeedsLayout()
        self.borderedView.layoutSubviews()
        self.LeftBarButton(image_name: "menu", button_text: "  Watchlist", text_color: .white) {
                    self.ShowMenu()

        }
        
    }
   
    override func viewDidLayoutSubviews ()
    {
        //_____________ add Border-------------------
        let border = CAShapeLayer();
          border.strokeColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1).cgColor
        
          border.fillColor = nil;
          border.lineDashPattern = [8, 6];
          border.path = UIBezierPath(rect: borderedView.bounds).cgPath
          border.frame = borderedView.bounds;
          self.borderedView.layer.addSublayer(border);
        self.borderedView.layer.cornerRadius = 8
        self.borderedView.layoutIfNeeded()
        //---------------------------------------------
        
    }
    // 250 5
    // diwan 5
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addButton.setUpcircularButton()
        
        self.navigationController?.isNavigationBarHidden = false
                      self.navigationController?.navigationBar.isTranslucent = false
                      self.navigationController?.navigationBar.barTintColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
        
        
        collctionviewTopTabs.register(UINib(nibName: "topTabCell", bundle: nil), forCellWithReuseIdentifier: "topTabCell")
        let indexpath = 0
        selection.add(indexpath)
        collctionviewTopTabs.reloadData()
        
        watchlistTableView.register(UINib(nibName: "watchlistCell", bundle: nil), forCellReuseIdentifier: "watchlistCell")
       
        let searchBtn = UIButton(type: .custom)
        searchBtn.setImage(UIImage(named: "search_white"), for: .normal)
        searchBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        searchBtn.addTarget(self, action: #selector(searchAction), for: .touchUpInside)
       // let item2 = UIBarButtonItem(customView: searchBtn)

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: searchBtn)
//        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
//            //   button.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.15)
//             //  button.backgroundColor = .red
//       button.setImage(UIImage(named: "search_white"), for: .normal)
//       button.fitImage()
//       button.actionHandle(controlEvents: .touchUpInside) {
//        let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "searchVC") as! searchVC
//              self.navigationController?.pushViewController(searchVC, animated: true)
//       }
//
//               self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
//               self.navigationItem.rightBarButtonItem?.tintColor = .clear
        //self.LeftBartMenuItem()
    }
    @IBAction func searchAction(_ sender: UIButton)
    {
     let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "searchVC") as! searchVC
    self.navigationController?.pushViewController(searchVC, animated: true)
    }
}
//MARK: Collectionview Methods
extension WatchlistVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,  UITableViewDelegate, UITableViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let label = UILabel(frame: CGRect.zero)
//        label.text = self.titlesArray[indexPath.item]
//        label.sizeToFit()
//        return CGSize(width: label.frame.width + 20, height: 35)
//    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           return CGSize(width: collectionView.frame.width / 3.3, height: collectionView.frame.height)
       }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.collctionviewTopTabs.dequeueReusableCell(withReuseIdentifier: "topTabCell", for: indexPath) as! topTabCell
        cell.lbl_Headings.text = self.titlesArray[indexPath.row]
        cell.lbl_Headings.roundUp()
        cell.lbl_Headings.textAlignment = .center
        if selection.contains(indexPath.row) {
        cell.lbl_Headings.backgroundColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
        cell.lbl_Headings.textColor = UIColor.white

           }else{
        cell.lbl_Headings.backgroundColor = UIColor.clear
        cell.lbl_Headings.textColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)

           }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if selection.contains(indexPath.row) {
            selection.removeAllObjects()
        }else{
            selection.removeAllObjects()
            selection.add(indexPath.row)
        }
        collectionView.reloadData()
    }
    // Layout: Set Edges
//       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//           // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
//           return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10) // top, left, bottom, right
//       }
//    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfSectionsInTableView section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "watchlistCell", for: indexPath) as! watchlistCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "detailview", sender: nil)
    }
    
}
