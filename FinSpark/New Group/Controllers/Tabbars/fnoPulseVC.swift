//
//  fnoPulseVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 01/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class fnoPulseVC: UIViewController {

    @IBOutlet var tableview: UITableView!
    @IBOutlet var expiry_date: UILabel!
    @IBOutlet var creation_date: UILabel!
    
    var collapsedSize = CGFloat(55)
    var expandedSize = CGFloat(375)
     
    var expand = true
    
    var prevIndex : IndexPath?
    var currentIndex : IndexPath?
    
    var titleArray = ["Market Today", "FS Fear & Greed Index", "FS Fear and Greed Chart"]
    var collapsedarray = [false, false, false]
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.tableview.tableFooterView = UIView()

        self.tableview.dataSource = self
        self.tableview.delegate = self
        
        let nib = UINib(nibName: "customHeader", bundle: nil)
        tableview.register(nib, forHeaderFooterViewReuseIdentifier: "customHeader")
        tableview.register(UINib(nibName: "fnopulseCell", bundle: nil), forCellReuseIdentifier: "fnopulseCell")
        tableview.register(UINib(nibName: "fsGearCell", bundle: nil), forCellReuseIdentifier: "fsGearCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sliderHolder.fadeOut { }

        self.CustomizeNavBar_with(title: "", button_text: "FNO Pulse", bartint: UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1), backButton: true) {
                   self.navigationController?.popViewController(animated: true)
                   sliderHolder.fadeIn { }
               }
        self.getFNOData()
    }
    
    //MARK:-
   
    func getFNOData() {
        FNOPulseModel.shared.process { (fmodl) in
            
            self.expiry_date.text =  "Expiry Date : \(self.Convertorequireformat(fmodl.cdate))" //self.Convertorequireformat(fmodl.cdate)
            self.creation_date.text = "\(self.Convertorequireformat(fmodl.expiry)) (EOD)"
            self.tableview.reloadData()
        }
    }
    func Convertorequireformat (_ date : String ) -> String{
        let inputFormatter = DateFormatter()
           inputFormatter.dateFormat = "yyyy-MM-dd"
           let showDate = inputFormatter.date(from: date)
           inputFormatter.dateFormat = "dd-MM-yyyy"
           let resultString = inputFormatter.string(from: showDate!)
          return resultString
    }

}
extension fnoPulseVC : UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 230
        }else {
            if indexPath.row == 0 {
                return 40
            } else {
                return 53
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let collapse = collapsedarray[section]
        if !collapse {
             return 0
         }else{
            if section == 0 {
                return 2
            }else if section == 1 {
                return FNOPulseModel.shared.fear_greed_index.list.count + 1
            }else{
                return 0
            }
         }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = self.tableview.dequeueReusableHeaderFooterView(withIdentifier: "customHeader") as! customHeader
        header.topTitle.text =  titleArray[section]
        header.setCollapsed(expand)
        header.section = section + 100
        header.delegate = self
        let tapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(headerTapped(_:))
        )
        header.tag = section
        header.addGestureRecognizer(tapGestureRecognizer)
        return header
    }
    
    @objc func headerTapped(_ sender: UITapGestureRecognizer?) {
        guard let section = sender?.view?.tag else {
            return
        }
        if section == 2
         {
        performSegue(withIdentifier: "gridchart", sender: nil)
        }
        else
        {
            if collapsedarray[section]
            {
                self.collapsedarray[section] = false
            }else
               {
                   self.collapsedarray[section] = true
               }
                           self.tableview.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "fnopulseCell", for: indexPath) as! fnopulseCell
            cell.selectionStyle = .none
            
            if indexPath.row == 0 {
                //Nifty
                cell.fno_title.text = "NIFTY\nFUTURES"
                if FNOPulseModel.shared.list.count > 0
                {
                let attribText = NSAttributedString(string: "\(FNOPulseModel.shared.list[0].nifty_close)\n(\(FNOPulseModel.shared.list[0].change))", attributes: [NSAttributedString.Key.foregroundColor : UIColor.red])
                cell.fno_title_value.attributedText = attribText
                
                cell.oi_value.text = "\(FNOPulseModel.shared.list[0].current_sum_nifty_oi)\n(\(FNOPulseModel.shared.list[0].change_oi_nifty))"
                
                cell.volume_value.text = "\(FNOPulseModel.shared.list[0].volume_nifty)\n (\(FNOPulseModel.shared.list[0].volume_niftypercent)%)"
                print("FNOPulseModel.shared.list[0].volume_niftypercent",FNOPulseModel.shared.list[0].volume_niftypercent)
                cell.score_value.text = FNOPulseModel.shared.list[0].score_nf
                
                cell.call_ratio_value.text = FNOPulseModel.shared.list[0].call_ratio_nf
                cell.call_ratio_pulse.text = FNOPulseModel.shared.list[0].curr_nifty_wap_call
                
                cell.put_ratio_value.text = FNOPulseModel.shared.list[0].put_ratio_nf
                cell.put_ratio_pulse.text = FNOPulseModel.shared.list[0].curr_nifty_wap_put
                    let close = Float(FNOPulseModel.shared.list[0].nifty_close)
                    let trigger = Float(FNOPulseModel.shared.list[0].nifty_close)

                    if let value = Float(FNOPulseModel.shared.list[0].nifty_close)
                            {

                    }
                    
                }else
                {
                    cell.fno_title_value.text = ""
                    
                    cell.oi_value.text = ""
                    
                    cell.volume_value.text = ""
                    
                    cell.score_value.text = ""
                    
                    cell.call_ratio_value.text = ""
                    cell.call_ratio_pulse.text = ""
                    
                    cell.put_ratio_value.text = ""
                    cell.put_ratio_pulse.text = ""
                }
                
            } else {
                //Bank Nifty
                cell.fno_title.text = "BANK NIFTY\nFUTURES"
                if FNOPulseModel.shared.list.count > 0
                               {
                               let attribText = NSAttributedString(string: "\(FNOPulseModel.shared.list[1].nifty_close)\n(\(FNOPulseModel.shared.list[1].change))", attributes: [NSAttributedString.Key.foregroundColor : UIColor.red])
                               cell.fno_title_value.attributedText = attribText
                               
                               cell.oi_value.text = "\(FNOPulseModel.shared.list[1].current_sum_nifty_oi)\n(\(FNOPulseModel.shared.list[1].change_oi_nifty))"
                               
                               cell.volume_value.text = "\(FNOPulseModel.shared.list[1].volume_nifty)\n (\(FNOPulseModel.shared.list[1].volume_niftypercent)%)"
                               print("FNOPulseModel.shared.list[0].volume_niftypercent",FNOPulseModel.shared.list[1].volume_niftypercent)
                               cell.score_value.text = FNOPulseModel.shared.list[1].score_nf
                               
                               cell.call_ratio_value.text = FNOPulseModel.shared.list[1].call_ratio_nf
                               cell.call_ratio_pulse.text = FNOPulseModel.shared.list[1].curr_nifty_wap_call
                               
                               cell.put_ratio_value.text = FNOPulseModel.shared.list[1].put_ratio_nf
                               cell.put_ratio_pulse.text = FNOPulseModel.shared.list[1].curr_nifty_wap_put
                               }else
                {
                    cell.fno_title_value.text = ""
                    
                    cell.oi_value.text = ""
                    
                    cell.volume_value.text = ""
                    
                    cell.score_value.text = ""
                    
                    cell.call_ratio_value.text = ""
                    cell.call_ratio_pulse.text = ""
                    
                    cell.put_ratio_value.text = ""
                    cell.put_ratio_pulse.text = ""
                }
            }
            
            return cell

        } else if indexPath.section == 1 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "fsGearCell", for: indexPath) as! fsGearCell
            cell.selectionStyle = .none
            
            if indexPath.row % 2 == 0 {
                cell.backgroundVew.backgroundColor =  UIColor(red: 0.8784, green: 0.9137, blue: 0.9647, alpha: 1)
            } else {
                cell.backgroundVew.backgroundColor = .clear
            }
            
            if indexPath.row == 0 {
                cell.parameterLabel.text = "Some Parameters:-"
                cell.ltdLabel.text = "LTD"
                cell.dayLabel.text = "Prev Day"
                
                cell.backgroundVew.backgroundColor = UIColor(red: 0.9059, green: 0.9059, blue: 0.9059, alpha: 1)
                
            }
            else if indexPath.row == 5 || indexPath.row == 7
            {
                if FNOPulseModel.shared.fear_greed_index.list.count > 0
                               {
                           cell.parameterLabel.text = FNOPulseModel.shared.fear_greed_index.list[indexPath.row - 1].particulars
                           cell.ltdLabel.text = FNOPulseModel.shared.fear_greed_index.list[indexPath.row - 1].ltd + "%"
                           cell.dayLabel.text = FNOPulseModel.shared.fear_greed_index.list[indexPath.row - 1].prev_day + "%"
                       }
            }
            else {
                    if FNOPulseModel.shared.fear_greed_index.list.count > 0
                    {
                cell.parameterLabel.text = FNOPulseModel.shared.fear_greed_index.list[indexPath.row - 1].particulars
                cell.ltdLabel.text = FNOPulseModel.shared.fear_greed_index.list[indexPath.row - 1].ltd
                cell.dayLabel.text = FNOPulseModel.shared.fear_greed_index.list[indexPath.row - 1].prev_day
                    }
            }
            
            return cell
            
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "fsGearCell", for: indexPath) as! fsGearCell
            cell.selectionStyle = .none
            
            if indexPath.row % 2 == 0 {
                cell.parameterLabel.text = "Change in Nifty (Five days)"
                cell.backgroundVew.backgroundColor =  UIColor(red: 0.8784, green: 0.9137, blue: 0.9647, alpha: 1)
                
            } else {
                cell.parameterLabel.text = "Change in Nifty (EOD)"
                cell.backgroundVew.backgroundColor = .clear
            }
            
            if indexPath.row == 0 {
                cell.parameterLabel.text = "Some Parameters:-"
                
                cell.ltdLabel.text = "LTD"
                cell.parameterLabel.text = "Prev Day"
                
                cell.backgroundVew.backgroundColor = UIColor(red: 0.9059, green: 0.9059, blue: 0.9059, alpha: 1)
                
            } else {
//                cell.ltdLabel.text = FNOPulseModel.shared.fear_greed_index.list[indexPath.row].ltd
//                cell.parameterLabel.text = FNOPulseModel.shared.fear_greed_index.list[indexPath.row].prev_day
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
}


// MARK: - Section Header Delegate
extension fnoPulseVC: CollapsibleTableViewHeaderDelegate {
    
    func toggleSection(_ header: customHeader, section: Int) {
        let collapse = !collapsedarray[section]
        
        if collapse {
            expand = false
        }else{
            expand = true
        }
        
        collapsedarray[section] = collapse
        tableview.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
    }
    
}
