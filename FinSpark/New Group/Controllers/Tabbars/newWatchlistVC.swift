//
//  newWatchlistVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 22/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextFields

class newWatchlistVC: UIViewController {
    
    @IBOutlet var newatchlist_tf: MDCTextField!
    
    var newatchlist_tfControl:MDCTextInputControllerUnderline!

    override func viewDidLoad() {
        super.viewDidLoad()
        newatchlist_tfControl = newatchlist_tf.floatingTextField()
    }
    
    
    @IBAction func UpdateAction(_ sender: UIButton) {
        if sender.tag == 1 {
            
            if newatchlist_tf.text!.isEmpty {
                self.ShowToast(message: "Watchlist name required!")
                
            }else{
                self.AddLoader()

                let parameters : NSDictionary = ["user_id":LoginModel().localData().finsparc_user_id, "id":"0", "watchlist":newatchlist_tf.text!]
                
                WatchlistModel.shared.create(params: parameters) { (model) in
                    self.RemoveLoader()
                    self.newatchlist_tf.text = ""
                    self.dismiss(animated: true, completion: nil)
                }
            }
            
        }else{
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
}
