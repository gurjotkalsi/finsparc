//
//  fnopulseCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 04/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class fnopulseCell: UITableViewCell {
    
    @IBOutlet var topTitle: UILabel!
    @IBOutlet var arrowIcon: UIImageView!
    @IBOutlet var boxArea: UIView!
    
    @IBOutlet var fno_title: UILabel!
    @IBOutlet var fno_title_value: UILabel!
    @IBOutlet var oi_value: UILabel!
    @IBOutlet var volume_value: UILabel!
    
    @IBOutlet var score_value: UILabel!
    @IBOutlet var call_ratio_value: UILabel!
    @IBOutlet var put_ratio_value: UILabel!
    
    @IBOutlet var call_ratio_pulse: UILabel!
    @IBOutlet var put_ratio_pulse: UILabel!
    
    @IBOutlet var status: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
