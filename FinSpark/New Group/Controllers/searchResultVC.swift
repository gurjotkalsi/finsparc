//
//  searchResultVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 11/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class searchResultVC: UIViewController {
    @IBOutlet weak var searchTableView: UITableView!
    
    //MARK:-
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.tabBarController?.tabBar.isHidden = true

        searchTableView.register(UINib(nibName: "watchlistCell", bundle: nil), forCellReuseIdentifier: "watchlistCell")

        self.CustomizeNavBar_with(title: "", button_text: "Search", bartint: UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1), backButton: true) {
            self.navigationController?.popViewController(animated: true)
        }
    }
}


//MARK: Collectionview Methods
extension searchResultVC: UITableViewDelegate, UITableViewDataSource {

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfSectionsInTableView section: Int) -> Int {
        return 1
    }
//    // Set the spacing between sections
//     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//    return 10
//    }
//
//    // Make the background color show through
//      func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//    let headerView = UIView()
//    headerView.backgroundColor = UIColor.clear
//
//    return headerView
//    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "watchlistCell", for: indexPath) as! watchlistCell
        return cell
    }
    
//    func tableview(_ tableView: UITableView, heightForHeaderIn indexPath: IndexPath) -> CGFloat {
//        return  10.0
//    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 50
//    }
    
}
