//
//  fillDataCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 24/02/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class fillDataCell: UITableViewCell {
    
    @IBOutlet weak var lbl_Headings: UILabel!
    @IBOutlet weak var lbl_ltd: UILabel!
    @IBOutlet weak var lbl_prev_day: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
