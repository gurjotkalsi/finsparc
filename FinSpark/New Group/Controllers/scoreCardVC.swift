//
//  scoreCardVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 03/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit
import iOSDropDown

class scoreCardVC: UIViewController {
    
    @IBOutlet weak var collctionviewTopTabs: UICollectionView!
    @IBOutlet weak var collctionTopTabs: UICollectionView!
    
    @IBOutlet weak var scorecardTableView: UITableView!
    
    @IBOutlet weak var created_lbl: UILabel!
    @IBOutlet weak var expiry_lbl: UILabel!
    
    @IBOutlet weak var industryButton: DropDown!
    @IBOutlet weak var selectionView: NSLayoutConstraint!
    @IBOutlet weak var industryselectionView: UIView!

    
    var titlesArray = ["Current Expiry", "Next Expiry"]
    var subArray = ["All", "Industry", "Nifty50"]
    
    var selection = NSMutableArray()
    var subselection = NSMutableArray()
    var symbols = [String]()
    var industryid = [String]()
    var industrysid = ""

    var showNextExpiry = false
    
    var currentScoreType : ScoreCardTypes = .all
    
    //Mark
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        sliderHolder.fadeOut { }
        self.CustomizeNavBar_with(title: "", button_text: "Score Card", bartint: UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1), backButton: true) {
               self.navigationController?.popViewController(animated: true)
               sliderHolder.fadeIn { }
               }
        self.getData()
        self.getInstrumentList()
    }
    //scoreCardSelectedIndustry
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        industryButton.didSelect{(selectedText , index ,id) in
            self.industryButton.text = selectedText
            if selectedText != "" {
                self.industryButton.text = selectedText
            }else{
                self.industryButton.text = "Select Industry" //("Select Industry", for: .normal)
            }
            
            self.AddLoader()
            self.industrysid = self.industryid[index]
            
            ScoreCardModel.shared.industryprocess(industryid: "\(self.industrysid)") { (modl) in
                self.RemoveLoader()
                    self.created_lbl.text = "\(self.Convertorequireformat (modl.cdate)) (EOD)"
                self.expiry_lbl.text = "Expiry \(self.Convertorequireformat (modl.current_expiry))"

                self.scorecardTableView.reloadData()
            }
            
        }

        collctionviewTopTabs.register(UINib(nibName: "topTabCell", bundle: nil), forCellWithReuseIdentifier: "topTabCell")
        let indexpath = 0
        selection.add(indexpath)
        collctionviewTopTabs.reloadData()
        
        collctionTopTabs.register(UINib(nibName: "topTabCell", bundle: nil), forCellWithReuseIdentifier: "topTabCell")
        let subindexpath = 0
        subselection.add(subindexpath)
        collctionTopTabs.reloadData()
        
        scorecardTableView.register(UINib(nibName: "scoreCardCell", bundle: nil), forCellReuseIdentifier: "scoreCardCell")
       
        industryselectionView.isHidden = true
        selectionView.constant = 0
    }
    
    func getInstrumentList() {
        self.AddLoader()
        StockRankingModel.shared.getIndustries { (model) in
           self.RemoveLoader()
            if model.industryList.count != 0 {
            print("inside")
             for obj in model.industryList
                              {
                                  self.symbols.append(obj.name)
                                  self.industryid.append(obj._id)
                              }
            print("ininside")
            self.industryButton.optionArray = self.symbols
          }
        }
           // }
    }
    
    func getData() {
        self.AddLoader()
        ScoreCardModel.shared.process(type: currentScoreType, completion: { (modl) in
            self.RemoveLoader()
            //DispatchQueue.main.async {
                self.created_lbl.text = "\(self.Convertorequireformat (modl.cdate)) (EOD)"
            self.expiry_lbl.text = "Expiry \(self.Convertorequireformat (modl.current_expiry))"
                self.scorecardTableView.reloadData()
           // }
        })
    }
    func Convertorequireformat (_ date : String ) -> String{
              let inputFormatter = DateFormatter()
                 inputFormatter.dateFormat = "yyyy-MM-dd"
                 let showDate = inputFormatter.date(from: date)
                 inputFormatter.dateFormat = "dd MMM yyyy"
                 let resultString = inputFormatter.string(from: showDate!)
                return resultString
          }
}

extension scoreCardVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if showNextExpiry {
            return ScoreCardModel.shared.next_list.count
        }else{
            return ScoreCardModel.shared.list.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "scoreCardCell", for: indexPath) as! scoreCardCell
        // cell.trend_Label.backgroundColor = UIColor(patternImage: UIImage(named: "dots")!)
        cell.selectionStyle = .none
        
        var item = ScoreCardItem()
        if showNextExpiry {
            item = ScoreCardModel.shared.next_list[indexPath.row]
        }else{
            item = ScoreCardModel.shared.list[indexPath.row]
        }
        
        cell.symbol_lbl.text = item.symbol
        cell.score_lbl.text = item.score
        cell.rank_lbl.text = item.rank
        cell.price_lbl.text = "\(item.price_close.getTwoDecimalString()) (\(item.percent_price_change.getTwoDecimalString())%)"
        cell.expiry_lbl.text = item.change_expiry_percent.getTwoDecimalString() + "%"
        cell.dots.image = UIImage(named: "dots_\(item.seven_days_trends)")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var item = ScoreCardItem()
        if showNextExpiry {
            item = ScoreCardModel.shared.next_list[indexPath.row]
        }else{
            item = ScoreCardModel.shared.list[indexPath.row]
        }
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "watchlistSubdetailVC") as! watchlistSubdetailVC
        vc.company = item.symbol
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- Collectionview Methods -
extension scoreCardVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collctionviewTopTabs {
            return 2
        }
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collctionviewTopTabs {
            let cell = self.collctionviewTopTabs.dequeueReusableCell(withReuseIdentifier: "topTabCell", for: indexPath) as! topTabCell
            cell.lbl_Headings.text = self.titlesArray[indexPath.row]
            cell.lbl_Headings.roundUp()

            if selection.contains(indexPath.row) {
                cell.lbl_Headings.backgroundColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
                cell.lbl_Headings.textColor = UIColor.white
                
            }else{
                cell.lbl_Headings.backgroundColor = UIColor.clear
                cell.lbl_Headings.textColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
            }
            return cell
            
        } else {
            let cell = self.collctionTopTabs.dequeueReusableCell(withReuseIdentifier: "topTabCell", for: indexPath) as! topTabCell
            cell.lbl_Headings.roundUp()
            cell.lbl_Headings.text = self.subArray[indexPath.row]
            
            if subselection.contains(indexPath.row) {
                cell.lbl_Headings.backgroundColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
                cell.lbl_Headings.textColor = UIColor.white
                
            } else {
                cell.lbl_Headings.backgroundColor = UIColor.clear
                cell.lbl_Headings.textColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collctionviewTopTabs {
            selection.removeAllObjects()
            selection.add(indexPath.row)
            
            if indexPath.item == 0 {
                
                self.expiry_lbl.text = "Expiry \(ScoreCardModel.shared.current_expiry)"
                self.showNextExpiry = false
            }else{
                self.expiry_lbl.text = "Expiry \(ScoreCardModel.shared.next_expiry)"
                self.showNextExpiry = true
            }
            self.scorecardTableView.reloadData()
            
        } else {
            
            if indexPath.item == 0 {
                currentScoreType = .all
                self.getData()

            }else if indexPath.item == 1 {
                currentScoreType = .industry
            }else{
                currentScoreType = .nifty
                self.getData()

            }
            if indexPath.item == 1 {
                selectionView.constant = 68
                industryselectionView.isHidden = false
            } else {
                selectionView.constant = 0
                industryselectionView.isHidden = true
            }
            
            subselection.removeAllObjects()
            subselection.add(indexPath.row)
        }
        
        collectionView.reloadData()
        
    }
    
    // Layout: Set Edges
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0) // top, left, bottom, right
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3.3, height: collectionView.frame.height)
    }
    
}
