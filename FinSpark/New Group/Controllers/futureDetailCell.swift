//
//  futureDetailCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 04/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class futureDetailCell: UITableViewCell,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    var typeofPuttext: String = ""

    @IBOutlet weak var collectionView: UICollectionView!
    
    var DataValues = GBRItem()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.register(UINib(nibName: "topTabCell", bundle: nil), forCellWithReuseIdentifier: "topTabCell")

        typeofPuttext = UserDefaults.standard.string(forKey: "type")!
        print("text",self.typeofPuttext)
        
        // Initialization code
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "topTabCell", for: indexPath as IndexPath) as! topTabCell
        
        if indexPath.row == 0 {
            cell.lbl_Headings.text = DataValues.symbol
        }else if indexPath.row == 1 {
            cell.lbl_Headings.text = DataValues.total
        }else if indexPath.row == 2 {
            cell.lbl_Headings.text = DataValues.rank
        }else if indexPath.row == 3 {
            cell.lbl_Headings.attributedText = DataValues.trigger_price.getTwoDecimalString().italic(cell.lbl_Headings.font.pointSize)
        }else if indexPath.row == 4 {
            cell.lbl_Headings.text = DataValues.future_closing
            print(DataValues.future_closing)
        }else {
            cell.lbl_Headings.text = DataValues.percent_change_last_days_price
        }
        cell.lbl_Headings.roundUp()
        cell.lbl_Headings.font = UIFont(name: "Assistant-Regular", size: 14)
        cell.lbl_Headings.textColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
        cell.lbl_Headings.numberOfLines = 0
        cell.lbl_Headings.textAlignment = .center

        if indexPath.row == 0 || indexPath.row == 4 {
            
            if indexPath.row == 0 {
//                cell.lbl_Headings.textAlignment = .left
            }else if indexPath.row == 4 {
//                cell.lbl_Headings.textAlignment = .right
            }

            cell.lbl_Headings.lineBreakMode = .byWordWrapping
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineHeightMultiple = 0.84
            
            if indexPath.row == 0 {
                cell.lbl_Headings.attributedText = NSMutableAttributedString(string: DataValues.symbol, attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
            } else {
               // cell.lbl_Headings.textAlignment = .right
                cell.lbl_Headings.attributedText = NSMutableAttributedString(string: DataValues.future_closing, attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
            }            
        } else {
//            cell.lbl_Headings.textAlignment = .right
        }
        
        if indexPath.row == 1  {
            cell.lbl_Headings.font = UIFont(name: "Assistant-Bold", size: 14)
        }else if indexPath.row == 2 && self.typeofPuttext == "green" {
            cell.lbl_Headings.textColor = UIColor(red: 0.192, green: 0.758, blue: 0.882, alpha: 1)
        }else if indexPath.row == 4 {
           // cell.lbl_Headings.textAlignment = .center
            cell.lbl_Headings.textColor =  UIColor(red: 0.274, green: 0.726, blue: 0, alpha: 1)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.size.width - 40) / 6
        return CGSize(width: width , height: width)
    }
    
   func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       collectionView.reloadData()
   }
    
    // Layout: Set Edges
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
      return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
  }

}
