//
//  perlotProfitVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 18/02/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit
import iOSDropDown

class perlotProfitVC: UIViewController {
    
    @IBOutlet weak var collctionviewTopTabs: UICollectionView!
    @IBOutlet weak var perlotProfitTableView: UITableView!
    @IBOutlet var lbl_date: UILabel!
    
    @IBOutlet weak var industryButton: DropDown!
    @IBOutlet weak var selectionView: NSLayoutConstraint!
    @IBOutlet weak var industryselectionView: UIView!

    var selectedoption = "symbol"
    var symbols = [String]()
    var industryid = [String]()
    var industrysid = ""
    
    var titlesArray = ["All", "Industry", "Nifty50"]
    var selection = NSMutableArray();
    var mrktModel = MarketModel()
    var expirydate = ""
    //MARK:-
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        sliderHolder.fadeIn { }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lbl_date.text = "Expiry Date: " + expirydate
        self.CustomizeNavBar_with(title: "", button_text: "PER LOT PROFIT (PLP)", bartint: CustomColours.blue, backButton: true) {
            self.navigationController?.popViewController(animated: true)
        }
        sliderHolder.fadeOut {}

        self.getMarketList()
        self.getInstrumentList()

    }
    
    func getInstrumentList() {
        self.AddLoader()
        StockRankingModel.shared.getIndustries { (model) in
           self.RemoveLoader()
            if model.industryList.count != 0 {
            print("inside")
             for obj in model.industryList
                              {
                                  self.symbols.append(obj.name)
                                  self.industryid.append(obj._id)
                              }
            print("ininside")
            self.industryButton.optionArray = self.symbols
          }
        }
           // }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        industryselectionView.isHidden = true
              selectionView.constant = 0
        collctionviewTopTabs.register(UINib(nibName: "topTabCell", bundle: nil), forCellWithReuseIdentifier: "topTabCell")
        let indexpath = 0
        selection.add(indexpath)
        collctionviewTopTabs.reloadData()
        perlotProfitTableView.register(UINib(nibName: "perlotProfitCell", bundle: nil), forCellReuseIdentifier: "perlotProfitCell")
        
        industryButton.didSelect{(selectedText , index ,id) in
            self.industryButton.text = selectedText
            self.selectedoption = selectedText
            if selectedText != "" {
                self.industryButton.text = selectedText
            }else{
                self.industryButton.text = "Select Industry" //("Select Industry", for: .normal)
            }
            
            self.AddLoader()
            self.industrysid = self.industryid[index]
                  self.getMarketList()

            
        }    }
    //MARK:-

    func getMarketList() {
        self.AddLoader()
        
        MarketModel().getList(symbolname: selectedoption) { (mkmodel) in
            
            self.RemoveLoader()
            self.mrktModel = mkmodel
            self.perlotProfitTableView.reloadData()
        }
        
    }
    
}

//MARK:- UITableView Delegates -
extension perlotProfitVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mrktModel.list.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "perlotProfitCell", for: indexPath) as! perlotProfitCell
        cell.selectionStyle = .none
        
        let object = mrktModel.list[indexPath.row]
        
        cell.symbol_value.text = "\(object.symbol)\n\(object.lot)"
        
        let attrs1 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 12.5), NSAttributedString.Key.foregroundColor : CustomColours.textGrey]

        let attrs2 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 12), NSAttributedString.Key.foregroundColor : UIColor.red]

        let attributedString1 = NSMutableAttributedString(string:object.symbol, attributes:attrs1 as [NSAttributedString.Key : Any])

        let attributedString2 = NSMutableAttributedString(string:"\n" + "(" + object.lot + ")", attributes:attrs2 as [NSAttributedString.Key : Any])

        attributedString1.append(attributedString2)
        cell.symbol_value.attributedText = attributedString1
        cell.price_value.textColor = CustomColours.green
        cell.symbol_value.textAlignment = .left
        if let close = NSInteger(object.close) {
            if close < 0
                  {
                      cell.price_value.textColor = .red
                  }
        }
        
        cell.price_value.text = object.close
        cell.ltp_value.textColor = CustomColours.green
        cell.today_plp_value.textColor = CustomColours.green
        cell.five_day_plp_value.textColor = CustomColours.green

        if let ltpvalue = NSInteger(object.lotcon) {
            if ltpvalue < 0
                  {
                      cell.ltp_value.textColor = .red
                  }
        }
      
      //  cell.ltp_value.text = object.lotcon
        cell.ltp_value.text = ""
        if let plpvalue = NSInteger(object.diff_in_close) {
            if plpvalue < 0
            {
                cell.today_plp_value.textColor = .red
            }
        }
       // print("plpvalue",plpvalue as Any)
        
        
        if let close_5 = NSInteger(object.diff_in_close_5) {
            if close_5 < 0
                   {
                       cell.five_day_plp_value.textColor = .red
                   }
        }
       
        cell.today_plp_value.text  = object.diff_in_close
        cell.five_day_plp_value.text = object.diff_in_close_5
        
        return cell
    }
    
}

//MARK:- UICollectionview Delegates -
extension perlotProfitVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = self.collctionviewTopTabs.dequeueReusableCell(withReuseIdentifier: "topTabCell", for: indexPath) as! topTabCell

        cell.lbl_Headings.text = self.titlesArray[indexPath.row]
        cell.lbl_Headings.roundUp()
        if selection.contains(indexPath.row) {
            cell.lbl_Headings.backgroundColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
            cell.lbl_Headings.textColor = UIColor.white

        }else{
            cell.lbl_Headings.backgroundColor = UIColor.clear
            cell.lbl_Headings.textColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
        }
        
        cell.layer.cornerRadius = collectionView.frame.height/2
        cell.layer.masksToBounds = true
        
        return cell
    }


    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.item == 0
        {
            selectedoption = "symbol"
        }else
        {
            selectedoption = ""
        }
        if selection.contains(indexPath.row) {
            selection.removeAllObjects()
        }else{
            selection.removeAllObjects()
            selection.add(indexPath.row)
        }
        
        if indexPath.item == 1 {
                       selectionView.constant = 68
                       industryselectionView.isHidden = false
                   } else {
                       selectionView.constant = 0
                       industryselectionView.isHidden = true
                   }
        self.getMarketList()

        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3.3, height: collectionView.frame.height)
    }

    // Layout: Set Edges
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0) // top, left, bottom, right
    }

}

    
   
