//
//  fillDataVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 01/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class fillDataVC: UIViewController {
    
    @IBOutlet weak var fillTableView: UITableView!
    @IBOutlet var expiry_lbl: UILabel!
    @IBOutlet var eod_lbl: UILabel!
    
    //MARK:-
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        sliderHolder.fadeOut {}
        self.CustomizeNavBar_with(title: "", button_text: "Fll Data", bartint: UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1), backButton: true) {
            self.navigationController?.popViewController(animated: true)
            sliderHolder.fadeIn {}
        }
        self.webservice()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true

        fillTableView.register(UINib(nibName: "fillDataCell", bundle: nil), forCellReuseIdentifier: "fillDataCell")
    }
    
    func webservice() {
        self.AddLoader()
        FillDataModel.shared.process { (modl) in
            
            self.RemoveLoader()
            if modl.status == "200" {
                print("expirydate",modl.expiry)
                self.expiry_lbl.text = "Expiry Date : \(self.Convertorequireformat(modl.expiry))"
                self.eod_lbl.text = "\(self.Convertorequireformat(modl.cdate)) (EOD)"
                self.fillTableView.reloadData()
            }else{
                self.expiry_lbl.text = "Expiry Date : --"
                self.eod_lbl.text = "-- (EOD)"
                self.ShowToast(message: modl.message)
            }
        }
    }

    func Convertorequireformat (_ date : String ) -> String{
        let inputFormatter = DateFormatter()
           inputFormatter.dateFormat = "yyyy-MM-dd"
           let showDate = inputFormatter.date(from: date)
           inputFormatter.dateFormat = "dd-MM-yyyy"
           let resultString = inputFormatter.string(from: showDate!)
          return resultString
    }
}
//MARK: Collectionview Methods
extension fillDataVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FillDataModel.shared.list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "fillDataCell", for: indexPath) as! fillDataCell
        cell.selectionStyle = .none
        
        let item = FillDataModel.shared.list[indexPath.row]
        cell.lbl_Headings.text = item.Particulars.makeBigWord()
        
        cell.lbl_ltd.attributedText = item.ltd.polarityColor()
        cell.lbl_prev_day.attributedText = item.prev_day.polarityColor()
        
        return cell
    }
    
}
