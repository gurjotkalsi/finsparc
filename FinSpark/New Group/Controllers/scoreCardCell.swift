//
//  scoreCardCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 03/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class scoreCardCell: UITableViewCell {
    @IBOutlet weak var trend_Label: UILabel!
    @IBOutlet var symbol_lbl: UILabel!
    @IBOutlet var score_lbl: UILabel!
    @IBOutlet var rank_lbl: UILabel!
    @IBOutlet var price_lbl: UILabel!
    @IBOutlet var expiry_lbl: UILabel!
    @IBOutlet var dots: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
