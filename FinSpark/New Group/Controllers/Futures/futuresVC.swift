//
//  futuresVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 04/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class futuresVC: UIViewController {
    
    @IBOutlet weak var collctionviewTopTabs: UICollectionView!
    @IBOutlet weak var futuresTableView: UITableView!
    
    var titlesArray = ["Current Expiry", "Next Expiry"]
    var selection = NSMutableArray();
    var listArray = ["Expiry High", "Expiry Low", "7 Days High", "7 Days Low","11 Days High", "11 Days Low"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        selection.add(0)
        
        collctionviewTopTabs.register(UINib(nibName: "topTabCell", bundle: nil), forCellWithReuseIdentifier: "topTabCell")
        collctionviewTopTabs.reloadData()
        
        futuresTableView.register(UINib(nibName: "futureCell", bundle: nil), forCellReuseIdentifier: "futureCell")
        
        self.webservice()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        sliderHolder.fadeOut { }
        self.CustomizeNavBar_with(title: "", button_text: "Futures High/Lows", bartint: UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1), backButton: true) {
            self.navigationController?.popViewController(animated: true)
            sliderHolder.fadeIn { }
        }
    }
    
    func webservice() {
        self.AddLoader()
        HighLowModel.shared.process { (modl) in
            self.RemoveLoader()
            self.futuresTableView.reloadData()
        }
    }
    
}

//MARK: Collectionview Methods

extension futuresVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.listArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "futureCell", for: indexPath) as! futureCell
        cell.selectionStyle = .none
        
        var count = 0
        
        if selection.contains(0) {
            
            if indexPath.section == 0 {
                count = HighLowModel.shared.current_expiry_high.count
            }else if indexPath.section == 1 {
                count = HighLowModel.shared.current_expiry_low.count
            }else if indexPath.section == 2 {
                count = HighLowModel.shared.current_seven_days_high.count
            } else if indexPath.section == 3 {
                count = HighLowModel.shared.current_seven_days_low.count
            } else if indexPath.section == 4 {
                count = HighLowModel.shared.current_eleven_days_high.count
            } else {
                //5
                count = HighLowModel.shared.current_eleven_days_low.count
            }
            
        } else {
            
            if indexPath.section == 0 {
                count = HighLowModel.shared.next_expiry_high.count
            }else if indexPath.section == 1 {
                count = HighLowModel.shared.next_expiry_low.count
            }else if indexPath.section == 2 {
                count = HighLowModel.shared.next_seven_days_high.count
            } else if indexPath.section == 3 {
                count = HighLowModel.shared.next_seven_days_low.count
            } else if indexPath.section == 4 {
                count = HighLowModel.shared.next_eleven_days_high.count
            } else {
                //5
                count = HighLowModel.shared.next_eleven_days_low.count
            }
            
        }

        
        cell.title_Label.text = "\(self.listArray[indexPath.section]) (\(count))"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HighLowDetailsVC") as! HighLowDetailsVC
        vc.createDate = HighLowModel.shared.cdate
        
        vc.number = indexPath.section
        if selection.contains(0) {
            vc.expiry = HighLowModel.shared.current_expiry
            
            if indexPath.section == 0 {
                vc.list = HighLowModel.shared.current_expiry_high
            }else if indexPath.section == 1 {
                vc.list = HighLowModel.shared.current_expiry_low
            }else if indexPath.section == 2 {
                vc.list = HighLowModel.shared.current_seven_days_high
            } else if indexPath.section == 3 {
                vc.list = HighLowModel.shared.current_seven_days_low
            } else if indexPath.section == 4 {
                vc.list = HighLowModel.shared.current_eleven_days_high
            } else {
                //5
                vc.list = HighLowModel.shared.current_eleven_days_low
            }
        }else{
            vc.expiry = HighLowModel.shared.next_expiry
            if indexPath.section == 0 {
                vc.list = HighLowModel.shared.next_expiry_high
            }else if indexPath.section == 1 {
                vc.list = HighLowModel.shared.next_expiry_low
            }else if indexPath.section == 2 {
                vc.list = HighLowModel.shared.next_seven_days_high
            } else if indexPath.section == 3 {
                vc.list = HighLowModel.shared.next_seven_days_low
            } else if indexPath.section == 4 {
                vc.list = HighLowModel.shared.next_eleven_days_high
            } else {
                //5
                vc.list = HighLowModel.shared.next_eleven_days_low
            }
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

extension futuresVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.titlesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.collctionviewTopTabs.dequeueReusableCell(withReuseIdentifier: "topTabCell", for: indexPath) as! topTabCell
        cell.lbl_Headings.text = self.titlesArray[indexPath.row]
        cell.lbl_Headings.roundUp()
        
        if selection.contains(indexPath.row) {
            cell.lbl_Headings.backgroundColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
            cell.lbl_Headings.textColor = UIColor.white
            
        }else{
            cell.lbl_Headings.backgroundColor = UIColor.clear
            cell.lbl_Headings.textColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selection.removeAllObjects()
        selection.add(indexPath.row)
        collectionView.reloadData()
        self.futuresTableView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3.3, height: collectionView.frame.height)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
        
}
