//
//  HighLowDetailsVC.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 08/05/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class HighLowDetailsVC: UIViewController {
    
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var created_lbl: UILabel!
    @IBOutlet weak var expiry_lbl: UILabel!
    
    @IBOutlet weak var second_last_label: UILabel!
    @IBOutlet weak var last_label: UILabel!
    
    var expiry = ""
    var createDate = ""
    
    var number = 0
    var list = [HighLowItem]()
    
    //MARK:-
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        //sliderHolder.fadeIn { }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        
        sliderHolder.fadeOut {
            
        }
        print("number: \(number)")
        
        if number == 0 {
           // self.second_last_label.text = "Rank"
            self.last_label.text = "Expiry High"
        }else if number == 1 {
            //self.second_last_label.text = "Rank"
            self.last_label.text = "Expiry Low"
        }else if number == 2 {
            //self.second_last_label.text = "Rank"
            self.last_label.text = "7 Days High"
        }else if number == 3 {
           // self.second_last_label.text = "Rank"
            self.last_label.text = "7 Days Low"
        }else if number == 4 {
           // self.second_last_label.text = "Rank"
            self.last_label.text = "11 Days High"
        }else {
           // self.second_last_label.text = "Rank"
            self.last_label.text = "11 Days Low"
        }
        self.CustomizeNavBar_with(title: "", button_text: self.last_label.text!, bartint: UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1), backButton: true) {
            self.navigationController?.popViewController(animated: true)
            sliderHolder.fadeIn { }
        }
        print("expiry",expiry)
        print("createDate",createDate)
        self.expiry_lbl.text = "Expiry Date \(self.Convertorequireformat(expiry) )"
        self.created_lbl.text = "\(self.Convertorequireformat(createDate)) (EOD)"
    }
    func Convertorequireformat (_ date : String ) -> String{
        let inputFormatter = DateFormatter()
           inputFormatter.dateFormat = "yyyy-MM-dd"
           let showDate = inputFormatter.date(from: date)
           inputFormatter.dateFormat = "dd MMM yy"
           let resultString = inputFormatter.string(from: showDate!)
          return resultString
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        UserDefaults.standard.set("IV", forKey: "type") //setObject
        tableview.register(UINib(nibName: "IVCell", bundle: nil), forCellReuseIdentifier: "IVCell")
    }
    //MARK:-
}


//MARK: Collectionview Methods
extension HighLowDetailsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IVCell", for: indexPath) as! IVCell
        cell.selectionStyle = .none
        let item = list[indexPath.row]
        
        cell.symbol_lbl.text = item.symbol
        cell.last_day_lbl.text = item.score
        cell.last_week_lbl.text = item.rank
        cell.last_month_lbl.attributedText = item.trigger_price.italic(cell.last_month_lbl.font.pointSize)
        
        cell.over_week_lbl.text = item.price_close + "\n(" + item.change_since_last_day + " %)"
        
        if let value = Float(item.change_since_last_day)
                {
                    if value > 0
                    {
                        cell.over_week_lbl.textColor = CustomColours.green
                    }else
                    {
                        cell.over_week_lbl.textColor = .red
                    }
                }
        if number == 0 {
            cell.over_month_lbl.text = item.expiry_high_max
        }else if number == 1 {
            cell.over_month_lbl.text = ""
        }else if number == 2 {
            cell.over_month_lbl.text = item.high_5
        }else if number == 3 {
            cell.over_month_lbl.text = item.low_5
        }else if number == 4 {
            cell.over_month_lbl.text = item.high_11
        }else {
            cell.over_month_lbl.text = item.low_11
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "watchlistSubdetailVC") as! watchlistSubdetailVC
               let item = list[indexPath.row]

        vc.company = item.symbol
        self.navigationController?.pushViewController(vc, animated: true)

    }
}
