//
//  ViewController.swift
//  Finsparc
//
//  Created by mayur on 2/5/20.
//  Copyright © 2020 Mayur Parmar. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class loginTablViewCell: UITableViewCell
{
    @IBOutlet weak var imgLogin: UIImageView!
    @IBOutlet weak var lbllogin: UILabel!
    @IBOutlet weak var txtPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtUsername: SkyFloatingLabelTextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnHideUnhidePassword: UIButton!
}

class LoginViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    //Outlets
    @IBOutlet weak var HeaderVW: UIView!
    {
        didSet
        {
            HeaderVW.setUpHeaderView()
        }
    }
   
    @IBOutlet weak var tblVW: UITableView!
    @IBOutlet weak var btnRightTopSignUp: UIButton!
    
    var app_Controller = App_Controller()
    var app_Delegate = AppDelegate()
    
    var cell = loginTablViewCell()
        
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let defaults = UserDefaults.standard
        defaults.set(true, forKey: "iconClick")
        
//       btnRightTopSignUp.titleLabel?.font = UIFont(name: self.app_Controller.App_Font_Style_SemiBold, size: CGFloat(UserDefaults.standard.integer(forKey: "Text_Size")-2))
//
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    @IBAction func onClickedForgotPassword(_ sender: UIButton)
    {
        let forgotVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(forgotVC, animated: true)
    }
    @IBAction func onClickedTopRightSignUp(_ sender: UIButton)
    {
        let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
               self.navigationController?.pushViewController(signUpVC, animated: true)
    }
    
    @IBAction func onClickedLogin(_ sender: UIButton)
    {
        if cell.txtUsername.text == "" {
            self.ShowToast(message: "Username can't be empty!")
        }else if cell.txtPassword.text == "" {
            self.ShowToast(message: "Password can't be empty!")
        }else{
            
            if isInternetConnected() {
                
                self.AddLoader()
                
                LoginModel().process(parameter: ["mobile":cell.txtUsername.text!, "password":cell.txtPassword.text!]) { (model) in
                    
                    self.RemoveLoader()
                    
                    if model.status == "200" {
                        UserDefaults.standard.set(true, forKey: Constants.OneTimeLogin)
                        UserDefaults.standard.synchronize()
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PageController") as! PageController
                        self.navigationController?.pushViewController(vc, animated: true)
                    }else{
                        self.ShowToast(message: model.msg)
                    }
                    
                }
                
            }else{
                self.ShowToast(message: "Check internet connection!")
            }
            
        }
    }
    
    @IBAction func iconAction(sender: AnyObject) {
        let defaults = UserDefaults.standard
        let iconClick = defaults.bool(forKey: "iconClick")

        defaults.set(!iconClick, forKey: "iconClick")
        tblVW.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        cell = tblVW.dequeueReusableCell(withIdentifier: "loginTablViewCell", for: indexPath) as! loginTablViewCell
        
        cell.lbllogin.font =  UIFont(name: self.app_Controller.App_Font_Style_Bold, size: CGFloat(UserDefaults.standard.integer(forKey: "Text_Size")-2))
        
        cell.btnLogin.titleLabel?.font =  UIFont(name: self.app_Controller.App_Font_Style_Bold, size: CGFloat(UserDefaults.standard.integer(forKey: "Text_Size")-2))
        cell.btnLogin.setUpButton()
        

        cell.txtUsername.lineHeight = 0.5
        cell.txtUsername.placeholderColor = .lightGray
        cell.txtUsername.selectedLineColor = .lightGray
        cell.txtUsername.selectedTitleColor = .lightGray
        cell.txtUsername.font = UIFont(name: self.app_Controller.App_Font_Style_Bold, size: CGFloat(UserDefaults.standard.integer(forKey: "Text_Size")-2))

        let defaults = UserDefaults.standard
        var iconClick = defaults.bool(forKey: "iconClick")

        if(iconClick == true) {
            cell.btnHideUnhidePassword.setImage(UIImage(named: "eye_closed"), for: .normal)
            cell.txtPassword.isSecureTextEntry = true
        } else {
            cell.txtPassword.isSecureTextEntry = false
            cell.btnHideUnhidePassword.setImage(UIImage(named: "open_eye"), for: .normal)
        }

              // iconClick = !iconClick
               
              // defaults.set(iconClick, forKey: "iconClick")
        cell.txtPassword.lineHeight = 0.5
        cell.txtPassword.placeholderColor = .lightGray
        cell.txtPassword.selectedLineColor = .lightGray
        cell.txtPassword.selectedTitleColor = .lightGray
        cell.txtPassword.font = UIFont(name: self.app_Controller.App_Font_Style_Bold, size: CGFloat(UserDefaults.standard.integer(forKey: "Text_Size")-2))
       
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 691.0
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

