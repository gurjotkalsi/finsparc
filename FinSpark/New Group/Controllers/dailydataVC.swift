//
//  dailydataVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 10/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit
import iOSDropDown
class dailydataVC: UIViewController {
    
    @IBOutlet weak var collctionviewTopTabs: UICollectionView!
    @IBOutlet weak var border_View: UIView!
    @IBOutlet var tableview: UITableView!
    @IBOutlet weak var dropdownBtn: DropDown!

    @IBOutlet var eod_lbl: UILabel!
    @IBOutlet var expiry_lbl: UILabel!
    @IBOutlet var futures_prices_lbl: UILabel!
    @IBOutlet var delta_price_lbl: UILabel!
    @IBOutlet var future_oi_lbl: UILabel!
    @IBOutlet var delta_oi_lbl: UILabel!
    @IBOutlet var delta_oi_5days_lbl: UILabel!
    
    var titlesArray = ["Current Expiry", "Next Expiry"]
    var selection = NSMutableArray();
    var searchresult = ""
    //MARK:-
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sliderHolder.fadeOut {}
        self.CustomizeNavBar_with(title: "", button_text: "Daily Data", bartint: UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1), backButton: true) {
            self.navigationController?.popViewController(animated: true)
            sliderHolder.fadeIn {}
        }
        
       // self.webservice()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true
        border_View.addBorder()
        selection.add(0)
        
        // The view to which the drop down will appear on
        // dropdownBtn.optionArray = ["Report a problem", "Give a suggestion", "Ask a question"]
         dropdownBtn.isSearchEnable = true
         // The list of items to display. Can be changed dynamically
         dropdownBtn.didSelect{(selectedText , index ,id) in
             self.dropdownBtn.text = selectedText
            self.searchresult = selectedText
            self.webservice()
         }
      //s  tableview.register(UINib(nibName: "dailydataCell", bundle: nil), forCellReuseIdentifier: "dailydataCell")
        tableview.register(UINib(nibName: "aggregateCell", bundle: nil), forCellReuseIdentifier: "aggregateCell")

        getInstrumentList()
        collctionviewTopTabs.register(UINib(nibName: "topTabCell", bundle: nil), forCellWithReuseIdentifier: "topTabCell")
        collctionviewTopTabs.reloadData()
    }
    
    
    func getInstrumentList() {
        self.AddLoader()
        StockListModel.shared.process { (model) in
            self.RemoveLoader()

            if model.list.count != 0 {
                self.dropdownBtn.optionArray = model.list
            }else{
                self.RemoveLoader()
            }
        }
    }
    
    func webservice() {
        self.AddLoader()
        DailyReportModel.shared.process (type: searchresult){ (modl) in
            
            self.RemoveLoader()
            self.setValues()
        }
    }
    
    func setValues() {
        var item = DailyReportModel.shared.current_date_value
        if selection.contains(0) {
            item = DailyReportModel.shared.current_date_value
        }else{
            item = DailyReportModel.shared.last_month_value
        }

        self.eod_lbl.text = "\(item.cdate.eodDate())"
        self.expiry_lbl.text = ""
        self.futures_prices_lbl.text = item.future_price
        self.delta_price_lbl.text = item.change_close
        self.future_oi_lbl.text = ""
        self.delta_oi_lbl.text = item.change_oi_last_day
        self.delta_oi_5days_lbl.text = item.change_oi_5_day
        
        self.tableview.reloadData()

    }
}

extension dailydataVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selection.contains(0) {
            print(DailyReportModel.shared.current_date_value.current_details)
            return DailyReportModel.shared.current_date_value.current_details.count

        }else{
            return DailyReportModel.shared.last_month_value.last_month_details.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableview.dequeueReusableCell(withIdentifier: "dailydataCell", for: indexPath) as! dailydataCell
        
        var item = DailyReportObjectItem()
        if selection.contains(0) {
            item = DailyReportModel.shared.current_date_value.current_details[indexPath.row]
        }else{
            item = DailyReportModel.shared.last_month_value.last_month_details[indexPath.row]
        }
        
        cell.strike_lbl.text = item.stike_pr
        cell.oi_lbl.text = item.open_int
        cell.oi_delta_lbl.text = item.change_oi
        cell.trigger_price_lbl.attributedText = String(format: "%.2f", item.trigger).italic(cell.trigger_price_lbl.font.pointSize)
        cell.price_close_lbl.text = item.close
        cell.sw_lbl.text = item.sw
        
        return cell
    }
    
    
}

extension dailydataVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3.3, height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.collctionviewTopTabs.dequeueReusableCell(withReuseIdentifier: "topTabCell", for: indexPath) as! topTabCell
        cell.lbl_Headings.text = self.titlesArray[indexPath.row]
        cell.lbl_Headings.roundUp()
        if selection.contains(indexPath.row) {
            cell.lbl_Headings.backgroundColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
            cell.lbl_Headings.textColor = UIColor.white
            
        }else{
            cell.lbl_Headings.backgroundColor = UIColor.clear
            cell.lbl_Headings.textColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selection.removeAllObjects()
        selection.add(indexPath.row)
        collectionView.reloadData()
        
        self.setValues()
        self.tableview.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
}
