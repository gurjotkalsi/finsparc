//
//  dailydataCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 10/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class dailydataCell: UITableViewCell {

    @IBOutlet var strike_lbl: UILabel!
    @IBOutlet var oi_lbl: UILabel!
    @IBOutlet var oi_delta_lbl: UILabel!
    @IBOutlet var trigger_price_lbl: UILabel!
    @IBOutlet var price_close_lbl: UILabel!
    @IBOutlet var sw_lbl: UILabel!
    
    
    //MARK:-
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
