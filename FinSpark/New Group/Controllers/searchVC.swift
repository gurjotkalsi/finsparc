//
//  searchVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 10/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit
import iOSDropDown
import MaterialComponents.MaterialTextFields

class searchVC: UIViewController {
    @IBOutlet weak var industryButton: DropDown!
    @IBOutlet weak var typeBtn: UIButton!
    @IBOutlet weak var seriesBtn: MDCTextField!
    var nameControl:MDCTextInputControllerFloatingPlaceholder!

    let dropDown = DropDown()
    var selectedTag = 0
    var industryid = [String]()
       var industrysid = ""
    var symbols = [String]()
    var typeArray = ["Futures", "Call", "Put"]

    override func viewDidLoad() {
        super.viewDidLoad()

        self.CustomizeNavBar_with(title: "", button_text: "Search", bartint: UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1), backButton: true) {
                   self.navigationController?.popViewController(animated: true)
               }

        // The view to which the drop down will appear on
        industryButton.didSelect{(selectedText , index ,id) in
            self.industryButton.text = selectedText
            if selectedText != "" {
                self.industryButton.text = selectedText
            }else{
                self.industryButton.text = "Select Industry" //("Select Industry", for: .normal)
            }
            
            self.AddLoader()
            self.industrysid = self.industryid[index]
            
            ScoreCardModel.shared.industryprocess(industryid: "\(self.industrysid)") { (modl) in
                self.RemoveLoader()
            }
            
        }
        // The list of items to display. Can be changed dynamically
        // Do any additional setup after loading the view.
        dropDown.didSelect{(item , index ,id) in
                self.typeBtn.setTitle(item, for: .normal)
        }

        // Will set a custom width instead of the anchor view width
        dropDown.frame.size.width = typeBtn.frame.size.width
    }
    
    func getInstrumentList() {
        self.AddLoader()
        StockRankingModel.shared.getIndustries { (model) in
           self.RemoveLoader()
            if model.industryList.count != 0 {
            print("inside")
             for obj in model.industryList
                              {
                                  self.symbols.append(obj.name)
                                  self.industryid.append(obj._id)
                              }
            print("ininside")
            self.industryButton.optionArray = self.symbols
          }
        }
           // }
    }
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           self.tabBarController?.tabBar.isHidden = true
    }
    
    @IBAction func instrumentAction(_ sender: UIButton) {
        
        selectedTag = 1
       // sender.isSelected = !sender.isSelected
        dropDown.optionArray = ["ACC", "Call", "Put", "FutStk"]

        dropDown.showList()
    }
    
    @IBAction func typeAction(_ sender: UIButton) {
        selectedTag = 2

         //  sender.isSelected = !sender.isSelected
        dropDown.optionArray = ["Futures", "Call", "Put"]

           dropDown.showList()
       }
    
    @IBAction func seriesAction(_ sender: UIButton) {
        selectedTag = 3

            //  sender.isSelected = !sender.isSelected
        dropDown.optionArray = ["1300", "1500", "1800", "2100", "2400"]

              dropDown.showList()
          }
    
    
    @IBAction func searchaction(_ sender: UIButton){
        
               if seriesBtn.text == "" {
                   self.ShowToast(message: "Please enter portfolio name")
               }else{
                   if !isInternetConnected() {
                       self.ShowToast(message: "Check internet connection!")
                       return
                   }

                   self.AddLoader()
                  /* portfolioNameModel().sendName(parameter: ["portfolio_name":name_tf.text as Any, "id":"0","user_id":(LoginModel().localData().finsparc_user_id)]) { (pmodel) in
                       self.RemoveLoader()
                    let nc = NotificationCenter.default
                           nc.post(name: Notification.Name("refreshdata"), object: nil)
                       self.ShowToast(message: pmodel.msg) {
                           self.name_tf.text = ""
                        self.dismiss(animated: true, completion: nil)

                       }
                   }*/
               }
     
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
