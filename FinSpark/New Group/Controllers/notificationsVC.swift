//
//  notificationsVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 20/02/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class notificationsVC: UIViewController {
    @IBOutlet weak var collctionviewTopTabs: UICollectionView!
    @IBOutlet weak var notificationTableView: UITableView!

    var titlesArray = ["All", "Updates", "Offerings"]
    var selection = NSMutableArray();

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true

        collctionviewTopTabs.register(UINib(nibName: "topTabCell", bundle: nil), forCellWithReuseIdentifier: "topTabCell")
                        notificationTableView.register(UINib(nibName: "notificationCell", bundle: nil), forCellReuseIdentifier: "notificationCell")

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


//MARK: Collectionview Methods
extension notificationsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,  UITableViewDelegate, UITableViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           return CGSize(width: collectionView.frame.width / 3.3, height: collectionView.frame.height)
       }
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
               
               let cell = self.collctionviewTopTabs.dequeueReusableCell(withReuseIdentifier: "topTabCell", for: indexPath) as! topTabCell
               cell.lbl_Headings.text = self.titlesArray[indexPath.row]
              cell.lbl_Headings.roundUp()

               if selection.contains(indexPath.row) {
               cell.lbl_Headings.backgroundColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
               cell.lbl_Headings.textColor = UIColor.white

                  }else{
               cell.lbl_Headings.backgroundColor = UIColor.clear
               cell.lbl_Headings.textColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)

                  }
               return cell
           }
           
           func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
               if selection.contains(indexPath.row) {
                   selection.removeAllObjects()
               }else{
                   selection.removeAllObjects()
                   selection.add(indexPath.row)
               }
               collectionView.reloadData()
           }
           // Layout: Set Edges
              func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
                  // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
                  return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0) // top, left, bottom, right
              }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as! notificationCell
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
}
