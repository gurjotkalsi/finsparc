//
//  fsGearCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 09/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class fsGearCell: UITableViewCell {
    @IBOutlet var backgroundVew: UIView!
    @IBOutlet var parameterLabel: UILabel!
    @IBOutlet var ltdLabel: UILabel!
    @IBOutlet var dayLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
