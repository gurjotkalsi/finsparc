//
//  weeklyNotesCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 19/02/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class weeklyNotesCell: UITableViewCell {

    @IBOutlet var particular_lbl: UILabel!
    @IBOutlet var ltd_lbl: UILabel!
    @IBOutlet var prev_day_lbl: UILabel!
    
    
    //MARK:-
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
