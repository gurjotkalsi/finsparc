//
//  IVVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 10/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class IVVC: UIViewController {
    
    @IBOutlet weak var ivTable: UITableView!
    @IBOutlet weak var created_lbl: UILabel!
    @IBOutlet weak var expiry_lbl: UILabel!

    var typeofPuttext: String = ""
    
    //MARK:-
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sliderHolder.fadeOut { }
        self.AddLoader()
        IVReportModel.shared.process { (modl) in
            self.RemoveLoader()
            
            self.created_lbl.text = "\(IVReportModel.shared.list[0].cdate) (EOD)"
            self.expiry_lbl.text = "Expiry Date \(IVReportModel.shared.list[0].expiry_dt)"

            self.ivTable.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true

        UserDefaults.standard.set("IV", forKey: "type") //setObject
        ivTable.register(UINib(nibName: "IVCell", bundle: nil), forCellReuseIdentifier: "IVCell")

        self.CustomizeNavBar_with(title: "", button_text: "IV Changes", bartint: UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1), backButton: true) {
            self.navigationController?.popViewController(animated: true)
            sliderHolder.fadeIn { }
        }
    }
    //MARK:-
}


//MARK: Collectionview Methods
extension IVVC: UITableViewDelegate, UITableViewDataSource {
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return IVReportModel.shared.list.count
    }
    
    func tableView(_ tableView: UITableView, numberOfSectionsInTableView section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IVCell", for: indexPath) as! IVCell
        //cell.typeofPuttext = self.typeofPuttext
        
        
        let item = IVReportModel.shared.list[indexPath.row]
        
        cell.symbol_lbl.text = item.symbol
        cell.last_day_lbl.text = "\(item.iv_last_day.getnodecimalString())%"
        cell.last_week_lbl.text = "\(item.iv_last_week.getnodecimalString())%"
        cell.last_month_lbl.text = "\(item.iv_last_month.getnodecimalString())%"
        cell.over_week_lbl.text = "\(item.change_over_last_week.getnodecimalString())%"
        cell.over_month_lbl.text = "\(item.change_over_last_month.getnodecimalString())%"
        
        return cell
    }
    
}
