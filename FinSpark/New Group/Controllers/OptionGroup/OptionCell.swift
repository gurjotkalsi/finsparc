//
//  OptionCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 24/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class OptionCell: UITableViewCell {
       @IBOutlet var priceclose: UILabel!
       @IBOutlet var pricestatus: UILabel!
       @IBOutlet var triggerprice: UILabel!
       @IBOutlet var oi: UILabel!
       @IBOutlet var oistatus: UILabel!
       @IBOutlet var changeinprice: UILabel!
       @IBOutlet var changeinoi: UILabel!
       @IBOutlet var average: UILabel!
    @IBOutlet var viewConstant: NSLayoutConstraint!
    @IBOutlet weak var mainview: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.mainview.addlightBorder()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
