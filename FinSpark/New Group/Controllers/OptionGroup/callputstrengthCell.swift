//
//  callputstrengthCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 26/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class callputstrengthCell: UITableViewCell {
    @IBOutlet var callfirst: UILabel!
    @IBOutlet var callsecond: UILabel!
    @IBOutlet var putfirst: UILabel!
    @IBOutlet var putsecond: UILabel!
    @IBOutlet var viewConstant: NSLayoutConstraint!
    @IBOutlet weak var mainview: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.mainview.addlightBorder()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
