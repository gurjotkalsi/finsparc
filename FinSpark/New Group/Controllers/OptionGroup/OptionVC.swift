//
//  OptionVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 26/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class OptionVC: UIViewController {
    @IBOutlet var viewConstant: NSLayoutConstraint!

    @IBOutlet var tableview: UITableView!

    var collapsedSize = CGFloat(55)
    var expandedSize = CGFloat(375)
    
    var expand = true
    
    var prevIndex : IndexPath?
    var currentIndex : IndexPath?
    
    var titlestopArray = ["Data", "Graph"]
    var selection = NSMutableArray();

    var titleArray = ["NIFTY", "5 Days Data", "Overall Call / Put Strength"]
    var collapsedarray = [true, true, true,true, true, true]
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        sliderHolder.fadeOut { }
       // self.viewConstant.constant = 0
        self.tabBarController?.tabBar.isHidden = true

        // You know them 😉
        self.tableview.dataSource = self
        self.tableview.delegate = self

        tableview.register(UINib(nibName: "OptionCell", bundle: nil), forCellReuseIdentifier: "OptionCell")
        tableview.register(UINib(nibName: "oifirstCell", bundle: nil), forCellReuseIdentifier: "oifirstCell")
        tableview.register(UINib(nibName: "collapsedcell", bundle: nil), forCellReuseIdentifier: "collapsedcell")
        tableview.register(UINib(nibName: "callputstrengthCell", bundle: nil), forCellReuseIdentifier: "callputstrengthCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        sliderHolder.fadeOut {}
        self.CustomizeNavBar_with(title: "", button_text: "Option Profile", bartint: UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1), backButton: true) {
            self.navigationController?.popViewController(animated: true)
            sliderHolder.fadeIn {}
        }
        OptionProfileModel.shared.process { (model) in
            
            self.RemoveLoader()
                        self.tableview.reloadData()

        }    }
    //MARK:-


}

extension OptionVC : UITableViewDataSource {

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
           let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 10))
           return footerView
       }

       func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
           return 10
       }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let collapse = collapsedarray[indexPath.section]

        if indexPath.section == 0 {
                    if collapse {
                        return 55
                     }else{
                        return 270
                     }
         }
            else if indexPath.section == 1 {
                   if collapse {
                       return 55
                    }else{
                       return 55 * 1
                    }
            }
           else
         {
            if collapse {
                       return 55
                    }else{
                       return 112
                    }
        }
       }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1
        {
            let collapse = collapsedarray[section]

            if collapse {
                return 1
            }else{
                
                return OptionProfileModel.shared.list.count + 2
            }
        }else
        {
        return 1
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.titleArray.count
    }
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let collapse = collapsedarray[indexPath.section]

        if indexPath.section == 0 {

            let cell = tableView.dequeueReusableCell(withIdentifier: "OptionCell", for: indexPath) as! OptionCell
            if collapse {
                cell.viewConstant.constant = 55
             }else{
                cell.viewConstant.constant = 270.0
             }
            cell.priceclose.text = OptionProfileModel.shared.option_close
            cell.pricestatus.text = OptionProfileModel.shared.option_close_percent
            cell.triggerprice.text = OptionProfileModel.shared.option_trigger_price
            cell.oi.text = OptionProfileModel.shared.oi
            cell.oistatus.text = OptionProfileModel.shared.percent_oi_5
            cell.changeinprice.text = OptionProfileModel.shared.wap
            cell.changeinoi.text = OptionProfileModel.shared.percent_poi
            cell.average.text = OptionProfileModel.shared.percent_poi
            cell.selectionStyle = .none
            
            return cell
        }else if indexPath.section == 1 {

          
             if indexPath.row == 0
             {
                let cell = tableView.dequeueReusableCell(withIdentifier: "collapsedcell", for: indexPath) as! collapsedcell
                                          if collapse {
                                            cell.label.isHidden = true
                                                  }else{
                                                     cell.label.isHidden = false
                                                  }
                return cell

             }
            else if indexPath.row == 1
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "oifirstCell", for: indexPath) as! oifirstCell
                cell.symbol.text = "Date"
               cell.expiry.text = "Volume \n ('000)"
               cell.lastexpiry.text = "OI (in Lacs)"
               cell.oiexpiry.text = "▵OI (in Lacs)"
               cell.lastweek.text = "Price Close (in Rs)"
                return cell

            }else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "oifirstCell", for: indexPath) as! oifirstCell

            let object : OptionProfileObject = OptionProfileModel.shared.list[indexPath.row - 2]
            cell.symbol.text = object.timestamp_finsparc_bhavcopy
            cell.expiry.text = object.volume
            cell.lastexpiry.text = object.open_int
            cell.oiexpiry.text = object.chg_in_oi
            cell.lastweek.text = object.close
                
                cell.selectionStyle = .none
                return cell

            }
        }
        else {

            let cell = tableView.dequeueReusableCell(withIdentifier: "callputstrengthCell", for: indexPath) as! callputstrengthCell
            
            if collapse {
                       cell.viewConstant.constant = 55
                    }else{
                       cell.viewConstant.constant = 168
                    }
            cell.callfirst.text = OptionProfileModel.shared.call_strength
           cell.callsecond.text = OptionProfileModel.shared.call_strength
           cell.putfirst.text = OptionProfileModel.shared.put_strength
           cell.putsecond.text = OptionProfileModel.shared.put_strength
            
            cell.selectionStyle = .none
            return cell
        }
    }
}

extension OptionVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let collapse = !collapsedarray[indexPath.section]
        if collapse {
             expand = false
         }else{
             expand = true
         }
         print("old",collapsedarray)
         collapsedarray[indexPath.section] = collapse
         print("new",collapsedarray)
        self.tableview.reloadData()
        
    }
}
// MARK: - Section Header Delegate
//
extension OptionVC: CollapsibleViewHeaderDelegate {
    
    func togglesSection(_ header: customsearchHeader, section: Int) {
        
       let collapse = !collapsedarray[section]
       if collapse {
            expand = false
        }else{
            expand = true
        }
        print("old",collapsedarray)
        collapsedarray[section] = collapse
        print("new",collapsedarray)

       // header.setCollapsed(expand)
        
        tableview.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
    }
    
}
