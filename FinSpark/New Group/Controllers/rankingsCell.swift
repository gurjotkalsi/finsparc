//
//  rankingsCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 01/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class rankingsCell: UITableViewCell {

    @IBOutlet var instrument_lbl: UILabel!
    
    @IBOutlet var rank_current: UILabel!
    @IBOutlet var rank_oneWeek: UILabel!
    @IBOutlet var rank_fourWeek: UILabel!
    
    @IBOutlet var change_oneWeek: UILabel!
    @IBOutlet var change_fourWeek: UILabel!
    
    
    //MARK:-
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
