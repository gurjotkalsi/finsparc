//
//  SignUpViewController.swift
//  Finsparc
//
//  Created by mayur on 2/5/20.
//  Copyright © 2020 Mayur Parmar. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class SignUpCell: UITableViewCell
{
    @IBOutlet weak var imgSignUp: UIImageView!
    @IBOutlet weak var lblSignUp: UILabel!
    
    @IBOutlet weak var txtUsername: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtMobilenumber: SkyFloatingLabelTextField!
   
    @IBOutlet weak var btnLogin: UIButton!
}

class SignUpViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    //MARK:- Outlets
    
    var app_Controller = App_Controller()
    var app_Delegate = AppDelegate()
    var cell = SignUpCell()
    
    //MARK:-
    
    @IBOutlet weak var HeaderVW: UIView!
    {
        didSet
        {
            HeaderVW.setUpHeaderView()
        }
    }
    @IBOutlet weak var tbl: UITableView!
    @IBOutlet weak var btnTopRightLogin: UIButton!
    
    @IBOutlet weak var btnVerify: UIButton!
    
    @IBOutlet var field_one: UITextField!
    @IBOutlet var field_two: UITextField!
    @IBOutlet var field_three: UITextField!
    @IBOutlet var field_four: UITextField!
    
    @IBOutlet var otpView: UIView!
    @IBOutlet var otpBox: UIView!

    
    //MARK:-
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    override func viewDidLayoutSubviews() {
        self.field_one.underlineIt()
        self.field_two.underlineIt()
        self.field_three.underlineIt()
        self.field_four.underlineIt()
        self.btnVerify.setUpButton()
        self.btnVerify.titleLabel?.font = UIFont(name: self.app_Controller.App_Font_Style_Bold, size: CGFloat(UserDefaults.standard.integer(forKey: "Text_Size")-2))
        self.otpBox.layer.cornerRadius = 6
        self.otpBox.addShadow()
        
    }

    //MARK:- IBActions -
    
    @IBAction func onTapDismiss(_ sender: UITapGestureRecognizer) {
        self.otpView.fadeOut {
            self.field_one.text = ""
            self.field_two.text = ""
            self.field_three.text = ""
            self.field_four.text = ""
        }
    }
    
    @IBAction func onClickVerify(_ sender: UIButton) {
        if self.field_one.text == "" {
            self.field_one.becomeFirstResponder()
        }else if self.field_two.text == "" {
            self.field_two.becomeFirstResponder()
        }else if self.field_three.text == "" {
            self.field_three.becomeFirstResponder()
        }else if self.field_four.text == "" {
            self.field_four.becomeFirstResponder()
        }else{
            if !isInternetConnected() {
                self.ShowToast(message: "Check internet connection!")
                return
            }
            
            self.AddLoader()
            
            let otpString = "\(field_one.text!)\(field_two.text!)\(field_three.text!)\(field_four.text!)"
            
            let fullparams : NSDictionary = ["usertype":"user", "finsparc_user_id":"0", "name":self.cell.txtUsername.text!, "email":self.cell.txtEmail.text!, "mobile":self.cell.txtMobilenumber.text!, "password":self.cell.txtPassword.text!, "in_otp": otpString]
            
            RegisterModel().userRegister(parameter: fullparams) { (regmodel) in
                
                self.RemoveLoader()
                if regmodel.register_user != "" {
                    if regmodel.register_user == "-1" {
                        self.ShowToast(message: "Mobile number already registered!") {
                            self.otpView.fadeOut {}
                        }
                    }else{
                        self.ShowToast(message: "User registered!") {
                            self.otpView.fadeOut {
                                if let navControllers = self.navigationController?.viewControllers {
                                    for item in navControllers {
                                        if item.restorationIdentifier == "LoginViewController" {
                                            self.navigationController?.popToViewController(item, animated: true)
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                }else{
                    self.ShowToast(message: "Unexpected error occurred!")
                }
                
            }
            
        }
    }
    
    @IBAction func onClickedLogin(_ sender: UIButton)
    {
        if cell.txtUsername.text == "" || cell.txtPassword.text == "" || cell.txtEmail.text == "" || cell.txtMobilenumber.text == "" {
            self.ShowToast(message: "All fields are required!")
        }else if !cell.txtEmail.text!.isValidEmail() {
            self.ShowToast(message: "Invalid Email address!")
        }else if cell.txtMobilenumber.text!.count < 10 {
            self.ShowToast(message: "Invalid Mobile number!")
        }else{
            
            if isInternetConnected() {
                
                self.AddLoader()
                
                RegisterModel().getOTP(parameter: ["mobile_no":cell.txtMobilenumber.text!, "id":"0"]) { (model) in
                
                    self.RemoveLoader()
                    if model.insert_otp != "" {
                        if model.insert_otp == "-1" {
                            self.ShowToast(message: "Mobile number already registered!") {
                                self.otpView.fadeOut {}
                            }
                        }else{
                            self.otpView.fadeIn {
                                self.field_one.becomeFirstResponder()
                            }
                        }
                        
                    }else{
                        self.ShowToast(message: "Unexpected error occurred!")
                    }
                }
                
            }else{
                self.ShowToast(message: "Check internet connection!")
            }

        }
        
    }
    
    
    @IBAction func onClickedTopRightLogin(_ sender: UIButton)
    {
        let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                      self.navigationController?.pushViewController(signUpVC, animated: true)
    }
    
    //MARK:- UITableView delegates -
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
       {
           return 1
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
       {
           cell = tbl.dequeueReusableCell(withIdentifier: "SignUpCell", for: indexPath) as! SignUpCell
           
            cell.lblSignUp.font =  UIFont(name: self.app_Controller.App_Font_Style_Bold, size: CGFloat(UserDefaults.standard.integer(forKey: "Text_Size")-2))
           
           cell.btnLogin.titleLabel?.font =  UIFont(name: self.app_Controller.App_Font_Style_Bold, size: CGFloat(UserDefaults.standard.integer(forKey: "Text_Size")-2))
           cell.btnLogin.setUpButton()
           

           cell.txtUsername.lineHeight = 0.5
           cell.txtUsername.placeholderColor = .lightGray
           cell.txtUsername.selectedLineColor = .lightGray
           cell.txtUsername.selectedTitleColor = .lightGray
           cell.txtUsername.font = UIFont(name: self.app_Controller.App_Font_Style_Regular, size: CGFloat(UserDefaults.standard.integer(forKey: "Text_Size")-2))

           cell.txtPassword.lineHeight = 0.5
          cell.txtPassword.placeholderColor = .lightGray
          cell.txtPassword.selectedLineColor = .lightGray
          cell.txtPassword.selectedTitleColor = .lightGray
          cell.txtPassword.font = UIFont(name: self.app_Controller.App_Font_Style_Regular, size: CGFloat(UserDefaults.standard.integer(forKey: "Text_Size")-2))
           
        cell.txtEmail.lineHeight = 0.5
        cell.txtEmail.placeholderColor = .lightGray
        cell.txtEmail.selectedLineColor = .lightGray
        cell.txtEmail.selectedTitleColor = .lightGray
        cell.txtEmail.font = UIFont(name: self.app_Controller.App_Font_Style_Regular, size: CGFloat(UserDefaults.standard.integer(forKey: "Text_Size")-2))

        
        cell.txtMobilenumber.lineHeight = 0.5
        cell.txtMobilenumber.placeholderColor = .lightGray
        cell.txtMobilenumber.selectedLineColor = .lightGray
        cell.txtMobilenumber.selectedTitleColor = .lightGray
        cell.txtMobilenumber.font = UIFont(name: self.app_Controller.App_Font_Style_Regular, size: CGFloat(UserDefaults.standard.integer(forKey: "Text_Size")-2))

           return cell
           
       }
    
       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
       {
           return 691.0
       }
       func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
           return UITableView.automaticDimension
       }
       
}


extension SignUpViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == "" {
            if field_four.isFirstResponder {
                textField.text = " "
                self.field_three.becomeFirstResponder()
                return false
            } else if field_three.isFirstResponder {
                textField.text = " "
                self.field_two.becomeFirstResponder()
                return false
            } else if field_two.isFirstResponder {
                textField.text = " "
                self.field_one.becomeFirstResponder()
                return false
            } else {
            }
            
        }else{
            if field_one.isFirstResponder {
                textField.text = string
                self.field_two.becomeFirstResponder()
            } else if field_two.isFirstResponder {
                textField.text = string
                self.field_three.becomeFirstResponder()
            } else if field_three.isFirstResponder {
                textField.text = string
                self.field_four.becomeFirstResponder()
            } else if field_four.isFirstResponder {
                //field_four
                textField.text = string
                textField.resignFirstResponder()
            }
            
        }
        
        return true
    }
    
    
}
