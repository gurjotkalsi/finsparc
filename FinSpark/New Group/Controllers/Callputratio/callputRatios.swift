//
//  callputRatios.swift
//  FinSpark
//
//  Created by Minkle Garg on 24/02/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class callputRatios: UITableViewCell {

    @IBOutlet var symbol: UILabel!
    @IBOutlet var score: UILabel!
    @IBOutlet var cp_value: UILabel!
    @IBOutlet var call_ratio: UILabel!
    @IBOutlet var put_ratio: UILabel!
    
    //MARK:-
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
