//
//  callPutRatiosVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 24/02/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class callPutRatiosVC: UIViewController {
    // @IBOutlet weak var borderedView: UIView!
    @IBOutlet weak var collctionviewTopTabs: UICollectionView!
    @IBOutlet weak var callputTableView: UITableView!
    @IBOutlet var expiry_date: UILabel!
    @IBOutlet var current_date: UILabel!
    
    var titlesArray = [" Current Expiry ", " Next Expiry"]
    var selection = NSMutableArray();
    
    
    //MARK:-
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        sliderHolder.fadeOut {}
        self.CustomizeNavBar_with(title: "", button_text: "Call/Put Ratios", bartint: UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1), backButton: true) {
            self.navigationController?.popViewController(animated: true)
            sliderHolder.fadeIn {}
        }
        self.webservice()
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        
        collctionviewTopTabs.register(UINib(nibName: "topTabCell", bundle: nil), forCellWithReuseIdentifier: "topTabCell")
        let indexpath = 0
        selection.add(indexpath)
        collctionviewTopTabs.reloadData()
        callputTableView.register(UINib(nibName: "callputRatios", bundle: nil), forCellReuseIdentifier: "callputRatios")
        
        self.CustomizeNavBar_with(title: "", button_text: "Call/Put Ratios", bartint: UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1), backButton: true) {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    //MARK:-
    
    func webservice() {
        self.AddLoader()
        CallPutRatioModel.shared.process { (modl) in
            self.RemoveLoader()
            var item = RatioItem()
                   
            if self.selection.contains(1) {
                       //next
                       item  = CallPutRatioModel.shared.next_callput[0]
                   }else{
                       //current
                       item  = CallPutRatioModel.shared.current_callput[0]
                   }
            self.expiry_date.text =  "Expiry Date: \(self.Convertorequireformat(item.current_expiry))"
            self.current_date.text = "\(self.Convertorequireformat(item.last_date)) (EOD)"
            self.callputTableView.reloadData()
            
//            if modl.status != "200" {
//                self.ShowToast(message: modl.message)
//            }
        }

    }
    func Convertorequireformat (_ date : String ) -> String{
          let inputFormatter = DateFormatter()
             inputFormatter.dateFormat = "yyyy-MM-dd"
             let showDate = inputFormatter.date(from: date)
             inputFormatter.dateFormat = "dd-MM-yyyy"
             let resultString = inputFormatter.string(from: showDate!)
            return resultString
      }
}

extension callPutRatiosVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selection.contains(1) {
            //next
            return CallPutRatioModel.shared.next_callput.count
        }else{
            //current
            return CallPutRatioModel.shared.current_callput.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfSectionsInTableView section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "callputRatios", for: indexPath) as! callputRatios
        
        var item = RatioItem()
        
        if selection.contains(1) {
            //next
            item  = CallPutRatioModel.shared.next_callput[indexPath.row]
        }else{
            //current
            item  = CallPutRatioModel.shared.current_callput[indexPath.row]
        }
        
        cell.symbol.text = item.symbol
        cell.score.text = item.total
        if var callvalue = Float(item.call_value)
        {
            if var putvalue = Float(item.put_value)
            {
                callvalue = callvalue / 100000.00
                putvalue = putvalue / 100000.00
                cell.cp_value.text = "\(String(callvalue).getTwoDecimalString())\n\(String(putvalue).getTwoDecimalString())"
            }
        }

        
       // cell.cp_value.text = "\(item.call_value)\n\(item.put_value)"
        
        if let value = Float(item.call_ratio)
        {
            if value > 1
            {
                cell.call_ratio.textColor = .green
            }else
            {
                cell.call_ratio.textColor = .red
            }
        }
        if let value = Float(item.put_ratio)
        {
            if value < 1
            {
                cell.put_ratio.textColor = .green
            }else
            {
                cell.put_ratio.textColor = .red
            }
        }
        cell.call_ratio.text = item.call_ratio
        cell.put_ratio.text = item.put_ratio
        
       // self.expiry_date.text = "Expiry Date \(item.current_expiry.eodDate())"
        //self.current_date.text = "\(item.last_date.eodDate()) EOD"
        
        return cell
    }

}

extension String {
    func eodDate() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-mm-dd"
        if let dd = formatter.date(from: self) {
            formatter.dateFormat = "LLLL d"
            let ss = formatter.string(from: dd)
            return ss
        }else{
            return self
        }
    }
}

extension callPutRatiosVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.collctionviewTopTabs.dequeueReusableCell(withReuseIdentifier: "topTabCell", for: indexPath) as! topTabCell
        cell.lbl_Headings.text = self.titlesArray[indexPath.row]
        cell.lbl_Headings.roundUp()
        
        if selection.contains(indexPath.row) {
            cell.lbl_Headings.backgroundColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
            cell.lbl_Headings.textColor = UIColor.white
            
        }else{
            cell.lbl_Headings.backgroundColor = UIColor.clear
            cell.lbl_Headings.textColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
            
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3.3, height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selection.removeAllObjects()
        selection.add(indexPath.row)
        collectionView.reloadData()
        self.callputTableView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
}
