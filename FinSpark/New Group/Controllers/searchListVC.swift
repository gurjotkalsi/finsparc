//
//  searchListVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 11/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class searchListVC: UIViewController {

    @IBOutlet var tableview: UITableView!
    @IBOutlet weak var collctionviewTopTabs: UICollectionView!

    var collapsedSize = CGFloat(55)
    var expandedSize = CGFloat(375)
    
    var expand = true
    
    var prevIndex : IndexPath?
    var currentIndex : IndexPath?
    
    var titlestopArray = ["Data", "Graph"]
    var selection = NSMutableArray();

    var titleArray = ["Stock Profile", "OI in lacs (for all Expires)", "Options", "Implied Volatility", "Finsparc Summary"]
    var titlesArray = ["Stock Profile", "5 Days Data", "OI in lacs (for all Expires)", "Options", "Implied Volatility", "Finsparc Summary"]

    var collapsedarray = [false, false, false,false, false, false]
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        collctionviewTopTabs.register(UINib(nibName: "topTabCell", bundle: nil), forCellWithReuseIdentifier: "topTabCell")
        let indexpath = 0
        selection.add(indexpath)
        collctionviewTopTabs.reloadData()

        self.CustomizeNavBar_with(title: "", button_text: "Search", bartint: UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1), backButton: true) {
            self.navigationController?.popViewController(animated: true)
        }
        self.tableview.tableFooterView = UIView()
        // You know them 😉
        self.tableview.dataSource = self
        self.tableview.delegate = self
        
        let nib = UINib(nibName: "customsearchHeader", bundle: nil)
        tableview.register(nib, forHeaderFooterViewReuseIdentifier: "customsearchHeader")
        tableview.register(UINib(nibName: "searchCell", bundle: nil), forCellReuseIdentifier: "searchCell")
    }
    //MARK:-


}
extension searchListVC : UITableViewDataSource {

     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                       return 204
       }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     //   return sections[section].collapsed ? 0 : 2

       // return 2
        let collapse = collapsedarray[section]
        if collapse {
             return 0
         }else{
             return 1
         }
       
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.titleArray.count
    }
    
   func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        // Dequeue with the reuse identifier
        let headers = self.tableview.dequeueReusableHeaderFooterView(withIdentifier: "customsearchHeader") as! customsearchHeader
        headers.topTitle.text =  titleArray[section]
        headers.setCollapsed(expand)
        headers.section = section
        headers.delegate = self
     //   headers.delegate = self

        return headers
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {

            let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath) as! searchCell
            cell.selectionStyle = .none
            return cell
        }else if indexPath.section == 1 {

            let cell = tableView.dequeueReusableCell(withIdentifier: "oiCell", for: indexPath) as! oiCell
            cell.selectionStyle = .none
            return cell
        }else if indexPath.section == 2 {

            let cell = tableView.dequeueReusableCell(withIdentifier: "oiCell", for: indexPath) as! oiCell
            cell.selectionStyle = .none
            return cell
        }else if indexPath.section == 3 {

            let cell = tableView.dequeueReusableCell(withIdentifier: "volatilityCell", for: indexPath) as! volatilityCell
            cell.selectionStyle = .none
            return cell
        }else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "summaryCell", for: indexPath) as! summaryCell
                       cell.selectionStyle = .none
                       return cell
        }
    }
    
}


extension searchListVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
// MARK: - Section Header Delegate
//
extension searchListVC: CollapsibleViewHeaderDelegate {
    
    func togglesSection(_ header: customsearchHeader, section: Int) {
        
        let collapse = !collapsedarray[section]
       if collapse {
            expand = false
        }else{
            expand = true
        }
        print("old",collapsedarray)
        collapsedarray[section] = collapse
        print("new",collapsedarray)

       // header.setCollapsed(expand)
        
        tableview.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
    }
    
}


//MARK: Collectionview Methods
extension searchListVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           return CGSize(width: collectionView.frame.width / 3.3, height: collectionView.frame.height)
       }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.collctionviewTopTabs.dequeueReusableCell(withReuseIdentifier: "topTabCell", for: indexPath) as! topTabCell
        cell.lbl_Headings.text = self.titlestopArray[indexPath.row]
        cell.lbl_Headings.roundUp()
        cell.lbl_Headings.textAlignment = .center
        if selection.contains(indexPath.row) {
        cell.lbl_Headings.backgroundColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
        cell.lbl_Headings.textColor = UIColor.white

           }else{
        cell.lbl_Headings.backgroundColor = UIColor.clear
        cell.lbl_Headings.textColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)

           }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if selection.contains(indexPath.row) {
            selection.removeAllObjects()
        }else{
            selection.removeAllObjects()
            selection.add(indexPath.row)
        }
        collectionView.reloadData()
    }
 
}
