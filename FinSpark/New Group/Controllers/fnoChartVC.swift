//
//  fnoChartVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 15/05/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit
import Charts

class fnoChartVC: UIViewController, ChartViewDelegate {
    @IBOutlet weak var collctionviewTopTabs: UICollectionView!
    var titlesArray = [ "Week", "3 Months", "6 Months"]
    var selection = NSMutableArray();
    var datearray = NSMutableArray()
    var valuearray = NSMutableArray()
    weak var axisFormatDelegate: IAxisValueFormatter?
    @IBOutlet var graphview: UIView!
    @IBOutlet var marketmood: UILabel!

    var modelresult = FNOPulseModel()
    @IBOutlet var chartView: LineChartView!
  //  @IBOutlet var chartView: LineChartView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.getFNOData()
        self.graphview.addShadow(radius: 5)

        self.CustomizeNavBar_with(title: "", button_text: "FS Fear and Greed Chart", bartint: UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1), backButton: true) {
                   self.navigationController?.popViewController(animated: true)
                   sliderHolder.fadeIn { }
               }
       // self.graphview.addShadow(radius: 5)

        collctionviewTopTabs.register(UINib(nibName: "topTabCell", bundle: nil), forCellWithReuseIdentifier: "topTabCell")
        let indexpath = 0
        selection.add(indexpath)
        collctionviewTopTabs.reloadData()
        // Do any additional setup after loading the view.
    }
    
    func getFNOData() {
           FNOPulseModel.shared.process { (fmmodel) in
            print("chartvalues",fmmodel.fear_greed_chart.day.count)
            self.modelresult = fmmodel
            self.marketmood.text = fmmodel.fear_greed_chart.mood_of_market
            for item in fmmodel.fear_greed_chart.week
            {
                if let title : String = item.mmi_score, !title.isEmpty
                                       {
                                        if let weekday = self.getDayOfWeek(item.cdate) {
                                            print(weekday)
                                            self.datearray.add(weekday)

                                        } else {
                                            print("bad input")
                                        }
              //  self.datearray.add(item.cdate)
                                        let floatvalue : Float = Float(item.mmi_score) as! Float
                                        self.valuearray.add(floatvalue as! Float)
                }
            }
            self.setLineC()
           // let lineChart = self.setLineChart()
           // self.graphview.addSubview(lineChart)

        }
       }
    
    func getDayOfWeek(_ today:String) -> String? {
               let inputFormatter = DateFormatter()
                inputFormatter.dateFormat = "yyyy-MM-dd"
                let showDate = inputFormatter.date(from: today)
                inputFormatter.dateFormat = "dd MMM"
                let resultString = inputFormatter.string(from: showDate!)

       return resultString
        
    }
    func setLineC ()
    {
         
                
//                chartView.delegate = self
//
//                chartView.chartDescription?.enabled = false
//                chartView.dragEnabled = true
//                chartView.setScaleEnabled(true)
//                chartView.pinchZoomEnabled = true
//        chartView.xAxis.labelPosition = .bottom
        
                // x-axis limit line
//                let llXAxis = ChartLimitLine(limit: 10, label: "Index 10")
//                llXAxis.lineWidth = 4
//                llXAxis.lineDashLengths = [10, 10, 0]
//                llXAxis.labelPosition = .bottomRight
//                llXAxis.valueFont = .systemFont(ofSize: 10)
//
//                chartView.xAxis.gridLineDashLengths = [10, 10]
//                chartView.xAxis.gridLineDashPhase = 0
                
//                let ll1 = ChartLimitLine(limit: 150, label: "Upper Limit")
//                ll1.lineWidth = 4
//                ll1.lineDashLengths = [5, 5]
//                ll1.labelPosition = .topRight
//                ll1.valueFont = .systemFont(ofSize: 10)
//
//                let ll2 = ChartLimitLine(limit: -30, label: "Lower Limit")
//                ll2.lineWidth = 4
//                ll2.lineDashLengths = [5,5]
//                ll2.labelPosition = .bottomRight
//                ll2.valueFont = .systemFont(ofSize: 10)
//
//                let leftAxis = chartView.leftAxis
//                leftAxis.removeAllLimitLines()
//                leftAxis.addLimitLine(ll1)
//                leftAxis.addLimitLine(ll2)
//               // leftAxis.axisMaximum = 200
//                leftAxis.axisMinimum = 0
//                leftAxis.gridLineDashLengths = [0, 0]
//                leftAxis.drawLimitLinesBehindDataEnabled = true
//
//                chartView.rightAxis.enabled = false
//                chartView.rightAxis.drawGridLinesEnabled = false
       // chartView.leftAxis.enabled = false
                //[_chartView.viewPortHandler setMaximumScaleY: 2.f];
                //[_chartView.viewPortHandler setMaximumScaleX: 2.f];

//                let marker = BalloonMarker(color: UIColor(white: 180/255, alpha: 1),
//                                           font: .systemFont(ofSize: 12),
//                                           textColor: .white,
//                                           insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8))
//                marker.chartView = chartView
//                marker.minimumSize = CGSize(width: 80, height: 40)
//                chartView.marker = marker
                
//                chartView.legend.form = .line
//
//
//                chartView.animate(xAxisDuration: 2.5)
        self.setDataCount(dataPoints: datearray as! [String], values: valuearray as! [Double])

    }
  
    func setDataCount(dataPoints: [String], values: [Double]) {
        var finalvalues: [ChartDataEntry] = []
           
           for i in 0..<dataPoints.count {
               let entry = ChartDataEntry(x: Double(i), y: values[i])
               finalvalues.append(entry)
           }
//        let values = (0..<count).map { (i) -> ChartDataEntry in
//            let val = Double(arc4random_uniform(range) + 3)
//            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "icon"))
//        }
        
        let set1 = LineChartDataSet(entries: finalvalues, label: "")
        set1.drawIconsEnabled = false
        set1.lineDashLengths = [5, 2.5]
        
       // set1.highlightLineDashLengths = [5, 2.5]
        set1.setColor(NSUIColor.lightGray)
        set1.setCircleColor(NSUIColor.lightGray)
        set1.lineWidth = 1
        set1.circleRadius = 2
        set1.drawCircleHoleEnabled = false
        set1.valueFont = .systemFont(ofSize: 0)
        set1.formLineDashLengths = [5, 2.5]
        set1.formLineWidth = 1
        set1.formSize = 8
        set1.lineDashPhase = 0
        
        let xAxis = chartView.xAxis
        xAxis.labelPosition = .bottom
        xAxis.labelCount = 2
       // xAxis.drawLabelsEnabled = true
       // xAxis.drawLimitLinesBehindDataEnabled = true
        xAxis.drawGridLinesBehindDataEnabled = true
        //xAxis.avoidFirstLastClippingEnabled = true
        let customformatter = ChartStringFormatter()
        customformatter.nameValues = datearray as! [String] //anything you want
        xAxis.valueFormatter = customformatter
        xAxis.granularity = 1
       chartView.rightAxis.enabled = false
        chartView.rightAxis.drawGridLinesEnabled = false
        
       // chartView.leftAxis.dra
        chartView.xAxis.drawGridLinesEnabled = false
       
        let data = LineChartData(dataSet: set1)
        chartView.data = data
    }
  
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


//MARK:- UICollectionview Delegates -
extension fnoChartVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = self.collctionviewTopTabs.dequeueReusableCell(withReuseIdentifier: "topTabCell", for: indexPath) as! topTabCell

        cell.lbl_Headings.text = self.titlesArray[indexPath.row]
        cell.lbl_Headings.roundUp()
        if selection.contains(indexPath.row) {
            cell.lbl_Headings.backgroundColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
            cell.lbl_Headings.textColor = UIColor.white

        }else{
            cell.lbl_Headings.backgroundColor = UIColor.clear
            cell.lbl_Headings.textColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
        }
        
        cell.layer.cornerRadius = collectionView.frame.height/2
        cell.layer.masksToBounds = true
        
        return cell
    }


    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if selection.contains(indexPath.row) {
            selection.removeAllObjects()
        }else{
            selection.removeAllObjects()
            selection.add(indexPath.row)
        }
        collectionView.reloadData()
//        for view in self.graphview.subviews {
//            view.removeFromSuperview()
//        }
        self.datearray.removeAllObjects()
        self.valuearray.removeAllObjects()
//        if indexPath.row == 0
//        {
//            for item in modelresult.fear_greed_chart.day
//                       {
//                           if let title : String = item.mmi_score, !title.isEmpty
//                                                  {
//                           self.datearray.add(item.cdate)
//                                                   let floatvalue : Float = Float(item.mmi_score) as! Float
//                                                   self.valuearray.add(floatvalue as! Float)
//                           }
//                       }
//                      // let lineChart = self.setLineChart()
//                    //   self.graphview.addSubview(lineChart)
//        } else
            if indexPath.row == 0
        {
            for item in modelresult.fear_greed_chart.week
                       {
                           if let title : String = item.mmi_score, !title.isEmpty
                                                  {
                                                   if let weekday = self.getDayOfWeek(item.cdate) {
                                                       print(weekday)
                                                       self.datearray.add(weekday)

                                                   } else {
                                                       print("bad input")
                                                   }
                         //  self.datearray.add(item.cdate)
                                                   let floatvalue : Float = Float(item.mmi_score) as! Float
                                                   self.valuearray.add(floatvalue as! Float)
                           }
                       }
                      // let lineChart = self.setLineChart()
                      // self.graphview.addSubview(lineChart)
        }
        else if indexPath.row == 1
        {
            for item in modelresult.fear_greed_chart.last_3_month
                       {
                           if let title : String = item.mmi_score, !title.isEmpty
                                                  {
                                                   if let weekday = self.getDayOfWeek(item.cdate) {
                                                       print(weekday)
                                                       self.datearray.add(weekday)

                                                   } else {
                                                       print("bad input")
                                                   }
                         //  self.datearray.add(item.cdate)
                                                   let floatvalue : Float = Float(item.mmi_score) as! Float
                                                   self.valuearray.add(floatvalue as! Float)
                           }
                       }
                     //  let lineChart = self.setLineChart()
                    //   self.graphview.addSubview(lineChart)
        }
        else if indexPath.row == 2
               {
                   for item in modelresult.fear_greed_chart.last_6_month
                              {
                                  if let title : String = item.mmi_score, !title.isEmpty
                                                         {
                                                          if let weekday = self.getDayOfWeek(item.cdate) {
                                                              print(weekday)
                                                              self.datearray.add(weekday)

                                                          } else {
                                                              print("bad input")
                                                          }
                                //  self.datearray.add(item.cdate)
                                                          let floatvalue : Float = Float(item.mmi_score) as! Float
                                                          self.valuearray.add(floatvalue as! Float)
                                  }
                              }
                             // let lineChart = self.setLineChart()
                          //    self.graphview.addSubview(lineChart)
               }
        self.setLineC()

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3.3, height: collectionView.frame.height)
    }

    // Layout: Set Edges
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0) // top, left, bottom, right
    }

}

 class ChartsStringFormatter: NSObject, IAxisValueFormatter  {

   var nameValues: [String]! =  ["A", "B", "C", "D"]

    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return String(describing: nameValues[Int(value)])
    }
}
