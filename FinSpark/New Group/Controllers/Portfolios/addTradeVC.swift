//
//  addTradeVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 23/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextFields
import iOSDropDown
import Alamofire
class addTradeVC: UIViewController {
    @IBOutlet weak var instrumentBtn: DropDown!
    @IBOutlet weak var typeBtn: DropDown!
    @IBOutlet weak var contractBtn: DropDown!
    @IBOutlet weak var dateBtn: DropDown!
    @IBOutlet weak var collctionviewTopTabs: UICollectionView!
    
    @IBOutlet var noflots_tf: MDCTextField!
    @IBOutlet var lotsize_tf: MDCTextField!
    @IBOutlet var quantity_tf: MDCTextField!
    @IBOutlet var price_tf: MDCTextField!
    @IBOutlet var newdate_tf: MDCTextField!
    
    @IBOutlet var portfolio_name: UILabel!
    
    var instrumentControl:MDCTextInputControllerFloatingPlaceholder!
    var instrumentypeControl:MDCTextInputControllerFloatingPlaceholder!
    var contractControl:MDCTextInputControllerFloatingPlaceholder!
    var dateControl:MDCTextInputControllerFloatingPlaceholder!
    var noflotsControl:MDCTextInputControllerFloatingPlaceholder!
    var lotsizeControl:MDCTextInputControllerFloatingPlaceholder!
    var quantityControl:MDCTextInputControllerFloatingPlaceholder!
    var priceControl:MDCTextInputControllerFloatingPlaceholder!
    var newdateControl:MDCTextInputControllerFloatingPlaceholder!
    
    var titlesArray = ["Buy", "Sell"]
    var InstrumentTypesArray = ["Futures", "Call", "Put"]
    var InstrumentTypesArrayTwo = ["near", "next", "far"]
    var strike = ""
           var option_type = ""
    var selection = NSMutableArray()
    
    var currentPortfolioItem = PorfolioItem()
    
    let dropDown = DropDown()
    var selectedTag = 0
    var symbols = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("portid",currentPortfolioItem.finsparc_portfolio_id)
        
        noflotsControl = noflots_tf.floatingTextField()
        lotsizeControl = lotsize_tf.floatingTextField()
        quantityControl = quantity_tf.floatingTextField()
        priceControl = price_tf.floatingTextField()
        newdateControl = newdate_tf.floatingTextField()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        newdate_tf.text = formatter.string(from: Date())
        collctionviewTopTabs.register(UINib(nibName: "topTabCell", bundle: nil), forCellWithReuseIdentifier: "topTabCell")
//        contractBtn.optionArray = InstrumentTypesArrayTwo
//        typeBtn.optionArray = InstrumentTypesArray
      //  instrumentBtn.frame = instrumentBtn.frame // UIView or UIBarButtonItem
        contractBtn.didSelect { (item , index ,id) in
            self.selectedTag = 3
            self.contractBtn.text = item

            //  self.contractBtn.setTitle(item, for: .normal)
              let type = (self.typeBtn.text)!
              
              if TradeModel.shared.tradeValues(ofKey: item, ofTrade: type).count != 0 {
                  let tradeitem = TradeModel.shared.tradeValues(ofKey: item, ofTrade: type)[0]
                  self.dateBtn.text = tradeitem.expiry_dt
              //    self.dateBtn.setTitle(tradeitem.expiry_dt, for: .normal)
                 // self.newdate_tf.text = tradeitem.cdate
                  self.lotsize_tf.text = tradeitem.stock
                  self.price_tf.text = tradeitem.close
                  self.strike = tradeitem.strike_pr
                  self.option_type = tradeitem.option_type

              }else{
                  self.dateBtn.text = "--"

                //  self.dateBtn.setTitle("--", for: .normal)
                //  self.newdate_tf.text = "--"
                  self.lotsize_tf.text = ""
                  self.price_tf.text = ""
              }
        }

        typeBtn.didSelect{ (item , index ,id) in
            self.selectedTag = 2
            
                self.typeBtn.text = item

              //  self.typeBtn.setTitle(item, for: .normal)
                
                if TradeModel.shared.tradeKeys(ofTrade: item).count != 0 {
                    
                    let contract = TradeModel.shared.tradeKeys(ofTrade: item)[0]
                    self.contractBtn.text = contract

                  //  self.contractBtn.setTitle(contract, for: .normal)
                    
                    if TradeModel.shared.tradeValues(ofKey: contract, ofTrade: item).count != 0 {
                        let tradeitem = TradeModel.shared.tradeValues(ofKey: contract, ofTrade: item)[0]
                        self.dateBtn.text = tradeitem.expiry_dt
                         self.newdate_tf.text = tradeitem.cdate

                      //  self.dateBtn.setTitle(tradeitem.expiry_dt, for: .normal)
                       // self.newdate_tf.text = tradeitem.cdate
                        self.lotsize_tf.text = tradeitem.stock
                        self.price_tf.text = tradeitem.close
                        
                    }else{
                        self.dateBtn.text = "--"

                       // self.dateBtn.setTitle("--", for: .normal)
                       // self.newdate_tf.text = "--"
                        self.lotsize_tf.text = ""
                        self.price_tf.text = ""
                    }
                }
            
                            
        }
        instrumentBtn.didSelect{(item , index ,id) in
       // self.valueLabel.text = "Selected String: \(selectedText) \n index: \(index)"
            print("Selected item: \(item) tag: \(self.selectedTag)")
            self.selectedTag = 1
            if self.selectedTag == 1 {
                self.instrumentBtn.text = item
              //  self.instrumentBtn.setTitle(item, for: .normal)
                self.getTrades()
                
            } else if self.selectedTag == 2 {
                self.typeBtn.text = item

              //  self.typeBtn.setTitle(item, for: .normal)
                
                if TradeModel.shared.tradeKeys(ofTrade: item).count != 0 {
                    
                    let contract = TradeModel.shared.tradeKeys(ofTrade: item)[0]
                    self.contractBtn.text = item

                  //  self.contractBtn.setTitle(contract, for: .normal)
                    
                    if TradeModel.shared.tradeValues(ofKey: contract, ofTrade: item).count != 0 {
                        let tradeitem = TradeModel.shared.tradeValues(ofKey: contract, ofTrade: item)[0]
                        self.dateBtn.text = item

                      //  self.dateBtn.setTitle(tradeitem.expiry_dt, for: .normal)
                       // self.newdate_tf.text = tradeitem.cdate
                        self.lotsize_tf.text = tradeitem.stock
                        self.price_tf.text = tradeitem.close
                        
                    }else{
                        self.dateBtn.text = "--"

                       // self.dateBtn.setTitle("--", for: .normal)
                       // self.newdate_tf.text = "--"
                        self.lotsize_tf.text = ""
                        self.price_tf.text = ""
                    }
                }
            
                            } else if self.selectedTag == 3 {
                self.contractBtn.text = item

              //  self.contractBtn.setTitle(item, for: .normal)
                let type = (self.typeBtn.text)!
                
                if TradeModel.shared.tradeValues(ofKey: item, ofTrade: type).count != 0 {
                    let tradeitem = TradeModel.shared.tradeValues(ofKey: item, ofTrade: type)[0]
                    self.dateBtn.text = tradeitem.expiry_dt
                //    self.dateBtn.setTitle(tradeitem.expiry_dt, for: .normal)
                   // self.newdate_tf.text = tradeitem.cdate
                    self.lotsize_tf.text = tradeitem.stock
                    self.price_tf.text = tradeitem.close
                    self.strike = tradeitem.strike_pr
                    self.option_type = tradeitem.option_type

                }else{
                    self.dateBtn.text = "--"

                  //  self.dateBtn.setTitle("--", for: .normal)
                  //  self.newdate_tf.text = "--"
                    self.lotsize_tf.text = ""
                    self.price_tf.text = ""
                }
                
            }else {
                self.dateBtn.text = "--"

              //  self.dateBtn.setTitle(item, for: .normal)
                
                let type = (self.typeBtn.text)!
                let contract  = (self.contractBtn.text)!
                let dates = TradeModel.shared.tradeDates(ofKey: contract, ofTrade: type)
                
                if dates.firstIndex(of: item) != nil {
                    if let index = dates.firstIndex(of: item) {
                        if TradeModel.shared.tradeValues(ofKey: contract, ofTrade: type).count != 0 {
                            let tradeitem = TradeModel.shared.tradeValues(ofKey: contract, ofTrade: type)[index]
                          //  self.newdate_tf.text = tradeitem.cdate
                            self.lotsize_tf.text = tradeitem.stock
                            self.price_tf.text = tradeitem.close
                            self.strike = tradeitem.strike_pr
                            self.option_type = tradeitem.option_type
                            
                        }else{
                          //  self.newdate_tf.text = "--"
                            self.lotsize_tf.text = ""
                            self.price_tf.text = ""
                        }
                    }
                }
            }
            
            let numberOfLots = Int(self.noflots_tf.text!) ?? 0
            let lotSize = Int(self.lotsize_tf.text!) ?? 0
            self.quantity_tf.text = "\(numberOfLots * lotSize)"
        }
        
       /* dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) tag: \(self.selectedTag)")
            
            if self.selectedTag == 1 {
                self.instrumentBtn.setTitle(item, for: .normal)
                self.getTrades()
                
            } else if self.selectedTag == 2 {
                self.typeBtn.setTitle(item, for: .normal)
                
                if TradeModel.shared.tradeKeys(ofTrade: item).count != 0 {
                    
                    let contract = TradeModel.shared.tradeKeys(ofTrade: item)[0]
                    self.contractBtn.setTitle(contract, for: .normal)
                    
                    if TradeModel.shared.tradeValues(ofKey: contract, ofTrade: item).count != 0 {
                        let tradeitem = TradeModel.shared.tradeValues(ofKey: contract, ofTrade: item)[0]
                        self.dateBtn.setTitle(tradeitem.expiry_dt, for: .normal)
                       // self.newdate_tf.text = tradeitem.cdate
                        self.lotsize_tf.text = tradeitem.stock
                        self.price_tf.text = tradeitem.close
                        
                    }else{
                        self.dateBtn.setTitle("--", for: .normal)
                       // self.newdate_tf.text = "--"
                        self.lotsize_tf.text = ""
                        self.price_tf.text = ""
                    }
                }
            
            } else if self.selectedTag == 3 {
                self.contractBtn.setTitle(item, for: .normal)
                let type = (self.typeBtn.titleLabel?.text)!
                
                if TradeModel.shared.tradeValues(ofKey: item, ofTrade: type).count != 0 {
                    let tradeitem = TradeModel.shared.tradeValues(ofKey: item, ofTrade: type)[0]
                    self.dateBtn.setTitle(tradeitem.expiry_dt, for: .normal)
                   // self.newdate_tf.text = tradeitem.cdate
                    self.lotsize_tf.text = tradeitem.stock
                    self.price_tf.text = tradeitem.close
                    self.strike = tradeitem.strike_pr
                    self.option_type = tradeitem.option_type

                }else{
                    self.dateBtn.setTitle("--", for: .normal)
                  //  self.newdate_tf.text = "--"
                    self.lotsize_tf.text = ""
                    self.price_tf.text = ""
                }
                
            }else {
                self.dateBtn.setTitle(item, for: .normal)
                
                let type = (self.typeBtn.titleLabel?.text)!
                let contract  = (self.contractBtn.titleLabel?.text)!
                let dates = TradeModel.shared.tradeDates(ofKey: contract, ofTrade: type)
                
                if dates.firstIndex(of: item) != nil {
                    if let index = dates.firstIndex(of: item) {
                        if TradeModel.shared.tradeValues(ofKey: contract, ofTrade: type).count != 0 {
                            let tradeitem = TradeModel.shared.tradeValues(ofKey: contract, ofTrade: type)[index]
                          //  self.newdate_tf.text = tradeitem.cdate
                            self.lotsize_tf.text = tradeitem.stock
                            self.price_tf.text = tradeitem.close
                            self.strike = tradeitem.strike_pr
                            self.option_type = tradeitem.option_type
                            
                        }else{
                          //  self.newdate_tf.text = "--"
                            self.lotsize_tf.text = ""
                            self.price_tf.text = ""
                        }
                    }
                }
            }
            
            let numberOfLots = Int(self.noflots_tf.text!) ?? 0
            let lotSize = Int(self.lotsize_tf.text!) ?? 0
            self.quantity_tf.text = "\(numberOfLots * lotSize)"
        }*/
        
        portfolio_name.text = currentPortfolioItem.portfolio_name
        dropDown.optionArray = []
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getInstrumentList()
    }
    //MARK:-
    
    func getInstrumentList() {
        self.AddLoader()
        StockListModel.shared.process { (model) in
            if model.list.count != 0 {
                self.instrumentBtn.text = model.list[0]
//                                  for obj in model.list
//                                  {
//                                      self.symbols.append(obj.symbol)
//                                  }
                self.instrumentBtn.optionArray = model.list
              //  self.instrumentBtn.setTitle(model.list[0], for: .normal)
                self.getTrades()
            }else{
                self.RemoveLoader()
            }
        }
    }
    
    func getTrades() {
        self.AddLoader()
        TradeModel.shared.getTradeInfo(trade: self.instrumentBtn.text!) { (model) in
            self.RemoveLoader()
            
            if TradeModel.shared.tradeTypes.count != 0 {
                let type = TradeModel.shared.tradeTypes[0]
                self.typeBtn.text = type
             //   self.typeBtn.setTitle(type, for: .normal)
                
                if TradeModel.shared.tradeKeys(ofTrade: type).count != 0 {
                    let contract = TradeModel.shared.tradeKeys(ofTrade: type)[0]
                    self.contractBtn.text = contract
                //    self.contractBtn.setTitle(contract, for: .normal)
                    
                    if TradeModel.shared.tradeValues(ofKey: contract, ofTrade: type).count != 0 {
                        let tradeitem = TradeModel.shared.tradeValues(ofKey: contract, ofTrade: type)[0]
                        self.dateBtn.text = tradeitem.expiry_dt
                    //    self.dateBtn.setTitle(tradeitem.expiry_dt, for: .normal)
                      //  self.newdate_tf.text = tradeitem.cdate
                        self.lotsize_tf.text = tradeitem.stock
                    }else{
                        self.dateBtn.text = ""
                       // self.dateBtn.setTitle("--", for: .normal)
                     //   self.newdate_tf.text = "--"
                        self.lotsize_tf.text = ""
                    }
                    self.typeBtn.optionArray = TradeModel.shared.tradeTypes
                    self.contractBtn.optionArray = TradeModel.shared.tradeKeys(ofTrade: (self.typeBtn.text)!)

                    let numberOfLots = Int(self.noflots_tf.text!) ?? 0
                    let lotSize = Int(self.lotsize_tf.text!) ?? 0
                    self.quantity_tf.text = "\(numberOfLots * lotSize)"
                }
                
            }
            
        }
        
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    @IBAction func dismissAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func instrumentAction(_ sender: UIButton) {
        selectedTag = 1
        dropDown.optionArray = StockListModel.shared.list
        dropDown.showList()
    }
    
    @IBAction func typeAction(_ sender: UIButton) {
        dropDown.frame = instrumentBtn.frame // UIView or UIBarButtonItem

        selectedTag = 2
        dropDown.optionArray = TradeModel.shared.tradeTypes
        dropDown.showList()
    }
    
    @IBAction func contractAction(_ sender: UIButton) {
        selectedTag = 3
        dropDown.optionArray = TradeModel.shared.tradeKeys(ofTrade: (self.typeBtn.text)!)
        dropDown.showList()
    }
    
    @IBAction func dateAction(_ sender: UIButton) {
        dropDown.frame = dateBtn.frame // UIView or UIBarButtonItem

        selectedTag = 4
        let type = (self.typeBtn.text)!
        let contract  = (self.contractBtn.text)!
        dropDown.optionArray = TradeModel.shared.tradeDates(ofKey: contract, ofTrade: type)
        dropDown.showList()
    }
    
    @IBAction func SubmitAction(_ sender: UIButton) {
        
        if instrumentBtn.text == "--" || typeBtn.text == "--" || contractBtn.text == "--" || dateBtn.text == "--" || price_tf.text!.isEmpty || noflots_tf.text!.isEmpty || quantity_tf.text!.isEmpty {
            return
        }
        
        var actionType = "BUY"
        if selection.contains(1) {
            actionType = "SELL"
        }
        
        let type = (self.typeBtn.text)!
        let contract  = (self.contractBtn.text)!
        let expDate = (self.dateBtn.text)!
        let dates = TradeModel.shared.tradeDates(ofKey: contract, ofTrade: type)
        
       

        if dates.firstIndex(of: expDate) != nil {
            if let index = dates.firstIndex(of: expDate) {
                if TradeModel.shared.tradeValues(ofKey: contract, ofTrade: type).count != 0 {
                    let tradeitem = TradeModel.shared.tradeValues(ofKey: contract, ofTrade: type)[index]
                    strike = tradeitem.strike_pr
                    option_type = tradeitem.option_type
                }
            }
        }
        
        let parameters : NSDictionary = ["type":actionType, "instrument_name":instrumentBtn.text as Any, "instrument_type":option_type, "contract":contract, "transaction_date":newdate_tf.text!, "strike_pr":strike, "quantity":quantity_tf.text!, "option_type":option_type, "expiry_dt":expDate, "user_id":LoginModel().localData().finsparc_user_id, "price":price_tf.text!, "portfolio_id":currentPortfolioItem.finsparc_portfolio_id, "bhavcopy_id":"0", "id":"0", "close":price_tf.text!]

        CreateTradeModel().process(params: parameters) { (model) in
            print("message",model.message)
              self.ShowToast(message: model.message) {
              self.RemoveLoader()
                let nc = NotificationCenter.default
                       nc.post(name: Notification.Name("refreshmylist"), object: nil)

              self.dismiss(animated: true, completion: nil)
            }
        }
    }
}

//MARK: Collectionview Methods
extension addTradeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3.3, height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collctionviewTopTabs.dequeueReusableCell(withReuseIdentifier: "topTabCell", for: indexPath) as! topTabCell
        cell.lbl_Headings.text = self.titlesArray[indexPath.row]
        cell.lbl_Headings.roundUp()
        
        if selection.count == 0 {
            selection.add(0)
        }
        
        if selection.contains(indexPath.row) {
            cell.lbl_Headings.backgroundColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
            cell.lbl_Headings.textColor = UIColor.white
            
        }else{
            cell.lbl_Headings.backgroundColor = UIColor.clear
            cell.lbl_Headings.textColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if selection.contains(indexPath.row) {
            selection.removeAllObjects()
        }else{
            selection.removeAllObjects()
            selection.add(indexPath.row)
        }
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
}

extension addTradeVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var num = noflots_tf.text!
        if string == "" {
            if num != "" {
                num = String(num.dropLast(1))
            }
        }
        
        let numberOfLots = Int(num+string) ?? 0
        let lotSize = Int(lotsize_tf.text!) ?? 0
        let quantity = numberOfLots * lotSize
        quantity_tf.text = "\(quantity)"
        return true
    }
}
