//
//  portfolioReviewVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 10/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class portfolioReviewVC: UIViewController {
    
    @IBOutlet var tableview: UITableView!
    
    var collapsedSize = CGFloat(55)
    var expandedSize = CGFloat(375)
    
    var expand = true
    
    var prevIndex : IndexPath?
    var currentIndex : IndexPath?
    
    var subtitleArray = NSMutableArray()
    var titleArray = NSMutableArray()
    var collapsedarray = [Bool]()
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.tableview.dataSource = self
        self.tableview.delegate = self
        
        let nib = UINib(nibName: "reviewHeaderCell", bundle: nil)
        tableview.register(nib, forHeaderFooterViewReuseIdentifier: "reviewHeaderCell")
        tableview.register(UINib(nibName: "reviewCell", bundle: nil), forCellReuseIdentifier: "reviewCell")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
        
        self.CustomizeNavBar_with(title: "", button_text: "Review", bartint: CustomColours.blue, backButton: true) {
            self.navigationController?.popViewController(animated: true)
        }
        self.getReviewData()
    }
    
    //MARK:-
    
    func getReviewData() {
        self.AddLoader()
        ReviewModel.shard.process { (fmodl) in
            self.RemoveLoader()
            self.collapsedarray = []
            for _ in ReviewModel.shard.list
            {
                self.collapsedarray.append(false)
            }
            self.tableview.reloadData()
        }
    }
}


extension portfolioReviewVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 37
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return ReviewModel.shard.list.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if ReviewModel.shard.list.count > 0
        {
        if collapsedarray[section] {
            return 12
        }else{
            return 0
        }
        }else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = self.tableview.dequeueReusableHeaderFooterView(withIdentifier: "reviewHeaderCell") as! reviewHeaderCell
        
        header.name.text =  ReviewModel.shard.list[section].symbol
        header.name.isUserInteractionEnabled = true
        let transaction = ReviewModel.shard.list[section].transaction
        header.price.text = transaction[0].remaining_qty
        header.status.text = transaction[0].mtm_price
        header.setCollapsed(expand)
        if collapsedarray[section] {
          //  header.singleLabel.isHidden = true
            header.arrowIcon.image = UIImage(named: "up")
        }else{
           // header.singleLabel.isHidden = false
            header.arrowIcon.image = UIImage(named: "down")
        }
        
        header.section = section
        header.delegate = self
        let tapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(headerTapped(_:))
        )
        header.tag = section
        header.addGestureRecognizer(tapGestureRecognizer)
        return header
    }
    
    @objc func headerTapped(_ sender: UITapGestureRecognizer?) {
        guard let section = sender?.view?.tag else {
            return
        }
        if collapsedarray[section] {
            self.collapsedarray[section] = false
        }else{
            self.collapsedarray[section] = true
        }
        self.tableview.reloadData()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "reviewCell", for: indexPath) as! reviewCell
        
        if ReviewModel.shard.list[indexPath.section].review_details.count > 0
        {
            let item = ReviewModel.shard.list[indexPath.section].review_details[0]
            if indexPath.row == 0
            {
            if item.future_closing > item.review_buy_trigger
            {

                    cell.review.text = "Stock is above trigger price"
                let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "Stock is above trigger price")
                               attributedString.setColorForText(textForAttribute: "above", withColor: UIColor.green)

                                   cell.review.attributedText = attributedString
                
            }else if item.future_closing < item.review_buy_trigger
            {
                cell.review.text = "Stock is below trigger price"
                let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "Stock is below trigger price")
                attributedString.setColorForText(textForAttribute: "below", withColor: UIColor.red)

                    cell.review.attributedText = attributedString
                
            }else if item.future_closing == item.review_buy_trigger
            {
                    cell.review.text = "Stock is below trigger price"
            }
            } else if indexPath.row == 1
            {
                cell.review.text = "Finsparc score for the stock is " + item.total_score
            }else if indexPath.row == 2
            {
                cell.review.text = "Stock ranks at " + item.rank + " out of " + item.stock_count
            }else if indexPath.row == 3
            {
                if item.review_itm_call == 1
                {
                cell.review.text = "NTM Call (" + item.itm_call_strike + ") of the stock is +ve"
                    let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: cell.review.text!)
                    attributedString.setColorForText(textForAttribute: "+ve", withColor: UIColor.green)
                        cell.review.attributedText = attributedString
                                    }else
                {
                    cell.review.text = "NTM Call (" + item.itm_call_strike + ") of the stock is -ve"
                    let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: cell.review.text!)
                                       attributedString.setColorForText(textForAttribute: "-ve", withColor: UIColor.red)

                    cell.review.attributedText = attributedString                }
            }else if indexPath.row == 4
                {
                    if item.review_itm_put == 1
                    {
                    cell.review.text = "NTM put (" + item.itm_put_strike + ") of the stock is -ve"
                        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: cell.review.text!)
                        attributedString.setColorForText(textForAttribute: "-ve", withColor: UIColor.green)
                            cell.review.attributedText = attributedString
                    }else
                    {
                        cell.review.text = "NTM put (" + item.itm_put_strike + ") of the stock is +ve"
                        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: cell.review.text!)
                        attributedString.setColorForText(textForAttribute: "+ve", withColor: UIColor.red)
                            cell.review.attributedText = attributedString

                    }
            } else if indexPath.row == 5
            {
            if item.expiry == 1
            {
                let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "Stock is above last expiry price")
                                              attributedString.setColorForText(textForAttribute: "above", withColor: UIColor.green)

                                                  cell.review.attributedText = attributedString
                    
                
            }else
            {

                    let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "Stock is below last expiry price")
                                                                 attributedString.setColorForText(textForAttribute: "above", withColor: UIColor.red)

                                                                     cell.review.attributedText = attributedString
                
            }
            }else if indexPath.row == 6
            {
            if item.percent_10 > 0
            {
                cell.review.text = "Stock has gained " + String(item.percent_10) + "% in the last 10 day"

                 let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "Stock has gained " + String(item.percent_10) + " in the last 10 day")
                                                                                    attributedString.setColorForText(textForAttribute: "gained " + String(item.percent_10) + "%" , withColor: UIColor.green)

                                                                                        cell.review.attributedText = attributedString
                
            }else
            {
                cell.review.text = "Stock has lost " + String(item.percent_10) + "% in the last 10 day"

                   let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "Stock has lost " + String(item.percent_10) + " in the last 10 day")
                    attributedString.setColorForText(textForAttribute: "lost " + String(item.percent_10) + "%" , withColor: UIColor.red)

                        cell.review.attributedText = attributedString
            }
            }else if indexPath.row == 7
            {
                    if item.ce_contract == 1
                    {
                    cell.review.text = "Highest volume Call (" + item.max_contract_ce + ") of the stock is "
                        let attrs1 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 14), NSAttributedString.Key.foregroundColor : CustomColours.textGrey]

                                           let attrs2 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 13), NSAttributedString.Key.foregroundColor : UIColor.green]

                        let attributedString1 = NSMutableAttributedString(string:cell.review.text!, attributes:attrs1 as [NSAttributedString.Key : Any])

                                           let attributedString2 = NSMutableAttributedString(string:"+ve", attributes:attrs2 as [NSAttributedString.Key : Any])

                                           attributedString1.append(attributedString2)
                                           cell.review.attributedText = attributedString1
                    }else
                    {
                        cell.review.text = "Highest volume Call (" + item.max_contract_ce + ") of the stock is "
                        let attrs1 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 14), NSAttributedString.Key.foregroundColor : CustomColours.textGrey]

                                                              let attrs2 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 14), NSAttributedString.Key.foregroundColor : UIColor.red]

                        let attributedString1 = NSMutableAttributedString(string:cell.review.text!, attributes:attrs1 as [NSAttributedString.Key : Any])

                                                              let attributedString2 = NSMutableAttributedString(string:"-ve", attributes:attrs2 as [NSAttributedString.Key : Any])

                                                              attributedString1.append(attributedString2)
                                                              cell.review.attributedText = attributedString1

                    }
            } else if indexPath.row == 8
            {
                    if item.pe_contract == 1
                    {
                    cell.review.text = "Highest volume Put (" + item.min_contract_pe + ") of the stock is "
                        let attrs1 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 14), NSAttributedString.Key.foregroundColor : CustomColours.textGrey]

                                           let attrs2 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 13), NSAttributedString.Key.foregroundColor : UIColor.green]

                        let attributedString1 = NSMutableAttributedString(string:cell.review.text!, attributes:attrs1 as [NSAttributedString.Key : Any])

                                           let attributedString2 = NSMutableAttributedString(string:"+ve", attributes:attrs2 as [NSAttributedString.Key : Any])

                                           attributedString1.append(attributedString2)
                                           cell.review.attributedText = attributedString1
                    }else
                    {
                        cell.review.text = "Highest volume Put (" + item.min_contract_pe + ") of the stock is "
                        let attrs1 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 14), NSAttributedString.Key.foregroundColor : CustomColours.textGrey]

                                                              let attrs2 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 14), NSAttributedString.Key.foregroundColor : UIColor.red]

                        let attributedString1 = NSMutableAttributedString(string:cell.review.text!, attributes:attrs1 as [NSAttributedString.Key : Any])

                                                              let attributedString2 = NSMutableAttributedString(string:"-ve", attributes:attrs2 as [NSAttributedString.Key : Any])

                                                              attributedString1.append(attributedString2)
                                                              cell.review.attributedText = attributedString1

                    }
            } else if indexPath.row == 9{
                if item.wap_call == 1
                {
                cell.review.text = "Overall Call position in the stock is +ve"
                    let attrs1 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 14), NSAttributedString.Key.foregroundColor : CustomColours.textGrey]

                                       let attrs2 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 14), NSAttributedString.Key.foregroundColor : UIColor.green]

                    let attributedString1 = NSMutableAttributedString(string:cell.review.text!, attributes:attrs1 as [NSAttributedString.Key : Any])

                                       let attributedString2 = NSMutableAttributedString(string:"+ve", attributes:attrs2 as [NSAttributedString.Key : Any])

                                       attributedString1.append(attributedString2)
                                       cell.review.attributedText = attributedString1
                }else
                {
                    cell.review.text = "Overall Call position in the stock is -ve"
                    let attrs1 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 14), NSAttributedString.Key.foregroundColor : CustomColours.textGrey]

                                                          let attrs2 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 14), NSAttributedString.Key.foregroundColor : UIColor.red]

                    let attributedString1 = NSMutableAttributedString(string:cell.review.text!, attributes:attrs1 as [NSAttributedString.Key : Any])

                                                          let attributedString2 = NSMutableAttributedString(string:"-ve", attributes:attrs2 as [NSAttributedString.Key : Any])

                                                          attributedString1.append(attributedString2)
                                                          cell.review.attributedText = attributedString1

                }
            }else if indexPath.row == 10{
                if item.wap_put == 1
                {
                cell.review.text = "Overall Put position in the stock is -ve"
                    let attrs1 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 14), NSAttributedString.Key.foregroundColor : CustomColours.textGrey]

                                       let attrs2 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 14), NSAttributedString.Key.foregroundColor : UIColor.green]

                    let attributedString1 = NSMutableAttributedString(string:cell.review.text!, attributes:attrs1 as [NSAttributedString.Key : Any])

                                       let attributedString2 = NSMutableAttributedString(string:"-ve", attributes:attrs2 as [NSAttributedString.Key : Any])

                                       attributedString1.append(attributedString2)
                                       cell.review.attributedText = attributedString1
                }else
                {
                    cell.review.text = "Overall Put position in the stock is +ve"
                    let attrs1 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 14), NSAttributedString.Key.foregroundColor : CustomColours.textGrey]

                                                          let attrs2 = [NSAttributedString.Key.font : UIFont(name: "assistant-regular", size: 14), NSAttributedString.Key.foregroundColor : UIColor.red]

                    let attributedString1 = NSMutableAttributedString(string:cell.review.text!, attributes:attrs1 as [NSAttributedString.Key : Any])

                                                          let attributedString2 = NSMutableAttributedString(string:"+ve", attributes:attrs2 as [NSAttributedString.Key : Any])

                                                          attributedString1.append(attributedString2)
                                                          cell.review.attributedText = attributedString1

                }
            }else if indexPath.row == 11
            {
                let innereview = item.prices
                cell.review.text = "The stock has made 11 days High/Low of Rs " + innereview[0].price_high + " and Rs " + innereview[0].price_low
            }
            }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
}


// MARK: - Section Header Delegate
extension portfolioReviewVC: CollapsiblesectionTableViewHeaderDelegate {
    
    func toggleheaderSection(_ header: reviewHeaderCell, section: Int) {
        let collapse = !collapsedarray[section]
        
        if collapse {
            expand = false
        }else{
            expand = true
        }
        
        collapsedarray[section] = collapse
        tableview.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
    }
    
}
extension NSMutableAttributedString {

    func setColorForText(textForAttribute: String, withColor color: UIColor) {
        let range: NSRange = self.mutableString.range(of: textForAttribute, options: .caseInsensitive)

        // Swift 4.2 and above
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)

        // Swift 4.1 and below
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
    }

}
