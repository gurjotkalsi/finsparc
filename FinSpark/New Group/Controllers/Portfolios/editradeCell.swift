//
//  editradeCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 08/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class editradeCell: UITableViewCell {
    @IBOutlet var pname: UILabel!
    @IBOutlet var type: UILabel!
    @IBOutlet var deleteButton: UIButton!
    @IBOutlet var pquantity: UITextField!
    @IBOutlet var pdate: UITextField!
    @IBOutlet var pprice: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        pquantity.addlightBorder()
        pdate.addlightBorder()
        pprice.addlightBorder()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
