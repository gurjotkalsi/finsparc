//
//  reviewHeaderCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 10/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit
protocol CollapsiblesectionTableViewHeaderDelegate {
    func toggleheaderSection(_ header: reviewHeaderCell, section: Int)
}
class reviewHeaderCell: UITableViewHeaderFooterView {
    var delegate: CollapsiblesectionTableViewHeaderDelegate?

    @IBOutlet var name: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet var status: UILabel!
    @IBOutlet var arrowIcon: UIImageView!
    var section: Int = 0

    
           override func awakeFromNib() {
            super.awakeFromNib()
            addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(customHeader.tapHeader(_:))))

    }

      @objc func tapHeader(_ gestureRecognizer: UITapGestureRecognizer) {
          guard let cell = gestureRecognizer.view as? reviewHeaderCell else {
              return
          }
          
          delegate?.toggleheaderSection(self, section: cell.section)
      }
      
      func setCollapsed(_ collapsed: Bool) {
          //
          // Animate the arrow rotation (see Extensions.swf)
          //
          if collapsed
          {
             // singleLabel.isHidden = true
              arrowIcon.image = UIImage(named: "up")
          }else
          {
             // singleLabel.isHidden = false

              arrowIcon.image = UIImage(named: "down")

          }
      }
    
}
