//
//  addPortfoilioNameVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 11/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTextFields

class addPortfoilioNameVC: UIViewController {
    @IBOutlet var name_tf: MDCTextField!
    var nameControl:MDCTextInputControllerFloatingPlaceholder!

    override func viewDidLoad() {
        super.viewDidLoad()
     

        // Do any additional setup after loading the view.
    }
    
    @IBAction func addName(_ sender: UIButton){
        
               if name_tf.text == "" {
                   self.ShowToast(message: "Please enter portfolio name")
               }else{
                   if !isInternetConnected() {
                       self.ShowToast(message: "Check internet connection!")
                       return
                   }

                   self.AddLoader()
                   portfolioNameModel().sendName(parameter: ["portfolio_name":name_tf.text as Any, "id":"0","user_id":(LoginModel().localData().finsparc_user_id)]) { (pmodel) in
                       self.RemoveLoader()
                    let nc = NotificationCenter.default
                           nc.post(name: Notification.Name("refreshdata"), object: nil)
                       self.ShowToast(message: pmodel.msg) {
                           self.name_tf.text = ""
                        self.dismiss(animated: true, completion: nil)

                       }
                   }
               }
     
    }
    
    @IBAction func dismissAction(_ sender: UIButton) {
        self.dismiss(animated:true,  completion:nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
