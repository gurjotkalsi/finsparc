//
//  portfoilioTransactionCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 07/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class portfoilioTransactionCell: UITableViewCell {
    @IBOutlet var tfirstdate: UILabel!
    @IBOutlet var tseconddate: UILabel!
    @IBOutlet var tremainingquantity: UILabel!
    @IBOutlet var tsellingquantity: UILabel!
    @IBOutlet var tsellingprice: UILabel!
    @IBOutlet var tfilterprice: UILabel!
    @IBOutlet var tbooked: UILabel!
    @IBOutlet var tmtm: UILabel!
    @IBOutlet var tposition: UILabel!
    @IBOutlet var tname: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
