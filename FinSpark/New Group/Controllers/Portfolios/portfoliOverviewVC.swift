//
//  portfoliOverviewVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 19/02/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class portfoliOverviewVC: UIViewController {
    
    @IBOutlet weak var collectionview: UICollectionView!
    @IBOutlet weak var tableview: UITableView!
    
    @IBOutlet var pname: UILabel!
    @IBOutlet var pbooked: UILabel!
    @IBOutlet var pmtm: UILabel!
    @IBOutlet weak var addButton: UIButton!

    var titlesArray = ["Day's Gain", "Week's Gain", "Month's Gain", "YTD"]
    var selection = NSMutableArray()
    var item = PorfolioItem()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
       // self.addButton.setUpcircularButton()

        collectionview.register(UINib(nibName: "topTabCell", bundle: nil), forCellWithReuseIdentifier: "topTabCell")
        selection.add(0)
        collectionview.reloadData()
        tableview.register(UINib(nibName: "portfoliOverviewCell", bundle: nil), forCellReuseIdentifier: "portfoliOverviewCell")
        
        pname.text = item.portfolio_name
        pbooked.text = item.portfolio_booked_amount
        pmtm.text = item.mtmjson
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        sliderHolder.fadeOut { }
        
        self.CustomizeNavBar_with(title: "", button_text: "Portfolio Overview", bartint: CustomColours.blue, backButton: true) {
            self.navigationController?.popViewController(animated: true)
        }
        
        self.AddLoader()
        PortfoliOverviewModel.shared.details(parameter: ["filter": "day" ,
                                                     "symbol":"null",
                                                     "user_id":LoginModel().localData().finsparc_user_id,
                                                     "mtm":"1"], type: .day) { (pmodel) in
                                                        self.RemoveLoader()
        print(pmodel.objectsList.count)
        self.tableview.reloadData()
        }

        
    }
    
    @IBAction func AddTradeAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "portfolioReviewVC") as! portfolioReviewVC
      //  vc.currentPortfolioItem = item
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

//MARK: Collectionview Methods
extension portfoliOverviewVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PortfoliOverviewModel.shared.objectsList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "portfoliOverviewCell", for: indexPath) as! portfoliOverviewCell
        let object = PortfoliOverviewModel.shared.objectsList[indexPath.row]
        
        cell.instrument_name.text = object.instrument_name + " " + object.expiry_dt
        
        cell.last_close.text = object.last_close
        cell.ht.text = "H"
        if let lastclose = Double(object.last_close)
        {
            if let close = Double(object.close) {
                let curday = ((close -  lastclose) / lastclose ) * 100
                cell.percent_pending.text = String(curday).getTwoDecimalString() + "%"
            }
        }
//        cell.percent_pending.text = ""
        
        cell.open_position.text = "+ " + object.remaining_qty + " @ " + object.open_buy_price.getTwoDecimalString()
        cell.booked.text = object.booked
        cell.mtm.text = object.mtm.getTwoDecimalString()
        cell.hbutton.tag = indexPath.row
        cell.hbutton.addTarget(self, action: #selector(connected(sender:)), for: .touchUpInside)

        return cell
    }
    @objc func connected(sender: UIButton){
        let buttonTag = sender.tag
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "particularOverviewVC") as! particularOverviewVC
                   let object = PortfoliOverviewModel.shared.objectsList[buttonTag]
        vc.item = object
                   self.navigationController?.pushViewController(vc, animated: true)

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 111
    }
    
}

extension portfoliOverviewVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.collectionview.dequeueReusableCell(withReuseIdentifier: "topTabCell", for: indexPath) as! topTabCell
        
        cell.lbl_Headings.text = self.titlesArray[indexPath.row]
        cell.lbl_Headings.roundUp()
        
        if selection.contains(indexPath.row) {
            cell.lbl_Headings.backgroundColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
            cell.lbl_Headings.textColor = UIColor.white
            
        }else{
            cell.lbl_Headings.backgroundColor = UIColor.clear
            cell.lbl_Headings.textColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
        }
        
        cell.layer.cornerRadius = collectionView.frame.height/2
        cell.layer.masksToBounds = true
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if selection.contains(indexPath.row) {
            selection.removeAllObjects()
        }else{
            selection.removeAllObjects()
            selection.add(indexPath.row)
        }
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3.3, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
}
