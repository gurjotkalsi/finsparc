//
//  squareupCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 07/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class squareupCell: UITableViewCell {
       @IBOutlet var squareView: UIView!
       @IBOutlet var pname: UILabel!
       @IBOutlet var pdate: UILabel!
       @IBOutlet var pquantity: UILabel!
       @IBOutlet var ptype: UILabel!
       @IBOutlet var pprice: UILabel!
    @IBOutlet var squareup: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.squareView.addRoundCorners(5.0)
        self.squareView.addBorder()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
