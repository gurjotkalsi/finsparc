//
//  portfoliOverviewCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 19/02/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class portfoliOverviewCell: UITableViewCell {

    @IBOutlet var instrument_name: UILabel!
    @IBOutlet var last_close: UILabel!
    @IBOutlet var ht: UILabel!

    @IBOutlet var percent_pending: UILabel!
    
    @IBOutlet var open_position: UILabel!
    @IBOutlet var booked: UILabel!
    @IBOutlet var mtm: UILabel!
    
    @IBOutlet var hbutton: UIButton!
    
    //MARK:-
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
