//
//  PortfolioVC.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 08/02/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class PortfolioVC: UIViewController {
    @IBOutlet weak var collctionviewTopTabs: UICollectionView!
    @IBOutlet weak var profileTableView: UITableView!
    @IBOutlet weak var addButton: UIButton!

    var titlesArray = ["Day's Gain", "Week's Gain", "Month's Gain", "YTD", "Do not zero net positions"]
    var selection = NSMutableArray()
    var currentType : PortfolioFilters = .day
    var totalall = 0.0
    override func viewDidLoad() {
        super.viewDidLoad()
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPress(sender:)))
        profileTableView.addGestureRecognizer(longPress)

        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(refreshdata), name: Notification.Name("refreshdata"), object: nil)
       
        self.addButton.setUpcircularButton()

        collctionviewTopTabs.register(UINib(nibName: "topTabCell", bundle: nil), forCellWithReuseIdentifier: "topTabCell")
        
        selection.add(0)
        collctionviewTopTabs.reloadData()
        
        profileTableView.register(UINib(nibName: "portfolioCell", bundle: nil), forCellReuseIdentifier: "portfolioCell")
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
        
        self.LeftBarButton(image_name: "menu", button_text: "  Portfolio", text_color: .white) {
            self.ShowMenu()
        }
    }
    @objc func refreshdata ()
    {
        self.getData()

    }
    
    @objc func addTapped()
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "portfoliOverviewVC") as! portfoliOverviewVC
      //  vc.item = PortfolioModel.shared.list[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        self.getData()
        sliderHolder.fadeIn { }
    }
    @objc func longPress(sender: UILongPressGestureRecognizer) {

        if sender.state == UIGestureRecognizer.State.began {
            let touchPoint = sender.location(in: profileTableView)
            if let indexPath = profileTableView.indexPathForRow(at: touchPoint) {
                // your code here, get the row for the indexPath or do whatever you want
                print("Long press Pressed:)")
                let item = PortfolioModel.shared.list[indexPath.row - 1]

                let alertController = UIAlertController(title: "PORTFOLIO", message: "", preferredStyle: .alert)

                alertController.addTextField { (textField : UITextField!) -> Void in
                    
                    textField.text = item.portfolio_name
                }

                let editAction = UIAlertAction(title: "Edit", style: .default, handler: { alert -> Void in
                    let firstTextField = alertController.textFields![0] as UITextField
                    
                    self.updatePortfolio(portfolioname: firstTextField.text!, portid: item.finsparc_portfolio_id)
                })

                let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { alert -> Void in
                })
                let deleteAction = UIAlertAction(title: "Delete", style: .default, handler: { alert -> Void in
                    self.deletePortfolio(portfolioid: item.finsparc_portfolio_id)
                })

               

                alertController.addAction(editAction)
                alertController.addAction(cancelAction)
                alertController.addAction(deleteAction)

                self.present(alertController, animated: true, completion: nil)
            }
        }


    }
    func updatePortfolio (portfolioname: String, portid: String)

    {
    self.AddLoader()
    portfolioNameModel().sendName(parameter: ["portfolio_name":portfolioname, "id":portid,"user_id":(LoginModel().localData().finsparc_user_id)]) { (pmodel) in
        self.RemoveLoader()
        self.ShowToast(message: pmodel.msg) {
            self.getData()

         self.dismiss(animated: true, completion: nil)

        }
    }
    }
    
    func deletePortfolio (portfolioid: String)
    {
        self.AddLoader()
        PortfolioDelete().delete(parameter: ["portfolio_id":portfolioid]) { (pmodel) in
            self.RemoveLoader()
            self.ShowToast(message: pmodel.msg) {
                self.getData()

             self.dismiss(animated: true, completion: nil)

            }
        }
    }
    
    func getData() {
        self.AddLoader()
        PortfolioModel.shared.process(type: currentType) { (model) in
            self.RemoveLoader()
            self.totalall = 0.0
            for item in PortfolioModel.shared.list
            {
                let doub = Double(item.mtmjson)
                let mtm = doub
                self.totalall += mtm!
            }
            self.profileTableView.reloadData()
        }
    }
}

extension PortfolioVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 53
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PortfolioModel.shared.list.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "portfolioCell", for: indexPath) as! portfolioCell
        if indexPath.row == 0
        {
            cell.name.text = "All"
            cell.booked.text = ".00"
            cell.mtm.text = String(totalall)
            cell.total.text = String(totalall)
        }else
        {
        let item = PortfolioModel.shared.list[indexPath.row - 1]
        cell.name.text = item.portfolio_name
        cell.booked.text = item.portfolio_booked_amount
        cell.mtm.text = item.mtmjson
        cell.total.text = item.mtmjson
        if let value = Float(item.mtmjson)
          {
              if value < 0
              {
                cell.booked.textColor = .red
                cell.total.textColor = .red
              }else
              {
                cell.booked.textColor = CustomColours.green
                cell.total.textColor = CustomColours.green
              }
          }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "portfoliOverviewVC") as! portfoliOverviewVC
//        vc.item = PortfolioModel.shared.list[indexPath.row]
//        self.navigationController?.pushViewController(vc, animated: true)
        
        if indexPath.row == 0
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "portfoliOverviewVC") as! portfoliOverviewVC
            //  vc.item = PortfolioModel.shared.list[indexPath.row]
              self.navigationController?.pushViewController(vc, animated: true)
        }else
        {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "portfolioNameDetail") as! portfolioNameDetail
            vc.item = PortfolioModel.shared.list[indexPath.row - 1]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}


extension PortfolioVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.titlesArray.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3.3, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.collctionviewTopTabs.dequeueReusableCell(withReuseIdentifier: "topTabCell", for: indexPath) as! topTabCell
        cell.lbl_Headings.text = self.titlesArray[indexPath.row]
        cell.lbl_Headings.roundUp()
        if selection.contains(indexPath.row) {
            cell.lbl_Headings.backgroundColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
            cell.lbl_Headings.textColor = UIColor.white
            
        }else{
            cell.lbl_Headings.backgroundColor = UIColor.clear
            cell.lbl_Headings.textColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.item == 0 {
            currentType = .day
        }else if indexPath.item == 1 {
            currentType = .week
        }else if indexPath.item == 2 {
            currentType = .month
        }else{//3
            currentType = .all
        }
        self.getData()
        
        selection.removeAllObjects()
        selection.add(indexPath.row)
        collectionView.reloadData()
    }
    
}
