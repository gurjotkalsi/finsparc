//
//  portfolioCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 18/02/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class portfolioCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var booked: UILabel!
    @IBOutlet weak var mtm: UILabel!
    @IBOutlet weak var total: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
