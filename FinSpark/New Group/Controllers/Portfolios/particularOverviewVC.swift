//
//  particularOverviewVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 11/07/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class particularOverviewVC: UIViewController {
    
    @IBOutlet weak var tableview: UITableView!
    
    @IBOutlet var pname: UILabel!
    @IBOutlet var price: UILabel!

    var item = PortfolioverviewObject()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
  
        tableview.register(UINib(nibName: "portOverviewDetailCell", bundle: nil), forCellReuseIdentifier: "portOverviewDetailCell")
        
        pname.text = item.instrument_name + "\n" + item.expiry_dt
        price.text = "Current Price\n" + item.last_close
        self.tableview.reloadData()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        sliderHolder.fadeOut { }
        
        self.CustomizeNavBar_with(title: "", button_text: "Portfolio Overview", bartint: CustomColours.blue, backButton: true) {
            self.navigationController?.popViewController(animated: true)
        }
        
       // self.AddLoader()
//        PortfoliOverviewModel.shared.details(parameter: ["filter": "day" ,
//                                                     "symbol":"null",
//                                                     "user_id":LoginModel().localData().finsparc_user_id,
//                                                     "mtm":"1"], type: .day) { (pmodel) in
//                                                        self.RemoveLoader()
//        print(pmodel.objectsList.count)
        }
    }
    


//MARK: Collectionview Methods
extension particularOverviewVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "portOverviewDetailCell", for: indexPath) as! portOverviewDetailCell
       // let object = PortfoliOverviewModel.shared.objectsList[indexPath.row]
        cell.booked.text = "0.0"
        cell.huf.text = "+ " + item.remaining_qty + " @ " + item.open_buy_price.getTwoDecimalString()
        cell.mtm.text = item.mtm.getTwoDecimalString()

        return cell
    }
 
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
}
