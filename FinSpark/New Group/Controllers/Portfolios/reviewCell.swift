//
//  reviewCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 10/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class reviewCell: UITableViewCell {
    @IBOutlet var review: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
