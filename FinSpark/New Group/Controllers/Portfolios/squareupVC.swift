//
//  squareupVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 07/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class squareupVC: UIViewController {
    
    @IBOutlet weak var tableview: UITableView!
    
    @IBOutlet var pname: UILabel!

    var selection = NSMutableArray()
    var item = PortfolioObject()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nc = NotificationCenter.default
               nc.addObserver(self, selector: #selector(refreshdata), name: Notification.Name("refresheditrade"), object: nil)
        self.tabBarController?.tabBar.isHidden = true
        tableview.register(UINib(nibName: "squareupCell", bundle: nil), forCellReuseIdentifier: "squareupCell")
    }
    @objc func refreshdata ()
      {
        SqaureUpModel.shared.process(portfolioid: item.portfolio_id, stockid: item.finsparc_stock_id, stock: item.instrument_name){ (model) in
               print("transaction",model.SqaureUps.count)
               self.tableview.reloadData()
                       }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        sliderHolder.fadeOut { }
        self.CustomizeNavBar_with(title: "", button_text: "Square up Trade", bartint: CustomColours.blue, backButton: true) {
            self.navigationController?.popViewController(animated: true)
        }
        pname.text = item.portfolio_name
        print("item",item.portfolio_id)
        SqaureUpModel.shared.process(portfolioid: item.portfolio_id, stockid: item.finsparc_stock_id, stock: item.instrument_name){ (model) in
        print("transaction",model.SqaureUps.count)
        self.tableview.reloadData()
                }
    }
}

//MARK: Collectionview Methods
extension squareupVC: UITableViewDelegate, UITableViewDataSource {
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SqaureUpModel.shared.SqaureUps.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "squareupCell", for: indexPath) as! squareupCell
        cell.selectionStyle = .none
        let object = SqaureUpModel.shared.SqaureUps[indexPath.row]
        cell.pname.text = object.stock_name
        cell.ptype.text = object.type
        cell.pprice.text = object.price
        cell.pdate.text = object.date
        
        if object.type == "BUY"
        {
            cell.pquantity.text = "+" + object.remaining_qty

        }else
        {
            cell.pquantity.text = "-" + object.remaining_sell_qty

        }
        cell.squareup.addTarget(self, action: #selector(connected(sender:)), for: .touchUpInside)

        return cell
    }
    
    @objc func connected(sender: UIButton){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "editparticularTradeVC") as! editparticularTradeVC
        vc.squareitem = SqaureUpModel.shared.SqaureUps[sender.tag]
        vc.type = "square"

        self.navigationController?.present(vc, animated: true, completion: nil)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
}
