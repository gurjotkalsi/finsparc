//
//  PortfolioverviewDetail.swift
//  FinSpark
//
//  Created by Minkle Garg on 12/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class PortfolioverviewDetail: UIViewController {
    @IBOutlet weak var profileTableView: UITableView!

    var selection = NSMutableArray()
    var currentType : PortfolioFilters = .day
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileTableView.register(UINib(nibName: "portfolioCell", bundle: nil), forCellReuseIdentifier: "portfolioCell")
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
        
        self.CustomizeNavBar_with(title: "", button_text: "Portfolio Overview", bartint: CustomColours.blue, backButton: true) {
        self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func addTapped()
    {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "portfoliOverviewVC") as! portfoliOverviewVC
              //  vc.item = PortfolioModel.shared.list[indexPath.row]
                self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        self.getData()
        sliderHolder.fadeIn {
            
        }
    }
    
    func getData() {
        self.AddLoader()
        PortfolioModel.shared.process(type: currentType) { (model) in
            self.RemoveLoader()
            self.profileTableView.reloadData()
        }
    }
}


extension PortfolioverviewDetail: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 53
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PortfolioModel.shared.list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "portfolioCell", for: indexPath) as! portfolioCell
        let item = PortfolioModel.shared.list[indexPath.row]
        cell.name.text = item.portfolio_name
        cell.booked.text = item.portfolio_booked_amount
        cell.mtm.text = item.mtmjson
        cell.total.text = ""
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "portfolioNameDetail") as! portfolioNameDetail
          vc.item = PortfolioModel.shared.list[indexPath.row]
          self.navigationController?.pushViewController(vc, animated: true)
    }
}
