//
//  ediTradeVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 08/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class ediTradeVC: UIViewController {
    
    @IBOutlet weak var tableview: UITableView!
    
    @IBOutlet var pname: UILabel!

    var selection = NSMutableArray()
    var item = PortfolioObject()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //refresheditrade
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(refreshdata), name: Notification.Name("refresheditrade"), object: nil)

        self.tabBarController?.tabBar.isHidden = true
        tableview.register(UINib(nibName: "editradeCell", bundle: nil), forCellReuseIdentifier: "editradeCell")
    }
    @objc func refreshdata ()
     {
        editradeModel.shared.process(portfolioid: item.portfolio_id, stockid: item.finsparc_stock_id, stock: item.instrument_name){ (model) in
               print("transaction",model.trades.count)
                   if model.trades.count > 0
                   {
               self.tableview.reloadData()
                   }
               }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        pname.text = item.portfolio_name
        sliderHolder.fadeOut { }
        self.CustomizeNavBar_with(title: "", button_text: "Edit Trade", bartint: CustomColours.blue, backButton: true) {
            self.navigationController?.popViewController(animated: true)
        }
        self.AddLoader()
        editradeModel.shared.process(portfolioid: item.portfolio_id, stockid: item.finsparc_stock_id, stock: item.instrument_name){ (model) in
        print("transaction",model.trades.count)
            self.RemoveLoader()
            if model.trades.count > 0
            {
        self.tableview.reloadData()
            }
        }
    }
}

//MARK: Collectionview Methods
extension ediTradeVC: UITableViewDelegate, UITableViewDataSource {
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return editradeModel.shared.trades.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "editradeCell", for: indexPath) as! editradeCell
        cell.selectionStyle = .none
        let object = editradeModel.shared.trades[indexPath.row]
        cell.pname.text = object.stock_name
        cell.pquantity.text = "   " + object.quantity
        cell.type.text = object.type
        cell.pprice.text = object.last_close + "   "
        cell.pdate.text = object.date
        cell.deleteButton.addTarget(self, action: #selector(connected(sender:)), for: .touchUpInside)
        return cell
    }
    
    @objc func connected(sender: UIButton){
        let alert = UIAlertController(title: nil, message: "Do you want to delete this stock?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
              switch action.style{
              case .default:
                    print("default")
                    self.deleteaction(tag: sender.tag)

              case .cancel:
                    print("cancel")

              case .destructive:
                    print("destructive")


              @unknown default:
                print("error")
            }}))
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil ))
        self.present(alert, animated: true, completion: nil)
       
}
    
    func deleteaction(tag: NSInteger) {
        let object = editradeModel.shared.trades[tag]

        self.AddLoader()
               DeleteModel.shared.deleteTrade(id: object.finsparc_transaction_id, stockid: object.stock_id, optiontype: object.option_type) { (model) in
                   if model.status == "200"
                   {
                       editradeModel.shared.process(portfolioid: self.item.portfolio_id, stockid:self.item.finsparc_stock_id, stock: self.item.instrument_name){ (model) in
                              print("transaction",model.trades.count)
                           self.RemoveLoader()
                        if model.trades.count > 0
                            {
                        self.tableview.reloadData()
                            }
else
                        {
                            self.navigationController?.popViewController(animated: true)

                        }
                    }
                   }else
                   {
                       self.RemoveLoader()
                   }

               }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "editparticularTradeVC") as! editparticularTradeVC
             vc.item = editradeModel.shared.trades[indexPath.row]
        vc.type = "edit"
             self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    
}
