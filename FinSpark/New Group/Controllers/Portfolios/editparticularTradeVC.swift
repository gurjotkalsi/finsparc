//
//  editparticularTradeVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 08/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class editparticularTradeVC: UIViewController {
    @IBOutlet var name_tf: UILabel!
    @IBOutlet var noflots_tf: UITextField!
    @IBOutlet var lotsize_tf: UITextField!
    @IBOutlet var quantity_tf: UITextField!
    @IBOutlet var price_tf: UITextField!
    @IBOutlet var newdate_tf: UITextField!
    let datePicker = UIDatePicker()

    var item = editradeObject()
    var squareitem = SqaureUpObject()
    var type = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("item..",item)
        if type == "edit"
        {
        name_tf.text = item.stock_name + " " + item.date
        quantity_tf.text = item.quantity
        lotsize_tf.text = item.stock
        let q : Double = Double(item.quantity)!
        let s : Double =  Double(item.stock)!
        let result: Double = q / s
        noflots_tf.text = String(result)
        price_tf.text = item.last_close
        }else
        {
            name_tf.text = squareitem.stock_name + " " + squareitem.date
                   quantity_tf.text = squareitem.quantity
                   lotsize_tf.text = squareitem.stock
                   let q : Double = Double(squareitem.quantity)!
                   let s : Double =  Double(squareitem.stock)!
                   let result: Double = q / s
                   noflots_tf.text = String(result)
                   price_tf.text = squareitem.last_close
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        newdate_tf.text = formatter.string(from: Date())
        showDatePicker()
        noflots_tf.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        // Do any additional setup after loading the view.
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        let noflots : Float = (textField.text! as NSString).floatValue
        let lotsize : Float = (lotsize_tf.text! as NSString).floatValue
        let multiple = noflots * lotsize
        quantity_tf.text = String(multiple)
    }
    
    func showDatePicker(){
       //Formate Date
        datePicker.datePickerMode = .date

        //ToolBar
       let toolbar = UIToolbar();
       toolbar.sizeToFit()

       //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelDatePicker))
       toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)

    // add toolbar to textField
    newdate_tf.inputAccessoryView = toolbar
     // add datepicker to textField
    newdate_tf.inputView = datePicker
    }
    
    @objc func donedatePicker(){
      //For date formate
        print("datePicker.date",datePicker.date)
       let formatter = DateFormatter()
       formatter.dateFormat = "yyyy-MM-dd"
       newdate_tf.text = formatter.string(from: datePicker.date)
       //dismiss date picker dialog
       self.view.endEditing(true)
        }

        @objc  func cancelDatePicker(){
           //cancel button dismiss datepicker dialog
            self.view.endEditing(true)
          }
       
    @IBAction func buttonAction(_ sender: UIButton) {
        if sender.tag == 4
        {
            self.dismiss(animated: true, completion: nil)
        }else
        {
            if type == "edit"

            {            let parameters : NSDictionary = ["type":item.type,"quantity":quantity_tf.text!, "price":price_tf.text!, "date":newdate_tf.text as Any, "finsparc_transaction_id":item.finsparc_transaction_id, "stock_id":item.stock_id]
print("parameters",parameters)
                  CreateTradeModel().updateprocess(params: parameters) { (model) in
                      print("message",model.message)
                    let nc = NotificationCenter.default
                           nc.post(name: Notification.Name("refresheditrade"), object: nil)

                        self.ShowToast(message: model.message) {
                        self.RemoveLoader()
                        self.dismiss(animated: true, completion: nil)
                      }
                  }
            }else
            
            {            let parameters : NSDictionary = ["type":squareitem.type,"quantity":quantity_tf.text!, "price":price_tf.text!, "date":newdate_tf.text as Any, "finsparc_transaction_id":squareitem.finsparc_transaction_id, "stock_id":squareitem.stock_id]
            print("parameters",parameters)
                              CreateTradeModel().updateprocess(params: parameters) { (model) in
                                  print("message",model.message)
                                let nc = NotificationCenter.default
                                       nc.post(name: Notification.Name("refresheditrade"), object: nil)

                                    self.ShowToast(message: model.message) {
                                    self.RemoveLoader()
                                    self.dismiss(animated: true, completion: nil)
                                  }
                              }
                        }
            
            
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
