//
//  portfolioTransactionVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 07/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class portfolioTransactionVC: UIViewController {
    
    @IBOutlet weak var collectionview: UICollectionView!
    @IBOutlet weak var tableview: UITableView!
    
    @IBOutlet var pname: UILabel!

    var titlesArray = ["All", "For the Week", "For the day"]
    var selection = NSMutableArray()
    var item = PortfolioObject()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(refreshdata), name: Notification.Name("refresh"), object: nil)

        collectionview.register(UINib(nibName: "topTabCell", bundle: nil), forCellWithReuseIdentifier: "topTabCell")
        selection.add(0)
        collectionview.reloadData()
        tableview.register(UINib(nibName: "portfoilioTransactionCell", bundle: nil), forCellReuseIdentifier: "portfoilioTransactionCell")
        
    }
    
    @objc func refreshdata ()
       {
           TransactionModel.shared.process(portfolioid: item.portfolio_id, stockid: item.finsparc_stock_id, stock: item.instrument_name){ (model) in
                              print("transaction",model.transactions.count)
                              self.tableview.reloadData()
                          }
       }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        pname.text = item.portfolio_name
        sliderHolder.fadeOut { }
        
        self.CustomizeNavBar_with(title: "", button_text: "Transactions", bartint: CustomColours.blue, backButton: true) {
            self.navigationController?.popViewController(animated: true)
        }
        
        TransactionModel.shared.process(portfolioid: item.portfolio_id, stockid: item.finsparc_stock_id, stock: item.instrument_name){ (model) in
                    print("transaction",model.transactions.count)
                    self.tableview.reloadData()
                }
//        TransactionModel.shared.process { (model) in
//            print("transaction",model.transactions.count)
//            self.tableview.reloadData()
//        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           if segue.identifier == "squareup" {
               if let vc = segue.destination as? squareupVC {
                //vc.pname.text = pname.text 
               }
           }
       }
    @IBAction func squpAction(_ sender: UIButton) {
        if sender.tag == 1
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "squareupVC") as! squareupVC
                  vc.item = item
                  self.navigationController?.pushViewController(vc, animated: true)
     // performSegue(withIdentifier: "squareup", sender: nil)
        }
        else
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ediTradeVC") as! ediTradeVC
                             vc.item = item
                             self.navigationController?.pushViewController(vc, animated: true)
          //  performSegue(withIdentifier: "editrade", sender: nil)
        }
    }
    
}


//MARK: Collectionview Methods
extension portfolioTransactionVC: UITableViewDelegate, UITableViewDataSource {
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TransactionModel.shared.transactions.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "portfoilioTransactionCell", for: indexPath) as! portfoilioTransactionCell
        let object = TransactionModel.shared.transactions[indexPath.row]
        cell.tname.text = object.stock_name

        cell.tfirstdate.text = object.date
        cell.tsellingprice.text = object.price
        if object.type == "BUY"
        {
            cell.tremainingquantity.text = "+" + object.remaining_qty

        }else
        {
            cell.tremainingquantity.text = "-" + object.remaining_qty

        }
     //   cell.tseconddate.text = object.date
//        cell.tfilterprice.text = object.filter_price
//        cell.tsellingquantity.text = object.selling_quantity
        cell.tbooked.text = object.booked
        cell.tmtm.text = object.mtm.getTwoDecimalString()
        cell.tposition.text = object.quantity

        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 185
    }
    
}

extension portfolioTransactionVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titlesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.collectionview.dequeueReusableCell(withReuseIdentifier: "topTabCell", for: indexPath) as! topTabCell
        
        cell.lbl_Headings.text = self.titlesArray[indexPath.row]
        cell.lbl_Headings.roundUp()
        
        if selection.contains(indexPath.row) {
            cell.lbl_Headings.backgroundColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
            cell.lbl_Headings.textColor = UIColor.white
            
        }else{
            cell.lbl_Headings.backgroundColor = UIColor.clear
            cell.lbl_Headings.textColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
        }
        
        cell.layer.cornerRadius = collectionView.frame.height/2
        cell.layer.masksToBounds = true
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if selection.contains(indexPath.row) {
            selection.removeAllObjects()
        }else{
            selection.removeAllObjects()
            selection.add(indexPath.row)
        }
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3.3, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
}
