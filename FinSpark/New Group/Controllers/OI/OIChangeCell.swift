//
//  putDetailCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 27/02/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class OIChangeCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    var typeofPuttext: String = ""
    
    @IBOutlet weak var collectionView: UICollectionView!
    var object : Any!
    var typeclass : NSObject.Type?
    var countsection : NSInteger!
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.register(UINib(nibName: "topTabCell", bundle: nil), forCellWithReuseIdentifier: "topTabCell")
        
        let userdefaults = UserDefaults.standard
        if userdefaults.string(forKey: "type") != nil{
           typeofPuttext = UserDefaults.standard.string(forKey: "type")!
        } else {
        }
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return countsection
    }
    
    func returnValue(index:Int) -> String {
        if typeclass == OIItem.self {
            if let valueObject = object as? OIItem {

                if index == 0 {
                    return valueObject.symbol
                }else if index == 1 {
                    return valueObject.change_expiry_percent + " %"
                } else if index == 2 {
                    return valueObject.percent_change_last_7_days_price  + " %"
                } else if index == 3 {
                    return valueObject.percent_expiry_oi  + " %"
                } else  {
                    return valueObject.percent_change_last_7_days_oi  + " %"
                }
            }
        }else if typeclass == OISignificantChangeItem.self {
            if let valueObject = object as? OISignificantChangeItem {
                if index == 0 {
                    return valueObject.symbol
                }else if index == 1 {
                    return valueObject.future_closing
                }
                else if index == 2 {
                    return valueObject.open_int
                } else if index == 3 {
                    return valueObject.change_oi
                } else if index == 4 {
                    return valueObject.volume
                } else {
                    return valueObject.percent_volume  + " %"
                }
            }
        }else{
            if let valueObject = object as? ReportThirdItemObject {
                if index == 0 {
                    return valueObject.symbol
                }else if index == 1 {
                    return valueObject.stike_pr
                } else if index == 2 {
                    return valueObject.close
                } else if index == 3 {
                    return valueObject.pclose
                } else if index == 4 {
                    return valueObject.open_int
                } else {
                    return valueObject.call_oi
                }
            }
        }
        
        return ""
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "topTabCell", for: indexPath as IndexPath) as! topTabCell
        
        cell.lbl_Headings.text = returnValue(index: indexPath.item)
        
        cell.lbl_Headings.roundUp()
        cell.lbl_Headings.font = UIFont(name: "Assistant-Regular", size: 14)
        cell.lbl_Headings.textColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
        cell.lbl_Headings.numberOfLines = 0
        if self.typeofPuttext != "NTM" {
          //  cell.lbl_Headings.textColor = UIColor(red: 0.274, green: 0.726, blue: 0, alpha: 1)
        }
        cell.lbl_Headings.lineBreakMode = .byWordWrapping
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 0.84

        cell.lbl_Headings.attributedText = NSMutableAttributedString(string: self.returnValue(index: indexPath.item), attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
        if indexPath.row != 0
        {
            cell.lbl_Headings.textAlignment = .right

        } 
        
        if self.typeofPuttext == "IV" {
            cell.lbl_Headings.font = UIFont(name: "Assistant-Regular", size: 14)
            cell.lbl_Headings.textColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
            cell.lbl_Headings.textAlignment = .center
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var width = (collectionView.frame.size.width - 40) / 6.2
        
        if countsection == 6
        {
            width = (collectionView.frame.size.width - 40) / 6.2
        }
        else
        {
            width = (collectionView.frame.size.width - 40) / 5.1
        }
        if typeofPuttext == "NTM" {
            width = (collectionView.frame.size.width - 40) / 5.1
        }
        return CGSize(width: width , height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
