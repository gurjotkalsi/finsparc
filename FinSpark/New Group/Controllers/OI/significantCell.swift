//
//  significantCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 22/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class significantCell: UITableViewCell {
    @IBOutlet var one: UILabel!
    @IBOutlet var two: UILabel!
    @IBOutlet var three: UILabel!
    @IBOutlet var four: UILabel!
    @IBOutlet var five: UILabel!
    @IBOutlet var six: UILabel!
    @IBOutlet var view: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
