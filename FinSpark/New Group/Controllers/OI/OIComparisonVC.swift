//
//  OIComparisonVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 14/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class OIComparisonVC: UIViewController {
    @IBOutlet weak var oichangesTableView: UITableView!
    @IBOutlet var expirydate: UILabel!

    //MARK:-
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.CustomizeNavBar_with(title: "", button_text: "OI Comparison on MoM basis", bartint: CustomColours.blue, backButton: true) {
            self.navigationController?.popViewController(animated: true)
        }
        sliderHolder.fadeOut {}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        oichangesTableView.register(UINib(nibName: "OIComparisonCell", bundle: nil), forCellReuseIdentifier: "OIComparisonCell")
        self.webservice()
    }
    //MARK:-
    func Convertorequireformat (_ date : String ) -> String{
        let inputFormatter = DateFormatter()
           inputFormatter.dateFormat = "yyyy-MM-dd"
           let showDate = inputFormatter.date(from: date)
           inputFormatter.dateFormat = "dd-MM-yyyy"
           let resultString = inputFormatter.string(from: showDate!)
          return resultString
    }
    func webservice() {
        self.AddLoader()
        OICompareModel.shared.process { (model) in
            
            self.RemoveLoader()
            if OICompareModel.shared.list.count > 0
            {
           self.expirydate.text = (self.Convertorequireformat(OICompareModel.shared.list[0].expiry_dt))
        }
            self.oichangesTableView.reloadData()
            
        }
        
    }
}

//MARK: Collectionview Methods
extension OIComparisonVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if OICompareModel.shared.list.count > 0
        {
        return 2
        }
        return 0
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OIComparisonCell", for: indexPath) as! OIComparisonCell
        cell.selectionStyle = .none
        //cell.title_Label.text = self.indicesArray[indexPath.row]
        
        if indexPath.row == 0
        {
            cell.date.text = OICompareModel.shared.list[0].cdate
            cell.futureprice.text = OICompareModel.shared.list[0].future_price
            cell.futureoi.text = OICompareModel.shared.list[0].total_sum
            cell.call.text = OICompareModel.shared.list[0].agg_oi_ce
            cell.put.text = OICompareModel.shared.list[0].agg_oi_pe
        }
        else
        {
           cell.date.text = OICompareModel.shared.list[0].last_month_date
           cell.futureprice.text = OICompareModel.shared.list[0].last_month_future_price
           cell.futureoi.text = OICompareModel.shared.list[0].last_month_total_sum
           cell.call.text = OICompareModel.shared.list[0].agg_oi_ce_last_month
           cell.put.text = OICompareModel.shared.list[0].agg_oi_pe_last_month
        }
        
        return cell
    }

    
}
