//
//  oifirstCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 22/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class oifirstCell: UITableViewCell {
      @IBOutlet var symbol: UILabel!
      @IBOutlet var expiry: UILabel!
      @IBOutlet var lastexpiry: UILabel!
      @IBOutlet var oiexpiry: UILabel!
      @IBOutlet var lastweek: UILabel!
    @IBOutlet var viewConstant: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
