//
//  OIChangesVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 03/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class OIChangesVC: UIViewController {
    @IBOutlet weak var oichangesTableView: UITableView!

    var indicesArray = ["OI Changes (Stocks)", "Significant Changes in OI ","OI comparison on MoM basis", "Adding Most OI", "Shedding Most OI"]
    var descriptionArray = ["Significant OI Changes do impact prices in subsequent days.", "Stocks with high % may have significant movement in 1-2 days","", "Strong OI addition over a period is a significant data point.", "Some of these stocks may have directional changes."]

    //MARK:-
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.CustomizeNavBar_with(title: "", button_text: "OI Changes", bartint: CustomColours.blue, backButton: true) {
            self.navigationController?.popViewController(animated: true)
        }
        sliderHolder.fadeOut {}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        oichangesTableView.register(UINib(nibName: "putCell", bundle: nil), forCellReuseIdentifier: "putCell")
        self.webservice()
    }
    //MARK:-
    
    func webservice() {
        self.AddLoader()
        OIChangeModel.shared.process { (model) in
            
            self.RemoveLoader()
            if model.status != "200" {
                self.ShowToast(message: model.message)
            }
        }
        
    }
}

//MARK: Collectionview Methods
extension OIChangesVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.indicesArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "putCell", for: indexPath) as! putCell
        cell.selectionStyle = .none
        cell.title_Label.text = self.indicesArray[indexPath.row]
        cell.sub_Label.text = self.descriptionArray[indexPath.row]

        return cell
    }
    func Convertorequireformat (_ date : String ) -> String{
           let inputFormatter = DateFormatter()
              inputFormatter.dateFormat = "yyyy-MM-dd"
              let showDate = inputFormatter.date(from: date)
              inputFormatter.dateFormat = "dd-MM-yyyy"
              let resultString = inputFormatter.string(from: showDate!)
             return resultString
       }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if indexPath.row == 2 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OIComparisonVC") as! OIComparisonVC
            self.navigationController?.pushViewController(vc, animated: true)
               }
        else
         {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OIChangeDetailView") as! OIChangeDetailView
        vc.titleHeading = self.indicesArray[indexPath.row]
        vc.index = indexPath.row
        vc.expiry = "Expiry Date : \(self.Convertorequireformat(OIChangeModel.shared.expiry))"
        vc.creation = (self.Convertorequireformat(OIChangeModel.shared.cdate))

        vc.significantObjectsArray = [OISignificantChangeItem]()
        
        if indexPath.row == 0 {
            vc.objectsArray = OIChangeModel.shared.current_oi_change
            vc.typeclass = OIItem.self
        }else if indexPath.row == 1 {
            vc.objectsArray = [OIItem]()
            vc.significantObjectsArray = OIChangeModel.shared.current_significant_chg_oi
            vc.typeclass = OISignificantChangeItem.self
        }
        else if indexPath.row == 3 {
            vc.objectsArray = OIChangeModel.shared.current_oi_change
        }else {
            vc.objectsArray = OIChangeModel.shared.current_oi_change
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
