//
//  OIChangeDetailView.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 12/05/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//
import UIKit

class OIChangeDetailView: UIViewController {

    @IBOutlet weak var collctionviewTopTabs: UICollectionView!
    @IBOutlet weak var callTableView: UITableView!
    
    @IBOutlet var expiry_date: UILabel!
    @IBOutlet var eod_date: UILabel!
    var object : Any!

    var titlesArray : [String]!
    var titleHeading: String = ""
    var typeclass : NSObject.Type?

    var objectsArray = [OIItem]()
    var significantObjectsArray = [OISignificantChangeItem]()
    var index = 0
     var expiry = ""
    var creation = ""

    //MARK:-
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        expiry_date.text = expiry
        eod_date.text = creation
        sliderHolder.fadeOut {}

        self.CustomizeNavBar_with(title: "", button_text: titleHeading, bartint: UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1), backButton: true) {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        
        if index == 0 {
            titlesArray = ["Symbol", "% ▵ \n Price from expiry", "% ▵ \n Price from Last Week", "% ▵ \n OI from expiry", "% ▵ \n OI from Last Week"]
        } else if index == 1 {
            titlesArray = ["Symbol", "Price Close", "OI (Lacs)", "▵ OI (Lacs)", "VOL (Lacs)", "▵ % of VOL"]
        } else if index == 3 {
            titlesArray = ["Symbol", "Price Close", "% ▵ in Price", "Future OI (In Lacs)", "% ▵ in OI"]
        } else {
            titlesArray = ["Symbol", "Price Close", "% ▵ in Price", "Future OI (In Lacs)", "% ▵ in OI"]
        }
        callTableView.register(UINib(nibName: "oiheadingCell", bundle: nil), forCellReuseIdentifier: "oiheadingCell")
        callTableView.register(UINib(nibName: "oifirstCell", bundle: nil), forCellReuseIdentifier: "oifirstCell")
        callTableView.register(UINib(nibName: "signiresponseCell", bundle: nil), forCellReuseIdentifier: "signiresponseCell")
        callTableView.register(UINib(nibName: "significantCell", bundle: nil), forCellReuseIdentifier: "significantCell")

//        collctionviewTopTabs.register(UINib(nibName: "topTabCell", bundle: nil), forCellWithReuseIdentifier: "topTabCell")
//        collctionviewTopTabs.reloadData()
        
    }
    //MARK:-
}

extension OIChangeDetailView: UITableViewDelegate, UITableViewDataSource {
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "watchlistSubdetailVC") as! watchlistSubdetailVC
        if index != 1
         {
        
             let object : OIItem = self.objectsArray[indexPath.row]
            vc.company = object.symbol
        }else
        {
            let object : OISignificantChangeItem = self.significantObjectsArray[indexPath.row]
            vc.company = object.symbol

        }
        
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if index == 1
        {
            return significantObjectsArray.count +  1

        }
        return objectsArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, numberOfSectionsInTableView section: Int) -> Int {
        return 1
    }
    
    func returnValue(index:Int) -> String {
        if typeclass == OIItem.self {
            if let valueObject = object as? OIItem {

                if index == 0 {
                    return valueObject.symbol
                }else if index == 1 {
                    return valueObject.change_expiry_percent + " %"
                } else if index == 2 {
                    return valueObject.percent_change_last_7_days_price  + " %"
                } else if index == 3 {
                    return valueObject.percent_expiry_oi  + " %"
                } else  {
                    return valueObject.percent_change_last_7_days_oi  + " %"
                }
            }
        }else if typeclass == OISignificantChangeItem.self {
            if let valueObject = object as? OISignificantChangeItem {
                if index == 0 {
                    return valueObject.symbol
                }else if index == 1 {
                    return valueObject.future_closing
                }
                else if index == 2 {
                    return valueObject.open_int
                } else if index == 3 {
                    return valueObject.change_oi
                } else if index == 4 {
                    return valueObject.volume
                } else {
                    return valueObject.percent_volume  + " %"
                }
            }
        }else{
            if let valueObject = object as? ReportThirdItemObject {
                if index == 0 {
                    return valueObject.symbol
                }else if index == 1 {
                    return valueObject.stike_pr
                } else if index == 2 {
                    return valueObject.close
                } else if index == 3 {
                    return valueObject.pclose
                } else if index == 4 {
                    return valueObject.open_int
                } else {
                    return valueObject.call_oi
                }
            }
        }
        
        return ""
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
           return 75
       }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if index != 1
        {
        let cell = tableView.dequeueReusableCell(withIdentifier: "oiheadingCell") as! oiheadingCell
            cell.one.text = titlesArray[0]
            cell.two.text = titlesArray[1]
            cell.three.text = titlesArray[2]
            cell.four.text = titlesArray[3]
            cell.five.text = titlesArray[4]
            return cell
        }else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "significantCell") as! significantCell
           cell.one.text = titlesArray[0]
           cell.two.text = titlesArray[1]
           cell.three.text = titlesArray[2]
           cell.four.text = titlesArray[3]
           cell.five.text = titlesArray[4]
           cell.six.text = titlesArray[5]
           return cell
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if index != 1
        {
       
            let object : OIItem = self.objectsArray[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "oifirstCell", for: indexPath) as! oifirstCell
            cell.symbol.text = object.symbol
            cell.expiry.text = object.change_expiry_percent
            cell.oiexpiry.text = object.percent_expiry_oi

             if index == 0
             {
                cell.expiry.text = object.change_expiry_percent + " %"
                cell.oiexpiry.text = object.percent_expiry_oi + " %"
            }
            cell.lastexpiry.text = object.percent_change_last_7_days_price  + " %"
            cell.lastweek.text = object.percent_change_last_7_days_oi + " %"
            return cell

        }else
        {
         
                let object : OISignificantChangeItem = self.significantObjectsArray[indexPath.row]
                let cell = tableView.dequeueReusableCell(withIdentifier: "signiresponseCell", for: indexPath) as! signiresponseCell
                cell.symbol.text = object.symbol
                if let value = Float(object.percent_change_last_days_price)
                {
                    if value < 0
                    {
                        cell.pclose.textColor = .red
                    }else
                    {
                     cell.pclose.textColor = CustomColours.green
                    }
                }
                cell.pclose.text = object.future_closing + "\n" + "(" + object.percent_change_last_days_price + " %)"
                cell.oi.text = object.open_int
                cell.changeinoi.text = object.change_oi
                cell.vol.text = object.volume
                cell.changeinvol.text = object.volume  + " %"

                return cell

            }

        //var item  = objectsArray[indexPath.row]
      //  cell.countsection = 5
//        if index == 1
//        {
//           // cell.countsection = 6
//            cell.object = self.significantObjectsArray[indexPath.row]
//
//        }else
//        {
//        cell.object = self.objectsArray[indexPath.row]
//        }
//        cell.typeclass = self.typeclass

//        cell.symbol_lbl.text = item.symbol
//               cell.label_one.text = item.total
//               cell.label_two.text = "\(item.call_value)\n\(item.put_value)"
//               cell.label_three.text = item.call_ratio
//               cell.label_four.text = item.put_ratio
//        cell.label_five.text = item.put_ratio

    }
    
}

//
//extension OIChangeDetailView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return titlesArray.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//
//        let cell = self.collctionviewTopTabs.dequeueReusableCell(withReuseIdentifier: "topTabCell", for: indexPath) as! topTabCell
//        cell.lbl_Headings.text = self.titlesArray[indexPath.row]
//        cell.lbl_Headings.roundUp()
//
//        cell.lbl_Headings.lineBreakMode = .byWordWrapping
//
//        let paragraphStyle = NSMutableParagraphStyle()
//        paragraphStyle.lineHeightMultiple = 0.84
////        cell.lbl_Headings.textAlignment = .right
////        if indexPath.row == 0
////        {
////            cell.lbl_Headings.textAlignment = .left
////        }
//
//        cell.lbl_Headings.attributedText = NSMutableAttributedString(string: self.titlesArray[indexPath.row], attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
//        return cell
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        var width = (collctionviewTopTabs.frame.size.width - 40) / 6.2
//
//        if index != 1
//        {
//        width = (collctionviewTopTabs.frame.size.width - 40) / 5.1
//        }
//        return CGSize(width: width , height: width)
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
//    }
//
//}
