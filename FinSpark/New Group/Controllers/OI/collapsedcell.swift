//
//  collapsedcell.swift
//  FinSpark
//
//  Created by Minkle Garg on 22/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class collapsedcell: UITableViewCell {
    @IBOutlet weak var mainview: UIView!
    @IBOutlet var viewConstant: NSLayoutConstraint!
    @IBOutlet weak var label: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.mainview.addlightBorder()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
