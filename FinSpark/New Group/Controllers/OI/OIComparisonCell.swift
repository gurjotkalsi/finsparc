//
//  OIComparisonCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 14/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class OIComparisonCell: UITableViewCell {
    @IBOutlet weak var mainview: UIView!
    @IBOutlet var date: UILabel!
    @IBOutlet var futureprice: UILabel!
    @IBOutlet var futureoi: UILabel!
    @IBOutlet var call: UILabel!
    @IBOutlet var put: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.mainview.addlightBorder()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
