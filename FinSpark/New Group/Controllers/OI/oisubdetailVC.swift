//
//  watchlistSubdetailVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 22/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//
//oisubdetailVC

import UIKit

class oisubdetailVC: UIViewController {
    @IBOutlet var viewConstant: NSLayoutConstraint!

    @IBOutlet var tableview: UITableView!
    @IBOutlet weak var collctionviewTopTabs: UICollectionView!

    var collapsedSize = CGFloat(55)
    var expandedSize = CGFloat(375)
    
    var expand = true
    
    var prevIndex : IndexPath?
    var currentIndex : IndexPath?
    
    var titlestopArray = ["Data", "Graph"]
    var selection = NSMutableArray();
var company = ""
    var titleArray = ["Stock Profile", "5 Days Data", "OI in lacs (for all Expires)", "Options", "Implied Volatility", "Finsparc Summary"]
    var collapsedarray = [true, true, true,true, true, true]
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        sliderHolder.fadeOut { }
        self.viewConstant.constant = 0
        self.tabBarController?.tabBar.isHidden = true

        
        collctionviewTopTabs.register(UINib(nibName: "topTabCell", bundle: nil), forCellWithReuseIdentifier: "topTabCell")
       let indexpath = 0
       selection.add(indexpath)
       collctionviewTopTabs.reloadData()
        // You know them 😉
        self.tableview.dataSource = self
        self.tableview.delegate = self
        
        tableview.register(UINib(nibName: "searchCell", bundle: nil), forCellReuseIdentifier: "searchCell")
        tableview.register(UINib(nibName: "oifirstCell", bundle: nil), forCellReuseIdentifier: "oifirstCell")
        tableview.register(UINib(nibName: "oiCell", bundle: nil), forCellReuseIdentifier: "oiCell")
        tableview.register(UINib(nibName: "volatilityCell", bundle: nil), forCellReuseIdentifier: "volatilityCell")
        tableview.register(UINib(nibName: "summaryCell", bundle: nil), forCellReuseIdentifier: "summaryCell")
        tableview.register(UINib(nibName: "OptionCustomCell", bundle: nil), forCellReuseIdentifier: "OptionCustomCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        sliderHolder.fadeOut {}
        self.CustomizeNavBar_with(title: "", button_text: company, bartint: UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1), backButton: true) {
            self.navigationController?.popViewController(animated: true)
            sliderHolder.fadeIn {}
        }
        StockProfileModel.shared.process(company: company) { (model) in
            
            self.RemoveLoader()
                        self.tableview.reloadData()

        }    }
    //MARK:-


}

//MARK: Collectionview Methods
extension oisubdetailVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           return CGSize(width: collectionView.frame.width / 3.3, height: collectionView.frame.height)
       }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.collctionviewTopTabs.dequeueReusableCell(withReuseIdentifier: "topTabCell", for: indexPath) as! topTabCell
        cell.lbl_Headings.text = self.titlestopArray[indexPath.row]
        cell.lbl_Headings.roundUp()
        cell.lbl_Headings.textAlignment = .center
        if selection.contains(indexPath.row) {
        cell.lbl_Headings.backgroundColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
        cell.lbl_Headings.textColor = UIColor.white

           }else{
        cell.lbl_Headings.backgroundColor = UIColor.clear
        cell.lbl_Headings.textColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)

           }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if selection.contains(indexPath.row) {
            selection.removeAllObjects()
        }else{
            selection.removeAllObjects()
            selection.add(indexPath.row)
        }
        collectionView.reloadData()
    }
   
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 10))
        return footerView
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
}

extension oisubdetailVC : UITableViewDataSource {

     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let collapse = collapsedarray[indexPath.section]

        if indexPath.section == 0 {
                    if collapse {
                        return 55
                     }else{
                        return 270

                     }
            

         }
            else if indexPath.section == 1 {

                   if collapse {
                                                   return 55
                                                }else{
                                                   return 65 * 6
                                                }
                
            }
        else if indexPath.section == 2 {

               if collapse {
                                               return 55
                                            }else{
                                               return 168
                                            }
            
        }
            else if indexPath.section == 3 {

                   if collapse {
                                                   return 55
                                                }else{
                                                   return 672
                                                }
                
            }
         else if indexPath.section == 4 {
                             if collapse {
                                 return 55
                              }else{
                                 return 140
                              }
        } else
         {
            if collapse {
                                                       return 55
                                                    }else{
                                                       return 270
                                                    }        }
       }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1
        {
            return StockProfileModel.shared.days_5_data.count
        }else
        {
        return 1
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.titleArray.count
    }
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let collapse = collapsedarray[indexPath.section]

        if indexPath.section == 0 {

            let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath) as! searchCell
            if collapse {
                cell.viewConstant.constant = 55
             }else{
                cell.viewConstant.constant = 270.0
             }
            cell.consistencyranking.text = StockProfileModel.shared.rank
            cell.score.text = StockProfileModel.shared.rank
            cell.tprice.text = StockProfileModel.shared.rank
            cell.priceclose.attributedText = StockProfileModel.shared.trigger_price.italic(cell.priceclose.font.pointSize)
            cell.lastexpiry.text = StockProfileModel.shared.percent_change_since_last_expiry
            cell.wap.text = StockProfileModel.shared.wap
            cell.highlow.text = StockProfileModel.shared.rank
            cell.volume.text = StockProfileModel.shared.volume
            cell.dma.text = StockProfileModel.shared.rank
            cell.selectionStyle = .none
            
            return cell
        }else if indexPath.section == 1 {

            let cell = tableView.dequeueReusableCell(withIdentifier: "oifirstCell", for: indexPath) as! oifirstCell
            
            if collapse {
                       cell.viewConstant.constant = 55
                    }else{
                       cell.viewConstant.constant = 66 * 5
                    }
            if indexPath.row == 0
            {
                cell.symbol.text = "Date"
                           cell.expiry.text = "Volume \n ('000)"
                           cell.lastexpiry.text = "OI (in Lacs)"
                           cell.oiexpiry.text = "▵OI (in Lacs)"
                           cell.lastweek.text = "Price Close (in Rs)"
            }else
            {
            let object : DayFiveItem = StockProfileModel.shared.days_5_data[indexPath.row - 1]
            cell.symbol.text = object.timestamp_finsparc_bhavcopy
            cell.expiry.text = object.volume
            cell.lastexpiry.text = object.open_int
            cell.oiexpiry.text = object.chg_in_oi
            cell.lastweek.text = object.close
            }
            cell.selectionStyle = .none
            return cell
        }
        else if indexPath.section == 2 {

            let cell = tableView.dequeueReusableCell(withIdentifier: "oiCell", for: indexPath) as! oiCell
            
            if collapse {
                       cell.viewConstant.constant = 55
                    }else{
                       cell.viewConstant.constant = 168
                    }
            cell.current.text = StockProfileModel.shared.current_oi_sum
                       cell.ltd.text = StockProfileModel.shared.change_oi
                       cell.day.text = StockProfileModel.shared.change_oi_5_days
                       cell.highlow.text = StockProfileModel.shared.oi_high_11 + " / " + StockProfileModel.shared.oi_low_11
            
            cell.selectionStyle = .none
            return cell
        }else if indexPath.section == 3 {

            let cell = tableView.dequeueReusableCell(withIdentifier: "OptionCustomCell", for: indexPath) as! OptionCustomCell
            
            if collapse {
                                                       cell.viewConstant.constant = 55
                                                    }else{
                                                       cell.viewConstant.constant = 672
                                                    }
            cell.selectionStyle = .none
            return cell
        }else if indexPath.section == 4 {

            let cell = tableView.dequeueReusableCell(withIdentifier: "volatilityCell", for: indexPath) as! volatilityCell
            cell.day.text = StockProfileModel.shared.data_5
              cell.week.text = StockProfileModel.shared.data_30
              cell.month.text = StockProfileModel.shared.annualise_volatility
            cell.selectionStyle = .none
            if collapse {
                                            cell.viewConstant.constant = 55
                                         }else{
                                            cell.viewConstant.constant = 140
                                         }
            return cell
        }else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "summaryCell", for: indexPath) as! summaryCell
                       cell.selectionStyle = .none
            if collapse {
                                                       cell.viewConstant.constant = 55
                                                    }else{
                                                       cell.viewConstant.constant = 85
                                                    }
                       return cell
        }
    }
    
    
}

extension oisubdetailVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let collapse = !collapsedarray[indexPath.section]
        if collapse {
             expand = false
         }else{
             expand = true
         }
         print("old",collapsedarray)
         collapsedarray[indexPath.section] = collapse
         print("new",collapsedarray)
        self.tableview.reloadData()
        
    }
}
// MARK: - Section Header Delegate
//
extension oisubdetailVC: CollapsibleViewHeaderDelegate {
    
    func togglesSection(_ header: customsearchHeader, section: Int) {
        
       let collapse = !collapsedarray[section]
       if collapse {
            expand = false
        }else{
            expand = true
        }
        print("old",collapsedarray)
        collapsedarray[section] = collapse
        print("new",collapsedarray)

       // header.setCollapsed(expand)
        
        tableview.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
    }
    
}
