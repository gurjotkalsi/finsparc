//
//  customsearchHeader.swift
//  FinSpark
//
//  Created by Minkle Garg on 12/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

protocol CollapsibleViewHeaderDelegate {
    func togglesSection(_ header: customsearchHeader, section: Int)
}

class customsearchHeader: UITableViewHeaderFooterView {
    
    var delegate: CollapsibleViewHeaderDelegate?
    var section: Int = 0

    @IBOutlet var topTitle: UILabel!
    @IBOutlet var singleLabel: UILabel!

    @IBOutlet var arrowIcon: UIImageView!

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
           override func awakeFromNib() {
            super.awakeFromNib()
            addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(customsearchHeader.tapHeader(_:))))
    }
       
    @objc func tapHeader(_ gestureRecognizer: UITapGestureRecognizer) {
        guard let cell = gestureRecognizer.view as? customsearchHeader else {
            return
        }
        
        delegate?.togglesSection(self, section: cell.section)
    }
    
    func setCollapsed(_ collapsed: Bool) {
        //
        // Animate the arrow rotation (see Extensions.swf)
        //
        if collapsed
        {
            singleLabel.isHidden = true
            arrowIcon.image = UIImage(named: "up")
        }else
        {
            singleLabel.isHidden = false

            arrowIcon.image = UIImage(named: "down")

        }
    }
    

}
