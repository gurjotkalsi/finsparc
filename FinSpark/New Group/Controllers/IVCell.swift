//
//  IVCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 10/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class IVCell: UITableViewCell {

    @IBOutlet var symbol_lbl: UILabel!
    @IBOutlet var last_day_lbl: UILabel!
    @IBOutlet var last_week_lbl: UILabel!
    @IBOutlet var last_month_lbl: UILabel!
    @IBOutlet var over_week_lbl: UILabel!
    @IBOutlet var over_month_lbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
