//
//  activeCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 14/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class activeCell: UITableViewCell {
    @IBOutlet weak var strike: UILabel!
    @IBOutlet weak var mainview: UIView!
    @IBOutlet weak var oi: UILabel!
    @IBOutlet weak var trigger: UILabel!
    @IBOutlet weak var priceclose: UILabel!
    @IBOutlet weak var remark: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
