//
//  summaryCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 11/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class summaryCell: UITableViewCell {
    @IBOutlet var viewConstant: NSLayoutConstraint!
            @IBOutlet weak var heading: UILabel!
            @IBOutlet weak var toggleimage: UIImageView!
            @IBOutlet weak var date: UILabel!
            @IBOutlet weak var review: UILabel!
    @IBOutlet weak var mainview: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.mainview.addlightBorder()
//
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
