//
//  oiCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 11/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class oiCell: UITableViewCell {
    @IBOutlet weak var date: UILabel!

       @IBOutlet weak var mainview: UIView!
       @IBOutlet var viewConstant: NSLayoutConstraint!
       @IBOutlet weak var heading: UILabel!
       @IBOutlet weak var toggleimage: UIImageView!
       @IBOutlet weak var current: UILabel!
       @IBOutlet weak var ltd: UILabel!
       @IBOutlet weak var day: UILabel!
       @IBOutlet weak var highlow: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.mainview.addlightBorder()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
