//
//  OptionCustomCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 14/06/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class OptionCustomCell: UITableViewCell {
    @IBOutlet weak var mainview: UIView!
    @IBOutlet var tableview: UITableView!
    @IBOutlet var viewConstant: NSLayoutConstraint!
    @IBOutlet var companyname: UILabel!

var company = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.mainview.addlightBorder()
        tableview.register(UINib(nibName: "activeCell", bundle: nil), forCellReuseIdentifier: "activeCell")
        tableview.register(UINib(nibName: "strengthCell", bundle: nil), forCellReuseIdentifier: "strengthCell")
        tableview.register(UINib(nibName: "mostCell", bundle: nil), forCellReuseIdentifier: "mostCell")
        print("company",company)
        StockProfileModel.shared.process(company: company) { (model) in
            self.tableview.reloadData()
        }

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}


extension OptionCustomCell : UITableViewDataSource {

     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
       }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       if StockProfileModel.shared.call_json.count > 0
        {
            return 9
        }
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.row == 7 || indexPath.row == 8
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "strengthCell", for: indexPath) as! strengthCell
            
            if indexPath.row == 7
            {
                cell.stength.text = "Call Strength"
                cell.perone.text = StockProfileModel.shared.call_strength + "%"
                let callstrength : Double = Double(StockProfileModel.shared.call_strength)!
                cell.pertwo.text = String(callstrength - 100)  + "%"
            }else
            {
               cell.stength.text = "Put Strength"
               cell.perone.text = StockProfileModel.shared.put_strength + "%"
                let putstrength : Double = Double(StockProfileModel.shared.call_strength)!

               cell.pertwo.text = String(putstrength - 100)  + "%"
            }
               cell.selectionStyle = .none
               return cell
           
        }else  if indexPath.row == 3
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "mostCell", for: indexPath) as! mostCell
            cell.selectionStyle = .none
            companyname.text = "Most Active Call " +  company.capitalized

            cell.companyname.text = "Most Active Put " + company.capitalized
            return cell
        }
        else
        {
           
           let cell = tableView.dequeueReusableCell(withIdentifier: "activeCell", for: indexPath) as! activeCell
            if indexPath.row < 3
           {
            if StockProfileModel.shared.call_json.count > 0
                           {
        cell.strike.text = StockProfileModel.shared.call_json[indexPath.row ].stike_pr
          cell.oi.text = StockProfileModel.shared.call_json[indexPath.row].open_int + "\n" + StockProfileModel.shared.call_json[indexPath.row].change_poi
          cell.trigger.text = StockProfileModel.shared.call_json[indexPath.row].cum_premium_cumlots
          cell.priceclose.text = StockProfileModel.shared.call_json[indexPath.row].close
            let pc : Double = Double(StockProfileModel.shared.call_json[indexPath.row].close)!
            let trigger : Double = Double(StockProfileModel.shared.call_json[indexPath.row].cum_premium_cumlots)!

            if pc > trigger
            {
              cell.remark.text =  "STRONG"
            }else if pc < trigger
            {
                cell.remark.text =  "WEAK"

            }else
            {
                cell.remark.text =  "SIDEWAYS"

            }
            }
            }else
            {
                if StockProfileModel.shared.put_json.count > 0
                {
                cell.strike.text = StockProfileModel.shared.put_json[indexPath.row - 4].stike_pr
                 cell.oi.text = StockProfileModel.shared.put_json[indexPath.row - 4].open_int  + "\n" +  StockProfileModel.shared.put_json[indexPath.row - 4].change_poi
                 cell.trigger.text = StockProfileModel.shared.put_json[indexPath.row - 4].round
                 cell.priceclose.text = StockProfileModel.shared.put_json[indexPath.row - 4].close
                let pc : Double = Double(StockProfileModel.shared.put_json[indexPath.row - 4].close)!
                let trigger : Double = Double(StockProfileModel.shared.put_json[indexPath.row - 4].round)!

                if pc > trigger
                {
                  cell.remark.text =  "STRONG"
                }else if pc < trigger
                {
                    cell.remark.text =  "WEAK"

                }else
                {
                    cell.remark.text =  "SIDEWAYS"

                }            }
            }
                                 cell.selectionStyle = .none
                                 return cell
        }
    
    }

}
