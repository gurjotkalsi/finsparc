//
//  searchCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 11/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class searchCell: UITableViewCell {
    @IBOutlet weak var mainview: UIView!
    @IBOutlet var viewConstant: NSLayoutConstraint!

    
    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var toggleimage: UIImageView!
    @IBOutlet weak var consistencyranking: UILabel!
    @IBOutlet weak var score: UILabel!
    @IBOutlet weak var tprice: UILabel!
    @IBOutlet weak var priceclose: UILabel!
    @IBOutlet weak var lastexpiry: UILabel!
    @IBOutlet weak var wap: UILabel!
    @IBOutlet weak var highlow: UILabel!
    @IBOutlet weak var volume: UILabel!
    @IBOutlet weak var dma: UILabel!

    @IBOutlet weak var date: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.mainview.addlightBorder()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
