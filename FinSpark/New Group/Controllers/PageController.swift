//
//  PageController.swift
//  MaCarteGrise
//
//  Created by Gurjot Kalsi on 11/01/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialPageControl

class PageController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet var page_control: UIPageControl!
    @IBOutlet var backButton: UIButton!
    
    @IBOutlet var page_image: UIImageView!
    
    @IBOutlet var page_topic: UILabel!
    @IBOutlet var page_text: UILabel!
    @IBOutlet var startedButton: UIButton!
    
    var currentIndex = 0
    
    var topic_array = ["Per Lot Profit", "Score", "Rank", "Call / Put Ratio", "Green, Blue, Red Symbols", "Trigger Price (TP)", "Finsparc FNO Mood Index", "Stock Profit, Options Profit", "Ten Lot Trades"]
    var text_array = ["", "", "", "", "", "", "", "", ""]
    var image_array = ["Pg1", "Pg2", "Pg3", "Pg4", "Pg5", "Pg6", "Pg7", "Pg8", "Pg9"]
    
    let pageControl = MDCPageControl()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pageControl.numberOfPages = 9
        
        let pageControlSize = pageControl.sizeThatFits(view.bounds.size)
        pageControl.frame = CGRect(x: 0, y: backButton.frame.origin.y - pageControlSize.height, width: view.bounds.width, height: pageControlSize.height)
        pageControl.addTarget(self, action: #selector(didChangePage), for: .valueChanged)
        pageControl.autoresizingMask = [.flexibleTopMargin, .flexibleWidth]
        
        pageControl.currentPageIndicatorTintColor = .systemBlue
//        pageControl.currentPage = 0
//        pageControl.setCurrentPage(0, animated: true)
        
        view.addSubview(pageControl)
    }
    
    @objc func didChangePage(sender: MDCPageControl) {
        currentIndex = sender.currentPage
        self.ChangePageContent()
    }
    
    //MARK:-
    func ChangePageContent() {
        self.page_topic.text = self.topic_array[currentIndex]
        self.page_image.image = UIImage(named: self.image_array[currentIndex])
        
        Timer.scheduledTimer(withTimeInterval: 0.4, repeats: false) { (tim) in
            self.pageControl.currentPage = self.currentIndex
        }
        
    }
    
    //MARK:- IBAction -
    @IBAction func ButtonActions(_ sender: UIButton) {
        
        if sender.tag == 1 {
            //next
            currentIndex += 1
            
            if currentIndex == 9 {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
                self.navigationController?.pushViewController(vc, animated: true)
                return
            }
        }else{
            //skip
            if currentIndex >= 1 {
                currentIndex -= 1
            }else{
                //navigate
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
                self.navigationController?.pushViewController(vc, animated: true)
                return
            }
        }
        
        if currentIndex == 0 {
            backButton.setTitle("Skip", for: .normal)
        }else{
            backButton.setTitle("Back", for: .normal)
        }
        
        page_control.currentPage = currentIndex
        
        
        pageControl.setCurrentPage(currentIndex, animated: true)
        
        Timer.scheduledTimer(withTimeInterval: 0.4, repeats: false) { (tim) in
            self.pageControl.currentPage = self.currentIndex
        }
        
        self.ChangePageContent()
    }
    
    @IBAction func WidgetAction(_ sender: UIPageControl) {
        currentIndex = sender.currentPage
        self.ChangePageContent()
    }
    
    @IBAction func GetStartedAction(_ sender: UIButton) {
        //Skip forever
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
