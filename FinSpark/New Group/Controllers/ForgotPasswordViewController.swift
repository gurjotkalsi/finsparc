//
//  ForgotPasswordViewController.swift
//  Finsparc
//
//  Created by mayur on 2/5/20.
//  Copyright © 2020 Mayur Parmar. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class forgotPasswordCell: UITableViewCell
{
    @IBOutlet weak var imgForgot: UIImageView!
    @IBOutlet weak var lblResetPassword: UILabel!
    @IBOutlet weak var txtSendCode: SkyFloatingLabelTextField!
    @IBOutlet weak var txtSecurityCode: SkyFloatingLabelTextField!
    @IBOutlet weak var btnContinue: UIButton!
}


class ForgotPasswordViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    
    @IBOutlet weak var HeaderVW: UIView! {
        didSet {
            HeaderVW.setUpHeaderView()
        }
    }

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnVerify: UIButton!
    
    @IBOutlet var field_one: UITextField!
    @IBOutlet var field_two: UITextField!
    @IBOutlet var field_three: UITextField!
    @IBOutlet var field_four: UITextField!
    
    @IBOutlet var otpView: UIView!
    @IBOutlet var otpBox: UIView!
    
    var app_Controller = App_Controller()
    var app_Delegate = AppDelegate()
    var cell = forgotPasswordCell()
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewDidLayoutSubviews() {
        self.field_one.underlineIt()
        self.field_two.underlineIt()
        self.field_three.underlineIt()
        self.field_four.underlineIt()
        self.btnVerify.setUpButton()
        self.btnVerify.titleLabel?.font = UIFont(name: self.app_Controller.App_Font_Style_Bold, size: CGFloat(UserDefaults.standard.integer(forKey: "Text_Size")-2))
        self.otpBox.layer.cornerRadius = 6
        self.otpBox.addShadow()
        
    }
    
    //MARK:- IBActions -
    
    @IBAction func onTapDismiss(_ sender: UITapGestureRecognizer) {
        self.otpView.fadeOut {
            self.field_one.text = ""
            self.field_two.text = ""
            self.field_three.text = ""
            self.field_four.text = ""
        }
    }
    
    @IBAction func onClickVerify(_ sender: UIButton) {
        if self.field_one.text == "" {
            self.field_one.becomeFirstResponder()
        }else if self.field_two.text == "" {
            self.field_two.becomeFirstResponder()
        }else if self.field_three.text == "" {
            self.field_three.becomeFirstResponder()
        }else if self.field_four.text == "" {
            self.field_four.becomeFirstResponder()
        }else{
            if !isInternetConnected() {
                self.ShowToast(message: "Check internet connection!")
                return
            }
            
            let otpString = "\(field_one.text!)\(field_two.text!)\(field_three.text!)\(field_four.text!)"
            
            self.AddLoader()
            ForgotVerifyModel().verifyOTP(parameter: ["mobile_no":self.cell.txtSendCode.text!, "otp":otpString]) { (vmodel) in
                self.RemoveLoader()
                
                if vmodel.status == "200" {
                    self.otpView.fadeOut {
                        let newPasswordVC = self.storyboard?.instantiateViewController(withIdentifier: "NewPasswordViewController") as! NewPasswordViewController
                        newPasswordVC.mobileNumber = self.cell.txtSendCode.text!
                        self.navigationController?.pushViewController(newPasswordVC, animated: true)
                    }
                }else{
                    self.ShowToast(message: vmodel.msg)
                }
            }
        }
    }
    
    @IBAction func onClickedBack(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickedContinue(_ sender: UIButton)
    {
        if cell.txtSendCode.text == "" || cell.txtSecurityCode.text == "" {
            self.ShowToast(message: "All fields are required!")
        }else{
            
            if !isInternetConnected() {
                self.ShowToast(message: "Check internet connection!")
                return
            }
            
            self.AddLoader()
            ForgotModel().getOTP(parameter: ["mobile_no":self.cell.txtSendCode.text!]) { (model) in
                
                self.RemoveLoader()
                if model.status == "200" {
                    //unhide otp view
                    self.otpView.fadeIn {
                        self.field_one.becomeFirstResponder()
                    }
                }else{
                    self.ShowToast(message: model.msg)
                }
            }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
      {
          return 1
      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
      {
        cell = tblView.dequeueReusableCell(withIdentifier: "forgotPasswordCell", for: indexPath) as! forgotPasswordCell

        cell.lblResetPassword.font =  UIFont(name: self.app_Controller.App_Font_Style_Bold, size: CGFloat(UserDefaults.standard.integer(forKey: "Text_Size")-2))

        cell.btnContinue.titleLabel?.font = UIFont(name: self.app_Controller.App_Font_Style_Bold, size: CGFloat(UserDefaults.standard.integer(forKey: "Text_Size")-2))
        cell.btnContinue.setUpButton()

        cell.txtSendCode.lineHeight = 0.5
        cell.txtSendCode.placeholderColor = .lightGray
        cell.txtSendCode.selectedLineColor = .lightGray
        cell.txtSendCode.selectedTitleColor = .lightGray
        cell.txtSendCode.font = UIFont(name: self.app_Controller.App_Font_Style_Regular, size: CGFloat(UserDefaults.standard.integer(forKey: "Text_Size")-2))

        cell.txtSecurityCode.lineHeight = 0.5
        cell.txtSecurityCode.placeholderColor = .lightGray
        cell.txtSecurityCode.selectedLineColor = .lightGray
        cell.txtSecurityCode.selectedTitleColor = .lightGray
        cell.txtSecurityCode.font = UIFont(name: self.app_Controller.App_Font_Style_Regular, size: CGFloat(UserDefaults.standard.integer(forKey: "Text_Size")-2))

        return cell
          
      }
      func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
      {
          return 691.0
      }
      func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
          return UITableView.automaticDimension
      }
  
}


extension ForgotPasswordViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == "" {
            if field_four.isFirstResponder {
                textField.text = " "
                self.field_three.becomeFirstResponder()
                return false
            } else if field_three.isFirstResponder {
                textField.text = " "
                self.field_two.becomeFirstResponder()
                return false
            } else if field_two.isFirstResponder {
                textField.text = " "
                self.field_one.becomeFirstResponder()
                return false
            } else {
            }
            
        }else{
            if field_one.isFirstResponder {
                textField.text = string
                self.field_two.becomeFirstResponder()
            } else if field_two.isFirstResponder {
                textField.text = string
                self.field_three.becomeFirstResponder()
            } else if field_three.isFirstResponder {
                textField.text = string
                self.field_four.becomeFirstResponder()
            } else if field_four.isFirstResponder {
                //field_four
                textField.text = string
                textField.resignFirstResponder()
            }
            
        }
        
        return true
    }
    
    
}
