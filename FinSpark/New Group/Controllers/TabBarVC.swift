//
//  TabBarVC.swift
//  Sales Tracker
//
//  Created by Gurjot Kalsi on 13/01/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class TabBarVC: UITabBarController {
    
    var Y = CGFloat(0)
    var X = CGFloat(0)
    
    //MARK:-
    //"About us", "Contact us", "Edit profile", "Change password", "Finsparc presentation", "T & C", "Feedback & Support", "Logout"
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        X = self.tabBar.subviews[1].center.x - 30
        Y = self.tabBar.frame.origin.y - 2
        sliderHolder.frame = CGRect(x: self.X, y: self.Y, width: 60, height: 3)
        sliderHolder.fadeIn {
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Y = self.tabBar.frame.origin.y - 2
        X = self.tabBar.subviews[1].center.x - 30
        sliderHolder = UIView(frame: CGRect(x: 11, y: Y, width: 60, height: 3))
        sliderHolder.backgroundColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
        
        if !view.subviews.contains(sliderHolder) {
            view.addSubview(sliderHolder)
        }
        
        sliderHolder.alpha = 0
        NotificationCenter.default.addObserver(self, selector: #selector(self.SideMenuAction(_:)), name: NSNotification.Name("NavigateTo"), object: nil)
    }
    
    //MARK:-
    
    @objc func SideMenuAction(_ notify : Notification) {
        if let obj = notify.object as? String {
            
            if obj == "AboutUs" {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AboutUs") as! AboutUs
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else if obj == "ContactUs" {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContactUs") as! ContactUs
                self.navigationController?.pushViewController(vc, animated: true)
            }else if obj == "EditProfile" {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditProfile") as! EditProfile
                self.navigationController?.pushViewController(vc, animated: true)
            }else if obj == "ChangePassword" {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangePassword") as! ChangePassword
                self.navigationController?.pushViewController(vc, animated: true)
            }else if obj == "Presentation" {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "Presentation") as! Presentation
                self.navigationController?.pushViewController(vc, animated: true)
            }else if obj == "TandC" {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "TandC") as! TandC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if obj == "Feedback" {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "Feedback") as! Feedback
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                //Feedback & Support
                
            }
            
            
        }
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if item.title! == "Home" {
            X = self.tabBar.subviews[1].center.x - 30
        } else if item.title! == "Watchlist" {
            X = self.tabBar.subviews[2].center.x - 30
        } else if item.title! == "Portfolio" {
            X = self.tabBar.subviews[3].center.x - 30
        }else{
            //FS Notes
            X = self.tabBar.frame.width - 75
        }
        
        UIView.animate(withDuration: 0.4) {
            sliderHolder.frame = CGRect(x: self.X, y: self.Y, width: 60, height: 3)
        }
        
    }
    
}
