//
//  NewPasswordViewController.swift
//  Finsparc
//
//  Created by mayur on 2/5/20.
//  Copyright © 2020 Mayur Parmar. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class newPasswordCell: UITableViewCell
{
    @IBOutlet weak var imgNewPassword: UIImageView!
    @IBOutlet weak var lblPleaseEnterPassword: UILabel!

    @IBOutlet weak var txtNewPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var txtConfirmNewPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var btnSave: UIButton!
}

class NewPasswordViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{

    var app_Controller = App_Controller()
    var app_Delegate = AppDelegate()
    var cell = newPasswordCell()
    var mobileNumber = ""
    
    @IBOutlet weak var HeaderVW: UIView! {
       didSet {
           HeaderVW.setUpHeaderView()
       }
    }
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnBack: UIButton!
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    //MARK:- IBActions -
    @IBAction func onClickedBack(_ sender: UIButton)
    {
       self.navigationController?.popViewController(animated: true)
   
    }
    
    @IBAction func onClickedSave(_ sender: UIButton)
    {
        if cell.txtNewPassword.text == "" || cell.txtConfirmNewPassword.text == "" {
            self.ShowToast(message: "All fields are required!")
        }else if cell.txtNewPassword.text! != cell.txtConfirmNewPassword.text! {
            self.ShowToast(message: "Password does not match!")
        }else{
            if !isInternetConnected() {
                self.ShowToast(message: "Check internet connection!")
                return
            }
            
            self.AddLoader()
            UpdateModel().updatePassword(parameter: ["mobile":mobileNumber, "password":cell.txtConfirmNewPassword.text!]) { (pmodel) in
                
                self.RemoveLoader()
                self.ShowToast(message: pmodel.msg) {
                    self.cell.txtNewPassword.text = ""
                    self.cell.txtConfirmNewPassword.text = ""
                    
                    if let navControllers = self.navigationController?.viewControllers {
                        for item in navControllers {
                            if item.restorationIdentifier == "LoginViewController" {
                                self.navigationController?.popToViewController(item, animated: true)
                            }
                        }
                    }
                    
                }
            }
        }
    }
       
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        cell = tblView.dequeueReusableCell(withIdentifier: "newPasswordCell", for: indexPath) as! newPasswordCell

        cell.lblPleaseEnterPassword.font =  UIFont(name: self.app_Controller.App_Font_Style_Bold, size: CGFloat(UserDefaults.standard.integer(forKey: "Text_Size")-2))

        cell.btnSave.titleLabel?.font =  UIFont(name: self.app_Controller.App_Font_Style_Bold, size: CGFloat(UserDefaults.standard.integer(forKey: "Text_Size")-2))
        cell.btnSave.setUpButton()


        cell.txtNewPassword.lineHeight = 0.5
        cell.txtNewPassword.placeholderColor = .lightGray
        cell.txtNewPassword.selectedLineColor = .lightGray
        cell.txtNewPassword.selectedTitleColor = .lightGray
        cell.txtNewPassword.font = UIFont(name: self.app_Controller.App_Font_Style_Regular, size: CGFloat(UserDefaults.standard.integer(forKey: "Text_Size")-2))

        cell.txtConfirmNewPassword.lineHeight = 0.5
        cell.txtConfirmNewPassword.placeholderColor = .lightGray
        cell.txtConfirmNewPassword.selectedLineColor = .lightGray
        cell.txtConfirmNewPassword.selectedTitleColor = .lightGray
        cell.txtConfirmNewPassword.font = UIFont(name: self.app_Controller.App_Font_Style_Regular, size: CGFloat(UserDefaults.standard.integer(forKey: "Text_Size")-2))

        return cell

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 691.0
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
  
}
