//
//  futureCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 04/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class futureCell: UITableViewCell {
    @IBOutlet weak var title_Label: UILabel!
    @IBOutlet weak var border_View: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        border_View.addBorder()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
