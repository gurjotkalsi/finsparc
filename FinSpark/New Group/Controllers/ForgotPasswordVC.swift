//
//  ForgotPasswordVC.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 07/02/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {

    @IBOutlet var submit_btn: UIButton!
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()

        self.LeftBarLogoItem()
        self.submit_btn.roundUp()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    //MARK:-
    
    @IBAction func SubmitAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    

}
