//
//  callDetailVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 01/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class callDetailVC: UIViewController {
    
    @IBOutlet weak var TopTabs: UICollectionView!
    @IBOutlet weak var collctionviewTopTabs: UICollectionView!
    @IBOutlet weak var callTableView: UITableView!
    var titleArray = ["Last Day", "Last Week"]
    var selection = NSMutableArray();

    @IBOutlet var expiry_date: UILabel!
    @IBOutlet var eod_date: UILabel!
    var expiry = ""
       var creation = ""
    @IBOutlet var viewConstant: NSLayoutConstraint!

    var typeofPuttext: String = ""
    var titlesArray = ["Symbol", "Strike price", "Trigger Price", "Price Close", "Expiry High\nExpiry Low"]
    var titleHeading: String = ""

    var objectsArray = NSArray()
    var typeclass : NSObject.Type?
    
    //MARK:-
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.CustomizeNavBar_with(title: "", button_text: titleHeading, bartint: UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1), backButton: true) {
            self.navigationController?.popViewController(animated: true)
        }
        
        if typeclass == ReportItem.self {
            print("ReportItem")
        }else if typeclass == ReportSecondItem.self {
            print("ReportSecondItem")
        }else{
            print("ReportThirdItemObject")
        }
        
        self.expiry_date.text =  "Expiry Date : \(self.Convertorequireformat(expiry))"
        self.eod_date.text = "\(self.Convertorequireformat(creation)) (EOD)"
    }
    func Convertorequireformat (_ date : String ) -> String{
        let inputFormatter = DateFormatter()
           inputFormatter.dateFormat = "yyyy-MM-dd"
           let showDate = inputFormatter.date(from: date)
           inputFormatter.dateFormat = "dd-MM-yyyy"
           let resultString = inputFormatter.string(from: showDate!)
          return resultString
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.viewConstant.constant = 0

        if typeofPuttext == "Indices" {
            titlesArray = ["Symbol", "Strike price", "Trigger Price", "Price Close", "Expiry High\nExpiry Low"]
        } else if typeofPuttext == "NTM" {
            titlesArray = ["Symbol", "Strike price", "Call Trigger Price", "Call Price Close", "Futures Price Close"]
        } else if typeofPuttext == "OI" {
           self.viewConstant.constant = 35
            titlesArray = ["Symbol", "Strike price", "Price", "OI \n▵ in OI"]

        } else {
            
        }
        TopTabs.register(UINib(nibName: "topTabCell", bundle: nil), forCellWithReuseIdentifier: "topTabCell")
        let indexpath = 0
               selection.add(indexpath)
        TopTabs.reloadData()
        
        collctionviewTopTabs.register(UINib(nibName: "topTabCell", bundle: nil), forCellWithReuseIdentifier: "topTabCell")
        collctionviewTopTabs.reloadData()
        
    }
    //MARK:-
}

extension callDetailVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objectsArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfSectionsInTableView section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "putDetailCell", for: indexPath) as! putDetailCell
        cell.selectionStyle = .none
        
        cell.typeofPuttext = self.typeofPuttext
        cell.object = self.objectsArray.object(at: indexPath.row)
        cell.typeclass = self.typeclass
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OptionVC") as! OptionVC
           self.navigationController?.pushViewController(vc, animated: true)
       }
}


extension callDetailVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        if collectionView == collctionviewTopTabs
               {
                if typeofPuttext == "NTM"
                {
                    return 5
                }else
                {
                    return titlesArray.count
                }
               }else
               {
                   return 2
               }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.collctionviewTopTabs.dequeueReusableCell(withReuseIdentifier: "topTabCell", for: indexPath) as! topTabCell
      if collectionView == collctionviewTopTabs
             {
                cell.lbl_Headings.text = self.titlesArray[indexPath.row]

        if indexPath.row == 0 {
            cell.lbl_Headings.lineBreakMode = .byWordWrapping
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineHeightMultiple = 0.84
            cell.lbl_Headings.attributedText = NSMutableAttributedString(string: self.titlesArray[indexPath.row], attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
            
        } else {
            cell.lbl_Headings.textAlignment = .right
        }
        }else
      {
           cell.lbl_Headings.text = self.titleArray[indexPath.row]
           cell.lbl_Headings.roundUp()
        if selection.contains(indexPath.row) {
                   cell.lbl_Headings.backgroundColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
                   cell.lbl_Headings.textColor = UIColor.white
                   
               }else{
                   cell.lbl_Headings.backgroundColor = UIColor.clear
                   cell.lbl_Headings.textColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
               }
      }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
      //  var width = (collctionviewTopTabs.frame.size.width - 40) / 6.2
        if collectionView == collctionviewTopTabs
        {
        var width = (collctionviewTopTabs.frame.size.width - 40) / 5.1

        if typeofPuttext == "NTM" {
            width = (collctionviewTopTabs.frame.size.width - 40) / 5.1
        }
        else if typeofPuttext == "OI" {
            
            width = (collctionviewTopTabs.frame.size.width - 40) / 3.3

        }
        return CGSize(width: width , height: width)
        }
        else
        {
            return CGSize(width: collectionView.frame.width / 3.3, height: collectionView.frame.height)

        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == TopTabs
        {
        selection.removeAllObjects()
        selection.add(indexPath.row)
        }
//        if indexPath.item == 0 {
//            callType = .callCurrent
//        }else{
//            callType = .callNext
//        }
//
//        self.webservice()
        
        collectionView.reloadData()
    }
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//
////         let vc = self.storyboard?.instantiateViewController(withIdentifier: "OptionVC") as! OptionVC
////                  self.navigationController?.pushViewController(vc, animated: true)
//    }
    
}
