//
//  callsVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 01/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class callsVC: UIViewController {
    @IBOutlet weak var collctionviewTopTabs: UICollectionView!
    @IBOutlet weak var callTableView: UITableView!
    
    var titlesArray = ["Current Expiry", "Next Expiry"]
    var selection = NSMutableArray();
    var indicesArray = ["Most Active Calls (Indices)", "Most Active Calls (Stocks)","Strong near the Money (NTM) Calls", "Weak Near the Money (NTM) Calls", "Adding Most OI (Calls)", "Shedding Most OI (Calls)"]
    var descriptionArray = ["(Check the most active Indices Calls and their strength)", "(Check the most active Stock Calls and their strength)","(May be bought with SL)", "(May be sold with SL)", "(Caution: OI Build up in Calls is not always bullish, may mean Call writing also)", "(Could be an account of profit booking or short covering)"]

    var callType : CallPutType = .callCurrent
    
    //MARK:-
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        sliderHolder.fadeOut {}
        self.CustomizeNavBar_with(title: "", button_text: "Call", bartint: UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1), backButton: true) {
            self.navigationController?.popViewController(animated: true)
            sliderHolder.fadeIn {}
        }
        self.webservice()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        
        collctionviewTopTabs.register(UINib(nibName: "topTabCell", bundle: nil), forCellWithReuseIdentifier: "topTabCell")
        let indexpath = 0
        selection.add(indexpath)
        collctionviewTopTabs.reloadData()
        callTableView.register(UINib(nibName: "putCell", bundle: nil), forCellReuseIdentifier: "putCell")
        
    }
    //MARK:-
    func webservice() {
        self.AddLoader()
        CallPutReportsModel.shared.process(type: callType) { (modl) in
            self.RemoveLoader()
            
            self.callTableView.reloadData()
        }
    }
    
}

extension callsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, numberOfSectionsInTableView section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "putCell", for: indexPath) as! putCell
        cell.selectionStyle = .none
        cell.title_Label.text = self.indicesArray[indexPath.row]
        cell.sub_Label.text = self.descriptionArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "callDetailVC") as! callDetailVC
        if indexPath.row == 0 || indexPath.row == 1 {
            vc.typeofPuttext = "Indices"
        } else if indexPath.row == 2 || indexPath.row == 3 {
            vc.typeofPuttext = "NTM"
        } else if indexPath.row == 4 || indexPath.row == 5 {
            vc.typeofPuttext = "OI"
        } else {
            vc.typeofPuttext = "Comparison"
        }
        
        vc.typeclass = ReportItem.self
        
        if indexPath.row == 0 {
            vc.objectsArray = CallPutReportsModel.shared.most_active_indices as NSArray
            vc.typeclass = ReportItem.self
            vc.expiry = CallPutReportsModel.shared.most_active_indices[0].expiry_dt
            vc.creation = CallPutReportsModel.shared.most_active_indices[0].cdate

        }else if indexPath.row == 1 {
            vc.objectsArray = CallPutReportsModel.shared.most_active_stock as NSArray
            vc.typeclass = ReportItem.self
            vc.expiry = CallPutReportsModel.shared.most_active_stock[0].expiry_dt
            vc.creation = CallPutReportsModel.shared.most_active_stock[0].cdate

        }else if indexPath.row == 2 {
            vc.objectsArray = CallPutReportsModel.shared.strong_near_money as NSArray
            vc.typeclass = ReportSecondItem.self
            vc.expiry = CallPutReportsModel.shared.strong_near_money[0].current_expiry
            vc.creation = CallPutReportsModel.shared.strong_near_money[0].last_date

        }else if indexPath.row == 3 {
            vc.objectsArray = CallPutReportsModel.shared.weak_near_money as NSArray
            vc.typeclass = ReportSecondItem.self
            vc.expiry = CallPutReportsModel.shared.weak_near_money[0].current_expiry
            vc.creation = CallPutReportsModel.shared.weak_near_money[0].last_date

        }else if indexPath.row == 4 {
            vc.objectsArray = CallPutReportsModel.shared.add_most_oi.data as NSArray
            vc.typeclass = ReportThirdItemObject.self
            vc.expiry = CallPutReportsModel.shared.add_most_oi.data[0].expiry_dt
            vc.creation = CallPutReportsModel.shared.add_most_oi.last_date

        }else{
            vc.objectsArray = CallPutReportsModel.shared.shed_most_oi.data as NSArray
            vc.typeclass = ReportThirdItemObject.self
            vc.expiry = CallPutReportsModel.shared.shed_most_oi.data[0].expiry_dt
            vc.creation = CallPutReportsModel.shared.shed_most_oi.last_date
        }
        vc.titleHeading = self.indicesArray[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}

extension callsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3.3, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.collctionviewTopTabs.dequeueReusableCell(withReuseIdentifier: "topTabCell", for: indexPath) as! topTabCell
        cell.lbl_Headings.text = self.titlesArray[indexPath.row]
        cell.lbl_Headings.roundUp()
        
        if selection.contains(indexPath.row) {
            cell.lbl_Headings.backgroundColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
            cell.lbl_Headings.textColor = UIColor.white
            
        }else{
            cell.lbl_Headings.backgroundColor = UIColor.clear
            cell.lbl_Headings.textColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selection.removeAllObjects()
        selection.add(indexPath.row)

        if indexPath.item == 0 {
            callType = .callCurrent
        }else{
            callType = .callNext
        }
        
        self.webservice()
        
        collectionView.reloadData()
    }
    
    // Layout: Set Edges
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
}
