//
//  watchlistCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 20/02/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class watchlistCell: UITableViewCell {
    @IBOutlet weak var watchlistView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        watchlistView.layer.borderColor = UIColor(red: 0.904, green: 0.904, blue: 0.904, alpha: 1).cgColor

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
