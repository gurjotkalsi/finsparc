//
//  industrieslistVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 02/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class industrieslistVC: UIViewController {
    
    @IBOutlet weak var industriesTableView: UITableView!
    
    var selection = ""
    var onDoneBlock : ((String) -> ())!

    //MARK:-
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        if StockRankingModel.shared.industryList.count == 0 {
            self.AddLoader()
            StockRankingModel.shared.getIndustries { (model) in
                self.RemoveLoader()
                self.industriesTableView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        industriesTableView.register(UINib(nibName: "industryCell", bundle: nil), forCellReuseIdentifier: "industryCell")
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.onDoneBlock(self.selection)
        }
    }

}

extension industrieslistVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return StockRankingModel.shared.industryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "industryCell", for: indexPath) as! industryCell
        let item = StockRankingModel.shared.industryList[indexPath.row]
        cell.typeLabel.text = item.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = StockRankingModel.shared.industryList[indexPath.row]
        self.selection = item.name
        self.dismiss(animated: true) {
            self.onDoneBlock(self.selection)
        }
    }
}
