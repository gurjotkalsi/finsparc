//
//  perlotProfitCell.swift
//  FinSpark
//
//  Created by Minkle Garg on 17/02/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class perlotProfitCell: UITableViewCell {
    
    @IBOutlet weak var symbol_value: UILabel!
    @IBOutlet weak var price_value: UILabel!
    @IBOutlet weak var ltp_value: UILabel!
    @IBOutlet weak var today_plp_value: UILabel!
    @IBOutlet weak var five_day_plp_value: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
