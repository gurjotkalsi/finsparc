//
//  gbrDetailVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 04/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class gbrDetailVC: UIViewController {
    
  //  @IBOutlet weak var collctionviewTopTabs: UICollectionView!
    @IBOutlet weak var putTableView: UITableView!
    
    var titlesArray = ["Symbol", "Score", "Trigger Price", "Price Close", "Call Ratio", "Put Ratio"]
    
    var typeofPuttext: String = ""
    var navTitle: String = ""
    var valuesArray = [GBRItem]()
    @IBOutlet var expiry_date: UILabel!
    @IBOutlet var creation_date: UILabel!
     var expiry = ""
    var creation = ""
    var cell = futureDetailCell()
    
    //MARK:-
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        expiry_date.text = expiry
        creation_date.text = creation
        self.CustomizeNavBar_with(title: "", button_text: navTitle, bartint: UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1), backButton: true) {
            self.navigationController?.popViewController(animated: true)
        }
        
        sliderHolder.fadeOut { }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true

        UserDefaults.standard.set(typeofPuttext, forKey: "type") //setObject

        if typeofPuttext == "blue" {
            titlesArray = ["Symbol", "Score", "Trigger Price", "Price Close", "Call Ratio", "Put Ratio"]
        } else {
            titlesArray = ["Symbol", "Score", "Rank", "Trigger Price", "Price Close", "Same Position count (days)"]
        }
               putTableView.register(UINib(nibName: "significantCell", bundle: nil), forCellReuseIdentifier: "significantCell")

//        collctionviewTopTabs.register(UINib(nibName: "topTabCell", bundle: nil), forCellWithReuseIdentifier: "topTabCell")
//        collctionviewTopTabs.reloadData()
        
    }
    //MARK:-
    
}

extension gbrDetailVC: UITableViewDelegate, UITableViewDataSource {
    //MARK:- UITableView Delegates -
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return valuesArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfSectionsInTableView section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "significantCell") as! significantCell
        cell.selectionStyle = .none
        cell.view.backgroundColor = .white
       // cell.DataValues = valuesArray[indexPath.row]
        cell.one.text = valuesArray[indexPath.row].symbol
        cell.two.text = valuesArray[indexPath.row].total
        cell.three.text = valuesArray[indexPath.row].rank
        cell.four.attributedText = valuesArray[indexPath.row].trigger_price.italic(cell.four.font.pointSize)
        cell.five.text = valuesArray[indexPath.row].future_closing +  "\n(" + valuesArray[indexPath.row].percent_change_last_days_price + "%)"
        cell.six.text = valuesArray[indexPath.row].color_count
      //  cell.DataValues = valuesArray[indexPath.row]
        cell.two.textAlignment = .right
        return cell
    }

    func numberOfSections(in tableView: UITableView) -> Int {
           return 1
       }
       
       func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
              return 75
          }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

            let cell = tableView.dequeueReusableCell(withIdentifier: "significantCell") as! significantCell
           cell.one.text = titlesArray[0]
           cell.two.text = titlesArray[1]
           cell.three.text = titlesArray[2]
           cell.four.text = titlesArray[3]
           cell.five.text = titlesArray[4]
           cell.six.text = titlesArray[5]
        cell.two.textAlignment = .right
        cell.three.textAlignment = .right
        cell.four.textAlignment = .right
        cell.five.textAlignment = .right
        cell.six.textAlignment = .right
        cell.one.textAlignment = .left

           return cell
    
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

                let vc = self.storyboard?.instantiateViewController(withIdentifier: "watchlistSubdetailVC") as! watchlistSubdetailVC
        vc.company = valuesArray[indexPath.row].symbol
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

//MARK: Collectionview Methods
/*extension gbrDetailVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if typeofPuttext == "NTM" {
            return 5
        }
        
        return 6
    }
       
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           
        let cell = self.collctionviewTopTabs.dequeueReusableCell(withReuseIdentifier: "topTabCell", for: indexPath) as! topTabCell
        cell.lbl_Headings.text = self.titlesArray[indexPath.row]
        cell.lbl_Headings.roundUp()
       // if indexPath.row == 0 {
            cell.lbl_Headings.lineBreakMode = .byWordWrapping
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineHeightMultiple = 0.84
            cell.lbl_Headings.attributedText = NSMutableAttributedString(string: self.titlesArray[indexPath.row], attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
//        } else {
//            cell.lbl_Headings.textAlignment = .center
//        }
           
        return cell
        
    }
       
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var width = (collctionviewTopTabs.frame.size.width - 40) / 6.1
        if typeofPuttext == "NTM" {
            width = (collctionviewTopTabs.frame.size.width - 40) / 5
        }
        return CGSize(width: width , height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
           collectionView.reloadData()
    }
    
    // Layout: Set Edges
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
      return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}*/
