//
//  gbrVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 04/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class gbrVC: UIViewController {
    @IBOutlet weak var collctionviewTopTabs: UICollectionView!
    @IBOutlet weak var gbrTableView: UITableView!

    var titlesArray = ["Current Expiry", "Next Expiry"]
    var selection = NSMutableArray();
    
    var listArray = ["Green Futures", "Blue Futures", "Red Futures", "New Greens", "New Reds"]
    var subArray = ["Strong stocks", "Sideways stocks", "Weak stocks", "Stocks which turned Strong Today.", "Stocks which turned Weak Today."]

    
    //MARK:-
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.CustomizeNavBar_with(title: "", button_text: "GBR", bartint: UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1), backButton: true) {
            self.navigationController?.popViewController(animated: true)
            sliderHolder.fadeIn { }
        }
        
        sliderHolder.fadeOut { }
        
        self.getGBRData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true

        collctionviewTopTabs.register(UINib(nibName: "topTabCell", bundle: nil), forCellWithReuseIdentifier: "topTabCell")
        let indexpath = 0
        selection.add(indexpath)
        collctionviewTopTabs.reloadData()
        gbrTableView.register(UINib(nibName: "putCell", bundle: nil), forCellReuseIdentifier: "putCell")

    }
    //MARK:-
    
    func getGBRData() {
        GBRModel.shared.process { (model) in
            self.gbrTableView.reloadData()
        }
    }

}


extension gbrVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "putCell", for: indexPath) as! putCell
        cell.sub_Label.text = self.subArray[indexPath.row]
        
        if selection.contains(0) {
            if indexPath.row == 0 {
                cell.title_Label.text = "\(self.listArray[indexPath.row]) (\(GBRModel.shared.current_green_future.count))"
            }else if indexPath.row == 1 {
                cell.title_Label.text = "\(self.listArray[indexPath.row]) (\(GBRModel.shared.current_blue_future.count))"
            }else{
                cell.title_Label.text = "\(self.listArray[indexPath.row]) (\(GBRModel.shared.current_red_future.count))"
            }

        }else{
            
            if indexPath.row == 0 {
                cell.title_Label.text = "\(self.listArray[indexPath.row]) (\(GBRModel.shared.next_green_future.count))"
            }else if indexPath.row == 1 {
                cell.title_Label.text = "\(self.listArray[indexPath.row]) (\(GBRModel.shared.next_blue_future.count))"
            }else{
                cell.title_Label.text = "\(self.listArray[indexPath.row]) (\(GBRModel.shared.next_red_future.count))"
            }

        }
                
        cell.selectionStyle = .none
        return cell
    }
    
    func Convertorequireformat (_ date : String ) -> String{
        let inputFormatter = DateFormatter()
           inputFormatter.dateFormat = "yyyy-MM-dd"
           let showDate = inputFormatter.date(from: date)
           inputFormatter.dateFormat = "dd-MM-yyyy"
           let resultString = inputFormatter.string(from: showDate!)
          return resultString
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "gbrDetailVC") as! gbrDetailVC

        vc.expiry = "Expiry Date : \(self.Convertorequireformat(GBRModel.shared.expiry))"
        vc.creation = (self.Convertorequireformat(GBRModel.shared.cdate))
        if selection.contains(0) {
            if indexPath.row == 0 {
                vc.typeofPuttext = "green"
                vc.navTitle = "Green Futures"
                vc.valuesArray = GBRModel.shared.current_green_future
            } else if indexPath.row == 1 {
                vc.typeofPuttext = "blue"
                vc.navTitle = "Blue Futures"
                vc.valuesArray = GBRModel.shared.current_blue_future
            } else {
                vc.typeofPuttext = "red"
                vc.navTitle = "Red Futures"
                vc.valuesArray = GBRModel.shared.current_red_future
            }
        }else{
            if indexPath.row == 0 {
                vc.typeofPuttext = "green"
                vc.navTitle = "Green Futures"
                vc.valuesArray = GBRModel.shared.next_green_future
            } else if indexPath.row == 1 {
                vc.typeofPuttext = "blue"
                vc.navTitle = "Blue Futures"
                vc.valuesArray = GBRModel.shared.next_blue_future
            } else {
                vc.typeofPuttext = "red"
                vc.navTitle = "Red Futures"
                vc.valuesArray = GBRModel.shared.next_red_future
            }
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }

}


extension gbrVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.titlesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3.3, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
               
        let cell = self.collctionviewTopTabs.dequeueReusableCell(withReuseIdentifier: "topTabCell", for: indexPath) as! topTabCell
        
        cell.lbl_Headings.text = self.titlesArray[indexPath.row]
        cell.lbl_Headings.roundUp()
        
        if selection.contains(indexPath.row) {
            cell.lbl_Headings.backgroundColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
            cell.lbl_Headings.textColor = UIColor.white
            
        }else{
            cell.lbl_Headings.backgroundColor = UIColor.clear
            cell.lbl_Headings.textColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
        }
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selection.removeAllObjects()
        selection.add(indexPath.row)
        
        collectionView.reloadData()
        self.gbrTableView.reloadData()
    }
    
   // Layout: Set Edges
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
        
}
