//
//  stockRankingsVC.swift
//  FinSpark
//
//  Created by Minkle Garg on 01/03/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit
import iOSDropDown

class stockRankingsVC: UIViewController {
    
    @IBOutlet weak var collctionviewTopTabs: UICollectionView!
    @IBOutlet weak var rankingsTableView: UITableView!
    @IBOutlet weak var industryButton: DropDown!
    @IBOutlet weak var selectionView: NSLayoutConstraint!
    @IBOutlet weak var industryselectionView: UIView!
    
    @IBOutlet weak var expiry_lbl: UILabel!
    
    var symbols = [String]()
    var industryid = [String]()

    var titlesArray = ["All", "Industry", "Nifty50"]
    var selection = NSMutableArray()
    var indicesArray = ["Most Active Puts (Indices)", "Most Active Puts (Stocks)","Strong near the Money (NTM) Puts", "Weak Near the Money (NTM) Puts", "Shedding Most OI (Puts)", "Put Comparison(month on month)"]
    
    var typeSelection : StockTypes = .all
    var industrysid = ""
    //MARK:-
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        sliderHolder.fadeOut {}
        self.CustomizeNavBar_with(title: "", button_text: "Stock Rankings", bartint: UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1), backButton: true) {
            self.navigationController?.popViewController(animated: true)
            sliderHolder.fadeIn {}
        }
        
        self.webservice()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        
        industryButton.didSelect{(selectedText , index ,id) in
            self.industryButton.text = selectedText
            if selectedText != "" {
                self.industryButton.text = selectedText
            }else{
                self.industryButton.text = "Select Industry" //("Select Industry", for: .normal)
            }
            
            self.AddLoader()
            self.industrysid = self.industryid[index]
            
            StockRankingModel.shared.process(industry: "\(self.industrysid)") { (model) in
                self.RemoveLoader()
                self.rankingsTableView.reloadData()
            }
            
        }
        
        collctionviewTopTabs.register(UINib(nibName: "topTabCell", bundle: nil), forCellWithReuseIdentifier: "topTabCell")
        selection.add(0)
        collctionviewTopTabs.reloadData()
        rankingsTableView.register(UINib(nibName: "rankingsCell", bundle: nil), forCellReuseIdentifier: "rankingsCell")
        
        industryselectionView.isHidden = true
        selectionView.constant = 0
    }
    //MARK:-
    
    func webservice() {
        self.AddLoader()
        StockRankingModel.shared.process(type: typeSelection) { (model) in
            self.RemoveLoader()
            
            StockRankingModel.shared.getIndustries { (model) in
                self.RemoveLoader()
                if model.industryList.count != 0 {
                    for obj in model.industryList
                    {
                        self.symbols.append(obj.name)
                        self.industryid.append(obj._id)
                    }
                    self.industryButton.optionArray = self.symbols
                }
            }
            self.rankingsTableView.reloadData()
        }
    }
    
    func getInstrumentList() {
        
        if StockRankingModel.shared.industryList.count == 0 {
            self.AddLoader()
            StockRankingModel.shared.getIndustries { (model) in
                self.RemoveLoader()

                if model.list.count != 0 {
                    for obj in model.list
                    {
                        self.symbols.append(obj.symbol)
                    }
                    self.industryButton.optionArray = self.symbols
                }
            }
        }else
        {
            if industrysid != ""
             {
               self.AddLoader()
              StockRankingModel.shared.process(industry: "\(self.industrysid)") { (model) in
                  self.rankingsTableView.reloadData()
               self.RemoveLoader()
              }
                       }
        }
//        StockRankingModel.shared.process(type: typeSelection) { (model) in
//            self.RemoveLoader()
//
//            if model.list.count != 0 {
//                for obj in model.list
//                {
//                    self.symbols.append(obj.symbol)
//                }
//                self.industryButton.optionArray = self.symbols
//            }else{
//                self.RemoveLoader()
//            }
//        }
    }
    
    
    
}

extension stockRankingsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return StockRankingModel.shared.list.count
    }
    
    func tableView(_ tableView: UITableView, numberOfSectionsInTableView section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "rankingsCell", for: indexPath) as! rankingsCell
        cell.selectionStyle = .none
    
        if StockRankingModel.shared.list.count > 0
        {
        let item = StockRankingModel.shared.list[indexPath.row]
        
        cell.instrument_lbl.text = item.symbol
        cell.rank_current.text = item.rank
        cell.rank_oneWeek.text = item.last_week_rank
        cell.rank_fourWeek.text = item.last_month_rank
        
        cell.change_oneWeek.attributedText = item.last_month_percent.polarityColor()
        cell.change_fourWeek.attributedText = item.last_3_month_percent.polarityColor()
        
        self.expiry_lbl.text = "Updated on: \(item.cdate)"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = StockRankingModel.shared.list[indexPath.row]

        let vc = self.storyboard?.instantiateViewController(withIdentifier: "watchlistSubdetailVC") as! watchlistSubdetailVC
        vc.company = item.symbol
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}



extension stockRankingsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           return CGSize(width: collectionView.frame.width / 3.3, height: collectionView.frame.height)
       }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.collctionviewTopTabs.dequeueReusableCell(withReuseIdentifier: "topTabCell", for: indexPath) as! topTabCell
        cell.lbl_Headings.text = self.titlesArray[indexPath.row]
        cell.lbl_Headings.roundUp()

        if selection.contains(indexPath.row) {
            cell.lbl_Headings.backgroundColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
            cell.lbl_Headings.textColor = UIColor.white
        }else{
            cell.lbl_Headings.backgroundColor = UIColor.clear
            cell.lbl_Headings.textColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 1 {
            selectionView.constant = 68
            industryselectionView.isHidden = false
        } else {
            selectionView.constant = 0
            industryselectionView.isHidden = true
        }
        
        if indexPath.row == 0 {
            typeSelection = .all
            self.webservice()

        }else if indexPath.row == 1 {
            StockRankingModel.shared.list.removeAll()
            rankingsTableView.reloadData()
            typeSelection = .industrial
            self.getInstrumentList()

           
        }else{
            
            typeSelection = .nifty
            self.webservice()

        }
        
        selection.removeAllObjects()
        selection.add(indexPath.row)
        collectionView.reloadData()
    }
    
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
       return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
   }

}
