//
//  Globals.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 20/03/19.
//  Copyright © 2019 Mayur Parmar. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SystemConfiguration


var sliderHolder = UIView()

var spinnerView = JTMaterialSpinner()

let appDelegate = UIApplication.shared.delegate as! AppDelegate

@available(iOS 13.0, *)
let scene = (UIApplication.shared.connectedScenes.first!).delegate! as! SceneDelegate

let myStoryboard = UIStoryboard(name: "Main", bundle: nil)
let MenuDelegate = myStoryboard.instantiateViewController(withIdentifier: "SideMenu") as! SideMenu

var myFirebaseToken = ""
var device_token = ""
var currentController = ""
var currentChatID = ""
//var newChatUser : User?
var serviceToken = ""
let TestUserOne = "client2"
//let TestUserOne = "supinderkalsi"

let baseUrl = "http://3.6.6.59:7005/"

struct ServiceURL {
    static let register = "\(baseUrl)register_user"
    static let register_otp_verify = "\(baseUrl)otp_verify"
    
    static let login = "\(baseUrl)get_login"
    static let forgot_password = "\(baseUrl)forgetPasswordSendOtp"
    static let verify_forget_password = "\(baseUrl)verifyForgetPasswordOtp"
    static let update_password = "\(baseUrl)updatePassword"
    
    static let viewFinsparcPortfolioHeader = "\(baseUrl)viewfinsparcportfolioheader"
  //  static let listPerLotProfit = "\(baseUrl)viewfinsparcmarketnowdetails/symbol/asc"
    static let listPerLotProfit = "\(baseUrl)viewfinsparcmarketnowdetails/"

    static let mmi_Score = "\(baseUrl)viewFinsparcMmiScore"
    static let mmi_scrore_graph = "\(baseUrl)MMIScoreGraph"
    static let mmi_score_updated = "\(baseUrl)viewfinsparcmmiscore_updated"
    static let market_pulse = "\(baseUrl)viewfinsparcmarketpulse"
    static let garph_details = "\(baseUrl)viewFinsparGarphDetails"
    
    static let viewhomepage = "\(baseUrl)viewhomepage"
    static let gbrReport = "\(baseUrl)viewFinsparcGRBReport"
    static let fnoPulse = "\(baseUrl)fnopulse"
    
    static let ivReport = "\(baseUrl)viewFinsparcIVReport"
    
    static let scoreCardAll = "\(baseUrl)viewFinsparcScoreCard/all/all"
    static let scoreCardIndustry = "\(baseUrl)viewFinsparcScoreCard/1/all"
    static let scoreCardNifty = "\(baseUrl)viewFinsparcScoreCard/all/17"
    static let scoreCardSelectedIndustry = "http://3.6.6.59:7005/viewFinsparcScoreCard/all/"
    static let callReportCurrent = "\(baseUrl)viewFinsparcCallReport/current"
    static let callReportNext = "\(baseUrl)viewFinsparcCallReport/next"
    static let putReportCurrent = "\(baseUrl)viewFinsparcPutReport/current"
    static let putReportNext = "\(baseUrl)viewFinsparcputReport/next"
    static let callPutRatio = "\(baseUrl)viewFinsparcCallPutValue"
    
    static let dailyData = "http://3.6.6.59:7005/viewFinsparcDailyReport/ACC/CE/2020-04-30"
    static let fsNotes = "http://3.6.6.59:7005/viewFSNotes"
    static let highLowReport = "http://3.6.6.59:7005/viewFinsparcHighLowReport"
    static let fillData = "http://3.6.6.59:7005/fiiData"
    
    static let stockInductryList = "http://3.6.6.59:7005/viewFinsparcWatchlistDetails/all/0/4"
    
    static let stockRatingAll = "http://3.6.6.59:7005/viewfinsparcstockranking/all/all"
    static let stockRatingNifty = "http://3.6.6.59:7005/viewfinsparcstockranking/all/1"
    static let stockRatingIndustrial = "http://3.6.6.59:7005/viewfinsparcstockranking/all/all"
    
    static let portfolio = "http://3.6.6.59:7005/viewfinsparc_portfolio/all/"
    static let particularportfolio = "http://3.6.6.59:7005/viewFinsparcStock/all/"
  //  static let dailyReport = "http://3.6.6.59:7005/viewFinsparcDailyReport/ACC/CE/2020-04-30"
   // static let dailyReport = "http://3.6.6.59:7005/viewFinsparcDailyReport/ACC/weekly/2020-04-30"
    static let dailyReport = "http://3.6.6.59:7005/viewFinsparcDailyReport/"

    static let oichangereport = "http://3.6.6.59:7005/viewFinsparcOIChangeReport"
    static let insertWatchlist = "http://3.6.6.59:7005/insertFinsparcWatchlist"
    
    static let stockList = "\(baseUrl)viewFinsparcStockList/all"
    static let tradeInfo = "\(baseUrl)viewfinsparcbhavcopy_trade/"
    static let deleteTrades = "\(baseUrl)deleteFinsparcStock/"
    static let createPortfolioTrade =  "http://3.6.6.59:7005/insertFinsparcStock"
   // static let createPortfolioTrade = "http://3.6.6.59:7005/viewfinsparc_portfolio/null/1/null/333"
    static let transaction = "http://3.6.6.59:7005/viewfinsparctransaction/"
    static let squareup = "http://3.6.6.59:7005/viewfinsparctransaction/343/1/4011/EQUITAS/null"
    static let tltMonthData = "http://3.6.6.59:7005/tltMonthData"
    static let currenttlt = ""
    static let ediTrade = "http://3.6.6.59:7005/viewfinsparctransaction/"
    static let updateTrade = "http://3.6.6.59:7005/finsparcEditStock/"
    
    
    static let feedback = "http://3.6.6.59:7005/postFeedback"
    static let portfolioname = "http://3.6.6.59:7005/insertFinsparcPortfolio"
    static let optionprofile = "http://3.6.6.59:7005/viewFinsparcOptionProfile/NIFTY/CE/11050.0/2020-04-23"
    static let stockProfileModel   = "\(baseUrl)viewfinsparcstockprofile/"
    static let deleteportfolio = "\(baseUrl)deleteFinsparcPortfolio"
    static let reviewsurl = "\(baseUrl)viewfinsparcreviewtransaction/"
    static let stockprofile = "\(baseUrl)viewfinsparcstockprofile/"
}


struct Constants {
    static let OneTimeLogin = "OneTimeLogin"
    static let currentCityCode = "currentCityCode"
    static let currentAreaCode = "currentAreaCode"
}

struct NotificationsText {
    static let tabSelection = "TabSelection"
    static let setRoot = "setRoot"
}

struct SavedValues {
    func currentCityCode() -> String {
        if let code = UserDefaults.standard.value(forKey: Constants.currentCityCode) as? String {
            return code
        }else{
            return ""
        }
    }
    
    func currentAreaCode() -> String {
        if let code = UserDefaults.standard.value(forKey: Constants.currentAreaCode) as? String {
            return code
        }else{
            return ""
        }
    }
}

struct CustomColours {
    static let blue = UIColor(red: 0.1843, green: 0.4902, blue: 0.8510, alpha: 1)
    static let blueV2 = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
    static let orange = UIColor(red: 1.0000, green: 0.6824, blue: 0.3882, alpha: 1)
    static let lightestGrey = UIColor(red: 0.9373, green: 0.9373, blue: 0.9569, alpha: 1)
    static let textGrey = UIColor(red: 51/255.0, green: 51/255.0, blue: 51/255.0, alpha: 1)
    static let green = UIColor(red: 70/255.0, green: 185/255.0, blue: 0/255.0, alpha: 1)
}


struct Services {
    private func parseToJSON (_ response : DataResponse<Any>) -> NSDictionary {
        
        if let dict = response.result.value as? NSDictionary {
            return dict
        } else if let error = response.error {
            return ["status":"0","data":[],"message":"Error : \(error)"]
        } else {
            return ["status":"0","data":[],"message":"Data parsing error!"]
        }
    }
    
    func getServiceJSon(url: String, parameters : NSDictionary, completion: @escaping ( _ jsonData : Data?) -> ()) {
        
        print("\n\n")
        print("URL : ")
        print(url)
        print("\n")
        print("Parameters : ")
        print("\n\n")
        
        var newURL = url
        if parameters.count != 0 {
            
            for (key,value) in parameters {
                newURL = "\(newURL)&\(key as! String)=\(value as! String)"
            }
            
            print(newURL)
            
        }
        
        Alamofire.request(newURL, method: .get, encoding: JSONEncoding.default).responseJSON { response in
            
            var jData : Data? = nil
            
            let jsonString = self.parseToJSON(response)
            print(jsonString)
            
            do {
                jData = try JSONSerialization.data(withJSONObject: jsonString, options: .prettyPrinted)
            } catch {
                print(error.localizedDescription)
            }
            
            completion(jData)
        }
    }
    
    func getService(url: String, parameters : NSDictionary, completion: @escaping ( _ result : NSDictionary) -> ()) {
        
        print("\n\n")
        print("URL isss : ")
        print(url)
        print("\n")
        print("Parameters : ")
        print("\n\n")
        
        var newURL = url
        if parameters.count != 0 {
            
            for (key,value) in parameters {
                newURL = "\(newURL)&\(key as! String)=\(value as! String)"
            }
            
            print(newURL)
            
        }
        
        Alamofire.request(newURL, method: .get).responseJSON { response in
//            completion(response.result.value as AnyObject)
            
            completion(self.parseToJSON(response))
        }
    }
    
    func postService(url: String, parameters : NSDictionary, completion: @escaping ( _ result : NSDictionary) -> ()) {
        
        print("\n\n")
        print("URL : ")
        print(url)
        print("\n")
        print("Parameters : ")
        print(parameters)
        print("\n\n")
        
        Alamofire.request(url, method: .post ,parameters: parameters as? Parameters).responseJSON { response in
            
            completion(self.parseToJSON(response))
        }
    }
    
    func postService(url: String, Headers : HTTPHeaders, parameters : NSDictionary, completion: @escaping ( _ result : NSDictionary) -> ()) {
       
        print("\n\n")
        print("URL : ")
        print(url)
        print("\n")
        print("Parameters : ")
        print(parameters)
        print("\n\n")
        
        Alamofire.request(url, method: .post ,parameters: parameters as? Parameters, headers: Headers ).responseJSON { response in
            
            completion(self.parseToJSON(response))
        }
    }
    
   
    func rawServiceMethod(url: String, parameters: NSDictionary, authenticate : Bool, getMethod : Bool, completion: @escaping ( _ result : NSDictionary) -> ()) {
        
//        let semaphore = DispatchSemaphore (value: 0)
        
        let parameterString = self.parametericString(dict: parameters)
        let postData = parameterString.data(using: .utf8)
        
        var request = URLRequest(url: URL(string: url)!,timeoutInterval: Double.infinity)
        
        request.addValue("json/application", forHTTPHeaderField: "Content-Type")
      //  request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")

        if authenticate {
            request.addValue(serviceToken, forHTTPHeaderField: "Authorization")
        }
        
        if getMethod {
            request.httpMethod = "GET"
        }else{
            request.httpMethod = "POST"
            request.httpBody = postData
        }
        
        
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                print(String(describing: error))
                let dict : NSDictionary = ["status":"404", "message":"\(String(describing: error))"]
                completion(dict)
                return
            }
            
            let jstring = String(data: data, encoding: .utf8)!
            
            if let filteredResult = self.convertToDictionary(text: jstring) as NSDictionary? {
                completion(filteredResult)
            }else{
                let dict : NSDictionary = ["status":"404", "message":"Error occurred!"]
                completion(dict)
            }
            
//            semaphore.signal()
        }
        
        
        task.resume()
//        semaphore.wait()

    }
    
    func parametericString(dict : NSDictionary) -> String {
        let ak = dict.allKeys
        let totalCount = ak.count
        var maxRank = 0
        let endString = "}"
        
        var mainString = "{"
        
        for key in ak {
            maxRank += 1
            
            if totalCount == maxRank {
                print("end")
                mainString.append("\"\(key)\" : \"\(dict[key]!)\"")
            }else{
                mainString.append("\"\(key)\" : \"\(dict[key]!)\", ")
            }
            
        }
        
        mainString.append(endString)
        
        return mainString
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func getService(url: String, Headers : HTTPHeaders, parameters : NSDictionary, completion: @escaping ( _ result : NSDictionary) -> ()) {
        
        print("\n\n")
        print("URL : ")
        print(url)
        print("\n")
        print("Parameters : ")
        print(parameters)
        print("\n\n")
        
        Alamofire.request(url, method: .get ,parameters: parameters as? Parameters, headers: Headers ).responseJSON { response in
            
            completion(self.parseToJSON(response))
        }
    }
    
}


func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}




func onlyString( _ value: Any?) -> String {
    if let stringValue = value as? String {
        return stringValue
    } else if let numberValue = value as? NSNumber {
        return "\(numberValue)"
    }else{
        return ""
    }
}

func AllAlphabets() -> NSMutableArray {
    let aScalars = "A".unicodeScalars
    let aCode = aScalars[aScalars.startIndex].value
    
    let letters: [Character] = (0..<26).map {
        i in Character(UnicodeScalar(aCode + i)!)
    }
    
    let AllLetters = NSMutableArray()
    for stringLetter in letters {
        AllLetters.add(stringLetter.description)
    }
    
    return AllLetters
}


extension UITabBarController {
    func setTabBarVisible(visible:Bool, duration: TimeInterval, animated:Bool) {
        if (tabBarIsVisible() == visible) { return }
        let frame = self.tabBar.frame
        let height = frame.size.height + 30
        let offsetY = (visible ? -height : height)
        
        // animation
        if #available(iOS 10.0, *) {
            UIViewPropertyAnimator(duration: duration, curve: .linear) {
                self.tabBar.frame.offsetBy(dx:0, dy:offsetY)
                self.view.frame = CGRect(x:0,y:0,width: self.view.frame.width, height: self.view.frame.height + offsetY)
                self.view.setNeedsDisplay()
                self.view.layoutIfNeeded()
                }.startAnimation()
        } else {
            // Fallback on earlier versions
        }
    }
    
    func tabBarIsVisible() ->Bool {
        return self.tabBar.frame.origin.y < UIScreen.main.bounds.height
    }
}

func remoteNotification(toToken: String, fromToken: String, title: String, body: String, badge: Int, uid: String) {
    let urlString = "https://fcm.googleapis.com/fcm/send"
    
    let headers : HTTPHeaders = ["Authorization":"key=AIzaSyAQToTgvWeD4B4k0HLipvNaxGLLOGerpxQ", "Content-Type":"application/json"]
    
    let params = ["to":toToken, "collapse_key" : "type_a", "notification" :["body" : body, "text" : body, "token": fromToken, "title": title, "badge" : badge], "data" :["text" : body, "title": title, "receiver_token":fromToken, "sender_token":toToken, "uid":uid, "username":title] ] as [String : Any]
    
    print(params)
    
    Alamofire.request(urlString, method: .post, parameters: params,encoding: JSONEncoding.default, headers: headers).responseJSON {
        response in
        switch response.result {
        case .success:
            print(response)
            
            break
        case .failure(let error):
            
            print(error)
        }
    }
    
}

func CurrentDate() -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    return "\(formatter.string(from: Date()))"
}

func isInternetConnected() -> Bool {
    
    var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
    zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
            SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
        }
    }
    
    var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
    if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
        return false
    }
    
    // Working for Cellular and WIFI
    let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
    let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
    let ret = (isReachable && !needsConnection)
    
    return ret
}
