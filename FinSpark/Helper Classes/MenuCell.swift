//
//  MenuCell.swift
//  BT Group
//
//  Created by Gurjot Kalsi on 14/11/19.
//  Copyright © 2019 Kalsi. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {

    @IBOutlet var menuText: UILabel!
    @IBOutlet var menuImage: UIImageView!
    @IBOutlet var leftConstraint: NSLayoutConstraint!
    
    //MARK:-
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
