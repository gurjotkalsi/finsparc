//
//  App_Controller.swift
//  DynamicSizeforApp
//
//  Created by mayur on 5/24/18.
//  Copyright © 2018 mayurwebsoft. All rights reserved.
//

import UIKit

class App_Controller: UIViewController
{

    let App_Name = "App Name"
    let App_Font_Color = "FFFFFF"
    let App_Icon_Color = "AAAAAA"
    let App_Background_Color = "000000"
    
    let App_Font_Style_Regular = "Assistant-Regular"
    let App_Font_Style_Bold = "Assistant-Bold"
    let App_Font_Style_ExtraBold = "Assistant-ExtraBold"
    let App_Font_Style_ExtraLight = "Assistant-ExtraLight"
    let App_Font_Style_Light = "Assistant-Light"
    let App_Font_Style_SemiBold = "Assistant-SemiBold"
    
    
    // Device Size
    
    var Current_Device_Width320 = 18 as CGFloat // iPhone 4, 4s , 5 , 5s , 5c , SE
    var Current_Device_Width375 = 22 as CGFloat // iPhone 6 , 6s , 7 , 7s , 8 , 8s ,11 pro , X , Xs
    var Current_Device_Width414 = 22 as CGFloat // iPhone 6+ , 6s+ , 7+ , 8+ , 11 , XR , Xs Max , 11 Pro Max
    
    var Current_Device_Width1024 = 30  as CGFloat // iPad
    
    
    //
   

}
