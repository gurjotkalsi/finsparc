//
//  IconCell.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 12/02/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit

class IconCell: UICollectionViewCell {
    
    @IBOutlet var icon: UIImageView!
    @IBOutlet var title: UILabel!
    
    
    override class func awakeFromNib() {
        
    }
}
