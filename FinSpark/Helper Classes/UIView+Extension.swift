//
//  UIView+Extension.swift
//  Finsparc
//
//  Created by mayur on 2/1/20.
//  Copyright © 2020 Mayur Parmar. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    
    func setUpHeaderView() {
        
        self.clipsToBounds = true
        self.layer.cornerRadius = 30
        if #available(iOS 11.0, *) {
            self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        }
        
    }
}
