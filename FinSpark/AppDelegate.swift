//
//  AppDelegate.swift
//  FinSpark
//
//  Created by Gurjot Kalsi on 07/02/20.
//  Copyright © 2020 Kalsi. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var app_Controller = App_Controller()

    // Hexa Color
       
       func hexStringToUIColor (hex:String) -> UIColor {
           var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
           
           if (cString.hasPrefix("#")) {
               cString.remove(at: cString.startIndex)
           }
           
           if ((cString.count) != 6) {
               return UIColor.gray
           }
           
           var rgbValue:UInt32 = 0
           Scanner(string: cString).scanHexInt32(&rgbValue)
           
           return UIColor(
               red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
               green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
               blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
               alpha: CGFloat(1.0)
           )
       }
       
       //--- Hexa Color Finish
       
       fileprivate func TextSize()
       {
           UserDefaults.standard.set(UIScreen.main.bounds.width, forKey: "Current_Screen_Width")
           if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad
           {
               UserDefaults.standard.set("iPad", forKey: "Storyboard_Name")
               
               UserDefaults.standard.set(self.app_Controller.Current_Device_Width1024, forKey: "Text_Size")
           }
           else
           {
               UserDefaults.standard.set("iPhone", forKey: "Storyboard_Name")
               
               if (UserDefaults.standard.string(forKey: "Current_Screen_Width") == String(320))
               {
                   UserDefaults.standard.set(self.app_Controller.Current_Device_Width320, forKey: "Text_Size")
               }
               else if (UserDefaults.standard.string(forKey: "Current_Screen_Width") == String(375))
               {
                   UserDefaults.standard.set(self.app_Controller.Current_Device_Width375, forKey: "Text_Size")
               }
               else if (UserDefaults.standard.string(forKey: "Current_Screen_Width") == String(414))
               {
                   UserDefaults.standard.set(self.app_Controller.Current_Device_Width414, forKey: "Text_Size")
               }
            
               
           }
       }


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
//        let navigationBarAppearace = UINavigationBar.appearance()
//
//        navigationBarAppearace.tintColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
//        navigationBarAppearace.barTintColor = UIColor(red: 0.192, green: 0.478, blue: 0.882, alpha: 1)
        //IQKeyboardManager.shared.enable = true
        setUpIQKeyboardManager()
        TextSize()
        NotificationCenter.default.addObserver(self, selector: #selector(self.ResetRoot), name: Notification.Name(NotificationsText.setRoot), object: nil)
        
        print(LoginModel().localData().finsparc_user_id)
      
        return true
    }
    
    func setUpIQKeyboardManager()
    {
        IQKeyboardManager.shared.enable=true
        IQKeyboardManager.shared.enableAutoToolbar=true
        IQKeyboardManager.shared.toolbarManageBehaviour = .byPosition
        IQKeyboardManager.shared.keyboardDistanceFromTextField=20
        IQKeyboardManager.shared.shouldResignOnTouchOutside=true
    }
   
    func CheckRoot() {
        if UserDefaults.standard.bool(forKey: Constants.OneTimeLogin) == true {
            let vc = myStoryboard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
            let nav = UINavigationController(rootViewController: vc)
            
            if #available(iOS 13.0, *) {
                scene.window?.rootViewController = nav
            } else {
                // Fallback on earlier versions
                nav.view.frame = (appDelegate.window?.bounds)!
                appDelegate.window?.rootViewController = nav
            }
            
        } else {
            let vc = myStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            let nav = UINavigationController(rootViewController: vc)
            
            if #available(iOS 13.0, *) {
                scene.window?.rootViewController = nav
            } else {
                // Fallback on earlier versions
                nav.view.frame = (appDelegate.window?.bounds)!
                appDelegate.window?.rootViewController = nav
            }
        }
    }
    
    @objc func ResetRoot(notify : Notification) {
        if (notify.object as! String) == "Login" {
            let vc = myStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            let nav = UINavigationController(rootViewController: vc)
            
            if #available(iOS 13.0, *) {
                scene.window?.rootViewController = nav
            } else {
                // Fallback on earlier versions
                nav.view.frame = (appDelegate.window?.bounds)!
                appDelegate.window?.rootViewController = nav
            }
        } else if (notify.object as! String) == "Home" {
            let vc = myStoryboard.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
            let nav = UINavigationController(rootViewController: vc)
            
            if #available(iOS 13.0, *) {
                scene.window?.rootViewController = nav
            } else {
                // Fallback on earlier versions
                nav.view.frame = (appDelegate.window?.bounds)!
                appDelegate.window?.rootViewController = nav
            }
        }
    }
    
    
    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "FinSpark")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

